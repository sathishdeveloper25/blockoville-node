// Npm modules
const mongoose = require('mongoose');
require('./model/db');
// Controller
const restAPiCtrl = require('./controller/restApiController');
const userCtrl = require('./controller/userController');

// Model
const groupsubscription = require('./model/groupsubscription');
const myPurchase = require('./model/mypurchase');
const subscriptions = require('./model/subscription');
const users = require('./model/user');
setInterval(function () {
    autopayements();
},5000);
// autopayements
function autopayements() {
    try {
        var currencyPercentage = {
            USD: 1,
            EUR: 2,
            BCH: 3,
            BTC: 4
        };
        var trialDurations = {
            monthly: 1,
            halfyearly: 6,
            yearly: 12,
        }
        var currDt = new Date();
        subscriptions.find({ endDate: { '$lte': currDt }, status: 'open', autoPayement: true }).sort({ purchasedate: -1 }).exec(function (error, expireTrial) {
            if (error) {
                process.exit(1)
            } else {
                if (expireTrial.length > 0) {
                    expireTrial.forEach(function (expired) {
                        groupsubscription.findOne({ _id: mongoose.Types.ObjectId(expired.groupid) }, function (error, GroupObj) {
                            if (error) {
                                process.exit(1)
                                throw error;
                            } else {
                                if (GroupObj !== null) {
                                    var d = new Date();
                                    var endDt = new Date(d.setMonth(d.getMonth() + Number(trialDurations[GroupObj.duration])));
                                    var PostObj = {
                                        senderid: expired.userid.toString(),
                                        receiverid: GroupObj.userid.toString(),
                                        currency: GroupObj.currency.toString(),
                                        amount: Number(Number(GroupObj.amount) + Number(currencyPercentage[GroupObj.currency])),
                                        endDate: endDt
                                    }
                                    users.findOne({ _id: mongoose.Types.ObjectId(PostObj.senderid) }, function (error, userdataObj) {
                                        if (error) {
                                            process.exit(1)
                                            throw error;
                                        } else {
                                            var tokens = userCtrl.generateJwt(userdataObj);
                                            restAPiCtrl.performRequest('wallet', 'subscribersPayements', 'GET', 'Bearer ' + tokens, PostObj, function (result) {
                                                if (result.status === 200) {
                                                    var apiResponse = result.message;
                                                    if (Number(apiResponse.status) === 200) {
                                                        var userid = PostObj.senderid;
                                                        var subscriberid = PostObj.receiverid;
                                                        var amount = apiResponse.message.amount;
                                                        var type = 'Subscribe';
                                                        var currency = apiResponse.message.currency;
                                                        var tr_id = apiResponse.message.tr_id;
                                                        var groupid = GroupObj._id;
                                                        var subscriptionid = PostObj.receiverid;
                                                        var endDate = PostObj.endDate;
                                                        var autoPayement = true;
                                                        var profileObj = {};
                                                        var errorObj = {};
                                                        if (typeof userid !== 'undefined' && userid && userid !== '' &&
                                                            typeof subscriberid !== 'undefined' && subscriberid && subscriberid !== '' &&
                                                            typeof amount !== 'undefined' && amount && amount !== '' &&
                                                            typeof type !== 'undefined' && type && type !== '' &&
                                                            typeof currency !== 'undefined' && currency && currency !== '' &&
                                                            typeof tr_id !== 'undefined' && tr_id && tr_id !== '' &&
                                                            typeof subscriptionid !== 'undefined' && subscriptionid && subscriptionid !== '' &&
                                                            typeof groupid !== 'undefined' && groupid && groupid !== '' &&
                                                            typeof endDate !== 'undefined' && endDate && endDate !== ''
                                                        ) {
                                                            profileObj.userid = userid;
                                                            profileObj.subscriberid = subscriberid;
                                                            profileObj.amount = amount;
                                                            profileObj.type = type;
                                                            profileObj.currency = currency;
                                                            profileObj.tr_id = tr_id;
                                                            profileObj.subscriptionid = subscriptionid;
                                                            profileObj.groupid = groupid;
                                                            profileObj.endDate = endDate;
                                                            profileObj.autoPayement = autoPayement;
                                                            profileObj.status = 'open';
                                                            myPurchase.create(profileObj, function (error, purchaseObj) {
                                                                if (error) {
                                                                    process.exit(1)
                                                                } else {
                                                                    subscriptions.update({ _id: mongoose.Types.ObjectId(expired._id) }, { status: 'expired' }, function (error, updateObj) {
                                                                        if (error) {
                                                                            process.exit(1)
                                                                        } else {
                                                                            subscriptions.create(profileObj, function (error, subObj) {
                                                                                if (error) {
                                                                                    process.exit(1)
                                                                                } else {
                                                                                    process.exit(1)
                                                                                }
                                                                            });
                                                                        }
                                                                    })
                                                                }
                                                            });
                                                        } else {
                                                            process.exit(1)
                                                            errorObj.userid = 'userid is required';
                                                            errorObj.subscriberid = 'subscriberid is required';
                                                            errorObj.amount = 'amount is required';
                                                            errorObj.type = 'type is required';
                                                            errorObj.currency = 'currency is required';
                                                            errorObj.tr_id = 'tr_id is required';
                                                            errorObj.subscriptionid = 'Subscriptionid is required';
                                                            errorObj.groupid = "Group id is required";
                                                            errorObj.endDate = "EndDate is required";
                                                            console.log(errorObj);
                                                        }
                                                    } else {
                                                        process.exit(1)
                                                    }
                                                }
                                                else {
                                                    process.exit(1)
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    })
                }
            }
        })
    } catch (e) {
        console.log(e)
        process.exit(1)
    }

}