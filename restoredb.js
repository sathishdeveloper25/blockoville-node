/*
var exec = require('child_process').exec;
var dbOptions =  {
    user: '',
    pass: '',
    host: 'localhost',
    port: 27017,
    database: 'blockoville2',
};
dbAutoRestore();
// Auto backup script
function dbAutoRestore() {
// check for auto backup is enabled or disabled
    var cmd = 'mongorestore -d ' + dbOptions.database + ' ./blockoville'; // Command for mongodb dump process
    exec(cmd, function (error, stdout, stderr) {
        console.log('error',error)
    });
}*/

const Fs = require('fs');
const Axios = require('axios');
const shell = require('shelljs');
const Path = require('path');

async function downloadImage () {
    try {
        //https://europe-api.basisid.com/mobcontent/67546842-84db-490a-a5a2-d683ab70932b/aat-348ac9e8-1e44-46fc-ba40-39d572747c32/passport/d0266fc9-20f1-4609-b783-c309afd5e568.png
        const url = 'https://europe-api.basisid.com/mobcontent/67546842-84db-490a-a5a2-d683ab70932b/aat-348ac9e8-1e44-46fc-ba40-39d572747c32/passport/d0266fc9-20f1-4609-b783-c309afd5e568.png'
        var dir = './public/uploads/kyc/'+ '5da557707a0db977223fd3b6/passport/';

        var filename = Path.basename('/passport/d0266fc9-20f1-4609-b783-c309afd5e568.png');
        if (!Fs.existsSync(dir)) {
            shell.mkdir('-p', dir);
        }
        console.log('dir',dir);
        const resovedPath = dir+filename;
            //Path.resolve(__dirname, dir, filename)
        console.log('path',resovedPath)
        const writer = Fs.createWriteStream(resovedPath)
        const response = await Axios({
            url,
            method: 'GET',
            responseType: 'stream'
        })

        response.data.pipe(writer)

        return new Promise((resolve, reject) => {
            writer.on('finish', resolve)
            writer.on('error', reject)
        })
    }catch (e){
        console.log(e.message)
    }
}
downloadImage();
