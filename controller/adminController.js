const Users = require('../model/user');
var mongoose = require('mongoose');

const Groups = require('../model/groups');
const AdminMenu = require('../model/adminmenus');
const AdminRules = require('../model/adminrules');

var adminController = [];
adminController.getGroups = function (req, res, next) {
    try {
        Groups.find({type: 'Admin', group: {$ne: 'super-admin'}}).exec(function (error, grouplist) {
            if (error) {
                res.status(500).json({message: error.message});
            } else {
                res.status(200).json(grouplist);
            }
        })
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}
adminController.getAdminUserList = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var perPage = Number(req.query.perpage ? req.query.perpage : 0);
            var page = Number(req.query.page ? req.query.page : 0);
            var search = req.query.search ? req.query.search : '';
            var filter = {};
            if (search !== '') {
                filter = {
                    group: {$nin: ['customer']},
                    $or: [
                        {name: {'$regex': search}},
                        {email: {'$regex': search}},
                        {phone: {'$regex': search}}
                    ]
                }
            } else {
                filter = {
                    group: {$nin: ['customer']}
                }
            }
            if (typeof perPage !== 'undefined' && perPage !== '' && perPage > 0 && typeof page !== 'undefined' && page !== '' && page > 0) {
                var skippage = (perPage * page) - perPage;
                Users.find(filter, {
                    password: 0
                }).skip(skippage).limit(perPage).sort({createdAt: -1}).exec(function (txerr, txdoc) {
                    Users.find(filter, {
                        password: 0
                    }).count().exec(function (err, count) {
                        if (err) {
                            res.status(500).json({status: false, message: 'Internal Server Error'});
                        } else {
                            var returnJson = {
                                status: true,
                                customers: txdoc,
                                current: page,
                                pages: Math.ceil(count / perPage)
                            }
                            res.status(200).json(returnJson);
                        }
                    });
                });
            } else {
                Users.find(filter, {
                    password: 0
                }).sort({createdAt: -1}).exec(function (txerr, txdoc) {
                    if (txerr) {
                        res.status(500).json({status: false, message: 'Internal Server Error'});
                    } else {
                        var returnJson = {
                            status: true,
                            customers: txdoc,
                        }
                        res.status(200).json(returnJson);
                    }
                });
            }
        } else {
            res.status(401).json({message: 'Unauthorized Request'})
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}
adminController.getAllAdminUsers = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var filter = {};
            filter = {
                group: {$ne: 'customer'}
            }
            Users.find(filter, {
                password: 0,
                email_verified: 0,
                phone_verified: 0
            }).sort({createdAt: -1}).exec(function (txerr, txdoc) {
                if (txerr) {
                    res.status(500).json({status: false, message: 'Internal Server Error'});
                } else {
                    res.status(200).json(txdoc);
                }
            });
        } else {
            res.status(401).json({message: 'Unauthorized Request'})
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}

adminController.getMenus = function (req, res, next) {
    try {
        AdminMenu.aggregate([
            {
                "$lookup": {
                    from: "adminmenus",
                    localField: "_id",
                    foreignField: "parentmenu",
                    as: "submenu"
                }
            }, {$match: {"parentmenu": {$exists: false}}}, {"$sort": {"order": 1}}
        ]).exec(function (error, menulist) {
            if (error) {
                res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
            } else {
                res.status(200).json(menulist);
            }
        })
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}
adminController.getGroupsList = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var perPage = Number(req.query.perpage ? req.query.perpage : 0);
            var page = Number(req.query.page ? req.query.page : 0);
            var search = req.query.search ? req.query.search : '';
            var filter = {type: 'Admin'};

            if (typeof perPage !== 'undefined' && perPage !== '' && perPage > 0 && typeof page !== 'undefined' && page !== '' && page > 0) {
                var skippage = (perPage * page) - perPage;
                Groups.find(filter).skip(skippage).limit(perPage).sort({createdAt: -1}).exec(function (txerr, txdoc) {
                    Groups.find(filter).count().exec(function (err, count) {
                        if (err) {
                            res.status(500).json({status: false, message: 'Internal Server Error'});
                        } else {
                            var returnJson = {
                                status: true,
                                customers: txdoc,
                                current: page,
                                pages: Math.ceil(count / perPage)
                            }
                            res.status(200).json(returnJson);
                        }
                    });
                });
            } else {
                Groups.find(filter).sort({createdAt: -1}).exec(function (txerr, txdoc) {
                    if (txerr) {
                        res.status(500).json({status: false, message: 'Internal Server Error'});
                    } else {
                        var returnJson = {
                            status: true,
                            customers: txdoc,
                        }
                        res.status(200).json(returnJson);
                    }
                });
            }
        } else {
            res.status(401).json({message: 'Unauthorized Request'})
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}
adminController.updateGroupStatus = function (req, res, next) {
    try {
        if (
            req.query.groupid && typeof req.query.groupid !== 'undefined' && req.query.groupid !== '' &&
            req.query.status && typeof req.query.status !== 'undefined' && req.query.status !== ''
        ) {
            var groupId = req.query.groupid;
            var status = req.query.status;
            Groups.update({_id: groupId}, {status: status}, function (error, updated) {
                if (error) {
                    res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                } else {
                    res.status(200).json(updated);
                }
            })
        }
    } catch (error) {
        res.status(500).json({message: error.message})
    }
}
adminController.saveGroup = function (req, res, next) {
    try {
        //console.log(req.body);
        //return false;
        var profileObj = {};
        var errorObj = {};

        var title = req.body.title;
        var group = '';
        var type = req.body.type;
        var permission = req.body.permission;
        var menulist = req.body.menulist;
        if (typeof title !== 'undefined' && title && title !== '') {
            profileObj.title = title;
            group = title.trim().replace(/\s/g, "-").toLowerCase();
        } else {
            errorObj.title = 'Title is required';
        }
        if (typeof group !== 'undefined' && group && group !== '') {
            profileObj.group = group.toLowerCase();
        } else {
            errorObj.group = 'Group Code is required';
        }
        if (typeof type !== 'undefined' && type && type !== '') {
            profileObj.type = type;
        } else {
            errorObj.type = 'Group Type is required';
        }
        if (typeof permission !== 'undefined' && permission && permission !== '') {
            profileObj.permission = permission;
        } else {
            errorObj.permission = 'Group Type is required';
        }
        if (typeof menulist !== 'undefined' && menulist && menulist.length > 0) {
        } else {
            errorObj.menu = 'Atleat One Menu is required';
        }
        if (Object.keys(errorObj).length === 0) {
            //var group_code = group_name.trim().replace(/\s/g, "-").toLowerCase();
            profileObj.status = 'Active';
            Groups.create(profileObj, function (error, created) {
                if (error) {
                    if (error.code && error.code === 11000) {
                        res.status(500).json({message: 'Group already exists.'})
                    } else {
                        //res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                        res.status(500).json({message: error.message})
                    }
                } else {
                    var groupId = created._id;
                    var menuObj = [];
                    for (var i = 0; i < menulist.length; i++) {
                        menuObj.push({
                            menu_id: mongoose.Types.ObjectId(menulist[i]),
                            group_id: mongoose.Types.ObjectId(groupId),
                            permission: permission
                        })
                    }
                    if (groupId && typeof groupId !== 'undefined' && groupId !== '') {
                        AdminRules.create(menuObj, function (error, rulescreated) {
                            if (error) {
                                //res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                                res.status(500).json({message: error.message})
                            } else {
                                res.status(200).json({message: 'Admin Rules added.'})
                            }
                        })
                    }
                }
            })
        } else {
            res.status(400).json({message: errorObj})
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}
adminController.deleteGroup = function (req, res, next) {
    try {
        var groupid = req.query.groupid;
        if (groupid && typeof groupid !== 'undefined' && groupid !== null) {
            Groups.findOne({_id: groupid}).exec(function (error, groups) {
                if (error) {
                    res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                } else {
                    if (groups !== null) {
                        Users.find({group: groups.group}).exec(function (error, existuser) {
                            if (error) {
                                res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                            } else {
                                if (existuser && typeof existuser !== 'undefined' && existuser.length === 0) {
                                    Groups.remove({_id: groupid}).exec(function (error, rows) {
                                        if (error) {
                                            res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                                        } else {
                                            AdminRules.remove({group_id: groupid}).exec(function (error, rulesdeleted) {
                                                if (error) {
                                                    res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                                                } else {
                                                    res.status(200).json({message: 'Record is deleted.'})
                                                }
                                            })
                                        }
                                    });
                                } else {
                                    res.status(500).json({message: 'This group has been assigned to users. Please first change the group type of those users'})
                                }
                            }
                        })
                    } else {
                        res.status(500).json({message: 'No Group Found'})
                    }
                }
            })
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}
adminController.getGroupById = function (req, res, next) {
    try {
        var groupid = req.query.groupid;
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
                Groups.aggregate([
                    {
                        "$lookup": {
                            from: "adminrules",
                            localField: "_id",
                            foreignField: "group_id",
                            as: "rules"
                        }
                    },
                    {
                        $match: {
                            _id:mongoose.Types.ObjectId(groupid)
                        }
                    }
                ]).exec(function (txerr, txdoc) {
                    if(txerr){
                        res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                    }else{
                        res.status(200).json(txdoc);
                    }
                });
            }
        } else {
            res.status(401).json({message: 'Unauthorized Request'})
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}
adminController.updateGroup = function (req, res, next) {
    try {
        var profileObj = {};
        var errorObj = {};
        var groupId = req.body._id;
        var title = req.body.title;
        var permission = req.body.permission;
        var menulist = req.body.menulist;
        if (typeof title !== 'undefined' && title && title !== '') {
            profileObj.title = title;
        } else {
            errorObj.title = 'Title is required';
        }
        if (typeof groupId !== 'undefined' && groupId && groupId !== '') {

        } else {
            errorObj._id = 'Group Id  is required';
        }
        if (typeof permission !== 'undefined' && permission && permission !== '') {
            profileObj.permission = permission;
        } else {
            errorObj.permission = 'Group Type is required';
        }
        if (typeof menulist !== 'undefined' && menulist && menulist.length > 0) {
        } else {
            errorObj.menu = 'Atleat One Menu is required';
        }
        if (Object.keys(errorObj).length === 0) {
            //var group_code = group_name.trim().replace(/\s/g, "-").toLowerCase();
            Groups.update({_id: mongoose.Types.ObjectId(groupId)}, profileObj, function (error, updated) {
                if (error) {
                    //res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                    res.status(500).json({message: error.message})
                } else {
                    if (menulist && typeof menulist !== 'undefined') {
                        var menuObj = [];
                        if (menulist.length > 0) {
                            for (var i = 0; i < menulist.length; i++) {
                                menuObj.push({
                                    menu_id: mongoose.Types.ObjectId(menulist[i]),
                                    group_id: mongoose.Types.ObjectId(groupId),
                                    permission: permission
                                })
                            }
                        }
                        if (groupId && typeof groupId !== 'undefined' && groupId !== '') {
                            AdminRules.remove({group_id: mongoose.Types.ObjectId(groupId)}).exec(function (error, deletedAll) {
                                if (!error && typeof deletedAll !== 'undefined') {
                                    if (menuObj.length > 0) {
                                        AdminRules.create(menuObj, function (error, rulescreated) {
                                            if (error) {
                                                //res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                                                res.status(500).json({message: error.message})
                                            } else {
                                                res.status(200).json({message: 'Admin Rules updated.'})
                                            }
                                        })
                                    } else {
                                        res.status(200).json({message: 'Admin Rules updated.'})
                                    }
                                } else {
                                    //res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                                    res.status(500).json({message: error.message})
                                }
                            })
                        }
                    } else {
                        res.status(200).json({message: 'Admin Rules updated.'})
                    }
                }
            })
        } else {
            res.status(400).json({message: errorObj})
        }
    }catch (e) {
        res.status(500).json({message: e.message})
    }
}

adminController.getAdminPanelMenu = function (req,res,next) {
    try{
        var userid = req.payload._id;
        if(typeof userid!=='undefined' && userid && userid!==''){
            Groups.aggregate([
                {
                    "$lookup": {
                        from: "users",
                        localField: "group",
                        foreignField: "group",
                        as: "user"
                    }
                },
                {
                    "$match":{
                        "user._id":mongoose.Types.ObjectId(userid)
                    }
                },
                {
                    "$unwind":"$user"
                },
                {
                    "$project":{
                        group:1,
                        type:1,
                        status:1,
                        createdAt:1,
                        "user._id":1
                    }
                }
            ]).exec(function (error,group) {
                if(error){
                    console.log(error)
                }else{
                    //console.log(group);
                    if(group.length>0) {
                        AdminRules.aggregate([
                            {
                                "$lookup": {
                                    from: "adminmenus",
                                    localField: "menu_id",
                                    foreignField: "_id",
                                    as: "menus"
                                },
                            },
                            {
                                "$unwind": "$menus"
                            },
                            {
                                "$match": {
                                    "group_id": group[0]._id
                                }
                            }
                        ]).exec(function (error, rules) {
                            if (error) {
                                console.log(error, rules)
                            } else {
                                if (rules.length > 0) {
                                    var menulist = [];
                                    var submenulist = {};
                                    rules.forEach(function (item) {
                                        if (typeof item.menus.parentmenu !== 'undefined' && item.menus.parentmenu && item.menus.parentmenu !== '') {
                                            if (Object.keys(submenulist).indexOf(item.menus.parentmenu.toString()) === -1) {
                                                submenulist[item.menus.parentmenu] = [{
                                                    _id: item.menus._id.toString(),
                                                    permission: item.permission,
                                                    menu: item.menus.menu,
                                                    menu_url: item.menus.menu_url,
                                                    order: item.menus.order,
                                                    styleclass: item.menus.styleclass,
                                                    status: item.menus.status
                                                }]
                                            } else {
                                                submenulist[item.menus.parentmenu].push({
                                                    _id: item.menus._id.toString(),
                                                    permission: item.permission,
                                                    menu: item.menus.menu,
                                                    menu_url: item.menus.menu_url,
                                                    order: item.menus.order,
                                                    styleclass: item.menus.styleclass,
                                                    status: item.menus.status
                                                });
                                            }
                                        } else {
                                            menulist.push({
                                                _id: item.menus._id.toString(),
                                                permission: item.permission,
                                                menu: item.menus.menu,
                                                menu_url: item.menus.menu_url,
                                                order: item.menus.order,
                                                styleclass: item.menus.styleclass,
                                                status: item.menus.status
                                            })
                                        }

                                    })
                                    res.status(200).json({menu: menulist, submenu: submenulist});
                                }else{
                                    res.status(500).json({message: 'Menu not assigned'})
                                }
                            }
                        })
                    }else{
                        res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                    }
                }
            })

            /*AdminMenu.aggregate([
                {
                    "$lookup": {
                        from: "adminmenus",
                        localField: "_id",
                        foreignField: "parentmenu",
                        as: "submenu"
                    },
                    "$lookup": {
                        from: "adminrules",
                        localField: "_id",
                        foreignField: "menu_id",
                        as: "rules"
                    }
                },
                {
                    $match: {
                        "parentmenu": {$exists: false},
                        ""
                    }
                },
                {"$sort": {"order": 1}}
            ]).exec(function (error, menulist) {
                if (error) {
                    res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                } else {
                    console.log(menulist);
                    res.status(200).json(menulist);
                }
            })*/
        }else{
            res.status(401).json({message: 'Unauthorized Request'})
        }
    }catch (error){
        res.status(500).json({message: error.message})
    }
}


var ctrl = module.exports = adminController