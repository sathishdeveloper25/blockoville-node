// Npm-modules
var mongoose = require('mongoose');
var multer = require('multer');
var fs = require('fs');
var forumController = [];
// Models
const content = require('../model/post_medias');
const Forums = require('../model/forums');

// Multer storage define for-imagePost
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        // var dir = process.env.KYCDOCURL + req.payload._id
        var dir = "./public/uploads/userpost/imageUpload";
        if (!fs.existsSync(dir)) {
            shell.mkdir('-p', dir);
        }
        cb(null, "./public/uploads/userpost/imageUpload"); // where to store it
    },
    filename: function (req, file, cb) {
        if (file.fileSize) {
            var err = new Error();
            return cb(err);
        }
        else if (!file.originalname.toLowerCase().match(/\.(png|jpg|jpeg|gif)$/)) {
            var err = new Error();
            err.code = 'fieltype'; // to check on file type
            return cb(err, null);
        } else {
            var date = new Date();
            var timeStamp = date.getTime();
            var name = file.originalname.toLowerCase();
            var ext = name.substr(file.originalname.lastIndexOf('.') + 1)
            var rename = timeStamp + "." + ext;
            cb(null, rename);
        }
    }
});

var upload = multer({
    storage: storage,
    limits: { fileSize: 20971520 }
}).any();

// User-post api
forumController.saveForum = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            upload(req, res, function (err) {
                if (err) {
                    throw err;
                }
                else {
                    var userid = req.payload._id
                    var subject = req.body.subject;
                    var text = req.body.body;
                    var filesArr = req.files;
                    var profileObj = {};
                    var errorObj = {};
                    if (typeof userid !== 'undefined' && userid && userid !== '' &&
                        typeof subject !== 'undefined' && subject && subject !== '' &&
                        typeof text !== 'undefined' && text && text !== ''
                    ) {
                        profileObj.userid = userid;
                        profileObj.subject = subject;
                        profileObj.body = text;

                        Forums.create(profileObj, function (error, postObj) {
                            if (error) {
                                res.status(500).json({ message: error.message });
                            } else {
                                var postid = postObj._id;
                                if (filesArr.length > 0) {
                                    var contentObj = [];
                                    filesArr.forEach(function (file) {
                                        var path = file.path.replace("public", '').substr(1);
                                        contentObj.push({
                                            content_url: path,
                                            postid: postid,
                                            userid: userid
                                        });
                                    });
                                    content.create(contentObj, function (error, contentResp) {
                                        if (error) {
                                            res.status(500).json({ message: error });
                                        }
                                        else {
                                            res.status(200).json({ postObject: postObj, contentResponse: contentResp });
                                        }
                                    })
                                } else {
                                    res.status(200).json(postObj);
                                }
                            }
                        })
                    }
                    else {
                        errorObj.text = 'Text is required';
                        errorObj.subject = 'Subject is required';
                        // errorObj.content_url = 'Image is required';
                        res.status(400).json({ message: errorObj });
                    }
                }
            })

        }
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}
// User-get
forumController.getForum = function (req, res) {
    try {
        var userid = req.payload._id;
        // var flags = req.query.flags;
        var questions = req.query.Question;
        var answers = req.query.Answer;
        var filter = {};
        //console.log('questions',questions,'answers',answers)

        /*if(questions == 'true' && answers == 'false'){
            filter.userid = mongoose.Types.ObjectId(userid);
            filter.parentid = { $exists: false }
        }
        else if (questions == 'true' && answers == 'true') {
            filter.userid = mongoose.Types.ObjectId(userid);
            filter.parentid = { $exists: false }
        }
        if (answers == 'true') {
            filter.userid = mongoose.Types.ObjectId(userid);
            filter.parentid = { $exists: true }
        }
        if(all == 'true'){
            filter.parentid= {$exists: false}
        }*/
        var flag = false;
        if (answers === 'false' && questions === 'false') {
            flag = true;
            filter.parentid = { $exists: false }
        } else {
            if (questions === 'true' && answers === 'true') {
                filter.userid = mongoose.Types.ObjectId(userid);
            }
            if (questions === 'true' && answers === 'false') {
                filter.userid = mongoose.Types.ObjectId(userid);
                filter.parentid = { $exists: false }
            }
            if (questions === 'false' && answers === 'true') {
                filter.userid = mongoose.Types.ObjectId(userid);
                filter.parentid = { $exists: true }
            }
        }
        //console.log('filter',filter)
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            Forums.aggregate([
                {
                    $match: filter
                },
                {
                    "$lookup": {
                        from: 'users',
                        localField: 'userid',
                        foreignField: '_id',
                        as: 'userinfo'
                    }
                },
                {
                    '$unwind': {
                        path: '$userinfo'
                    }
                },
                {
                    "$project": {
                        _id: 1,
                        body: 1,
                        subject: -1,
                        userid: 1,
                        parentid: 1,
                        createdAt: -1,
                        reviews: 1,
                        "userinfo.name": 1,
                        "userinfo.avatar": 1
                    }
                },
                { "$sort": { "createdAt": -1 } }
            ]).exec(function (error, forumData) {
                if (error) {
                    res.status(500).json({ message: 'An anonymous error was occurred, please try again.' })
                } else {
                    //console.log('forumData',forumData)
                    var postids = [];
                    if (flag) {
                        forumData.forEach(function (post) {
                            postids.push(post._id);
                        })
                    } else {
                        forumData.forEach(function (post) {
                            if (questions === 'true' && answers === 'true') {
                                if (typeof post.parentid !== 'undefined' && post.parentid && post.parentid !== '') {
                                    postids.push(post.parentid);
                                } else {
                                    postids.push(post._id);
                                }
                            } else {
                                if (questions === 'true' && answers === 'false') {
                                    postids.push(post._id);
                                }
                                if (questions === 'false' && answers === 'true') {
                                    if (typeof post.parentid !== 'undefined' && post.parentid && post.parentid !== '') {
                                        postids.push(post.parentid);
                                    }
                                }
                            }
                        })
                    }
                    //console.log('postids',postids)
                    postids.filter((v, i) => postids.indexOf(v) == i)
                    var filter2 = {};
                    var filter3 = {};
                    filter2.parentid = { $in: postids };
                    filter3._id = { $in: postids };
                    if (postids.length > 0) {
                        Forums.find(filter2, { parentid: 1, _id: 1, createdAt: 1 }).exec(function (error, replycount) {
                            if (error) {
                                res.status(500).json({ message: error });
                            } else {
                                var countObj = {};
                                replycount.forEach(function (reply) {
                                    if (Object.keys(countObj).indexOf(reply.parentid.toString()) === -1) {
                                        countObj[reply.parentid] = { count: 1, createdAt: reply.createdAt };
                                        //countObj[reply.parentid]= [reply];
                                    } else {
                                        countObj[reply.parentid].count = Number(countObj[reply.parentid].count) + 1;
                                        countObj[reply.parentid].createdAt = reply.createdAt;
                                        //countObj[reply.parentid].count.push(reply);
                                        //countObj[reply.parentid].createdAt.push(reply);
                                    }
                                });
                                Forums.aggregate([
                                    {
                                        "$match": filter3
                                    },
                                    {
                                        "$lookup": {
                                            from: 'users',
                                            localField: 'userid',
                                            foreignField: '_id',
                                            as: 'userinfo'
                                        }
                                    },
                                    {
                                        '$unwind': {
                                            path: '$userinfo'
                                        }
                                    },
                                    {
                                        "$project": {
                                            _id: 1,
                                            body: 1,
                                            subject: 1,
                                            parentid: 1,
                                            userid: 1,
                                            createdAt: 1,
                                            reviews: 1,
                                            "userinfo.name": 1,
                                            "userinfo.avatar": 1
                                        }
                                    },
                                    { "$sort": { "createdAt": -1 } }
                                ]).exec(function (error, forumAsnwers) {
                                    if (error) {
                                        res.status(500).json({ message: 'An anonymous error was occurred, please try again.' })
                                    }
                                    else {
                                        //console.log('forumAsnwers',forumAsnwers)
                                        if (forumAsnwers.length > 0) {
                                            var wallpost = {};
                                            /*forumAsnwers.forEach(function (item) {
                                                if (Object.keys(wallpost).indexOf(item.parentid.toString()) === -1) {
                                                    wallpost[item.parentid] = [item];
                                                } else {
                                                    wallpost[item._id].push(item)
                                                }
                                            });
                                            console.log('wallpost',wallpost)*/
                                            /*if (answers === 'true') {
                                                forumData.forEach(function (item) {
                                                    if (Object.keys(wallpost).indexOf(item.parentid.toString()) === -1) {
                                                        wallpost[item.parentid] = [item];
                                                    } else {
                                                        wallpost[item.parentid].push(item)
                                                    }
                                                });
                                            } else {
                                                forumAsnwers.forEach(function (item) {
                                                    if (Object.keys(wallpost).indexOf(item.parentid.toString()) === -1) {
                                                        wallpost[item.parentid] = [item];
                                                    } else {
                                                        wallpost[item.parentid].push(item)
                                                    }
                                                });
                                            }
                                            if (answers === 'true') {
                                                res.status(200).json({ questions: forumAsnwers, answers: wallpost, count: countObj });
                                            } else {
                                                res.status(200).json({ questions: forumData, answers: wallpost, count: countObj });
                                            }*/
                                            res.status(200).json({ questions: forumAsnwers, answers: wallpost, count: countObj });
                                        } else {
                                            res.status(200).json({ questions: {}, answers: {}, count: {} });
                                        }
                                    }
                                })
                            }
                        })

                    } else {
                        res.status(200).json({ questions: forumData, answers: {}, count: {} });
                    }
                }
            })
        }
        else {
            res.status(400).json({ message: 'internal server error' })
        }
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}
forumController.getforumDataById = function (req, res) {
    try {
        var userid = req.payload._id;
        var id = req.params.id;
        var flags = req.query.flags;
        var filter = {};
        if (flags === '') {
            filter.parentid = { $exists: false }
        } else if (flags === 'question') {
            filter.userid = mongoose.Types.ObjectId(userid);
            filter.parentid = { $exists: false }
        } else if (flags === 'answer') {
            filter.userid = mongoose.Types.ObjectId(userid);
            filter.parentid = { $exists: true }
        }
        if (typeof userid !== 'undefined' && userid && userid !== '' &&
            typeof id !== 'undefined' && id && id !== '') {
            Forums.aggregate([
                {
                    "$match": {
                        // filter,
                        _id: mongoose.Types.ObjectId(id)
                    }
                },
                {
                    "$lookup": {
                        from: 'post_medias',
                        localField: '_id',
                        foreignField: 'postid',
                        as: 'contents'
                    }
                },
                {
                    "$lookup": {
                        from: 'users',
                        localField: 'userid',
                        foreignField: '_id',
                        as: 'userinfo'
                    }
                },
                {
                    '$unwind': {
                        path: '$userinfo'
                    }
                },
                {
                    "$project": {
                        _id: 1,
                        body: 1,
                        subject: -1,
                        userid: 1,
                        parentid: 1,
                        createdAt: -1,
                        reviews: 1,
                        "contents": 1,
                        "userinfo.name": 1,
                        "userinfo.avatar": 1
                    }
                },
                { "$sort": { "createdAt": -1 } }
            ]).exec(function (error, forumData) {
                if (error) {
                    res.status(500).json({ message: 'An anonymous error was occurred, please try again.' })
                } else {
                    var postids = [];
                    forumData.forEach(function (post) {
                        if (flags === 'answer') {
                            postids.push(post.parentid);
                        } else {
                            postids.push(post._id);
                        }
                    });
                    var filter2 = {};
                    if (flags === 'answer') {
                        filter2._id = { $in: postids }
                    } else {
                        filter2.parentid = { $in: postids }
                    }
                    if (postids.length > 0) {
                        Forums.aggregate([
                            {
                                "$match": {
                                    // filter2, 
                                    parentid: mongoose.Types.ObjectId(id)
                                }
                            },
                            {
                                "$lookup": {
                                    from: 'post_medias',
                                    localField: '_id',
                                    foreignField: 'postid',
                                    as: 'contents'
                                }
                            },
                            {
                                "$lookup": {
                                    from: 'users',
                                    localField: 'userid',
                                    foreignField: '_id',
                                    as: 'userinfo'
                                }
                            },
                            {
                                '$unwind': {
                                    path: '$userinfo'
                                }
                            },
                            {
                                "$project": {
                                    _id: 1,
                                    body: 1,
                                    subject: 1,
                                    parentid: 1,
                                    userid: 1,
                                    createdAt: 1,
                                    reviews: 1,
                                    'contents': 1,
                                    "userinfo.name": 1,
                                    "userinfo.avatar": 1
                                }
                            },
                            { "$sort": { "createdAt": -1 } }
                        ]).exec(function (error, forumAsnwers) {
                            if (error) {
                                res.status(500).json({ message: 'An anonymous error was occurred, please try again.' })
                            } else {
                                if (forumAsnwers.length > 0) {
                                    var wallpost = {};
                                    if (flags === 'answer') {
                                        forumData.forEach(function (item) {
                                            if (Object.keys(wallpost).indexOf(item.parentid.toString()) === -1) {
                                                wallpost[item.parentid] = [item];
                                            } else {
                                                wallpost[item.parentid].push(item)
                                            }
                                        });
                                    } else {
                                        forumAsnwers.forEach(function (item) {
                                            if (Object.keys(wallpost).indexOf(item.parentid.toString()) === -1) {
                                                wallpost[item.parentid] = [item];
                                            } else {
                                                wallpost[item.parentid].push(item)
                                            }
                                        });
                                    }
                                    if (flags === 'answer') {
                                        res.status(200).json({ question: forumAsnwers, answer: wallpost });
                                    } else {
                                        res.status(200).json({ question: forumData, answer: wallpost });
                                    }
                                } else {
                                    res.status(200).json({ question: forumData, answer: {} });
                                }
                            }
                        })
                    } else {
                        res.status(200).json({ question: forumData, answer: {} });
                    }
                }
            })
        }
        else {
            res.status(400).json({ message: 'internal server error' })
        }
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}

forumController.forumreply = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            upload(req, res, function (err) {
                if (err) {
                    throw err;
                }
                else {
                    var userid = req.payload._id;
                    var reply = req.body.replyText;
                    var parentid = req.body.parentid;
                    var filesArr = req.files;
                    var objreply = {};
                    var errorObj = {};
                    if (typeof userid !== 'undefined' && userid && userid != '' &&
                        typeof reply !== 'undefined' && reply && reply != '' &&
                        typeof parentid !== 'undefined' && parentid && parentid != '') {

                        objreply.userid = userid;
                        objreply.body = reply;
                        objreply.parentid = parentid;
                        Forums.create(objreply, function (err, result) {
                            if (err) {
                                res.status(500).json({ message: err.message })
                            }
                            else {
                                var postid = result._id;
                                if (filesArr.length > 0) {
                                    var contentObj = [];
                                    filesArr.forEach(function (file) {
                                        var path = file.path.replace("public", '').substr(1);
                                        contentObj.push({
                                            content_url: path,
                                            postid: postid,
                                            userid: userid
                                        });
                                    });
                                    content.create(contentObj, function (error, contentResp) {
                                        if (error) {
                                            res.status(500).json({ message: error });
                                        }
                                        else {
                                            res.status(200).json({ resultObj: result, contentResponse: contentResp });
                                        }
                                    })
                                } else {
                                    res.status(200).json(result);
                                }
                            }
                        });
                    }
                    else {
                        errorObj.userid = "Userid is required";
                        errorObj.body = 'Reply is required';
                        errorObj.parentid = "Parentid is required";
                        res.status(400).json({ message: errorObj });
                    }
                }
            });
        }
    }
    catch (e) {
        res.status(400).json({ message: e.message })
    }
}

forumController.updatereview = function (req, res, next) {
    try {
        var userid = req.query.id;
        var reviews = req.query.review;
        if (userid !== 'undefined' && userid && userid !== '') {
            Forums.findOneAndUpdate({ _id: mongoose.Types.ObjectId(userid) }, { reviews: reviews }, function (err, result) {
                if (err) { res.status(500).json({ message: err.message }) }
                else {
                    res.status(200).json({ message: result })
                }
            })
        }
        else {
            res.status(400).json({ message: 'internal server error' })
        }
    }
    catch (e) {
        res.status(400).json({ message: e.message })
    }
}

var ctrl = module.exports = forumController