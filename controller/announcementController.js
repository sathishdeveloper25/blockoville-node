// Npm-modules
var multer = require('multer');
var mongoose = require('mongoose')
var fs = require('fs');
var shell = require('shelljs');

//Database Models
const Announcements = require('../model/announcements');
const AnnouncementStats = require('../model/announcements_stats');

var postController = [];

// User-post api
postController.saveAnnouncement = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var title = req.body.title;
            var body = req.body.body;
            var type = req.body.type;
            var profileObj = {};
            if (typeof title !== 'undefined' && title && title !== '' &&
                typeof body !== 'undefined' && body && body !== '') {
                profileObj.title = title;
                profileObj.body = body;
                profileObj.type = type;
                Announcements.create(profileObj, function (error, user) {
                    if (error) {
                        res.status(500).json({message: error.message});
                    } else {
                        res.status(200).json({message: 'Announcement Created'})
                    }
                })
            }
            else {
                res.status(400).json({message: 'Details Missing'});
            }

        } else {
            res.status(401).json({message: 'Unauthorized Request'})
        }
    }
    catch (e) {
        res.status(500).json({message: e.message})
    }
}
postController.getAnnouncement = function (req, res, next) {
    if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
        //var userid = req.payload._id;
        var perPage = Number(req.query.perpage ? req.query.perpage : 0);
        var page = Number(req.query.page ? req.query.page : 0);
        if (typeof perPage !== 'undefined' && perPage !== '' && perPage > 0 &&
            typeof page !== 'undefined' && page !== '' && page > 0
        ) {
            var skippage = (perPage * page) - perPage;
            Announcements.find().sort({createdAt: -1}).skip(skippage).limit(perPage).exec(function (txerr, txdoc) {
                Announcements.find().exec(function (err, count) {
                    if (err) {
                        console.log(
                            err
                        )
                        res.status(500).json({status: false, message: 'Internal Server Error'});
                    } else {
                        var returnJson = {
                            status: true,
                            announcements: txdoc,
                            current: page,
                            pages: Math.ceil(count.length / perPage)
                        }
                        res.status(200).json(returnJson);
                    }
                });
            });
        }
        else {
            Announcements.find().sort({createdAt: -1}).exec(function (txerr, txdoc) {
                if (txerr) {
                    res.status(500).json({status: false, message: 'Internal Server Error'});
                } else {
                    var returnJson = {
                        status: true,
                        announcements: txdoc,
                    }
                    res.status(200).json(returnJson);
                }
            });
        }
    } else {
        res.status(401).json({
            "message": "Unauthorized Request"
        });
    }
}
postController.updateStatus = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var id = req.query.id;
            var status = req.query.status;
            if (typeof id !== 'undefined' && id && id !== '' &&
                typeof status !== 'undefined' && status && status !== '') {
                Announcements.update({_id: mongoose.Types.ObjectId(id)}, {status: status}, function (error, user) {
                    if (error) {
                        res.status(500).json({message: error.message});
                    } else {
                        res.status(200).json({message: 'Announcement status changed to ' + status})
                    }
                })
            }
            else {
                res.status(400).json({message: 'Details Missing'});
            }

        } else {
            res.status(401).json({message: 'Unauthorized Request'})
        }
    }
    catch (e) {
        res.status(500).json({message: e.message})
    }
}
postController.deleteAnnouncement = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var id = req.query.id;
            if (typeof id !== 'undefined' && id && id !== '') {
                Announcements.findOneAndDelete({_id: mongoose.Types.ObjectId(id)}, function (error, user) {
                    if (error) {
                        res.status(500).json({message: error.message});
                    } else {
                        AnnouncementStats.remove({ann_id:mongoose.Types.ObjectId(id)}).exec(function (error, delrows) {
                            res.status(200).json({message: 'Announcement Deleted'})
                        })
                    }
                })
            }
            else {
                res.status(400).json({message: 'Details Missing'});
            }

        } else {
            res.status(401).json({message: 'Unauthorized Request'})
        }
    }
    catch (e) {
        res.status(500).json({message: e.message})
    }
}
//notification of message
postController.getNotificationDetail = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var type = req.query.type;
            ctrl.findNotificationsUsers(userid, type, function (result) {
                res.status(result.status).json(result.message)
            })
        } else {
            res.status(401).json({message: 'Unauthorized Request'})
        }
    }
    catch (e) {
        res.status(400).json({message: e.message})
    }
}
postController.findNotificationsUsers = function (userid, type, callback) {
    try {
        var filter = {
            userid: mongoose.Types.ObjectId(userid)
        };
        if (typeof type !== 'undefined' && type && type !== '') {
            filter.type = type;
        }
        AnnouncementStats.find(filter).exec(function (error, stats) {
            //console.log('stats',error, stats)
            if (!error) {
                var checked = [];
                var unread = [];
                if(stats.length > 0) {
                    stats.forEach((item) => {
                        if (item.checked == 1) {
                            checked.push(mongoose.Types.ObjectId(item.ann_id));
                        }
                        if (item.read == 0) {
                            unread.push(mongoose.Types.ObjectId(item.ann_id));
                        }
                    })
                }
                var checkFilter={
                    status:'Active'
                };
                if(checked.length>0){
                    checkFilter['_id'] = {$nin: checked}
                }
                Announcements.countDocuments(checkFilter).exec(function (Cnterror, counts) {
                    //console.log('Cnterror',Cnterror, counts)
                    if (!Cnterror) {
                        var unreadFilter={
                            status:'Active'
                        };
                        if(checked.length>0){
                            unreadFilter['_id'] = {$in: unread}
                        }
                        Announcements.find(unreadFilter).sort({createdAt:-1}).exec(function (error, list) {
                            //console.log('list',error, list)
                            if (!error) {
                                return callback({status: 200, message: {count: counts, detailss: list}})
                            } else {
                                return callback({status: 200, message: {count: 0, detailss: []}})
                            }
                        })
                    } else {
                        return callback({status: 200, message: {count: 0, detailss: []}})
                    }
                })
            } else {
                return callback({status: 200, message: {count: 0, detailss: []}})
            }
        })
    }
    catch (e) {
        return callback({status: 500, message: e.message})
    }
}
postController.getNotificationsbyid = function (req, res, next) {
    try {
        var id = req.query.id;
        var filter = {};
        var filter2={}
        if (typeof id !== 'undefined' && id && id !== null) {
            filter._id = mongoose.Types.ObjectId(id)
            filter2.ann_id = mongoose.Types.ObjectId(id)
        }
        AnnouncementStats.updateMany(filter2, {$set: {read: 1}}).exec(function (error, uprows) {
            Announcements.find(filter).sort({createdAt:-1}).exec(function (err, result) {
                if (err) {
                    res.status(500).json({message: err.message})
                } else {
                    res.status(200).json(result)
                }
            })
        })
    }
    catch (e) {
        console.log('getNotificationsbyid',e.message )
        res.status(400).json({message: e.message})
    }
}
postController.getAllNotificationDetail = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var perPage = Number(req.query.perpage ? req.query.perpage : 0);
        var page = Number(req.query.page ? req.query.page : 0);
        if (typeof perPage !== 'undefined' && perPage !== '' && perPage > 0 &&
            typeof page !== 'undefined' && page !== '' && page > 0
        ) {
            var skippage = (perPage * page) - perPage;
            Announcements.find({status: 'Active'}).sort({createdAt: -1}).skip(skippage).limit(perPage).exec(function (txerr, txdoc) {
                Announcements.countDocuments({status: 'Active'}).exec(function (err, count) {
                    if (err) {
                        console.log(err)
                        res.status(500).json({status: false, message: 'Internal Server Error'});
                    } else {
                        var returnJson = {
                            status: true,
                            announcements: txdoc,
                            current: page,
                            pages: Math.ceil(count / perPage),
                            total:count
                        }
                        if (txdoc.length > 0) {
                            var dataObj = [];
                            txdoc.forEach((item) => {
                                dataObj.push(mongoose.Types.ObjectId(item._id));
                            })
                            AnnouncementStats.updateMany({
                                userid: mongoose.Types.ObjectId(userid),
                                ann_id: {$in: dataObj}
                            }, {$set: {read: 1,checked:1}}).exec(function (error, uprows) {
                                res.status(200).json(returnJson);
                            })
                        }else{
                            res.status(200).json(returnJson);
                        }
                    }
                });
            });
        }
        else {
            Announcements.find({status: 'Active'}).sort({createdAt: -1}).exec(function (txerr, txdoc) {
                if (txerr) {
                    res.status(500).json({status: false, message: 'Internal Server Error'});
                } else {
                    var returnJson = {
                        status: true,
                        announcements: txdoc,
                    }
                    if (txdoc.length > 0) {
                        var dataObj = [];
                        txdoc.forEach((item) => {
                            dataObj.push(mongoose.Types.ObjectId(item._id));
                        })
                        AnnouncementStats.updateMany({
                            userid: mongoose.Types.ObjectId(userid),
                            ann_id: {$in: dataObj}
                        }, {$set: {read: 1,checked:1}}).exec(function (error, uprows) {
                            //console.log(error, uprows)
                            res.status(200).json(returnJson);
                        })
                    }else{
                        res.status(200).json(returnJson);
                    }
                }
            });
        }
    }
    catch (e) {
        res.status(400).json({message: e.message})
    }
}
postController.markAsCheckedAnnouncements = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            AnnouncementStats.find({userid: mongoose.Types.ObjectId(userid)}).exec(function (error, stats) {
                //console.log('stats',error,stats)
                if (!error) {
                    var annids = [];
                    if(stats.length > 0) {
                        stats.forEach((item) => {
                            annids.push(item.ann_id);
                        })
                    }
                    var annIdFilter={
                        status:'Active'
                    }
                    if(annids.length>0){
                        annIdFilter['_id'] = {$nin: annids}
                    }
                    Announcements.find(annIdFilter).exec(function (error, list) {
                        //console.log('list',error,list)
                        if (!error && list.length > 0) {
                            var dataObj = [];
                            list.forEach((item) => {
                                dataObj.push({
                                    ann_id: mongoose.Types.ObjectId(item._id),
                                    userid: mongoose.Types.ObjectId(userid),
                                    checked: 1
                                })
                            })
                            AnnouncementStats.create(dataObj, function (error, uprows) {
                                //console.log('uprows',error, uprows)
                                ctrl.findNotificationsUsers(userid, '', function (result) {
                                    res.status(result.status).json(result.message)
                                })
                            })
                        } else {
                            ctrl.findNotificationsUsers(userid, '', function (result) {
                                res.status(result.status).json(result.message)
                            })
                        }
                    })
                }else{
                    ctrl.findNotificationsUsers(userid, '', function (result) {
                        res.status(result.status).json(result.message)
                    })
                }
            })
        } else {
            res.status(401).json({message: 'Unauthorized Request'})
        }
    } catch (e) {
        console.log(e.message)
        res.status(500).json({message: e.message})
    }
}
var ctrl = module.exports = postController;

//5e0b2315d73d553a44f25162