var passport = require('passport');
const Users = require('../model/user');
const Security = require('../model/security');
var Function = require('../others/functions');
const Otp = require('../model/otp');
const Referral = require('../model/referral');
const announcement = require('../model/announcements');
const UserCounter = require('../model/usercounter');
const Sendgrid = require('../others/sendgrid');
const crypt = require('../others/cryptojs');
const Kyc = require('../model/kyc')
const Logs = require('../model/logs');
const jwt = require("jsonwebtoken");
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const countrylist = require('countries-list');
const Apiendpoints = require('../model/api_endpoints');
const ApiendpointAccess = require('../model/api_endpoint_access');
const UserApiKeys = require('../model/user_apikeys');
const Userliqproviders = require('../model/user_liqproviders');
const FollowerModel = require("../model/following");
const crypto = require('crypto');
var randomize = require('randomatic');
let csc = require('country-state-city').default

var randomstring = require("randomstring");
var uniqid = require('uniqid');
var rn = require('random-number');
var otp = rn.generator({
    min: 1000000
    , max: 10000000
    , integer: true
});
var mongoose = require('mongoose');

var accountSid = process.env.TWILLO_ACCOUNT_SID;
var authToken = process.env.TWILLO_ACCOUNT_AUTH_TOKEN;
var twilio = require('twilio');
// var smsClient = new twilio(accountSid, authToken);


var Actions = require('../model/actions');
var useragent = require('useragent');
var fs = require("fs");
var shell = require('shelljs')
var multer = require('multer');
const RestApi = require('./restApiController');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        var dir = process.env.KYCDOCURL + req.payload._id
        //var dir = "./public/uploads/kyc/" + req.payload._id;
        if (!fs.existsSync(dir)) {
            shell.mkdir('-p', dir);
        }
        cb(null, process.env.KYCDOCURL + req.payload._id); // where to store it
    },
    filename: function (req, file, cb) {
        if (file.fileSize) {
            var err = new Error();
            return cb(err);
        }
        else if (!file.originalname.toLowerCase().match(/\.(png|jpg|jpeg|pdf)$/)) {
            var err = new Error();
            err.code = 'fieltype'; // to check on file type
            return cb(err, null);
        } else {
            var date = new Date();
            var timeStamp = date.getTime();
            var name = file.originalname.toLowerCase();
            var ext = name.substr(file.originalname.lastIndexOf('.') + 1)
            var rename = timeStamp + "." + ext;
            cb(null, rename);
        }
    }
});
var upload = multer({
    storage: storage,
    limits: { fileSize: 3145728 } // Max file size: 3MB (3145728 Bytes)
}).any(); // name in form
var bcryptJS = require('bcrypt');
const Kycplans = require('../model/kycplans');
const ct = require('countries-and-timezones');
const DeviceDetector = require('node-device-detector');

var userController = {};
//common functions
userController.getCSRF = function (req, res, next) {
    console.log('userController.getCSRF')
    try {
        var csrf = req.csrfToken();
        res.status(200).json({ _csrf: csrf });
    } catch (e) {
        console.log(e.message);
    }
}
userController.getAuthenticationOptions = function (req, res, next) {
    try {
        var userid = req.payload._id;
        ctrl.get3faOptions(userid, function (options) {
            if (options.status === 200) {
                /*var securityObj = {};
                if (options.length > 0) {
                    options.forEach(function (item, index) {
                        securityObj[index] = item.option;
                    })
                }*/
                res.status(200).json(options.message)
            } else {
                res.status(options.status).json({ message: options.message })
            }
        })
    } catch (e) {
        callback({ status: 500, message: 'Internal Server Error' })
    }
}

userController.getCountryList = function (req, res, next) {
    try {
        // var countrycode = Object.keys(countrylist.countries);
        // var countries = [];
        // countrycode.forEach(function (item) {
        //     var countObj = countrylist.countries[item];
        //     countObj.code = item;
        //     countries.push(countObj)
        // })
        res.status(200).json(csc.getAllCountries())
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.getState = function (req, res, next) {
    try {
        //  countries.push(countObj)
        // })
        res.status(200).json(csc.getStatesOfCountry(String(req.body.id)));
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.sendEmailCode = function (userid, email, callback) {
    try {
        if (typeof userid !== 'undefined' && userid && userid !== '' && typeof email !== 'undefined' && email && email !== '') {
            var otpcodeData = {};
            var timestamp = Date.now();
            otpcodeData.userid = userid;
            otpcodeData.code = uniqid();
            otpcodeData.timestamp = timestamp;

            var sendGrid = new Sendgrid();
            var options = { email: email, code: otpcodeData.code, timestamp: timestamp };
            sendGrid.sendEmail(
                email,
                'Access Key ',
                "views/emailtemplate/keyemail.ejs",
                options
            );
            Otp.create(otpcodeData, function (error, rows) {
                if (error) {
                    callback({ error: true, status: 500, message: 'Internal Server Error' })
                } else {
                    callback({ error: false, status: 200, message: timestamp })

                }
            })
        } else {
            callback({ error: true, status: 400, message: 'Invalid Details' })
        }
    } catch (e) {
        callback({ error: true, status: 500, message: 'Internal Server Error' })
    }
}
userController.extend = function (target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    return target;
}
userController.generateJwt = function (valobj) {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);
    //expiry.setMinutes(expiry.getMinutes()+5);
    return jwt.sign({
        _id: valobj._id,
        email: valobj.email,
        name: valobj.name,
        phone: valobj.phone,
        userid: valobj.userid,
        group: valobj.group,
        exp: parseInt(expiry.getTime() / 1000),
    }, "MY_SECRET")//process.env.JWT_TOKEN_SECRET); // DO NOT KEEP YOUR SECRET IN THE CODE!
}
userController.get3faOptions = function (userid, callback) {
    try {
        Security.find({ userid: userid }).sort({ order: 1 }).exec(function (error, options) {
            if (error) {
                callback({ status: 500, message: 'Internal Server Error' })
            } else {
                callback({ status: 200, message: options })
            }
        })
    } catch (e) {
        callback({ status: 500, message: 'Internal Server Error' })
    }
}
userController.verifyAuthentication = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var dataObj = req.body;
            var userid = req.payload._id;
            var code = dataObj.code;
            if (typeof userid !== 'undefined' && userid && userid !== '' &&
                typeof code !== 'undefined' && code && code !== '') {
                if (typeof dataObj.email !== 'undefined' && dataObj.email && dataObj.email !== '' && dataObj.email == true) {

                    Otp.findOne({
                        userid: userid,
                        code: code
                    }).exec(function (error, rows) {
                        if (error) {
                            res.status(500).json({ message: "Internal Server Error" })
                        } else {
                            if (rows === null) {
                                res.status(400).json({ message: "Invalid Code" })
                            } else {
                                var currentTmeStmp = Date.now();
                                var OTPAge = (currentTmeStmp - rows.timestamp) / 100;
                                if (OTPAge > 900000) {
                                    res.status(400).json({ message: "Code Expired" });
                                } else {
                                    Otp.remove({ userid: userid, code: code }).exec(function (error, delrows) {
                                        if (error) {
                                            res.status(400).json({ message: "Invalid Code" })
                                        } else {
                                            res.status(200).json({ message: 'Code Verified' })
                                        }
                                    })
                                }
                            }
                        }
                    })
                }
                if (typeof dataObj.sms !== 'undefined' && dataObj.sms && dataObj.sms !== '' && dataObj.sms == true) {
                    Otp.findOne({
                        userid: userid,
                        code: code
                    }).exec(function (error, rows) {

                        if (error) {
                            res.status(500).json({ message: "Internal Server Error" })
                        } else {
                            if (rows === null) {
                                res.status(400).json({ message: "Invalid Code" })
                            } else {
                                var currentTmeStmp = Date.now();
                                var OTPAge = (currentTmeStmp - rows.timestamp) / 100;
                                if (OTPAge > 900000) {
                                    res.status(400).json({ message: "Code Expired" });
                                } else {
                                    Otp.remove({ userid: userid, code: code }).exec(function (error, delrows) {
                                        if (error) {
                                            res.status(400).json({ message: "Invalid Code" })
                                        } else {
                                            res.status(200).json({ message: 'Code Verified' })
                                        }
                                    })
                                }
                            }
                        }
                    })
                }
                if (typeof dataObj.twofa !== 'undefined' && dataObj.twofa && dataObj.twofa !== '' && dataObj.twofa === true) {

                    var _s = dataObj.secret;
                    var method = new Function(req);
                    method.googleAuthenticator(res, function (ga) {
                        if (code) {
                            if (ga.verifyCode(_s, code)) {
                                res.status(200).json({ message: 'Code Verified' })
                            } else {
                                res.status(400).json({ message: 'Invalid Code.' })
                            }
                        } else {
                            res.status(400).json({ message: 'Code is required.' })
                        }
                    });
                }
            }
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

//login ,registration related functions
userController.saveUser = function (req, res, next) {
    try {

        console.log('req.body', req.body);
        let email = req.body.email,
            phone = req.body.full_phone,
            password = req.body.password,
            passwordrepeat = req.body.passwordrepeat,
            group = req.body.group,
            name = req.body.name,
            liquidity = req.body.liquidity,
            profileObj = {},
            errorObj = {},
            emailtest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        name ? profileObj.name = name : errorObj.name = 'Name is required or Invalid';
        !password ? errorObj.password = 'Password is required or Invalid' : '';
        !passwordrepeat ? errorObj.passwordrepeat = 'Confirm Password is required or Invalid' : '';
        password !== passwordrepeat ? errorObj.passwordrepeat = 'Password does not match' : '';
        group ? profileObj.group = group : errorObj.group = 'User Group is required';

        if (email) {
            if (emailtest.test(String(email).toLowerCase())) {
                profileObj.email = email;
            } else {
                errorObj.email = 'Invalid Email Address';
            }
        } else errorObj.email = 'Email Address required or Invalid';

        if (typeof phone !== 'undefined' && phone && phone !== '') {
            const number = phoneUtil.parseAndKeepRawInput(phone);
            if (phoneUtil.isPossibleNumber(number) && phoneUtil.isValidNumber(number)) {
                profileObj.phone = phone;
            } else {
                errorObj.phone = 'Phone is Invalid';
            }
        } else errorObj.phone = 'Phone is required or Invalid';

        if (Object.keys(errorObj).length === 0) {
            bcryptJS.hash(req.body.password, 10, function (err, hash) {
                if (err) {
                    res.status(500).json({ message: err.message })
                } else {
                    profileObj = ctrl.extend({}, profileObj, { password: hash });

                    UserCounter.find()
                        .exec(function (error, counterIndex) {
                            var counter = [];
                            if (error || !counterIndex) {
                            } else {
                                counter = counterIndex
                            }
                            console.log('counter', counter);

                            if (counter.length === 0) {
                                UserCounter.create({
                                    '_id': 'userid',
                                    'sequence_value': 0
                                }, function (error, newcounter) {

                                    // UserCounter.findByIdAndUpdate('userid', { $inc: { sequence_value: 1 } }, { new: true }, function (err, counterseqence) {
                                    UserCounter.findOneAndUpdate(
                                        {
                                            findfield: "userid"
                                        },
                                        {
                                            $inc: { sequence_value: 1 }
                                        }, function (err, counterseqence) {

                                            if (err) {
                                                console.log(err)
                                            }
                                            profileObj.userid = Number(counterseqence.sequence_value);
                                            var profileId = Math.random().toString(36).substr(2, 8);
                                            var depid = randomize('000000000');
                                            var refcode = 'DEP' + profileId + depid;
                                            profileObj.profileid = profileId;
                                            profileObj.bankrefcode = refcode;

                                            Users.create(profileObj, function (error, user) {
                                                if (error) {
                                                    if (error.code && error.code === 11000) {
                                                        if (error.errmsg.indexOf('phone_1') === -1) {
                                                            res.status(400).json({ message: "Email already exists, please try another." });
                                                        } else {
                                                            res.status(400).json({ message: "Phone already exists, please try another." });
                                                        }
                                                    } else {
                                                        console.log('error', error);
                                                        res.status(500).json({ message: "Internal Server Error" })
                                                    }
                                                } else {
                                                    if (group !== 'customer') {
                                                        if (typeof liquidity !== 'undefined' && liquidity && Object.keys(liquidity).length > 0) {
                                                            var liqkeys = Object.keys(liquidity);
                                                            var liqArr = []
                                                            liqkeys.forEach((liq) => {
                                                                liqArr.push({ userid: mongoose.Types.ObjectId(user._id), liquidity: liq })
                                                            });
                                                            if (liqArr.length > 0) {
                                                                Userliqproviders.create(liqArr, function (error, liqrows) { });
                                                            }
                                                        }
                                                    }
                                                    var otpcodeData = {};
                                                    var timestamp = Date.now();
                                                    otpcodeData.userid = user._id;
                                                    otpcodeData.code = uniqid();
                                                    otpcodeData.timestamp = timestamp;
                                                    var sendGrid = new Sendgrid();
                                                    var verificationObj = {
                                                        'id': user.id,
                                                        'email': profileObj.email,
                                                        'phone': profileObj.phone,
                                                        'code': otpcodeData.code,
                                                        'timestamp': otpcodeData.timestamp
                                                    }
                                                    console.log('verificationObj', verificationObj)
                                                    var encrptStr = crypt.encrypt(JSON.stringify(verificationObj));
                                                    var host = req.headers.origin + '/verifyemail/';

                                                    const options = {
                                                        LINK: host + encrptStr
                                                    };

                                                    console.log('host', host);
                                                    console.log('options', options);

                                                    sendGrid.sendEmail(
                                                        email,
                                                        'Welcome to Blockoville',
                                                        "views/emailtemplate/confirmemail.ejs",
                                                        options
                                                    );
                                                    Otp.create(otpcodeData, function (error, rows) {
                                                        if (error) {
                                                            console.log('error', error);
                                                            res.status(500).json({ message: "Internal Server Error" })
                                                        } else {
                                                            res.status(200).json({
                                                                message: "A account verification email has been sent. Please verify your account.",
                                                            })
                                                        }
                                                    });
                                                }
                                            });
                                        })
                                })
                            } else {
                                // UserCounter.findByIdAndUpdate('userid', { $inc: { sequence_value: 1 } }, { new: true }, function (err, counterseqence) {
                                UserCounter.findOneAndUpdate(
                                    {
                                        findfield: "userid"
                                    },
                                    {
                                        $inc: { sequence_value: 1 }
                                    }, function (err, counterseqence) {

                                        if (err) {
                                            console.log(err);
                                        }
                                        profileObj.userid = Number(counterseqence.sequence_value);
                                        var profileId = Math.random().toString(36).substr(2, 8)
                                        var depid = randomize('000000000');
                                        var refcode = 'DEP' + profileId + depid;
                                        profileObj.profileid = profileId;
                                        profileObj.bankrefcode = refcode;
                                        Users.create(profileObj, function (error, user) {
                                            if (error) {
                                                if (error.code && error.code === 11000) {
                                                    if (error.errmsg.indexOf('phone_1') === -1) {
                                                        res.status(400).json({ message: "Email already exists, please try another." })
                                                    } else {
                                                        res.status(400).json({ message: "Phone already exists, please try another." })
                                                    }
                                                } else {
                                                    console.log('error', error);
                                                    res.status(500).json({ message: "Internal Server Error" })
                                                }
                                            } else {
                                                if (group !== 'customer') {
                                                    if (typeof liquidity !== 'undefined' && liquidity && Object.keys(liquidity).length > 0) {
                                                        var liqkeys = Object.keys(liquidity);
                                                        var liqArr = []
                                                        liqkeys.forEach((liq) => {
                                                            liqArr.push({ userid: mongoose.Types.ObjectId(user._id), liquidity: liq })
                                                        });
                                                        if (liqArr.length > 0) {
                                                            Userliqproviders.create(liqArr, function (error, liqrows) { });
                                                        }
                                                    }
                                                }
                                                var otpcodeData = {};
                                                var timestamp = Date.now();
                                                otpcodeData.userid = user._id;
                                                otpcodeData.code = uniqid();
                                                otpcodeData.timestamp = timestamp;
                                                var sendGrid = new Sendgrid();
                                                var verificationObj = {
                                                    'id': user.id,
                                                    'email': profileObj.email,
                                                    'phone': profileObj.phone,
                                                    'code': otpcodeData.code,
                                                    'timestamp': otpcodeData.timestamp
                                                }
                                                console.log('verificationObj', verificationObj, user._id, user.id);

                                                var encrptStr = crypt.encrypt(JSON.stringify(verificationObj));
                                                var host = req.headers.origin + '/verifyemail/';

                                                const options = {
                                                    LINK: host + encrptStr
                                                };

                                                console.log('options', options)
                                                sendGrid.sendEmail(
                                                    email,
                                                    'Welcome to Blockoville',
                                                    "views/emailtemplate/confirmemail.ejs",
                                                    options
                                                );
                                                Otp.create(otpcodeData, function (error, rows) {
                                                    if (error) {
                                                        console.log('error', error);
                                                        res.status(500).json({ message: "Internal Server Error" })
                                                    } else {
                                                        res.status(200).json({
                                                            message: "An account verification email has been sent. Please verify your account.",
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    })
                            }
                        });
                }
            })
        } else {
            res.status(400).json({
                message: errorObj
            })
        }
    } catch (e) {
        if (e.message == 'Invalid country calling code') {
            res.status(400).json({ message: "Invalid Phone" })
        } else {
            res.status(500).json({ message: e.message })
        }
    }
}

userController.saveUserAdmin = function (req, res, next) {
    try {

        var email = req.body.email;
        var phone = req.body.full_phone;

        var password = req.body.password;
        var passwordrepeat = req.body.passwordrepeat;
        var group = req.body.group;
        var name = req.body.name;
        var liquidity = req.body.liquidity;
        /*

        var address = req.body.address;
        var city = req.body.city;
        var state = req.body.state;
        var postalcode = req.body.postalcode;
        var country = req.body.country;
        */

        var errorObj = {};

        var passtest = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
        var emailtest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var profileObj = {};


        if (typeof email !== 'undefined' && email && email !== '') {

            if (emailtest.test(String(email).toLowerCase())) {
                profileObj.email = email;
            } else {
                errorObj.email = 'Invalid Email Address';
            }
        } else {
            errorObj.email = 'Email Address required or Invalid';
        }
        if (typeof password !== 'undefined' && password && password !== '') {

            /*if (passtest.test(password)) {
                //profileObj.password = password;
            } else {
                errorObj.password = 'Password is Invalid';
            }*/
        } else {
            errorObj.password = 'Password is required or Invalid';
        }
        if (typeof passwordrepeat !== 'undefined' && passwordrepeat && passwordrepeat !== '') {
            /*if (passtest.test(passwordrepeat)) {
                if (password === passwordrepeat) {
                    profileObj.passwordrepeat = passwordrepeat;
                } else {
                    errorObj.passwordrepeat = 'Password and Repeat Password does not match';
                }
            } else {
                errorObj.passwordrepeat = 'Repeat Password is Invalid';
            }*/
            if (password === passwordrepeat) {
                //profileObj.passwordrepeat = passwordrepeat;
            } else {
                errorObj.passwordrepeat = 'Password does not match';
            }
        } else {
            errorObj.passwordrepeat = 'Confirm Password is required or Invalid';
        }
        if (typeof phone !== 'undefined' && phone && phone !== '') {
            const number = phoneUtil.parseAndKeepRawInput(phone);
            if (phoneUtil.isPossibleNumber(number) && phoneUtil.isValidNumber(number)) {
                profileObj.phone = phone;
            } else {
                errorObj.phone = 'Phone is Invalid';
            }
        } else {
            errorObj.phone = 'Phone is required or Invalid';
        }
        if (typeof name !== 'undefined' && name && name !== '') {
            profileObj.name = name;
        } else {
            errorObj.name = 'Name is required or Invalid';
        }
        /*

        if (typeof country !== 'undefined' && country && country !== '') {

            if (Object.keys(countrylist.countries).indexOf(country) === -1) {
                errorObj.country = 'Country is Invalid';
            } else {
                profileObj.country = country
            }
        } else {
            errorObj.country = 'Country is required';
        }
        if (typeof address !== 'undefined' && address && address !== '') {
            profileObj.address = address;
        }
        if (typeof city !== 'undefined' && city && city !== '') {
            profileObj.city = city;
        }
        if (typeof state !== 'undefined' && state && state !== '') {
            profileObj.state = state;
        }
        if (typeof postalcode !== 'undefined' && postalcode && postalcode !== '') {
            if (isNaN(postalcode)) {
                errorObj.postalcode = 'Postal Code is Invalid';
            } else {
                profileObj.postalcode = postalcode;
            }
        } else {
            errorObj.postalcode = 'Postal Code is required';
        }*/

        if (typeof group !== 'undefined' && group && group !== '') {
            profileObj.group = group;
        } else {
            errorObj.group = 'User Group is required';
        }
        if (Object.keys(errorObj).length === 0) {
            bcryptJS.hash(req.body.password, 10, function (err, hash) {
                if (err) {
                    res.status(500).json({ message: err.message })
                } else {
                    //var passobj = jwtmodel.setPassword(req.body.password);
                    profileObj = ctrl.extend({}, profileObj, { password: hash });
                    UserCounter.find().exec(function (error, counterIndex) {
                        var counter = [];
                        if (error || !counterIndex) {
                        } else {
                            counter = counterIndex
                        }
                        if (counter.length === 0) {
                            UserCounter.create({
                                '_id': 'userid',
                                'sequence_value': 0
                            }, function (error, newcounter) {
                                UserCounter.findByIdAndUpdate('userid', { $inc: { sequence_value: 1 } }, { new: true }, function (err, counterseqence) {
                                    if (err) {
                                        console.log(err)
                                    }
                                    profileObj.userid = Number(counterseqence.sequence_value);
                                    var profileId = Math.random().toString(36).substr(2, 8);
                                    var depid = randomize('000000000');
                                    var refcode = 'DEP' + profileId + depid;
                                    profileObj.profileid = profileId;
                                    profileObj.bankrefcode = refcode;

                                    Users.create(profileObj, function (error, user) {
                                        if (error) {
                                            if (error.code && error.code === 11000) {
                                                if (error.errmsg.indexOf('phone_1') === -1) {
                                                    res.status(400).json({
                                                        message: "Email already exists, please try another."
                                                    })
                                                } else {
                                                    res.status(400).json({
                                                        message: "Phone already exists, please try another."
                                                    })
                                                }
                                            } else {
                                                res.status(500).json({ message: "Internal Server Error" })
                                            }
                                        }
                                        else {
                                            if (group !== 'customer') {
                                                if (typeof liquidity !== 'undefined' && liquidity && Object.keys(liquidity).length > 0) {
                                                    var liqkeys = Object.keys(liquidity);
                                                    var liqArr = []
                                                    liqkeys.forEach((liq) => {
                                                        liqArr.push({
                                                            userid: mongoose.Types.ObjectId(user._id),
                                                            liquidity: liq
                                                        })
                                                    })
                                                    if (liqArr.length > 0) {
                                                        Userliqproviders.create(liqArr, function (error, liqrows) {

                                                        })
                                                    }
                                                }
                                            }
                                            var otpcodeData = {};
                                            var timestamp = Date.now();
                                            otpcodeData.userid = user._id;
                                            otpcodeData.code = uniqid();
                                            otpcodeData.timestamp = timestamp;
                                            var sendGrid = new Sendgrid();
                                            var verificationObj = {
                                                'id': user._id,
                                                'email': profileObj.email,
                                                'phone': profileObj.phone,
                                                'code': otpcodeData.code,
                                                'timestamp': otpcodeData.timestamp
                                            }
                                            var encrptStr = crypt.encrypt(JSON.stringify(verificationObj));
                                            var host = '';
                                            var url = (group === 'customer') ? req.headers.origin.replace('admin.', '') : req.headers.origin;
                                            console.log(group, url);
                                            if (group === 'customer') {
                                                host = url + '/verifyemail/';
                                            }
                                            else {
                                                host = url + '/verifyemail/';
                                            }
                                            const options = {
                                                LINK: host + encrptStr
                                            }
                                            console.log('options', options)
                                            sendGrid.sendEmail(
                                                email,
                                                'Welcome to Blockoville',
                                                "views/emailtemplate/confirmemail.ejs",
                                                options
                                            );
                                            Otp.create(otpcodeData, function (error, rows) {
                                                if (error) {
                                                    res.status(500).json({ message: "Internal Server Error" })
                                                } else {
                                                    res.status(200).json({
                                                        message: "A account verification email has been sent. Please verify your account.",
                                                    })
                                                }
                                            })
                                        }
                                    })
                                })
                            })
                        } else {
                            UserCounter.findByIdAndUpdate('userid', { $inc: { sequence_value: 1 } }, { new: true }, function (err, counterseqence) {
                                if (err) {
                                    console.log(err)
                                }
                                profileObj.userid = Number(counterseqence.sequence_value);
                                var profileId = Math.random().toString(36).substr(2, 8)
                                var depid = randomize('000000000');
                                var refcode = 'DEP' + profileId + depid;
                                profileObj.profileid = profileId;
                                profileObj.bankrefcode = refcode;
                                Users.create(profileObj, function (error, user) {
                                    if (error) {
                                        if (error.code && error.code === 11000) {
                                            if (error.errmsg.indexOf('phone_1') === -1) {
                                                res.status(400).json({
                                                    message: "Email already exists, please try another."
                                                })
                                            } else {
                                                res.status(400).json({
                                                    message: "Phone already exists, please try another."
                                                })
                                            }
                                        } else {
                                            res.status(500).json({ message: "Internal Server Error" })
                                        }
                                    }
                                    else {
                                        if (group !== 'customer') {
                                            if (typeof liquidity !== 'undefined' && liquidity && Object.keys(liquidity).length > 0) {
                                                var liqkeys = Object.keys(liquidity);
                                                var liqArr = []
                                                liqkeys.forEach((liq) => {
                                                    liqArr.push({
                                                        userid: mongoose.Types.ObjectId(user._id),
                                                        liquidity: liq
                                                    })
                                                })
                                                if (liqArr.length > 0) {
                                                    Userliqproviders.create(liqArr, function (error, liqrows) {

                                                    })
                                                }
                                            }
                                        }
                                        var otpcodeData = {};
                                        var timestamp = Date.now();
                                        otpcodeData.userid = user._id;
                                        otpcodeData.code = uniqid();
                                        otpcodeData.timestamp = timestamp;
                                        var sendGrid = new Sendgrid();
                                        var verificationObj = {
                                            'id': user._id,
                                            'email': profileObj.email,
                                            'phone': profileObj.phone,
                                            'code': otpcodeData.code,
                                            'timestamp': otpcodeData.timestamp
                                        }
                                        var encrptStr = crypt.encrypt(JSON.stringify(verificationObj));
                                        var url = (group === 'customer') ? req.headers.origin.replace('admin.', '') : req.headers.origin;
                                        console.log(group, url);
                                        var host = '';
                                        if (group === 'customer') {
                                            host = url + '/verifyemail/';
                                        }
                                        else {
                                            host = url + '/verifyemail/';
                                        }
                                        const options = {
                                            LINK: host + encrptStr
                                        }
                                        console.log('options', options)
                                        sendGrid.sendEmail(
                                            email,
                                            'Welcome to Blockoville',
                                            "views/emailtemplate/confirmemail.ejs",
                                            options
                                        );
                                        Otp.create(otpcodeData, function (error, rows) {
                                            if (error) {
                                                res.status(500).json({ message: "Internal Server Error" })
                                            } else {
                                                res.status(200).json({
                                                    message: "An account verification email has been sent. Please verify your account.",
                                                })
                                            }
                                        })
                                    }
                                })
                            })
                        }
                    });
                }
            })
        } else {
            res.status(400).json({
                message: errorObj
            })
        }
    } catch (e) {
        if (e.message == 'Invalid country calling code') {
            res.status(400).json({ message: "Invalid Phone" })
        } else {
            res.status(500).json({ message: e.message })
        }
    }

}
userController.Login = function (req, res, next) {

    try {
        let authflag = req.body.authflag;
        passport.authenticate('local', function (err, userinfo, info) {
            var token;
            if (err) {
                res.status(404).json(err);
                return;
            }
            // If a user is found
            if (userinfo) {
                if (userinfo.group === 'customer') {
                    var methods = new Function(req);
                    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

                    methods.verifyDevice(userinfo, ip, function (deviceinfo) {
                        if (deviceinfo.error) {
                            res.status(500).json({ message: deviceinfo.result })
                        } else {
                            if (deviceinfo.result) {
                                if (authflag) {
                                    ctrl.get3faOptions(userinfo._id, function (options) {
                                        if (options.status === 200 && options.message.length > 0) {

                                            let userid = userinfo._id,
                                                email = userinfo.email,
                                                phone = userinfo.phone;
                                            options.message.forEach(function (item, index) {
                                                item.option === 'email' ? ctrl.sendEmailCode(userid, email, function (result) { }) : ''
                                                item.option === 'sms' ? ctrl.sendOTP(userid, phone, function (result) { }) : ''
                                            });
                                            res.status(200).json(options.message);

                                        } else {

                                            methods.addLoginLog(userinfo);
                                            token = ctrl.generateJwt(userinfo);
                                            res.status(200);
                                            res.json({ "token": token, userinfo });
                                        }
                                    });
                                } else {
                                    methods.addLoginLog(userinfo);
                                    token = ctrl.generateJwt(userinfo);
                                    res.status(200);
                                    res.json({ "token": token, userinfo });
                                }
                            } else {
                                res.status(401).json({ message: 'unauthorized_device' });
                            }
                        }
                    });
                } else {
                    res.status(403).json({ message: 'Only customers allowed' });
                }
            } else {
                res.status(401).json(info);
            }
        })(req, res);

    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.AdminLogin = function (req, res, next) {
    try {
        var authflag = req.body.authflag;
        passport.authenticate('local', function (err, userinfo, info) {
            var token;
            // If Passport throws/catches an error
            if (err) {
                res.status(404).json(err);
                return;
            }
            // If a user is found
            if (userinfo) {
                if (userinfo.group !== 'customer') {
                    var methods = new Function(req);
                    if (authflag) {
                        ctrl.get3faOptions(userinfo._id, function (options) {
                            if (options.status === 200 && options.message.length > 0) {
                                var userid = userinfo._id;
                                var email = userinfo.email;
                                var phone = userinfo.phone;
                                options.message.forEach(function (item, index) {
                                    if (item.option === 'email') {
                                        ctrl.sendEmailCode(userid, email, function (result) {
                                        })
                                    }
                                    if (item.option === 'sms') {
                                        ctrl.sendOTP(userid, phone, function (result) {
                                        })
                                    }
                                })
                                res.status(200).json(options.message);

                            } else {

                                methods.addLoginLog(userinfo);
                                token = ctrl.generateJwt(userinfo);
                                res.status(200);
                                res.json({
                                    "token": token
                                });
                            }
                        })
                    } else {
                        methods.addLoginLog(userinfo);
                        token = ctrl.generateJwt(userinfo);
                        res.status(200);
                        res.json({
                            "token": token
                        });
                    }
                } else {
                    res.status(403).json({ message: 'Only admin user allowed' });
                }
            } else {
                // If user is not found
                res.status(401).json(info);
            }
        })(req, res);
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.verifyAuthenticationLogin = function (req, res, next) {
    try {
        var dataObj = req.body;
        var userid = dataObj._id;
        var code = dataObj.code;
        if (typeof userid !== 'undefined' && userid && userid !== '' &&
            typeof code !== 'undefined' && code && code !== '') {

            if (typeof dataObj.email !== 'undefined' && dataObj.email && dataObj.email !== '' && dataObj.email == true) {

                Otp.findOne({
                    userid: userid,
                    code: code
                }).exec(function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: "Internal Server Error" })
                    } else {
                        if (rows === null) {
                            res.status(400).json({ message: "Invalid Code" })
                        } else {
                            var currentTmeStmp = Date.now();
                            var OTPAge = (currentTmeStmp - rows.timestamp) / 100;
                            if (OTPAge > 900000) {
                                res.status(400).json({ message: "Code Expired" });
                            } else {
                                Otp.remove({ userid: userid, code: code }).exec(function (error, delrows) {
                                    if (error) {
                                        res.status(400).json({ message: "Invalid Code" })
                                    } else {
                                        res.status(200).json({ message: 'Code Verified' })
                                    }
                                })
                            }
                        }
                    }
                })
            }
            if (typeof dataObj.sms !== 'undefined' && dataObj.sms && dataObj.sms !== '' && dataObj.sms == true) {
                Otp.findOne({
                    userid: userid,
                    code: code
                }).exec(function (error, rows) {

                    if (error) {
                        res.status(500).json({ message: "Internal Server Error" })
                    } else {
                        if (rows === null) {
                            res.status(400).json({ message: "Invalid Code" })
                        } else {
                            var currentTmeStmp = Date.now();
                            var OTPAge = (currentTmeStmp - rows.timestamp) / 100;
                            if (OTPAge > 900000) {
                                res.status(400).json({ message: "Code Expired" });
                            } else {
                                Otp.remove({ userid: userid, code: code }).exec(function (error, delrows) {
                                    if (error) {
                                        res.status(400).json({ message: "Invalid Code" })
                                    } else {
                                        res.status(200).json({ message: 'Code Verified' })
                                    }
                                })
                            }
                        }
                    }
                })
            }
            if (typeof dataObj.twofa !== 'undefined' && dataObj.twofa && dataObj.twofa !== '' && dataObj.twofa === true) {

                var _s = dataObj.secret;
                var method = new Function(req);
                method.googleAuthenticator(res, function (ga) {
                    if (code) {
                        if (ga.verifyCode(_s, code)) {
                            res.status(200).json({ message: 'Code Verified' })
                        } else {
                            res.status(400).json({ message: 'Invalid Code.' })
                        }
                    } else {
                        res.status(400).json({ message: 'Code is required.' })
                    }
                });
            }
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.verifyAccount = function (req, res, next) {
    try {
        var data = JSON.parse(crypt.decrypt(req.query.str));
        var userid = data.id;
        var code = data.code;
        var authkey = data.timestamp;
        var email = data.email;
        if (typeof userid !== 'undefined' && userid !== '' &&
            typeof code !== 'undefined' && code !== '' &&
            typeof authkey !== 'undefined' && authkey !== '' &&
            typeof email !== 'undefined' && email !== ''
        ) {
            Users.findOne({ _id: userid }).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                } else {
                    if (rows == null) {
                        res.status(400).json({ message: "No Account Found" });
                    } else {
                        if (rows.email_verified == '1') {
                            res.status(400).json({ message: "Already Verified" });
                        } else {
                            Otp.findOne({
                                userid: userid,
                                timestamp: authkey,
                                code: code
                            }).sort({ createdAt: -1 }).exec(function (error, rows) {
                                if (error) {
                                    res.status(500).json({ message: "Internal Server Error" })
                                } else {
                                    if (rows === null) {
                                        res.status(400).json({ message: "Invalid Request" })
                                    } else {
                                        var currentTmeStmp = Date.now();
                                        var OTPAge = (currentTmeStmp - authkey) / 100;
                                        if (OTPAge > 900000) {
                                            res.status(400).json({ message: "Link Expired" });
                                        } else {
                                            Users.update({ _id: mongoose.Types.ObjectId(userid) }, { email_verified: 1 }).exec(function (error, uprows) {
                                                if (error) {
                                                    res.status(500).json({ message: "Internal Server Error" })
                                                } else {
                                                    var kycObj = {
                                                        userid: userid
                                                    }
                                                    Actions.findOne({ action: 'new_device_added' }).exec(function (error, action) {
                                                        if (!error && action !== null) {
                                                            var ipAdd = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                                                            var agentinfo = req.headers['user-agent'];
                                                            const detector = new DeviceDetector;
                                                            const result = detector.detect(agentinfo);
                                                            const DEVICE_TYPE = require('node-device-detector/parser/const/device-type');
                                                            const isTabled = result.device && [DEVICE_TYPE.TABLET].indexOf(result.device.type) !== -1;
                                                            const isMobile = result.device && [DEVICE_TYPE.SMARTPHONE, DEVICE_TYPE.FEATURE_PHONE].indexOf(result.device.type) !== -1;
                                                            const isPhablet = result.device && [DEVICE_TYPE.PHABLET].indexOf(result.device.type) !== -1;
                                                            const isIOS = result.os && result.os.family === 'iOS';
                                                            const isAndroid = result.os && result.os.family === 'Android';
                                                            const isDesktop = !isTabled && !isMobile && !isPhablet;
                                                            var device = {
                                                                id: '',
                                                                type: '',
                                                                brand: '',
                                                                model: ''
                                                            }
                                                            if (isDesktop) {
                                                                const result = detector.parseOs(agentinfo);
                                                                device.type = 'Desktop';
                                                                device.model = result.name + ', ' + result.version + ', ' + result.platform
                                                            } else {
                                                                var type = result.device.type
                                                                device.type = type.charAt(0).toUpperCase() + type.slice(1);
                                                                device.model = result.device.brand + ', ' + result.device.model;
                                                            }
                                                            var agentparse = useragent.parse(agentinfo);
                                                            var agentStr = agentparse.toAgent();
                                                            var os = agentparse.os.toString();
                                                            var agent = agentStr + ' ' + os;

                                                            var logData = {
                                                                userid: userid,
                                                                ipaddress: ipAdd,
                                                                agent: agent,
                                                                description: agentinfo,
                                                                action: action._id,
                                                                type: device.type,
                                                                model: device.model,
                                                                timestamp: Date.now()
                                                            }
                                                            Logs.create(logData, function (error, added) {
                                                                Kyc.create(kycObj, function (error, kycrows) {
                                                                    res.status(200).json({ message: "Account Verified" })
                                                                })
                                                            })
                                                        } else {
                                                            Kyc.create(kycObj, function (error, kycrows) {
                                                                res.status(200).json({ message: "Account Verified" })
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
        else {
            res.status(400).json({ message: 'Link is Invalid' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.resentConfirmEmail = function (req, res, next) {
    try {
        var email = req.body.email;
        if (typeof email !== 'undefined' && email && email !== '') {
            Users.findOne({ email: email }).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({ message: 'Internal Server Error' })
                } else {
                    if (rows == null) {
                        res.status(400).json({ message: 'Invalid Email' })
                    } else {
                        if (rows.email_verified == '0') {
                            var otpcodeData = {};
                            var timestamp = Date.now();
                            otpcodeData.userid = rows._id;
                            otpcodeData.code = uniqid();
                            otpcodeData.timestamp = timestamp;
                            var sendGrid = new Sendgrid();
                            var verificationObj = {
                                'id': rows._id,
                                'email': rows.email,
                                'phone': rows.phone,
                                'code': otpcodeData.code,
                                'timestamp': otpcodeData.timestamp
                            }
                            var encrptStr = crypt.encrypt(JSON.stringify(verificationObj));
                            const options = {
                                //'LINK': env.process.SERVERURL + 'verifytr/' + encrptStr
                                //LINK: process.env.CLIENTURL + '/verifyemail/' + encrptStr
                                LINK: req.headers.origin + '/verifyemail/' + encrptStr
                            }

                            sendGrid.sendEmail(
                                email,
                                'Welcome to Blockoville',
                                "views/emailtemplate/confirmemail.ejs",
                                options
                            );
                            Otp.create(otpcodeData, function (error, rows) {
                                if (error) {
                                    res.status(500).json({ message: "Internal Server Error" })
                                } else {
                                    res.status(200).json({
                                        message: "A verification email has been send. Please verify your email.",
                                    })
                                }
                            })
                        } else {
                            res.status(400).json({
                                message: "Account already verified.",
                            })
                        }
                    }
                }
            })
        } else {
            res.status(400).json({ message: 'Invalid Email' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message });
    }
}

//profile functions
userController.getProfile = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userId = req.payload._id;
            Users.findOne({ _id: mongoose.Types.ObjectId(userId) }, {
                password: 0
            }).exec(function (error, profile) {
                if (error) {
                    console.log("ERROR IN GET PROFILE", error);
                    res.status(500).json({ message: 'Internal Server Error' })
                } else {
                    if (profile !== null) {
                        res.status(200).json(profile);
                    } else {
                        res.status(400).json({ message: 'No User Found' })
                    }
                }
            })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.getkyc = function (req, res, next) {
    try {
        console.log(req.payload._id);
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userId = req.payload._id;
            Kyc.findOne({ 'userid': mongoose.Types.ObjectId(userId) }, {
                password: 0
            }).exec(function (error, profile) {
                if (error) {
                    console.log("ERROR IN GET PROFILE", error);
                    res.status(500).json({ message: 'Internal Server Error' })
                } else {
                    if (profile !== null) {
                        res.status(200).json(profile);
                    } else {
                        res.status(400).json({ message: 'No User Found' })
                    }
                }
            })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.getkycdetails = function (req, res, next) {
    try {
        res.status(400).json({ message: 'No User Found' })
        // if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
        //     var userId = req.payload._id;
        //     Users.findOne({_id: mongoose.Types.ObjectId(userId)}, {
        //         password: 0
        //     }).exec(function (error, profile) {
        //         if (error) {
        //             console.log("ERROR IN GET PROFILE", error);
        //             res.status(500).json({message: 'Internal Server Error'})
        //         } else {
        //             if (profile !== null) {
        //                 res.status(200).json(profile);
        //             } else {
        //                 res.status(400).json({message: 'No User Found'})
        //             }
        //         }
        //     })
        // }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.updateProfile = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var profileObj = {};
            if (typeof req.body.name != 'undefined') {
                profileObj.name = req.body.name;
            }
            profileObj.address = req.body.address;
            profileObj.city = req.body.city;
            profileObj.state = req.body.state;
            profileObj.postalcode = req.body.postalcode;
            profileObj.country = req.body.country;
            profileObj.dob = req.body.dob;
            profileObj.avatar = req.body.avatar;
            profileObj.nickname = req.body.nickname;
            profileObj.legalname = req.body.legalname;
            profileObj.dob = req.body.year + '-' + req.body.month + '-' + req.body.date;
            profileObj.pin = req.body.pin;
            profileObj.language = req.body.language;
            profileObj.timezone = req.body.timezone;
            profileObj.phonecode = req.body.phonecode;
            profileObj.stateCode = req.body.state;
            profileObj.phoneno = req.body.phoneno;
            profileObj.state = req.body.state

            var errorObj = {};
            var userid = req.payload._id;
            console.log(userid)
            // if (typeof name !== 'undefined' && name && name !== '') {
            //     profileObj.name = name;
            // } else {
            //     errorObj.name = 'Name is required or Invalid';
            // }
            // if (typeof dob !== 'undefined' && dob && dob !== '') {
            //     profileObj.dob = dob;
            // } else {
            //     errorObj.dob = 'Date of Birth is required or Invalid';
            // }
            // if (typeof country !== 'undefined' && country && country !== '') {

            //     if (Object.keys(countrylist.countries).indexOf(country) === -1) {
            //         errorObj.country = 'Country is Invalid';
            //     } else {
            //         profileObj.country = country
            //     }
            // } else {
            //     errorObj.country = 'Country is required';
            // }
            // if (typeof address !== 'undefined' && address && address !== '') {
            //     profileObj.address = address;
            // }
            // if (typeof city !== 'undefined' && city && city !== '') {
            //     profileObj.city = city;
            // }
            // if (typeof state !== 'undefined' && state && state !== '') {
            //     profileObj.state = state;
            // }
            // if (typeof postalcode !== 'undefined' && postalcode && postalcode !== '') {
            //     if (isNaN(postalcode)) {
            //         errorObj.postalcode = 'Postal Code is Invalid';
            //     } else {
            //         profileObj.postalcode = postalcode;
            //     }
            // } else {
            //     errorObj.postalcode = 'Postal Code is required';
            // }
            // if (typeof avatar !== 'undefined' && avatar && avatar !== '') {
            //     profileObj.avatar = avatar;
            // }
            // if (typeof timezone !== 'undefined') {
            //     profileObj.timezone = timezone;
            // }
            // if (Object.keys(errorObj).length === 0) {
            if (req.body.authflag) {
                // ctrl.get3faOptions(userid, function (options) {
                // if (options.status === 200 && options.message.length > 0) {

                //     var email = req.payload.email;
                //     var phone = req.payload.phone;
                //     options.message.forEach(function (item, index) {
                //         if (item.option === 'email') {
                //             ctrl.sendEmailCode(userid, email, function (result) {
                //             })
                //         }
                //         if (item.option === 'sms') {
                //             ctrl.sendOTP(userid, phone, function (result) {
                //             })
                //         }
                //     })
                //     res.status(200).json({});

                // } else {

                Users.update({ _id: req.payload._id }, profileObj).exec(function (error, rows) {
                    console.log(error)
                    if (error) {
                        res.status(500).json({ message: 'Internal Server Error' })
                    } else {
                        res.status(200).json({
                            status: 200,
                            message: 'Profile Updated'
                        })
                    }
                })
                // }
                // })
            } else {
                Users.update({ _id: req.payload._id }, profileObj).exec(function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: 'Internal Server Error' })
                    } else {
                        res.status(200).json({ message: 'Profile Updated' })
                    }
                })
            }
            // } else {
            //     res.status(400).json({
            //         message: errorObj
            //     })
            // }
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

userController.getTimeZoneByCountryCode = function (req, res, next) {
    try {
        var countryid = req.query.countryid;
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '' &&
            typeof countryid !== 'undefined' && countryid && countryid !== '') {
            var timezoneList = ct.getTimezonesForCountry(countryid);
            res.status(200).json(timezoneList);
        } else {
            res.status(500).json({ message: 'Invalid Details' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.sendPhoneVerificationOTP = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, { phone: 1 }).exec(function (error, phonestatus) {
                if (error) {
                    res.status(500).json({ message: e.message })
                } else {
                    if (phonestatus !== null) {
                        var phone = phonestatus.phone;
                        ctrl.sendOTP(userid, phone, function (result) {
                            res.status(result.status).json({ message: result.message });
                        })
                    } else {
                        res.status(500).json({ message: 'Phone not found' });
                    }
                }
            })
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

userController.getVerifiedPhone = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var code = req.body.code;
            var authkey = req.body.authkey;

            if (typeof code !== 'undefined' && code && code !== '' &&
                typeof authkey !== 'undefined' && authkey && authkey !== '') {
                Otp.findOne({
                    userid: userid,
                    timestamp: authkey,
                    code: code
                }).sort({ createdAt: -1 }).exec(function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: "Internal Server Error" })
                    } else {
                        if (rows === null) {
                            res.status(400).json({ message: "Invalid Code" })
                        } else {
                            var currentTmeStmp = Date.now();
                            var OTPAge = (currentTmeStmp - authkey) / 100;
                            if (OTPAge > 900000) {
                                res.status(400).json({ message: "Code Expired" });
                            } else {
                                Users.updateOne({ _id: mongoose.Types.ObjectId(userid) }, { $set: { phone_verified: 1 } }).exec(function (error, phonestatus) {
                                    if (error) {
                                        res.status(500).json({ message: e.message })
                                    } else {
                                        res.status(200).json({ message: 'Phone Verified' })
                                    }
                                })
                            }
                        }
                    }
                })
            } else {
                res.status(400).json({ message: "Invalid Details" });
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

// 3fa security authentication functions
userController.getSecurity = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var email = req.payload.email;
            Security.find({ userid: userid }).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                } else {
                    var securityObj = {};
                    rows.forEach(function (item) {
                        securityObj[item.option] = item;
                    })
                    var method = new Function(req);
                    method.googleAuthenticator(res, function (ga) {
                        method.isGoogleAuthActive(userid, function (isActive) {
                            if (isActive) {
                                var returnJson = {
                                    enabled: true,
                                    security: securityObj
                                }
                            } else {
                                var secret = ga.createSecret();
                                var qcode = ga.getGoogleQRCodeAPIUrl("Blockoville(" + email + ")", secret);
                                var returnJson = {
                                    secret: secret,
                                    qcode: qcode,
                                    enabled: false,
                                    security: securityObj
                                }
                            }
                            res.status(200).json(returnJson);
                        })
                    })
                }
            })

        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.sendEmailAuthenticationCode = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var email = req.payload.email;
            ctrl.sendEmailCode(userid, email, function (result) {
                res.status(result.status).json({ message: result.message });
            })
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.enableEmailSecurity = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var code = req.body.code;
            var authkey = req.body.authkey;
            var securityObj = [];
            if (typeof code !== 'undefined' && code && code !== '' &&
                typeof authkey !== 'undefined' && authkey && authkey !== '' &&
                typeof req.body.email !== 'undefined' && req.body.email && req.body.email !== '') {
                securityObj.push({ userid: userid, option: 'email' });
                Otp.findOne({
                    userid: userid,
                    timestamp: authkey,
                    code: code
                }).sort({ createdAt: -1 }).exec(function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: "Internal Server Error" })
                    } else {
                        if (rows === null) {
                            res.status(400).json({ message: "Invalid Code" })
                        } else {
                            var currentTmeStmp = Date.now();
                            var OTPAge = (currentTmeStmp - authkey) / 100;
                            if (OTPAge > 900000) {
                                res.status(400).json({ message: "Code Expired" });
                            } else {
                                Security.create(securityObj, function (error, rows) {
                                    if (error) {
                                        res.status(500).json({ message: "Internal Server Error" })
                                    } else {
                                        res.status(200).json({ message: 'Code Verified' })
                                    }
                                })
                            }
                        }
                    }
                })
            } else {
                res.status(400).json({ message: "Invalid Details" });
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.sscode = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, { phone_verified: 1 }).exec(function (error, phonestatus) {
                if (error) {
                    res.status(500).json({ message: e.message })
                } else {
                    if (phonestatus !== null) {
                        if (phonestatus.phone_verified === 1) {
                            var phone = req.payload.phone;
                            ctrl.sendOTP(userid, phone, function (result) {
                                res.status(result.status).json({ message: result.message });
                            })
                        } else {
                            res.status(500).json({ message: 'Phone is not verified' })
                        }
                    } else {
                        res.status(500).json({ message: 'Phone not found' });
                    }
                }
            })
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.enableSMSSecurity = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var code = req.body.code;
            var authkey = req.body.authkey;
            var securityObj = [];

            if (typeof code !== 'undefined' && code && code !== '' &&
                typeof authkey !== 'undefined' && authkey && authkey !== '' &&
                typeof req.body.sms !== 'undefined' && req.body.sms && req.body.sms !== '') {
                securityObj.push({ userid: userid, option: 'sms' });
                Otp.findOne({
                    userid: userid,
                    timestamp: authkey,
                    code: code
                }).sort({ createdAt: -1 }).exec(function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: "Internal Server Error" })
                    } else {
                        if (rows === null) {
                            res.status(400).json({ message: "Invalid Code" })
                        } else {
                            var currentTmeStmp = Date.now();
                            var OTPAge = (currentTmeStmp - authkey) / 100;
                            if (OTPAge > 900000) {
                                res.status(400).json({ message: "Code Expired" });
                            } else {
                                Security.create(securityObj, function (error, rows) {
                                    if (error) {
                                        res.status(500).json({ message: "Internal Server Error" })
                                    } else {
                                        res.status(200).json({ message: 'Code Verified' })
                                    }
                                })
                            }
                        }
                    }
                })
            } else {
                res.status(400).json({ message: "Invalid Details" });
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.VerifyTwofa = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var code = req.body.code;
            var _s = req.body.secret;
            if (typeof code !== 'undefined' && code && code !== '' &&
                typeof _s !== 'undefined' && _s && _s !== '') {
                var method = new Function(req);
                method.googleAuthenticator(res, function (ga) {
                    if (code) {
                        if (ga.verifyCode(_s, code)) {
                            Security.create({
                                userid: userid,
                                option: 'twofa',
                                secret: _s
                            }, function (error, rows) {
                                if (error) {
                                    res.status(500).json({ message: "Internal Server Error" })
                                } else {
                                    res.status(200).json({ message: 'Two-Factor Enabled.' })
                                }
                            })

                        } else {
                            res.status(400).json({ message: 'Invalid Code.' })
                        }
                    } else {
                        res.status(400).json({ message: 'Code is required.' })
                    }
                });
            } else {
                res.status(404).json({ message: 'Code and Secret is required' })
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.DisableTwofa = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var code = req.body.code;
            if (typeof code !== 'undefined' && code && code !== '') {

                Security.findOne({ userid: userid, option: 'twofa' }).exec(function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: "Internal Server Error" })
                    } else {
                        if (rows === null) {
                            res.status(400).json({ message: 'Two factor already disabled' })
                        } else {
                            var _s = rows.secret;
                            if (typeof code !== 'undefined' && code && code !== '') {
                                var method = new Function(req);
                                method.googleAuthenticator(res, function (ga) {
                                    if (code) {
                                        if (ga.verifyCode(_s, code)) {
                                            Security.remove({
                                                userid: userid,
                                                option: 'twofa',
                                            }, function (error, rows) {
                                                if (error) {
                                                    res.status(500).json({ message: "Internal Server Error" })
                                                } else {
                                                    res.status(200).json({ message: 'Two-Factor Disabled.' })
                                                }
                                            })
                                        } else {
                                            res.status(400).json({ message: 'Invalid Code.' })
                                        }
                                    } else {
                                        res.status(400).json({ message: 'Code is required.' })
                                    }
                                });
                            } else {
                                res.status(400).json({ message: 'Two factor already disabled' })
                            }
                        }
                    }
                })
            } else {
                res.status(404).json({ message: 'Code and Secret is required' })
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.disableEmailSecurity = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var code = req.body.code;
            var authkey = req.body.authkey;
            if (typeof code !== 'undefined' && code && code !== '' &&
                typeof authkey !== 'undefined' && authkey && authkey !== '') {

                Otp.findOne({
                    userid: userid,
                    timestamp: authkey,
                    code: code
                }).sort({ createdAt: -1 }).exec(function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: "Internal Server Error" })
                    } else {
                        if (rows === null) {
                            res.status(400).json({ message: "Invalid Code" })
                        } else {
                            var currentTmeStmp = Date.now();
                            var OTPAge = (currentTmeStmp - authkey) / 100;
                            if (OTPAge > 900000) {
                                res.status(400).json({ message: "Code Expired" });
                            } else {
                                Security.remove({ userid: userid, option: 'email' }).exec(function (error, rows) {
                                    if (error) {
                                        res.status(500).json({ message: "Internal Server Error" })
                                    } else {
                                        res.status(200).json({ message: 'Code Verified' })
                                    }
                                })
                            }
                        }
                    }
                })
            } else {
                res.status(400).json({ message: "Invalid Details" })
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.disableSMSSecurity = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var code = req.body.code;
            var authkey = req.body.authkey;
            if (typeof code !== 'undefined' && code && code !== '' &&
                typeof authkey !== 'undefined' && authkey && authkey !== '') {

                Otp.findOne({
                    userid: userid,
                    timestamp: authkey,
                    code: code
                }).sort({ createdAt: -1 }).exec(function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: "Internal Server Error" })
                    } else {
                        if (rows === null) {
                            res.status(400).json({ message: "Invalid Code" })
                        } else {
                            var currentTmeStmp = Date.now();
                            var OTPAge = (currentTmeStmp - authkey) / 100;
                            if (OTPAge > 900000) {
                                res.status(400).json({ message: "Code Expired" });
                            } else {
                                Security.remove({ userid: userid, option: 'sms' }).exec(function (error, rows) {
                                    if (error) {
                                        res.status(500).json({ message: "Internal Server Error" })
                                    } else {
                                        res.status(200).json({ message: 'Code Verified' })
                                    }
                                })
                            }
                        }
                    }
                })
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.setAuthPriority = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var authid = req.body._id;
            var order = req.body.order;
            if (typeof authid !== 'undefined' && authid && authid !== '' &&
                typeof order !== 'undefined') {
                Security.update({ _id: authid }, { order: order }).exec(function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: "Internal Server Error" })
                    } else {
                        res.status(200).json({ message: 'Priority Updated' })
                    }
                })
            } else {
                res.status(400).json({ message: "Invalid Details" })
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.getUserAuthOptions = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            Security.find({ userid: userid }).sort({ order: 1 }).exec(function (error, options) {
                if (error) {
                    res.status(500).json({ message: error.message })
                } else {
                    res.status(200).json(options)
                }
            })
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}


// forget passwor recovery functions
userController.sendForgetPassRecoveryCode = function (req, res, next) {
    try {
        var target = req.query.str;
        if (typeof target !== 'undefined' && target && target !== '') {
            var flag = '';
            var emailtest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (emailtest.test(String(target).toLowerCase())) {
                flag = 'email';
            } else {
                //target = '+' + target.trim();
                const number = phoneUtil.parseAndKeepRawInput(target);
                if (phoneUtil.isPossibleNumber(number) && phoneUtil.isValidNumber(number)) {
                    flag = 'phone';
                }
            }
            if (flag !== '') {
                var filter = {};
                if (flag === 'email') {
                    filter.email = target;
                }
                if (flag === 'phone') {
                    filter.phone = target;
                }

                Users.findOne(filter).exec(function (error, user) {
                    if (error) {
                        res.status(500).json({ message: 'Internal Server Error' })
                    } else {
                        if (user !== null) {
                            if (user.status === 'Active') {
                                if (flag === 'email') {
                                    ctrl.sendEmailCode(user._id, target, function (response) {
                                        res.status(response.status).json({ message: response.message, name: user.name });
                                    })
                                }
                                if (flag === 'phone') {
                                    ctrl.sendOTP(user._id, target, function (response) {
                                        res.status(response.status).json({ message: response.message, name: user.name });
                                    })
                                }
                            } else {
                                res.status(500).json({ message: 'Account is inactive. Please contact to support.' })
                            }
                        } else {
                            res.status(400).json({ message: 'Invalid Details' })
                        }
                    }
                })
            } else {
                res.status(400).json({ message: 'Invalid Details' })
            }

        } else {
            res.status(400).json({ message: 'Invalid Details' })
        }

    } catch (e) {
        console.log(e.message)
        if (e.message == 'The string supplied did not seem to be a phone number') {
            res.status(400).json({ message: 'Invalid Details' })
        } else {
            res.status(500).json({ message: e.message })
        }
        //The string supplied did not seem to be a phone number

    }
}
userController.verifyForgetRecoveryCode = function (req, res, next) {
    try {
        var data = req.query.str;
        var obj = JSON.parse(data);
        var target = obj.target;
        var code = obj.code;
        var authkey = obj.authkey;
        if (typeof target !== 'undefined' && target && target !== '' &&
            typeof code !== 'undefined' && code && code !== '' &&
            typeof authkey !== 'undefined' && authkey && authkey !== '') {

            var flag = '';
            var emailtest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (emailtest.test(String(target).toLowerCase())) {
                flag = 'email';
            } else {
                target = '+' + target.trim();
                const number = phoneUtil.parseAndKeepRawInput(target);
                if (phoneUtil.isPossibleNumber(number) && phoneUtil.isValidNumber(number)) {
                    flag = 'phone';
                }
            }
            if (flag !== '') {
                var filter = {};
                if (flag === 'email') {
                    filter.email = target;
                }
                if (flag === 'phone') {
                    filter.phone = target;
                }

                Users.findOne(filter).exec(function (error, user) {
                    if (error) {
                        res.status(500).json({ message: 'Internal Server Error' })
                    } else {
                        if (user !== null) {
                            Otp.findOne({
                                userid: user._id,
                                timestamp: authkey,
                                code: code
                            }).sort({ createdAt: -1 }).exec(function (error, rows) {
                                if (error) {
                                    res.status(500).json({ message: "Internal Server Error" })
                                } else {
                                    if (rows === null) {
                                        res.status(400).json({ message: "Invalid Key" })
                                    } else {
                                        var currentTmeStmp = Date.now();
                                        var OTPAge = (currentTmeStmp - authkey) / 100;
                                        if (OTPAge > 900000) {
                                            res.status(400).json({ message: "Access Key Expired" });
                                        } else {
                                            res.status(200).json({ message: 'Verified' })
                                        }
                                    }
                                }
                            })
                        } else {
                            res.status(400).json({ message: 'Invalid Details' })
                        }
                    }
                })
            } else {
                res.status(400).json({ message: 'Invalid Details' })
            }

        } else {
            res.status(400).json({ message: 'Invalid Details' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.setForgetPassword = function (req, res, next) {
    try {
        var target = req.body.target;
        var password = req.body.password;
        var confirmpassword = req.body.confirmpassword;
        var step = req.body.step;
        if (typeof target !== 'undefined' && target && target !== '' &&
            typeof password !== 'undefined' && password && password !== '' &&
            typeof confirmpassword !== 'undefined' && confirmpassword && confirmpassword !== '' &&
            typeof step !== 'undefined' && step && step !== '' && step == '3') {

            if (password === confirmpassword) {
                var flag = '';
                var emailtest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (emailtest.test(String(target).toLowerCase())) {
                    flag = 'email';
                } else {
                    const number = phoneUtil.parseAndKeepRawInput(target);
                    if (phoneUtil.isPossibleNumber(number) && phoneUtil.isValidNumber(number)) {
                        flag = 'phone';
                    }
                }
                if (flag !== '') {
                    var filter = {};
                    if (flag === 'email') {
                        filter.email = target;
                    }
                    if (flag === 'phone') {
                        filter.phone = target;
                    }

                    Users.findOne(filter).exec(function (error, user) {
                        if (error) {
                            res.status(500).json({ message: 'Internal Server Error' })
                        } else {
                            if (user !== null) {
                                bcryptJS.hash(password, 10, function (err, hash) {
                                    if (err) {
                                        res.status(500).json({ message: 'Internal Server Error' })
                                    } else {
                                        Users.update({ _id: user._id }, { password: hash }).exec(function (error, updatedrow) {
                                            if (error) {
                                                res.status(500).json({ message: 'Internal Server Error' })
                                            } else {
                                                res.status(200).json({ message: 'Password has been set successfully' })
                                            }
                                        })
                                    }
                                })
                                //var encypass = jwtmodel.setPassword(password);
                                //encypass.passwordrepeat = confirmpassword;

                            } else {
                                res.status(400).json({ message: 'Invalid Details' })
                            }
                        }
                    })
                } else {
                    res.status(400).json({ message: 'Invalid Details' })
                }
            } else {
                res.status(400).json({ message: 'Password and Confirm Password does not match' })
            }

        } else {
            res.status(400).json({ message: 'Invalid Details' })
        }
    } catch (e) {
        console.log(e.message)
        res.status(500).json({ message: e.message })
    }
}

// change password functions
userController.changePassword = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var oldpassword = req.body.oldpassword;
            var password = req.body.password;
            var confirmpassword = req.body.confirmpassword;
            var authflag = req.body.authflag;
            if (typeof oldpassword !== 'undefined' && oldpassword && oldpassword !== '' &&
                typeof password !== 'undefined' && password && password !== '' &&
                typeof confirmpassword !== 'undefined' && confirmpassword && confirmpassword !== '') {
                if (password === confirmpassword) {
                    if (authflag) {
                        ctrl.get3faOptions(userid, function (options) {
                            if (options.status === 200 && options.message.length > 0) {

                                var email = req.payload.email;
                                var phone = req.payload.phone;
                                options.message.forEach(function (item, index) {
                                    if (item.option === 'email') {
                                        ctrl.sendEmailCode(userid, email, function (result) {
                                        })
                                    }
                                    if (item.option === 'sms') {
                                        ctrl.sendOTP(userid, phone, function (result) {
                                        })
                                    }
                                })
                                res.status(200).json({});

                            } else {
                                ctrl.savePassword(userid, oldpassword, password, confirmpassword, function (rows) {
                                    res.status(rows.status).json({ message: rows.message });
                                })
                            }
                        })
                    } else {
                        ctrl.savePassword(userid, oldpassword, password, confirmpassword, function (rows) {
                            res.status(rows.status).json({ message: rows.message });
                        })
                    }
                } else {
                    res.status(400).json({ message: 'Password and Confirm Password does not match' })
                }
            } else {
                res.status(400).json({ message: 'Invalid Details' })
            }

        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.savePassword = function (userid, oldpassword, password, confirmpassword, callback) {
    try {
        Users.findOne({ _id: userid }).exec(function (error, user) {
            if (error || user == null) {
                return callback({ status: 500, message: 'Internal Server Error' });
            } else {
                bcryptJS.compare(oldpassword, user.password, function (err, resp) {
                    if (err) {
                        return callback({ status: 500, message: 'Internal Server Error' });
                    } else {
                        if (resp) {
                            bcryptJS.hash(password, 10, function (err, hash) {
                                if (err) {
                                    return callback({ status: 500, message: 'Internal Server Error' });
                                } else {
                                    Users.update({ _id: userid }, { password: hash }).exec(function (error, rows) {
                                        if (error) {
                                            callback({ status: 500, message: 'Internal Server Error' })
                                        } else {
                                            callback({ status: 200, message: 'Password changed successfully' })
                                        }
                                    })
                                }
                            })
                        } else {
                            callback({ status: 400, message: 'Wrong Old Password' })
                        }
                    }
                })
                /*if (jwtmodel.validPassword(oldpassword, user)) {
                    var newPassObj = jwtmodel.setPassword(password);
                    //newPassObj.passwordrepeat = confirmpassword;

                    Users.update({ _id: userid }, newPassObj).exec(function (error, rows) {
                        if (error) {
                            callback({ status: 500, message: 'Internal Server Error' })
                        } else {
                            callback({ status: 200, message: 'Password changed successfully' })
                        }
                    })
                } else {
                    callback({ status: 400, message: 'Wrong Old Password' })
                }*/
            }
        })
    } catch (e) {
        return callback({ status: 500, message: e.message });
    }
}

//customer functions
userController.getCustomer = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var perPage = Number(req.query.perpage ? req.query.perpage : 0);
            var page = Number(req.query.page ? req.query.page : 0);
            var search = req.query.search ? req.query.search : '';
            var filter = {};
            if (search !== '') {
                filter = {
                    group: { $eq: 'customer' },
                    $or: [
                        { name: { $regex: new RegExp(search, "i") } },
                        { email: { $regex: new RegExp(search, "i") } },
                        { phone: { $regex: new RegExp(search, "i") } }
                    ]
                }
            } else {
                filter = {
                    group: { $eq: 'customer' }
                }
            }
            if (typeof perPage !== 'undefined' && perPage !== '' && perPage > 0 && typeof page !== 'undefined' && page !== '' && page > 0) {
                var skippage = (perPage * page) - perPage;
                Users.find(filter, {
                    password: 0,
                    email_verified: 0,
                    phone_verified: 0
                }).skip(skippage).limit(perPage).sort({ createdAt: -1 }).exec(function (txerr, txdoc) {
                    Users.find(filter, {
                        password: 0,
                        email_verified: 0,
                        phone_verified: 0
                    }).count().exec(function (err, count) {
                        if (err) {
                            res.status(500).json({ status: false, message: 'Internal Server Error' });
                        } else {
                            var returnJson = {
                                status: true,
                                customers: txdoc,
                                current: page,
                                pages: Math.ceil(count / perPage),
                                total: count
                            }
                            res.status(200).json(returnJson);
                        }
                    });
                });
            } else {
                Users.find(filter, {
                    password: 0,
                    email_verified: 0,
                    phone_verified: 0
                }).sort({ createdAt: -1 }).exec(function (txerr, txdoc) {
                    if (txerr) {
                        res.status(500).json({ status: false, message: 'Internal Server Error' });
                    } else {
                        var returnJson = {
                            status: true,
                            customers: txdoc,
                            total: 0
                        }
                        res.status(200).json(returnJson);
                    }
                });
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Request' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }

}
userController.getCustomerById = function (req, res, next) {
    try {
        var userid = req.query.id;
        Users.findOne({ _id: userid }, {
            password: 0,
            email_verified: 0,
            phone_verified: 0
        }).exec(function (error, results) {
            if (error) {
                res.status(500).json({ message: "Internal Server Error" })
            } else {
                res.status(200).json(results)
            }
        })
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.getAdminUserById = function (req, res, next) {
    try {
        var userid = req.query.userid;
        Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, {
            password: 0,
            email_verified: 0,
            phone_verified: 0
        }).exec(function (error, results) {
            if (error) {
                res.status(500).json({ message: "Internal Server Error" })
            } else {
                Userliqproviders.find({ userid: mongoose.Types.ObjectId(userid) }).exec(function (error, liqprovider) {
                    if (!error && liqprovider.length > 0) {
                        var liqproObj = {};
                        liqprovider.forEach((item) => {
                            liqproObj[item.liquidity] = true;
                        })
                        res.status(200).json({ user: results, liquidity: liqproObj });
                    } else {
                        res.status(200).json({ user: results, liquidity: {} })
                    }
                })
            }
        })
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.updateAdminUser = function (req, res, next) {
    try {
        var userid = req.body._id;
        var name = req.body.name;
        var gender = req.body.gender;
        var liquidity = req.body.liquidity;
        var errorObj = {};

        var profileObj = {};

        if (typeof name !== 'undefined' && name && name !== '') {
            profileObj.name = name;
        } else {
            errorObj.name = 'Name is required or Invalid';
        }
        if (typeof gender !== 'undefined' && gender && gender !== '') {
            profileObj.gender = gender;
        } else {
            errorObj.gender = 'Gender is required or Invalid';
        }
        if (Object.keys(errorObj).length === 0) {
            Users.updateOne({ _id: mongoose.Types.ObjectId(userid) }, profileObj).exec(function (error, user) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                }
                else {
                    Userliqproviders.remove({ userid: mongoose.Types.ObjectId(userid) }).exec(function (error, delrows) {
                        if (!error) {
                            if (typeof liquidity !== 'undefined' && liquidity && Object.keys(liquidity).length > 0) {
                                var liqkeys = Object.keys(liquidity);
                                var liqArr = []
                                liqkeys.forEach((liq) => {
                                    if (liquidity[liq]) {
                                        liqArr.push({
                                            userid: mongoose.Types.ObjectId(userid),
                                            liquidity: liq
                                        })
                                    }
                                })
                                if (liqArr.length > 0) {
                                    Userliqproviders.create(liqArr, function (error, liqrows) {

                                    })
                                }
                            }
                        }
                        res.status(200).json({ message: "User Updated Successfully" })
                    })
                }
            })
        } else {
            res.status(400).json({
                message: errorObj
            })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

userController.updateCustomerStatusById = function (req, res, next) {
    try {
        var userid = req.query.id;
        var status = req.query.status;
        if (typeof userid !== 'undefined' && userid && userid !== '' && typeof status !== 'undefined' && status && status !== '') {
            Users.update({ _id: userid }, { status: status }).exec(function (error, results) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                } else {
                    ctrl.sendEmailAccountUpdates(userid, status, function (updates) {
                        res.status(200).json({ message: 'updated' })
                    })
                }
            })
        } else {
            res.status(400).json({ message: "Invalid Parameters" })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.deleteCustomerById = function (req, res, next) {
    try {
        var userid = req.query.id;

        if (typeof userid !== 'undefined' && userid && userid !== '') {
            Users.remove({ _id: userid }).exec(function (error, results) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                } else {
                    res.status(200).json({ message: 'Deleted' })
                }
            })
        } else {
            res.status(400).json({ message: "Invalid Parameters" })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.sendEmailAccountUpdates = function (userid, status, callback) {
    try {
        if (typeof userid !== 'undefined' && userid && userid !== '' && typeof status !== 'undefined' && status && status !== '') {
            Users.findOne({ _id: userid }).exec(function (error, rows) {
                if (error) {
                    callback({ error: true, status: 500, message: 'Internal Server Error' })
                } else {
                    if (status === 'Active') {
                        var sendGrid = new Sendgrid();
                        var options = { name: rows.name };
                        sendGrid.sendEmail(
                            rows.email,
                            'Blockoville Notification ',
                            "views/emailtemplate/accountactive.ejs",
                            options
                        );
                    }
                    if (status === 'Deactive') {
                        var sendGrid = new Sendgrid();
                        var options = { name: rows.name };
                        sendGrid.sendEmail(
                            rows.email,
                            'Blockoville Notification ',
                            "views/emailtemplate/accountdeactive.ejs",
                            options
                        );
                    }
                    callback({ error: false, status: 200, message: '' })
                }
            })

        } else {
            callback({ error: true, status: 400, message: 'Invalid Details' })
        }
    } catch (e) {
        callback({ error: true, status: 500, message: 'Internal Server Error' })
    }
}

// kyc
userController.getCustomerKyc = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            Kyc.findOne({ userid: mongoose.Types.ObjectId(req.payload._id) }).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({ message: 'Internal Server Error' });
                } else {
                    res.status(200).json(rows);
                }
            })
        } else {
            res.status(401).json({ message: 'Unauthorized Request' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message });
    }
}
userController.getCustomerKycWithPlan = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            Kyc.aggregate([
                {
                    "$lookup": {
                        from: "kycplans",
                        localField: "level",
                        foreignField: "order",
                        as: "plan"
                    }
                },
                { $unwind: "$plan" },
                { $match: { userid: mongoose.Types.ObjectId(req.payload._id) } }
            ]).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({ message: 'Internal Server Error' });
                } else {
                    res.status(200).json(rows);
                }
            })
        } else {
            res.status(401).json({ message: 'Unauthorized Request' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message });
    }
}
userController.getCustomerKycAdmin = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var perPage = Number(req.query.perpage ? req.query.perpage : 0);
            var page = Number(req.query.page ? req.query.page : 0);
            var search = req.query.search ? req.query.search : '';
            var filter = {};
            if (search !== '') {
                filter = {
                    "user.status": 'Active',
                    "user.group": { $ne: 'admin' },
                    $or: [
                        { "user.name": { $regex: new RegExp(search, "i") } },
                        { "user.email": { $regex: new RegExp(search, "i") } },
                        { "user.phone": { $regex: new RegExp(search, "i") } }
                    ]
                }
            }

            if (typeof perPage !== 'undefined' && perPage !== '' && perPage > 0 && typeof page !== 'undefined' && page !== '' && page > 0) {
                var skippage = (perPage * page) - perPage;
                Kyc.aggregate([
                    {
                        "$lookup": {
                            from: "users",
                            localField: "userid",
                            foreignField: "_id",
                            as: "user"
                        }
                    },
                    { $unwind: "$user" },
                    { $match: filter },
                    {
                        "$project": {
                            "user.gender": 0,
                            "user.email_verified": 0,
                            "user.phone_verified": 0,
                            "user.status": 0,
                            "user.usercurrency": 0,
                            "user.group": 0,
                            "user.password": 0,
                            "user.createdAt": 0,
                            "user.dob": 0,
                            "user.avatar": 0,
                            "user.apikey": 0,
                            "user.bankrefcode": 0
                        }
                    }
                ]).sort({ createdAt: -1 }).skip(skippage).limit(perPage).exec(function (txerr, txdoc) {
                    if (txerr) {
                        res.status(500).json({ status: false, message: 'Internal Server Error' });
                    } else {
                        Kyc.aggregate([
                            {
                                "$lookup": {
                                    from: "users",
                                    localField: "userid",
                                    foreignField: "_id",
                                    as: "user"
                                }
                            },
                            { $unwind: "$user" },
                            { $match: filter },
                            {
                                "$project": {
                                    "user.gender": 0,
                                    "user.email_verified": 0,
                                    "user.phone_verified": 0,
                                    "user.status": 0,
                                    "user.usercurrency": 0,
                                    "user.group": 0,
                                    "user.password": 0,
                                    "user.createdAt": 0,
                                    "user.dob": 0,
                                    "user.avatar": 0,
                                    "user.apikey": 0,
                                    "user.bankrefcode": 0
                                }
                            }
                        ]).exec(function (err, count) {
                            if (err) {
                                res.status(500).json({ status: false, message: 'Internal Server Error' });
                            } else {
                                var returnJson = {
                                    status: true,
                                    customers: txdoc,
                                    current: page,
                                    pages: Math.ceil(count.length / perPage),
                                    total: count.length
                                }
                                res.status(200).json(returnJson);
                            }
                        });
                    }
                });
            } else {
                Kyc.aggregate([
                    {
                        "$lookup": {
                            from: "users",
                            localField: "userid",
                            foreignField: "_id",
                            as: "user"
                        }
                    },
                    { $unwind: "$user" },
                    { $match: filter },
                    {
                        "$project": {
                            "user.gender": 0,
                            "user.email_verified": 0,
                            "user.phone_verified": 0,
                            "user.status": 0,
                            "user.usercurrency": 0,
                            "user.group": 0,
                            "user.password": 0,
                            "user.createdAt": 0,
                            "user.dob": 0,
                            "user.avatar": 0,
                            "user.apikey": 0,
                            "user.bankrefcode": 0
                        }
                    }
                ]).sort({ createdAt: -1 }).exec(function (txerr, txdoc) {
                    if (txerr) {
                        res.status(500).json({ status: false, message: 'Internal Server Error' });
                    } else {
                        var returnJson = {
                            status: true,
                            kyclist: txdoc,
                            total: txdoc.length
                        }
                        res.status(200).json(returnJson);
                    }
                });
            }

        } else {
            res.status(401).json({ message: 'Unauthorized Request' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message });
    }
}
userController.updateKYCStatusAdmin = function (req, res, next) {
    try {
        var userid = req.query.id;
        var status = req.query.status;
        if (typeof userid !== 'undefined' && userid && userid !== '' && typeof status !== 'undefined' && status && status !== '') {
            Kyc.updateOne({ userid: userid }, { status: status }).exec(function (error, results) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                } else {
                    ctrl.sendEmailKYCUpdates(userid, status, function (updates) {
                        res.status(200).json({ message: 'updated' })
                    })
                }
            })
        } else {
            res.status(400).json({ message: "Invalid Parameters" })
        }
    } catch (e) {
        res.status(500).json({ message: e.message });
    }
}
userController.deleteKYCAdmin = function (req, res, next) {
    try {
        var userid = req.query.id;
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            var updateObj = {
                level: 1,
                hash: '',
                hashobj: null,
                response: null,
                status: 'Approve'
            }
            Kyc.updateOne({ userid: mongoose.Types.ObjectId(userid) }, updateObj).exec(function (error, results) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                } else {
                    fs.unlink(process.env.KYCDOCURL + userid, function (err) {
                    })
                    ctrl.sendEmailKYCUpdates(userid, 'removed', function (updates) {
                        res.status(200).json({ message: 'updated' })
                    })
                }
            })
        } else {
            res.status(400).json({ message: "Invalid Parameters" })
        }
    } catch (e) {
        res.status(500).json({ message: e.message });
    }
}

userController.sendEmailKYCUpdates = function (userid, status, callback) {
    try {
        if (typeof userid !== 'undefined' && userid && userid !== '' && typeof status !== 'undefined' && status && status !== '') {
            Users.findOne({ _id: userid }).exec(function (error, rows) {
                if (error) {
                    callback({ error: true, status: 500, message: 'Internal Server Error' })
                } else {
                    var sendGrid = new Sendgrid();
                    var options = { name: rows.name, status: status };
                    sendGrid.sendEmail(
                        rows.email,
                        'Blockoville Notification ',
                        "views/emailtemplate/kycupdate.ejs",
                        options
                    );
                    callback({ error: false, status: 200, message: '' })
                }
            })

        } else {
            callback({ error: true, status: 400, message: 'Invalid Details' })
        }
    } catch (e) {
        callback({ error: true, status: 500, message: 'Internal Server Error' })
    }
}
// userController.updateKyc = function (req, res, next) {
//     try {
//         if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
//             upload(req, res, function (err) {
//                 if (err) {
//                     fs.unlink(process.env.KYCDOCURL + req.payload._id, function (err) {
//                     })
//                     console.log(err);
//                     if (err.code === 'LIMIT_FILE_SIZE') {
//                         res.status(400).json({message: 'File size is too large. Max limit is 3MB'});
//                     } else if (err.code === 'fieltype') {
//                         res.status(400).json({message: 'File type is invalid. Only accepted .png/.jpg/.jpeg/.pdf .'});
//                     } else {
//                         res.status(400).json({message: 'File was not able to be uploaded'});
//                     }
//                 } else {
//                   console.log(req.body);
//                     var idproof = req.body.idproof;
//                     var idnumber = req.body.idnumber;
//                     var addressproof = req.body.addressproof;
//                     var kycObj = {};
//                     var errorObj = {};
//                     kycObj.userid = req.payload._id;
//                     if (typeof idproof !== 'undefined' && idproof && idproof !== '') {
//                         kycObj.idproof = idproof;
//                         if (typeof idnumber !== 'undefined' && idnumber && idnumber !== '') {
//                             kycObj.idnumber = idnumber;
//                         } else {
//                             errorObj.idnumber = 'Identity Proof Number is required'
//                         }
//                     } else {
//                         errorObj.idproof = 'Identity Proof Document is required'
//                     }
//                     if (typeof addressproof !== 'undefined' && addressproof && addressproof !== '') {
//                         kycObj.addressproof = addressproof;
//                     } else {
//                         errorObj.idproof = 'Address Proof Document is required'
//                     }
//
//                     if (Object.keys(errorObj).length === 0) {
//                         var filesObj = req.files;
//
//                         filesObj.forEach(function (obj) {
//                             switch (obj.fieldname) {
//                                 case 'idproofdoc':
//                                     var string = obj.path + "";
//                                     var path = string.replace("public/", '');
//                                     kycObj.idproofdoc = path;
//                                     break;
//                                 case 'addressproofdoc':
//                                     var string = obj.path + "";
//                                     var path = string.replace("public/", '');
//                                     kycObj.addressproofdoc = path;
//                                     break;
//                                 default:
//                                     res.status(400).json({
//                                         "message": "File Upload Failed"
//                                     });
//                             }
//                         })
//                         Kyc.create(kycObj, function (error, rows) {
//                             if (error) {
//                                 res.status(500).json({message: 'Internal Server Error'});
//                             } else {
//                                 res.status(200).json({message: 'Kyc Updated'})
//                             }
//                         })
//
//                     } else {
//                         fs.unlink(process.env.KYCDOCURL + req.payload._id, function (err) {
//                         })
//                         res.status(400).json(errorObj);
//                     }
//                 }
//             })
//         } else {
//             res.status(401).json({message: 'Unauthorized Request'})
//         }
//     } catch (e) {
//         res.status(500).json({message: e.message});
//     }
// }

userController.updateKyc = function (req, res, next) {
    try {
        console.log(req.payload._id);
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {


            var kycObj = {};
            var errorObj = {};
            kycObj.idprooffront = req.body.front;
            kycObj.idproofback = req.body.back;
            kycObj.addressproof = req.body.address;
            kycObj.photoproof = req.body.idproof;
            kycObj.userid = req.payload._id;
            kycObj.status = 'Pending';
            var filesObj = req.files;


            Kyc.findOne({ 'userid': mongoose.Types.ObjectId(req.payload._id) }).exec(function (error, user) {
                if (user != null) {
                    Kyc.update({ 'userid': mongoose.Types.ObjectId(req.payload._id) }, kycObj).exec(function (error, rows) {
                        if (error) {
                            res.status(500).json({ message: 'Internal Server Error' });
                        } else {
                            res.status(200).json({
                                status: 200,
                                message: 'Kyc Updated'
                            })
                        }
                    })
                } else {
                    Kyc.create(kycObj, function (error, rows) {
                        if (error) {
                            res.status(500).json({ message: 'Internal Server Error' });
                        } else {
                            res.status(200).json({
                                status: 200,
                                message: 'Kyc Updated'
                            })
                        }
                    })
                }



            });

        } else {
            res.status(401).json({ message: 'Unauthorized Request' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message });
    }
}

userController.addKYCPlan = function (req, res, next) {
    try {
        var title = req.body.title;
        var order = req.body.order;
        var features = req.body.features;
        var requirements = req.body.requirements;
        var dl = req.body.deposit_limit;
        var dl_unlimited = req.body.deposit_limit_unlimited;
        var dl_overall = req.body.deposit_limit_overall;
        var ddl = req.body.deposit_limit_daily;
        var mdl = req.body.deposit_limit_monthly;
        var wl = req.body.withdraw_limit;
        var wl_unlimited = req.body.withdraw_limit_unlimited;
        var wl_overall = req.body.withdraw_limit_overall;
        var dwl = req.body.withdraw_limit_daily;
        var mwl = req.body.withdraw_limit_monthly;
        /*
        var dfc = req.body.deposit_fee_charges;
        var dff = req.body.deposit_fee_apply;
        var wfc = req.body.deposit_fee_charges;
        var wff = req.body.withdraw_fee_apply;
        */
        var planid = req.body._id;
        var kycObj = {};
        var errorObj = {};
        if (typeof title !== 'undefined' && title && title !== '') {
            kycObj.title = title;
        } else {
            errorObj.title = 'Title is required'
        }
        if (typeof dl !== 'undefined' && dl) {
            kycObj.dl = true;
            if (typeof dl_unlimited !== 'undefined' && dl_unlimited) {
                kycObj.dl_unlimited = true;
            } else {
                kycObj.dl_unlimited = false;
                if (typeof dl_overall !== 'undefined' && dl_overall && dl_overall !== '') {
                    kycObj.dl_overall = dl_overall;
                }
            }
            if (typeof ddl !== 'undefined' && ddl && ddl !== '') {
                kycObj.ddl = ddl;
            }
            if (typeof mdl !== 'undefined' && mdl && mdl !== '') {
                kycObj.mdl = mdl;
            }
        } else {
            kycObj.dl = false;
        }
        if (typeof wl !== 'undefined' && wl) {
            kycObj.wl = true;
            if (typeof wl_unlimited !== 'undefined' && wl_unlimited) {
                kycObj.wl_unlimited = true;
            } else {
                kycObj.wl_unlimited = false;
                if (typeof wl_overall !== 'undefined' && wl_overall && wl_overall !== '') {
                    kycObj.wl_overall = wl_overall;
                }
            }
            if (typeof dwl !== 'undefined' && dwl && dwl !== '') {
                kycObj.dwl = dwl;
            }
            if (typeof mwl !== 'undefined' && mwl && mwl !== '') {
                kycObj.mwl = mwl;
            }
        } else {
            kycObj.wl = false;
        }
        if (typeof features !== 'undefined' && features && Object.keys(features).length > 0) {
            var feaArr = [];
            Object.keys(features).forEach(function (item) {
                feaArr.push(features[item]);
            })
            kycObj.features = feaArr;
        }
        if (typeof requirements !== 'undefined' && requirements && Object.keys(requirements).length > 0) {
            var reqArr = [];
            Object.keys(requirements).forEach(function (item) {
                reqArr.push(requirements[item]);
            })
            kycObj.requirements = reqArr;
        }
        //console.log(typeof planid, planid);
        if (typeof planid !== 'undefined' && planid && planid !== '') {

        } else {
            if (typeof order !== 'undefined' && order && order !== '') {
                kycObj.order = order;
            } else {
                errorObj.order = 'Order is required'
            }
        }
        /*
        if (typeof dfc !== 'undefined' && dfc && dfc !== '') {
            kycObj.dfc = dfc;
        } else {
            kycObj.dfc = null;
        }
        if (typeof dff !== 'undefined' && dff && dff !== '') {
            kycObj.dff = dff;
        }
        if (typeof wfc !== 'undefined' && wfc && wfc !== '') {
            kycObj.wfc = wfc;
        } else {
            kycObj.wfc = null;
        }
        if (typeof wff !== 'undefined' && wff && wff !== '') {
            kycObj.wff = wff;
        }
        */
        //console.log('kycObj', kycObj);
        if (Object.keys(errorObj).length === 0) {
            if (typeof planid !== 'undefined' && planid && planid !== '') {
                Kycplans.update({ _id: mongoose.Types.ObjectId(planid) }, kycObj).exec(function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: 'Internal Server Error' });
                    } else {
                        res.status(200).json({ message: 'Kyc Updated' })
                    }
                })
            } else {
                Kycplans.create(kycObj, function (error, rows) {
                    if (error) {
                        res.status(500).json({ message: 'Internal Server Error' });
                    } else {
                        res.status(200).json({ message: 'Kyc Updated' })
                    }
                })
            }

        } else {
            res.status(400).json(errorObj);
        }
    } catch (e) {
        res.status(500).json({ status: e.message })
    }
}
userController.getAllKYCPlans = function (req, res, next) {
    try {
        Kycplans.find().sort({ order: 1 }).exec(function (error, rows) {
            if (error) {
                res.status(500).json({ message: 'Internal Server Error' });
            } else {
                res.status(200).json(rows)
            }
        })
    } catch (e) {
        res.status(500).json({ status: e.message })
    }
}
userController.getKYCPlanById = function (req, res, next) {
    try {
        console.log(req.body);
        console.log('-------------')
        if (typeof req.body._id !== 'undefined' && req.body._id && req.body._id !== '') {
            Kycplans.findOne({ _id: mongoose.Types.ObjectId(req.body._id) }).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({ message: 'Internal Server Error' });
                } else {
                    res.status(200).json(rows)
                }
            })
        } else {
            res.status(500).json({ message: 'Plan ID is missing.............' });
        }
    } catch (e) {
        res.status(500).json({ status: e.message })
    }
}


// user related
userController.getUserCurrency = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            if (typeof userid !== 'undefined' && userid && userid !== '') {
                Users.findOne({ _id: userid }, { usercurrency: 1 }).exec(function (error, currencies) {
                    if (error) {
                        res.status(500).json({ message: error.message })
                    } else {
                        res.status(200).json(currencies);
                    }
                })
            } else {
                res.status(400).json({ message: 'Invalid User' })
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Request' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
userController.updateUserCurrency = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var currency = req.query.currency;
            if (typeof userid !== 'undefined' && userid && userid !== '' && typeof currency !== 'undefined' && currency && currency !== '') {
                Users.update({ _id: userid }, { usercurrency: currency }, function (error, uprows) {
                    if (error) {

                        res.status(500).json({ message: error.message })
                    } else {

                        res.status(200).json(uprows);
                    }
                })
            } else {
                res.status(400).json({ message: 'Invalid User' })
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Request' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
};
userController.verifyEmailPhone = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var userid = req.payload._id;
            var emailphone = req.query.str;
            var emailtest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var emailflag = false;
            var phoneflag = false;
            if (emailtest.test(String(emailphone).toLowerCase())) {
                emailflag = true
            } else {
                if (emailphone.indexOf('+') === -1) {
                    emailphone = '+' + emailphone;
                }
                const number = phoneUtil.parseAndKeepRawInput(emailphone);
                if (phoneUtil.isPossibleNumber(number) && phoneUtil.isValidNumber(number)) {
                    phoneflag = true;
                }
            }
            if (typeof userid !== 'undefined' && userid && userid !== '' && typeof emailphone !== 'undefined' && emailphone && emailphone !== '') {
                var filter = {};
                if (emailflag) {
                    filter = {
                        _id: userid,
                        email: emailphone
                    }
                }
                if (phoneflag) {
                    filter = {
                        _id: userid,
                        phone: emailphone
                    }
                }
                if (Object.keys(filter).length > 0) {
                    Users.find(filter).exec(function (error, uprows) {
                        if (error) {
                            res.status(500).json({ message: error.message })
                        } else {
                            if (typeof uprows !== 'undefined' && uprows != null && uprows.length > 0) {
                                res.status(200).json();
                            } else {

                            }
                        }
                    })
                }

            } else {
                res.status(400).json({ message: 'Email Or Phone is invalid' })
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Request' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
};


//login log api
userController.getloginlogs = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var perPage = Number(req.query.perpage ? req.query.perpage : 0);
            var page = Number(req.query.page ? req.query.page : 0);
            var userid = req.query.uid;
            if (typeof userid !== 'undefined' && userid && userid !== '') {
                if (typeof perPage !== 'undefined' && perPage !== '' && perPage > 0 && typeof page !== 'undefined' && page !== '' && page > 0) {
                    var skippage = (perPage * page) - perPage;

                    Logs.aggregate([
                        { $match: { userid: mongoose.Types.ObjectId(userid) } },
                        {
                            "$lookup": {
                                from: "actions",
                                localField: "action",
                                foreignField: "_id",
                                as: "act"
                            }
                        },
                        { $unwind: "$act" },
                        {
                            $project: {
                                "act._id": 0,
                                "act.action": 0,
                                "action": 0,
                            }
                        }
                    ]).sort({ createdAt: -1 }).skip(skippage).limit(perPage).exec(function (txerr, txdoc) {
                        if (txerr) {
                            res.status(500).json({ status: false, message: 'Internal Server Error' });
                        } else {
                            Logs.aggregate([
                                { $match: { userid: mongoose.Types.ObjectId(userid) } },
                                {
                                    "$lookup": {
                                        from: "actions",
                                        localField: "action",
                                        foreignField: "_id",
                                        as: "act"
                                    }
                                },
                                { $unwind: "$act" },
                                {
                                    $project: {
                                        "act._id": 0,
                                        "act.action": 0,
                                        "action": 0,
                                    }
                                }
                            ]).exec(function (err, count) {
                                if (err) {
                                    res.status(500).json({ status: false, message: 'Internal Server Error' });
                                } else {
                                    var returnJson = {
                                        status: true,
                                        logs: txdoc,
                                        current: page,
                                        pages: Math.ceil(count.length / perPage)
                                    }
                                    res.status(200).json(returnJson);
                                }
                            });

                        }
                    });
                }
            } else {
                res.status(400).json({ message: 'Invalid User' })
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Request' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}


//new device call

userController.sendNewDeviceAlert = function (req, res, next) {
    try {
        var email = req.query.str;
        if (typeof email !== 'undefined' && email && email !== '') {
            var timestamp = Date.now();
            var sendGrid = new Sendgrid();
            var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            var verificationObj = {
                'email': email,
                'ip': ip,
                'timestamp': timestamp
            }
            var encrptStr = crypt.encrypt(JSON.stringify(verificationObj));
            var host = req.headers.origin + '/verifydevice/';
            /*if (typeof process.env.NODE_ENV !== 'undefined' && process.env.NODE_ENV == 'production') {
                host = process.env.CLIENTURL + '/verifydevice/';
            } else {
                host = localURL + '/verifydevice/';
            }*/
            const options = {
                LINK: host + encrptStr
            }
            console.log('sendNewDeviceAlert', options)
            /*const options = {
                //'LINK': env.process.SERVERURL + 'verifytr/' + encrptStr
                LINK: process.env.CLIENTURL + '/verifydevice/' + encrptStr
                //LINK: "http://blockoville.com" + '/verifydevice/' + encrptStr,
            }*/

            sendGrid.sendEmail(
                email,
                'New Device Verification',
                "views/emailtemplate/newdevice.ejs",
                options
            );
            res.status(200).json({
                message: "Email Sent. Please confirm your device",
            })
        } else {
            res.status(400).json({ message: 'Invalid Details' })
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}
userController.addNewDevice = function (req, res, next) {
    try {
        var data = JSON.parse(crypt.decrypt(req.query.str));
        var email = data.email;
        var ip = data.ip;
        var authkey = data.timestamp;
        if (typeof ip !== 'undefined' && ip !== '' &&
            typeof authkey !== 'undefined' && authkey !== '' &&
            typeof email !== 'undefined' && email !== ''
        ) {
            Users.findOne({ email: email }).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                } else {
                    if (rows == null) {
                        res.status(400).json({ message: "No Account Found" });
                    } else {
                        var currentTmeStmp = Date.now();
                        var OTPAge = (currentTmeStmp - authkey) / 100;
                        if (OTPAge > 900000) {
                            res.status(400).json({ message: "Link Expired" });
                        } else {

                            Actions.findOne({ action: 'new_device_added' }).exec(function (error, action) {
                                if (error) {
                                    res.status(500).json({ message: "Internal Server Error" })
                                } else {
                                    var ipAdd = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                                    if (action !== null) {
                                        if (ip === ipAdd) {

                                            var agentinfo = req.headers['user-agent'];
                                            const detector = new DeviceDetector;
                                            const result = detector.detect(agentinfo);
                                            const DEVICE_TYPE = require('node-device-detector/parser/const/device-type');
                                            const isTabled = result.device && [DEVICE_TYPE.TABLET].indexOf(result.device.type) !== -1;
                                            const isMobile = result.device && [DEVICE_TYPE.SMARTPHONE, DEVICE_TYPE.FEATURE_PHONE].indexOf(result.device.type) !== -1;
                                            const isPhablet = result.device && [DEVICE_TYPE.PHABLET].indexOf(result.device.type) !== -1;
                                            const isIOS = result.os && result.os.family === 'iOS';
                                            const isAndroid = result.os && result.os.family === 'Android';
                                            const isDesktop = !isTabled && !isMobile && !isPhablet;
                                            var device = {
                                                id: '',
                                                type: '',
                                                brand: '',
                                                model: ''
                                            }
                                            if (isDesktop) {
                                                const result = detector.parseOs(agentinfo);
                                                device.type = 'Desktop';
                                                device.model = result.name + ', ' + result.version + ', ' + result.platform
                                            } else {
                                                var type = result.device.type
                                                device.type = type.charAt(0).toUpperCase() + type.slice(1);
                                                device.model = result.device.brand + ', ' + result.device.model;
                                            }
                                            var agentparse = useragent.parse(agentinfo);
                                            var agentStr = agentparse.toAgent();
                                            var os = agentparse.os.toString();
                                            var agent = agentStr + ' ' + os;

                                            var logData = {
                                                userid: rows._id,
                                                ipaddress: ip,
                                                agent: agent,
                                                description: agentinfo,
                                                action: action._id,
                                                type: device.type,
                                                model: device.model,
                                                timestamp: Date.now()
                                            }
                                            Logs.create(logData, function (error, added) {
                                                if (error) {
                                                    res.status(500).json({ message: error.message })
                                                } else {
                                                    res.status(200).json({ message: 'Device has been verified.' });
                                                }
                                            })
                                        } else {
                                            res.status(400).json({ message: "Invalid Link" });
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
        else {
            res.status(400).json({ message: 'Link is Invalid' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

// extra functions
userController.sendOTP = function (userid, phone, callback) {
    try {
        if (typeof userid !== 'undefined' && userid && userid !== '' && typeof phone !== 'undefined' && phone && phone !== '') {
            var timestamp = Date.now();
            var otpcodeData = {};
            otpcodeData.userid = userid;
            otpcodeData.code = otp();
            otpcodeData.timestamp = timestamp;
            console.log('phone', phone)
            smsClient.messages.create({
                body: 'Your Blockoville authentication otp is: ' + otpcodeData.code,
                to: phone,
                from: process.env.TWILLO_PHONE_NUMBER,
            }).then((data) => {
                console.log('data', data);
                Otp.create(otpcodeData, function (error, rows) {
                    if (error) {
                        callback({ error: true, status: 500, message: 'Internal Server Error' })
                    } else {
                        callback({ error: false, status: 200, message: timestamp })

                    }
                })
            }).catch((err) => {
                console.log('err', err)
                callback({ error: true, status: 500, message: 'Internal Server Error' })
            });
        } else {
            callback({ error: true, status: 400, message: 'Invalid Details' })
        }
    } catch (e) {
        callback({ error: true, status: 500, message: 'Internal Server Error' })
    }
}
userController.verifyOTP = function (req, res, next) {
    try {
        var errorObj = {};
        var otpcodeData = {};

        var user_id = '5b878993ed8061543c41f9a3';
        var code = req.body.code;
        var authkey = req.body.authkey;

        if (typeof user_id !== 'undefined' && user_id && user_id !== '') {
            otpcodeData.userid = user_id;
        } else {
            errorObj.user = 'User Invalid';
        }
        if (typeof code !== 'undefined' && code && code !== '') {
            otpcodeData.code = code;
        } else {
            errorObj.code = 'Code is required';
        }
        if (typeof authkey !== 'undefined' && authkey && authkey !== '') {
            otpcodeData.authkey = authkey;
        } else {
            errorObj.authkey = 'Auth Key is required';
        }

        if (Object.keys(errorObj).length === 0) {
            Otp.findOne({
                userid: otpcodeData.userid,
                timestamp: otpcodeData.authkey,
                code: otpcodeData.code
            }).sort({ createdAt: -1 }).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                } else {
                    if (rows === null) {
                        res.status(400).json({ message: "Invalid OTP" })
                    } else {
                        var currentTmeStmp = Date.now();
                        var OTPAge = (currentTmeStmp - otpcodeData.authkey) / 100;
                        if (OTPAge > 900000) {
                            res.status(400).json({ message: "OTP Expired" });
                        } else {
                            Users.update({ _id: otpcodeData.userid }, { phone_verified: 1 }).exec(function (error, uprows) {
                                if (error) {
                                    res.status(500).json({ message: "Internal Server Error" })
                                } else {
                                    res.status(200).json({ message: "OTP Verified" })
                                }
                            })
                        }
                    }
                }
            })
        } else {
            res.status(400).json({ message: errorObj })
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}
userController.sendConfirmationEmail = function (req, res, next) {
    try {
        var errorObj = {};
        var otpcodeData = {};
        var user_id = '5b878993ed8061543c41f9a3';
        var email = 'pawan@lapits.com'
        if (typeof user_id !== 'undefined' && user_id && user_id !== '') {
            otpcodeData.userid = user_id;
        } else {
            errorObj.email = 'Invalid User';
        }
        if (typeof email !== 'undefined' && email && email !== '') {
        } else {
            errorObj.phone = 'Invalid Email';
        }

        if (Object.keys(errorObj).length === 0) {

            var timestamp = Date.now();
            otpcodeData.code = uniqid();
            otpcodeData.timestamp = timestamp;
            var sendGrid = new Sendgrid();
            var options = { email: email, code: otpcodeData.code, timestamp: timestamp };
            sendGrid.sendEmail(
                email,
                'Welcome to Blockoville',
                "views/keyemail.ejs",
                options
            );
            Otp.create(otpcodeData, function (error, rows) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                } else {
                    res.status(200).json({

                        message: "A verification email has been send. Please verify your email.",
                        authkey: timestamp
                    })
                }
            })
        } else {
            res.status(400).json({ message: errorObj })
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}
userController.verifyEmail = function (req, res, next) {
    try {
        var errorObj = {};
        var otpcodeData = {};

        var user_id = '5b878993ed8061543c41f9a3';
        var code = req.body.code;
        var authkey = req.body.authkey;

        if (typeof user_id !== 'undefined' && user_id && user_id !== '') {
            otpcodeData.userid = user_id;
        } else {
            errorObj.email = 'User Invalid';
        }
        if (typeof code !== 'undefined' && code && code !== '') {
            otpcodeData.code = code;
        } else {
            errorObj.code = 'Code is required';
        }
        if (typeof authkey !== 'undefined' && authkey && authkey !== '') {
            otpcodeData.authkey = authkey;
        } else {
            errorObj.authkey = 'Auth Key is required';
        }

        if (Object.keys(errorObj).length === 0) {
            Otp.findOne({
                userid: otpcodeData.userid,
                timestamp: otpcodeData.authkey,
                code: otpcodeData.code
            }).sort({ createdAt: -1 }).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" })
                } else {
                    if (rows === null) {
                        res.status(400).json({ message: "Invalid Code" })
                    } else {
                        var currentTmeStmp = Date.now();
                        var OTPAge = (currentTmeStmp - otpcodeData.authkey) / 100;
                        if (OTPAge > 900000) {
                            res.status(400).json({ message: "Code Expired" });
                        } else {
                            Users.update({ _id: otpcodeData.userid }, { email_verified: 1 }).exec(function (error, uprows) {
                                if (error) {
                                    res.status(500).json({ message: "Internal Server Error" })
                                } else {
                                    res.status(200).json({ message: "Email Verified" })
                                }
                            })
                        }
                    }
                }
            })
        } else {
            res.status(400).json({ message: errorObj })
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}
userController.getVerifyTwofa = function (req, res, next) {
    var csrf = '';
    res.render('twofa', { title: 'Two Factor Authentication', _csrf: csrf });
}

userController.saveSecurity = function (req, res, next) {
    var securityObj = [];
    var options = [];
    var errorObj = {};
    var user_id = '5b878993ed8061543c41f9a3';
    if (typeof req.body.sms !== 'undefined' && req.body.sms && req.body.sms !== '' && typeof req.body.sms_order !== 'undefined' && req.body.sms_order && req.body.sms_order !== '') {
        securityObj.push({ userid: user_id, option: 'sms', order: Number(req.body.sms_order) });
        options.push('sms');
    }
    if (typeof req.body.email !== 'undefined' && req.body.email && req.body.email !== '' && typeof req.body.email_order !== 'undefined' && req.body.email_order && req.body.email_order !== '') {
        securityObj.push({ userid: user_id, option: 'email', order: Number(req.body.email_order) });
        options.push('email');
    }
    if (typeof req.body.twofa !== 'undefined' && req.body.twofa && req.body.twofa !== '' && typeof req.body.twofa_order !== 'undefined' && req.body.twofa_order && req.body.twofa_order !== '') {
        securityObj.push({ userid: user_id, option: 'twofa', order: Number(req.body.twofa_order) });
        options.push('twofa');
    }
    Users.findOne({ _id: user_id }).exec(function (error, profile) {
        if (error) {
            res.status(500).json({ message: "Internal Server Error" })
        } else {
            if (typeof profile !== 'undefined' && profile && profile !== null) {
                if (options.indexOf('sms') !== -1) {
                    if (profile.phone_verified === 0) {
                        errorObj.sms = 'Phone is not verified';
                    }
                }
                if (options.indexOf('email') !== -1) {
                    if (profile.email_verified === 0) {
                        errorObj.email = 'Email is not verified';
                    }
                }

                if (options.indexOf('twofa') !== -1) {
                    var code = req.body.code;
                    var secret = req.body.secret;
                    if (typeof code === 'undefined' || code === '' || code === null) {
                        errorObj.code = 'Two Factor code is required';
                    }
                    if (typeof secret === 'undefined' || secret === null || secret === '') {
                        errorObj.secret = 'Two Factor secret is required';
                    }
                }
                if (Object.keys(errorObj).length === 0) {
                    if (options.indexOf('twofa') !== -1) {
                        var method = new Function(req);
                        method.googleAuthenticator(res, function (ga) {
                            if (ga.verifyCode(secret, code)) {
                                Users.update({ _id: user_id }, { secret: secret }, {
                                    upsert: true,
                                    setDefaultsOnInsert: true
                                }, function (err) {
                                    if (err) {
                                        res.status(500).json({ message: "Internal Server Error" })
                                    } else {
                                        Security.create(securityObj, function (err, updated) {
                                            if (err) {
                                                res.status(500).json({ message: err })
                                            } else {
                                                res.status(200).json({ message: 'Updated Successfully' })
                                            }
                                        }
                                        );
                                    }
                                });
                            } else {
                                res.status(500).json({ message: "Invalid Code" })
                            }
                        })
                    } else {
                        Security.create(securityObj, function (err, updated) {
                            if (err) {
                                res.status(500).json({ message: err })
                            } else {
                                res.status(200).json({ message: 'Updated Successfully' })
                            }
                        }
                        );
                    }
                } else {
                    res.status(400).json({ message: errorObj })
                }
            }
        }
    })

}

// generatSecretId
userController.generatekey = function (req, res, next) {
    try {
        //console.log(req.body);
        //return false;
        var userid = req.payload._id;
        var label = req.body.label;
        var endpoints = req.body.endpoints;
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            if (typeof label !== 'undefined' && label && label !== '' && typeof endpoints !== 'undefined' && endpoints) {
                var timestamp = Date.now();
                var str = userid + timestamp;
                bcryptJS.hash(str, 10, function (err, hash) {
                    if (err) {
                        res.status(500).json({ message: err.message })
                    } else {
                        var keyObj = {
                            userid: userid,
                            label: label,
                            key: hash,
                        }
                        UserApiKeys.create(keyObj, function (error, uprows) {
                            if (error) {
                                res.status(500).json({ message: error.message })
                            } else {
                                var endpointkeys = Object.keys(endpoints);
                                if (endpointkeys.length > 0) {
                                    var endPtArr = [];
                                    endpointkeys.forEach(function (item) {
                                        endPtArr.push({
                                            userid: mongoose.Types.ObjectId(userid),
                                            keyid: uprows._id,
                                            endpointid: mongoose.Types.ObjectId(item),
                                            access: endpoints[item]
                                        })
                                    })
                                    if (endPtArr.length > 0) {
                                        ApiendpointAccess.create(endPtArr, function (error, uprows) {
                                            res.status(200).json({ message: 'API Key generated Successfully' })
                                        })
                                    } else {
                                        res.status(200).json({ message: 'API Key generated Successfully' })
                                    }
                                }
                            }
                        })

                    }
                })
            } else {
                res.status(400).json({ message: "Bad Request" });
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}
userController.updateGenerateKey = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var label = req.body.label;
        var endpoints = req.body.endpoints;
        var apikeyid = req.body._id;
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            if (typeof label !== 'undefined' && label && label !== '' && typeof endpoints !== 'undefined' && endpoints) {
                var keyObj = {
                    label: label,
                }
                UserApiKeys.updateOne({ _id: mongoose.Types.ObjectId(apikeyid) }, keyObj).exec(function (error, uprows) {
                    if (error) {
                        res.status(500).json({ message: error.message })
                    } else {
                        ApiendpointAccess.remove({
                            userid: mongoose.Types.ObjectId(userid),
                            keyid: mongoose.Types.ObjectId(apikeyid)
                        }).exec(function (error, delrows) {
                            if (!error && delrows) {
                                var endpointkeys = Object.keys(endpoints);
                                if (endpointkeys.length > 0) {
                                    var endPtArr = [];
                                    endpointkeys.forEach(function (item) {
                                        endPtArr.push({
                                            userid: mongoose.Types.ObjectId(userid),
                                            keyid: apikeyid,
                                            endpointid: mongoose.Types.ObjectId(item),
                                            access: endpoints[item]
                                        })
                                    })
                                    if (endPtArr.length > 0) {
                                        ApiendpointAccess.create(endPtArr, function (error, uprows) {
                                            res.status(200).json({ message: 'API key generated successfully' })
                                        })
                                    } else {
                                        res.status(200).json({ message: 'API key generated successfully' })
                                    }
                                } else {
                                    res.status(200).json({ message: 'API key generated successfully' })
                                }
                            }
                        })
                    }
                })
            } else {
                res.status(400).json({ message: "Bad Request" });
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}

// getGenerateKey
userController.getUserApiKeys = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var errorObj = {};
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            UserApiKeys.find({ userid: mongoose.Types.ObjectId(userid) }, function (error, apikeys) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    Apiendpoints.find({ status: 'active' }).exec(function (error, endpointlist) {
                        ApiendpointAccess.find({ userid: mongoose.Types.ObjectId(userid) }).exec(function (error, accesslist) {
                            var accessObj = {};
                            if (!error && accesslist.length > 0) {
                                accesslist.forEach(function (item) {
                                    accessObj[item.keyid + '_' + item.endpointid] = item;
                                })
                            }
                            //console.log(apikeys,endpointlist,accessObj);
                            res.status(200).json({
                                'apikey': apikeys,
                                'endpoints': endpointlist,
                                'access': accessObj
                            });
                        })
                    })

                }
            })
        } else {
            errorObj.userid = 'Userid is required';
            res.status(400).json({ message: errorObj });
        }

    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}
//delete api key
userController.deleteUserAPIkey = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var keyid = req.query.keyid;
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            if (typeof keyid !== 'undefined' && keyid && keyid !== '') {
                ApiendpointAccess.remove({
                    userid: mongoose.Types.ObjectId(userid),
                    keyid: mongoose.Types.ObjectId(keyid)
                }).exec(function (error, delrows) {
                    if (!error) {
                        UserApiKeys.remove({
                            userid: mongoose.Types.ObjectId(userid),
                            _id: mongoose.Types.ObjectId(keyid)
                        }).exec(function (error, keydelrows) {
                            if (error) {
                                res.status(500).json({ message: error.message });
                            } else {
                                res.status(200).json({ message: 'API Key Deleted Successfully' });
                            }
                        })
                    } else {
                        res.status(500).json({ message: error.message });
                    }
                })
            } else {
                res.status(500).json({ message: 'Invalid API Key' });
            }
        } else {
            res.status(401).json({ message: 'Unauthorized Access' });
        }

    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

userController.authentication = function (req, res, next) {
    try {
        var id = req.query.key;
        var expiry = new Date();
        expiry.setMinutes(expiry.getMinutes() + 7);
        var errorObj = {};
        if (typeof id !== 'undefined' && id && id !== '') {
            UserApiKeys.findOne({ key: id, status: 'active' }).exec(function (error, apikey) {
                if (!error && apikey !== null) {
                    Users.findOne({ _id: mongoose.Types.ObjectId(apikey.userid) }).exec(function (error, userdetails) {
                        if (!error && userdetails !== null) {
                            userdetails['apikey'] = apikey._id;
                            var token = userController.generateJwtApiKey(userdetails);
                            var authenticate = {
                                token: token,
                                exp: parseInt(expiry.getTime() / 1000),
                            }
                            res.status(200).json(authenticate);
                        } else {
                            res.status(401).json({ message: 'Unauthorized Access' });
                        }
                    })
                } else {
                    res.status(401).json({ message: 'Unauthorized Access' });
                }
            })
        } else {
            errorObj.key = "Key is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}
/*
userController.sendOTP = function (req, res, next) {
    try {
        var errorObj = {};
        var otpcodeData = {};
        var user_id = '5b878993ed8061543c41f9a3';
        var phone = req.body.phone;
        if (typeof user_id !== 'undefined' && user_id && user_id !== '') {
            otpcodeData.userid = user_id;
        } else {
            errorObj.email = 'Invalid User';
        }
        if (typeof phone !== 'undefined' && phone && phone !== '') {
            const number = phoneUtil.parseAndKeepRawInput(phone);
            if (phoneUtil.isPossibleNumber(number) && phoneUtil.isValidNumber(number)) {

            } else {
                errorObj.phone = 'Phone is Invalid';
            }
        } else {
            errorObj.phone = 'Phone is required or Invalid';
        }

        if (Object.keys(errorObj).length === 0) {
            var timestamp = Date.now();
            otpcodeData.code = otp();
            otpcodeData.timestamp = timestamp;
            Otp.create(otpcodeData, function (error, rows) {
                if (error) {
                    res.status(500).json({message: "Internal Server Error"})
                } else {
                    res.status(200).json({

                        message: 'An otp has been send.Please verify your phone.',
                        authkey: timestamp
                    })
                }
            })

            /*var key = otp();
            smsClient.api.messages
                .create({
                    body: 'Your XTRemCoin authentication key is: ' + key,
                    to: profileObj.phone,
                    from: '+17752047475',
                }).then(function (data) {
                var otpcodeData = {
                    email: profileObj.email,
                    code: key,
                    action: "signupkeyapi",
                }
                console.log(otpcodeData)
                Otpcode.update({
                        email: profileObj.email,
                        action: "signupkeyapi"
                    }, otpcodeData, {
                        upsert: true, setDefaultsOnInsert: true
                    },
                    function (err, updated) {
                        if (err) {
                            errorObj.error = "Internal Server Error";
                            res.status(500).json({ message: errorObj})
                        }
                    }
                );
            }).catch(function (err) {
                res.status(500).json({ message: "Internal Server Error"})
            });
            res.status(200).json({

                message: 'An otp has been send.Please verify your phone.'
            })*/

/*} else {
    res.status(400).json({message: errorObj})
}
} catch (e) {
res.status(500).json({message: "Internal Server Error"})
}
}*/

// forum query updated

userController.generateReferralCode = function (req, res, next) {
    try {
        var userid = req.payload._id;
        if (userid !== 'undefined' && userid && userid != '') {
            Referral.findOne({ userid: mongoose.Types.ObjectId(userid) }).exec(function (err, result) {
                if (err) {
                    res.status(500).json({ message: err.message })
                }
                else {
                    if (result !== null && result.code) {
                        res.status(200).json({ code: result.code })
                    }
                    else {
                        var code = randomstring.generate({
                            length: 20,
                            charset: 'alphanumeric'
                        });
                        Referral.create({ code: code, userid: userid }, function (error, result) {
                            if (error) {
                                res.status(500).json({ message: error.message })
                            }
                            else {
                                res.status(200).json({ code: result.code })
                            }
                        })
                    }
                }
            })
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    }
    catch (e) {
        res.status(400).json({ message: e.message })
    }
}
userController.KycDetailStatus = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var userkyc = {};
        if (typeof userid !== 'undefined' && userid && userid != '') {
            Users.aggregate([
                {
                    "$lookup": {
                        from: "kycs",
                        localField: "_id",
                        foreignField: "userid",
                        as: "kycs"
                    }
                },
                { $unwind: "$kycs" },
                {
                    $match: {
                        _id: mongoose.Types.ObjectId(userid)
                    }
                }
            ]).exec(function (err, result) {
                if (err) {
                    res.status(500).json({ message: err.message })
                }
                else {
                    if (result.length > 0) {
                        var email = result[0].email;
                        var idproofdoc = result[0].kycs.idproofdoc;
                        var addressproofdoc = result[0].kycs.addressproofdoc;
                        var status = result[0].kycs.status;
                        if (email) {
                            userkyc.level1 = true;
                        } else {
                            userkyc.level1 = false
                        }
                        if (idproofdoc) {
                            userkyc.level2 = true;
                        } else {
                            userkyc.level2 = false
                        }
                        if (addressproofdoc) {
                            userkyc.level3 = true;
                        } else {
                            userkyc.level3 = false
                        }
                        if (status === 'Approve') {
                            userkyc.level4 = true;
                        } else {
                            userkyc.level4 = false
                        }
                        res.status(200).json(userkyc)
                    }
                    else {
                        Users.findOne({ _id: userid }, function (error, resultdata) {
                            if (error) {
                                res.status(500).json({ message: error.message })
                            }
                            else {
                                var email = resultdata.email;
                                if (email) {
                                    userkyc.level1 = true;
                                } else {
                                    userkyc.level1 = false
                                }
                                res.status(200).json(userkyc)
                            }
                        })

                    }
                }
            })
        }
        else {
            res.status(400).json({ message: "unauthorized access" });
        }
    }
    catch (e) {
        res.status(400)
    }
}
userController.generateJwtApiKey = function (valobj) {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);
    //expiry.setMinutes(expiry.getMinutes()+5);
    return jwt.sign({
        _id: valobj._id,
        email: valobj.email,
        name: valobj.name,
        phone: valobj.phone,
        userid: valobj.userid,
        group: valobj.group,
        apikey: valobj.apikey,
        exp: parseInt(expiry.getTime() / 1000),
    }, "MY_SECRET");//process.env.JWT_TOKEN_SECRET); // DO NOT KEEP YOUR SECRET IN THE CODE!
}

userController.getUserBankRefCode = function (req, res, next) {
    try {
        var userid = req.payload._id;
        if (userid !== 'undefined' && userid && userid != '') {
            Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, { bankrefcode: 1 }).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({ message: error.message })
                } else {
                    res.status(200).json(rows);
                }
            })
        } else {
            res.status(401).json({ message: 'Unauthorized Access' })
        }
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

var ctrl = module.exports = userController
