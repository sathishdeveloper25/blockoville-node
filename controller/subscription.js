// Npm-modules
var mongoose = require('mongoose');
var multer = require('multer');
var restAPiCtrl = require('../controller/restApiController');
var fs = require('fs');
var shell = require('shelljs');
var pathmodule = require('path');
const ImageSupported = ['.png', '.jpg', '.jpeg', '.gif', '.PNG', '.JPG', '.JPEG', '.GIF'];
const VideoSupported = ['.3g2', '.3gp', '.3gpp', '.asf', '.avi ', '.dat', '.divx', '.dv', '.f4v', '.flv', '.m2ts', '.m4v', '.mkv',
    '.mod', '.mov', '.mp4', '.mpe', '.mpeg ', '.mpeg4', '.mpg', '.mts', '.nsv', '.ogm', '.ogv ', '.qt', '.tod', '.ts', '.vob', '.wmv'];
const fileattach = ['.pdf', '.txt', '.doc'];
var subscriptionController = [];

// Models
const GroupModel = require('../model/groupByUsers');
const PostModel = require('../model/userPosts');
const likes = require('../model/post_like');
const content = require('../model/post_medias');
const mypurchase = require('../model/mypurchase');
const GroupSubscriptionModel = require('../model/subscription');
const GroupInvitationModel = require("../model/groupInvitationSchema");
const UserModel = require("../model/user");
const FollowerModel = require("../model/following");
const BlockedUserModel = require("../model/blockUserFromGroup");
const GroupJoinRequestsModel = require("../model/groupJoinRequest");
const PostController = require("./postController");
const UserController = require("./userController");

// Multer storage for single image
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        // var dir = process.env.KYCDOCURL + req.payload._id
        var dir = "./public/uploads/userpost/imageUpload";
        if (!fs.existsSync(dir)) {
            shell.mkdir('-p', dir);
        }
        cb(null, "./public/uploads/userpost/imageUpload"); // where to store it
    },
    filename: function (req, file, cb) {
        if (file.fileSize) {
            var err = new Error();
            return cb(err);
        }
        else if (!file.originalname.toLowerCase().match(/\.(png|jpg|jpeg)$/)) {
            var err = new Error();
            err.code = 'fieltype'; // to check on file type
            return cb(err, null);
        } else {
            var date = new Date();
            var timeStamp = date.getTime();
            var name = file.originalname.toLowerCase();
            var ext = name.substr(file.originalname.lastIndexOf('.') + 1)
            var rename = timeStamp + "." + ext;
            cb(null, rename);
        }
    }
});
var upload = multer({
    storage: storage,
    limits: { fileSize: 40000000 }
}).any()

// Multer storage for multilpe image
var storages = multer.diskStorage({
    destination: function (req, file, cb) {
        var dir = "./public/uploads/userpost/imageUpload";
        if (!fs.existsSync(dir)) {
            shell.mkdir('-p', dir);
        }
        cb(null, "./public/uploads/userpost/imageUpload"); // where to store it
    },
    filename: function (req, file, cb) {
        if (file.fileSize) {
            var err = new Error();
            return cb(err);
        }
        else if (!file.originalname.toLowerCase().match(/\.(png|jpg|jpeg|gif|mp4|webm|ogg|pdf|doc|txt)$/)) {
            var err = new Error();
            err.code = 'fieltype'; // to check on file type
            return cb(err, null);
        } else {
            var date = new Date();
            var timeStamp = date.getTime();
            var name = file.originalname.toLowerCase();
            var ext = name.substr(file.originalname.lastIndexOf('.') + 1)
            var rename = timeStamp + "." + ext;
            cb(null, rename);
        }
    }
});
var uploads = multer({
    storage: storages,
    limits: { fileSize: 20971520 }
}).any()
var storage1 = multer.diskStorage({});
var upload1 = multer({}).any();

var ioSocket = {};

subscriptionController.loadSocketIO = function (socketIO) {
    if (socketIO) {
        iosocket = socketIO;
        // console.log("SUBSCRIPTION iosocket-->",iosocket);
    }
};


/********************************Group Work**************************************/
// Get currency
subscriptionController.getcurrency = function (req, res, next) {
    var tokens = req.headers.authorization;
    restAPiCtrl.performRequest('wallet', 'getcurrencylist', 'get', tokens, null, function (result) {
        if (result.status === 200) {
            res.status(200).json(result.message);
        } else {
            res.status(result.status).json({ message: result.message });
        }
    });
}
// Post Subscription
subscriptionController.Postsubscription = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            upload(req, res, function (err) {
                if (err) {
                    throw err;
                }
                else {
                    var userid = req.payload._id;
                    var name = req.body.name;
                    var Gtype = req.body.Gtype;
                    var fileArr = req.files;
                    var privacy = req.body.privacy;
                    var adminApprovalForJoiningGroup = req.body.adminapproval;
                    var membersCanInvite = req.body.memberinvitation;
                    var subscription = req.body.subscription;
                    var currency = req.body.currency;
                    var amount = req.body.amount;
                    var wallet = req.body.wallet;
                    var duration = req.body.duration;
                    var trial = req.body.trial;
                    var trialDuration = req.body.trialDuration;
                    var status = 'active'
                    var errorObj = {};
                    var profileObj = {};

                    if (typeof userid !== 'undefined' && userid && userid !== '' &&
                        typeof name !== 'undefined' && name && name !== '' &&
                        typeof Gtype !== 'undefined' && Gtype && Gtype !== '' &&
                        typeof status !== 'undefined' && status && status !== '' &&
                        typeof privacy !== 'undefined' && privacy && privacy !== '' &&
                        typeof membersCanInvite !== 'undefined' && membersCanInvite && membersCanInvite !== '' &&
                        typeof adminApprovalForJoiningGroup !== 'undefined' && adminApprovalForJoiningGroup && adminApprovalForJoiningGroup !== ''
                    ) {

                        profileObj.userid = userid;
                        profileObj.name = name;
                        profileObj.Gtype = Gtype;
                        profileObj.status = status;
                        profileObj.privacy = privacy;

                        if (privacy == 'public') {
                            profileObj.adminApprovalForJoiningGroup = false;
                            profileObj.membersCanInvite = true;
                        }
                        if (privacy == 'private') {
                            profileObj.adminApprovalForJoiningGroup = adminApprovalForJoiningGroup;
                            profileObj.membersCanInvite = membersCanInvite;
                        }
                        if (privacy == 'secret') {
                            profileObj.adminApprovalForJoiningGroup = true;
                            profileObj.membersCanInvite = false;
                        }

                        if (typeof subscription !== 'undefined' && subscription && subscription !== 'null' &&
                            typeof currency !== 'undefined' && currency && currency !== 'null' &&
                            typeof amount !== 'undefined' && amount && amount !== 'null' &&
                            typeof duration !== 'undefined' && duration !== 'null') {

                            profileObj.subscription = subscription;
                            profileObj.currency = currency;
                            profileObj.amount = amount;
                            profileObj.duration = duration;
                        }

                        if (typeof wallet !== 'undefined' && wallet && wallet !== 'null') {
                            profileObj.wallet = wallet;
                        }

                        if (typeof trialDuration !== 'undefined' && trialDuration && trialDuration !== 'null' &&
                            typeof trial !== 'undefined' && trial && trial !== 'null') {
                            profileObj.trialDuration = trialDuration;
                            profileObj.trial = trial
                        }

                        if (fileArr.length > 0) {
                            var contentObj = [];
                            fileArr.forEach(function (file) {
                                var path = file.path.replace("public", '').substr(1);
                                contentObj.push({
                                    groupImage: path,
                                });
                            });
                        }
                        if (contentObj.length > 0) {
                            profileObj['groupImage'] = contentObj[0].groupImage;
                        }
                        if (profileObj.subscription == undefined) {
                            profileObj.type = "free";
                        } else {
                            profileObj.type = "paid";
                        }
                        GroupModel.create(profileObj, function (error, subscriptionobj) {
                            if (error) {
                                res.status(500).json({ message: error });
                            } else {
                                res.status(200).json(subscriptionobj);
                            }
                        });
                    }
                    else {
                        errorObj.userid = 'userid is required';
                        errorObj.name = 'Name is required';
                        errorObj.Gtype = "Group type is required";
                        errorObj.groupImage = "Image is required";
                        errorObj.privacy = "Privacy is required";
                        errorObj.membersCanInvite = "Invitation is required";
                        errorObj.adminApprovalForJoiningGroup = "Admin approval is required";
                        errorObj.subscription = "Subscription is required";
                        errorObj.currency = 'Currency is required';
                        errorObj.amount = 'Amount is required';
                        errorObj.wallet = 'Wallet is required';
                        errorObj.duration = "Duration is required";
                        errorObj.trial = "Trial is required";
                        errorObj.trialDuration = "TrialDuration is required";
                        res.status(400).json({ message: errorObj });
                    }
                }
            })
        }
    } catch (e) {
        res.status(500).json({ message: 'Something went wrong' })
    }
}
// Get Subscription
subscriptionController.getSubscription = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var errorObj = {};
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            GroupModel.find({ userid: userid }, function (error, subscriptionobj) {
                if (error) {
                    res.status(500).json({ message: "Something went wrong" })
                } else {
                    res.status(200).json(subscriptionobj);
                }
            });
        } else {
            errorObj.userid = 'userid is required';
            res.status(400).json({ message: errorObj });
        }

    } catch (e) {
        res.status(500).json({ message: "Something went wrong" })
    }
}
// deleteData
subscriptionController.deletesubscribe = function (req, res, next) {
    try {
        var id = req.params.id;
        var userid = req.payload._id;
        var errorObj = {};
        if (typeof id !== 'undefined' && id && id !== '') {

            GroupModel.findOne({ _id: id }).then((foundgroup) => {
                if (foundgroup.userid == userid) {
                    // console.log("ADMIN");
                    GroupModel.remove({ _id: mongoose.Types.ObjectId(id) }, function (error, dropSubscription) {
                        if (error) {
                            res.status(500).json({ message: error });
                        } else {
                            res.status(200).json(dropSubscription)
                        }
                    });
                } else {
                    // console.log("NON ADMIN");
                    res.status(400).json({ message: "Only admin can update group info" });
                }
            }).catch((err) => {
                console.log("ERROR IN DELETE SUBSCRIBE 1", err);
                res.status(500).json({ message: "Something went wrong!!!" });
            })
        } else {
            errorObj.id = 'Id is required';
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong" })
    }
}
// Update Subsctiption
subscriptionController.getDetails = function (req, res, next) {
    try {
        var id = req.params.id;
        var errorObj = {};
        if (typeof id !== 'undefined' && id && id !== '') {
            GroupModel.findOne({ _id: mongoose.Types.ObjectId(id) }, function (error, getDetails) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json({ getDetails });
                }
            })
        } else {
            errorObj.id = 'Id is required';
            res.status(400).json({ message: errorObj });
        }
    }
    catch (e) {
        res.status(500).json({ message: "Something went wrong" })
    }
}

subscriptionController.updateSubscription = function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            upload(req, res, function (err) {
                if (err) {
                    throw err;
                }
                var id = req.params.id
                var userid = req.payload._id;
                var name = req.body.name;
                var Gtype = req.body.Gtype;
                var fileArr = req.files;
                var privacy = req.body.privacy;
                var adminApprovalForJoiningGroup = req.body.adminapproval;
                var membersCanInvite = req.body.memberinvitation;
                var status = 'active'
                var subscription = req.body.subscription;
                var currency = req.body.currency;
                var wallet = req.body.wallet;
                var amount = req.body.amount;
                var duration = req.body.duration;
                var trial = req.body.trial;
                var trialDuration = req.body.trialDuration;
                var errorObj = {};
                var profileObj = {};

                GroupModel.findOne({ _id: mongoose.Types.ObjectId(id) }).then((foundgroup) => {
                    if (foundgroup.userid == userid) {
                        if (typeof id !== 'undefined' && id && id !== '' &&
                            typeof name !== 'undefined' && name && name !== '' &&
                            typeof Gtype !== 'undefined' && Gtype && Gtype !== '' &&
                            typeof privacy !== 'undefined' && privacy && privacy !== '' &&
                            typeof membersCanInvite !== 'undefined' && membersCanInvite && membersCanInvite !== '' &&
                            typeof adminApprovalForJoiningGroup !== 'undefined' && adminApprovalForJoiningGroup && adminApprovalForJoiningGroup !== '' &&
                            typeof status !== 'undefined' && status && status !== '') {

                            profileObj.id = id;
                            profileObj.userid = userid;
                            profileObj.name = name;
                            profileObj.Gtype = Gtype;
                            profileObj.privacy = privacy;

                            if (privacy == 'public') {
                                profileObj.adminApprovalForJoiningGroup = false;
                                profileObj.membersCanInvite = true;
                            }
                            if (privacy == 'private') {
                                profileObj.adminApprovalForJoiningGroup = adminApprovalForJoiningGroup;
                                profileObj.membersCanInvite = membersCanInvite;
                            }
                            if (privacy == 'secret') {
                                profileObj.adminApprovalForJoiningGroup = true;
                                profileObj.membersCanInvite = false;
                            }

                            if (typeof currency !== 'undefined' && currency && currency !== 'undefined' &&
                                typeof amount !== 'undefined' && amount && amount !== 'undefined' &&
                                typeof subscription !== 'undefined' && subscription && subscription !== 'undefined' &&
                                typeof duration !== 'undefined' && duration && duration !== 'undefined') {

                                profileObj.subscription = subscription;
                                profileObj.amount = amount;
                                profileObj.currency = currency;
                                profileObj.duration = duration;
                                profileObj.type = "paid";

                                if (typeof wallet !== 'undefined' && wallet && wallet !== 'undefined') {
                                    profileObj.wallet = wallet;
                                }

                                if (typeof trialDuration !== 'undefined' && trialDuration && trialDuration !== 'undefined' &&
                                    typeof trial !== 'undefined' && trial && trial !== 'undefined') {
                                    profileObj.trialDuration = trialDuration;
                                    profileObj.trial = trial;
                                }
                                if (subscription === 'false') {
                                    if (typeof id !== 'undefined' && id && id !== '' &&
                                        typeof currency !== 'undefined' && currency && currency !== '' &&
                                        typeof amount !== 'undefined' && amount && amount !== '' &&
                                        typeof subscription !== 'undefined' && subscription && subscription !== '' &&
                                        typeof duration !== 'undefined' && duration && duration !== '' &&
                                        // typeof wallet !== 'undefined' && wallet && wallet !== '' &&
                                        // typeof trialDuration !== 'undefined' && trialDuration && trialDuration !== '' &&
                                        typeof trial !== 'undefined' && trial && trial !== '') {

                                        profileObj.subscription = subscription;
                                        profileObj.amount = '';
                                        profileObj.currency = '';
                                        profileObj.duration = "";
                                        profileObj.trial = false;
                                        profileObj.trialDuration = '';
                                        profileObj.type = "free";
                                        GroupModel.updateMany({ id: mongoose.Types.ObjectId(id) }, profileObj, function (error, show) {
                                            if (error) {
                                                res.status(500).json({ message: error });
                                            }
                                        });
                                    }
                                }
                            }
                            if (fileArr.length > 0) {
                                var contentObj = [];
                                fileArr.forEach(function (file) {
                                    var path = file.path.replace("public", '').substr(1);
                                    contentObj.push({
                                        groupImage: path,
                                    });
                                });
                                profileObj['groupImage'] = contentObj[0].groupImage;
                            }
                            GroupModel.update({ _id: mongoose.Types.ObjectId(id) }, profileObj, function (error, updateSubscription) {
                                if (error) {
                                    res.status(500).json({ message: error });
                                } else {
                                    res.status(200).json(updateSubscription);
                                }
                            });
                        } else {
                            errorObj.id = 'Id is required';
                            errorObj.userid = 'userid is required';
                            errorObj.name = 'Name is required';
                            errorObj.Gtype = "Group type is required";
                            errorObj.privacy = "Privacy is required";
                            errorObj.membersCanInvite = "Invitation is required";
                            errorObj.adminApprovalForJoiningGroup = "Admin approval is required";
                            errorObj.subscription = "Subscription is required";
                            errorObj.wallet = 'Wallet is required';
                            errorObj.currency = 'Currency is required';
                            errorObj.amount = 'Amount is required';
                            errorObj.duration = "Duration is required";
                            errorObj.trial = "Trial is required";
                            errorObj.trialDuration = "TrialDuration is required";
                            res.status(400).json({ message: errorObj });
                        }
                    } else {
                        res.status(400).json({ message: "Only admin can update group info" });
                    }
                }).catch((err) => {
                    console.log("ERROR IN UPDATE SUBSCRIPTION 1", err);
                    res.status(500).json({ message: "Something went wrong!!!" });
                });
            });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}

subscriptionController.invitePeopleToJoinGroup = async (req, res) => {
    try {
        var groupId = {};
        var selectedUser = [];
        upload1(req, res, function (err) {
            if (err) {
                throw err;
            } else {
                groupId = JSON.parse(req.body.groupid).groupid;
                selectedUser = JSON.parse(req.body.inviteList);
                var userId = req.payload._id;
                var canInvite = false;

                // var selectedUser = [{ name: 'Pawan Kumar', id: '5ce58c8637155653c4206f5a' }, { name: 'Pawan Kumar', id: '5ba73d4fdfd2a11ed8933cef' }, { name: 'pardeep', id: '5d1f7c5c22c4e78888a364d6' }, { name: 'Pawan Dahiya', id: '5d300a0e0826542fc82f42b0' }]
                // var selectedUser = [{ name: 'Pawan Saini', id: '5cef7fdc2ce1245ef03fdb6d' }]
                GroupModel.findOne({ _id: groupId }).then(async (groupInfo) => {
                    if (groupInfo.privacy === 'secret') {
                        if (groupInfo.userid == userId) {
                            canInvite = true;
                        } else {
                            canInvite = false;
                        }
                    } else if (groupInfo.privacy === 'private' || groupInfo.privacy === 'public') {
                        if (groupInfo.userid == userId) {
                            canInvite = true;
                        } else {
                            if (groupInfo.membersCanInvite === true) {
                                await GroupSubscriptionModel.findOne({ userid: userId, groupid: groupId, type: "Subscribe", status: "open" }).then((memberOfGroup) => {
                                    if (memberOfGroup) {
                                        canInvite = true;
                                    } else {
                                        canInvite = false;
                                    }
                                })
                            } else if (groupInfo.membersCanInvite === false) {
                                canInvite = false;
                            }
                        }
                    }
                    if (canInvite) {
                        var counter = 0;
                        for (let i = 0; i < selectedUser.length; i++) {
                            var inviteObject = {};
                            inviteObject.userid = selectedUser[i].id;
                            inviteObject.userName = selectedUser[i].name;
                            inviteObject.groupName = groupInfo.name;
                            inviteObject.groupId = groupInfo._id;
                            inviteObject.groupAdmin = groupInfo.userid;
                            inviteObject.groupImage = groupInfo.groupImage;
                            var blocked = 0;

                            await BlockedUserModel.findOne({ userid: selectedUser[i].id, groupid: groupInfo._id }).then(async (foundInBlockedUsers) => {
                                counter++;
                                if (foundInBlockedUsers) {
                                    blocked++;
                                } else {
                                    await GroupInvitationModel.findOne({ userid: selectedUser[i].id, groupId: groupInfo._id }).then(async (alreadyExistingInvitation) => {
                                        if (alreadyExistingInvitation !== '' && alreadyExistingInvitation !== null && alreadyExistingInvitation !== undefined && alreadyExistingInvitation) {
                                            // GroupInvitationModel.remove({ userid: selectedUser[i].id, groupId: groupInfo._id }).then(async (removedInvitation) => {
                                            //     console.log("INVITE OBJECT", inviteObject)
                                            //     await GroupInvitationModel.create(inviteObject).then((saved) => {
                                            //         // console.log("SAVED", saved)
                                            //     }).catch((err) => {
                                            //         console.log("ERROR IN INVITE PEOPLE TO JOIN GROUP 2", err);
                                            //         res.status(200).json({ success: false, message: "Something went wrong!!!" });
                                            //     })
                                            // }).catch((err) => {
                                            //     console.log("ERROR IN INVITE PEOPLE TO JOIN GROUP 3", err);
                                            //     res.status(200).json({ success: false, message: "Something went wrong!!!" });
                                            // })
                                            // console.log("ALREADY INVITED")
                                        } else {
                                            await GroupInvitationModel.create(inviteObject).then((saved) => {
                                                // console.log("SAVED", saved)
                                            }).catch((err) => {
                                                console.log("ERROR IN INVITE PEOPLE TO JOIN GROUP 1", err);
                                                res.status(200).json({ success: false, message: "Something went wrong!!!" });
                                            })
                                        }
                                    }).catch((err) => {
                                        console.log("ERROR IN INVITE PEOPLE TO JOIN GROUP 4", err);
                                        res.status(200).json({ success: false, message: "Something went wrong!!!" });
                                    })
                                }
                                if (counter === selectedUser.length) {
                                    if (blocked > 0) {
                                        return res.status(200).json({ success: true, message: "Invites send successsfully except for the blocked ones" });
                                    }
                                    return res.status(200).json({ success: true, message: "Invites send successsfully" });
                                }
                            }).catch((err) => {
                                console.log("ERROR IN INVITE PEOPLE TO JOIN GROUP 5", err);
                                res.status(200).json({ success: false, message: "Something went wrong!!!" });
                            })
                        }
                    } else {
                        res.status(200).json({ success: false, message: "You cannot invite other users" });
                    }
                }).catch((err) => {
                    console.log("ERROR IN INVITE PEOPLE TO JOIN GROUP 6", err);
                    res.status(200).json({ success: false, message: "Something went wrong!!!" });
                })
            }
        });
    } catch (error) {
        console.log("CATCH ERROR OF SENDING INVITES", error);
        return res.status(200).json({ success: false, message: "INVITES NOT SEND" });
    }
}

subscriptionController.acceptInvitationOfGroup = async (req, res, next) => {
    try {
        var userid = req.payload._id;
        var groupId = req.params.id;
        console.log('userid ', userid, 'groupId ', groupId);
        GroupInvitationModel.findOne({ userid: userid, groupId: groupId }).then(async (foundInvitation) => {
            if (foundInvitation) {
                GroupModel.findOne({ _id: groupId }).then(async (groupInfo) => {
                    if (groupInfo.type === "free") {
                        await ctrl.followgroup(userid, groupId).then((result) => {
                            GroupInvitationModel.remove({ _id: foundInvitation._id }).then(async (removedRequestObject) => {
                                if (removedRequestObject) {
                                    res.status(200).json({ message: "Request Accepted Successfully!!!" });
                                } else {
                                    res.status(500).json({ message: "Something went wrong" });
                                }
                            }).catch((errorInDeletingRequest) => {
                                console.log("ERROR IN DELETING REQUEST", errorInDeletingRequest);
                                res.status(500).json({ message: "Something went wrong" });
                            })
                        }, (err) => {
                            console.log("PROMISE REJECTION FROM FOLLOW GROUP", err);
                            res.status(500).json({ message: error.message });
                        })
                    } else if (groupInfo.type === "paid") {
                        await PostController.subscribeForAGroup(req).then((result) => {
                            // console.log("RESULT OF SUBSCRIBERS PAYMENT", result);
                            GroupInvitationModel.remove({ userid: userid, groupId: groupId }).then(async (removedInvitation) => {
                                res.status(200).json({ message: "Invitation Accepted" });
                            }).catch((errorInDeletingRequest) => {
                                console.log("ERROR IN DELETING REQUEST", errorInDeletingRequest);
                                res.status(500).json({ message: "Something went wrong" });
                            })
                        }, (err) => {
                            console.log("ERROR OF SUBSCRIBERS PAYMENT", err);
                            res.status(500).json(err);
                        })
                    }
                })
            } else {
                res.status(500).json({ message: "No such invitation present" });
            }
        }).catch((err) => {
            console.log("ERROR IN ACCEPT INVITATION OF GROUP 1", err);
            res.status(500).json({ message: "Something went wrong!!!" });
        })
    } catch (error) {
        console.log("CATCH ERROR OF ACCEPT INVITATION", error);
        return res.status(200).json({ success: false, message: "Error in accepting Invitation" });
    }
}

/*********************************User-Group work *******************************/
// getDetailsByid
subscriptionController.getDetailsByid = function (req, res, next) {
    try {
        var id = req.params.id;
        var errorObj = {};

        if (typeof id !== 'undefined' && id && id !== '') {
            GroupModel.find({ _id: id }, function (error, getDetailObj) {
                if (error) {
                    res.status(500).json({ message: error })
                } else {
                    res.status(200).json(getDetailObj);
                }
            })
        }
        else {
            errorObj.id = "Id is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong" });
    }
}

// groupPosts
subscriptionController.groupPosts = async function (req, res, next) {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            uploads(req, res, async function (error) {
                if (error) {
                    throw error;
                }
                var text = req.body.text;
                var subject = req.body.subject;
                var groupid = req.body.groupid;
                var userid = req.payload._id;
                var payements;
                var status = 'active'
                var file = req.files;
                var profileObj = {};
                var errorObj = {};

                if (typeof userid !== 'undefined' && userid && userid !== '' &&
                    typeof text !== 'undefined' && text && text !== '' &&
                    typeof subject !== 'undefined' && subject && subject !== '' &&
                    typeof groupid !== 'undefined' && groupid && groupid !== '' &&
                    typeof status !== 'undefined' && status && status !== '') {

                    await GroupModel.findOne({ _id: groupid }).then((foundGroup) => {
                        if (foundGroup) {
                            if (foundGroup.privacy === "public" && foundGroup.type === "free") {
                                profileObj.payements = false;
                            } else if (foundGroup.privacy === "private" && foundGroup.type === "free") {
                                profileObj.payements = false;
                            } else {
                                profileObj.payements = true;
                            }
                        } else {
                            return res.status(200).json({ success: false, message: "Some error is there while posting in the post" });
                        }
                    }).catch((errorInFindingGroup) => {
                        console.log("ERROR IN FINDING GROUP WHILE POSTING", errorInFindingGroup);
                        return res.status(200).json({ success: false, message: "Some error is there while posting in the post" });
                    })
                    profileObj.userid = userid;
                    profileObj.text = text;
                    profileObj.subject = subject;
                    profileObj.groupid = groupid;
                    profileObj.status = status;
                    // profileObj.payements = payements;

                    PostModel.create(profileObj, function (error, groupPostObj) {
                        if (error) {
                            res.status(500).json({ message: error });
                        } else {
                            var postid = groupPostObj._id
                            if (file.length > 0) {
                                var contentObj = [];
                                file.forEach(function (file) {
                                    var path = file.path.replace("public", '').substr(1);
                                    var ext = pathmodule.extname(path).toString();
                                    var type = '';
                                    if (fileattach.indexOf(ext) !== -1) {
                                        type = 'Files'
                                    }
                                    if (ImageSupported.indexOf(ext) !== -1) {
                                        type = 'Image'
                                    }
                                    if (VideoSupported.indexOf(ext) !== -1) {
                                        type = 'Video'
                                    }
                                    contentObj.push({
                                        content_url: path,
                                        contenttype: type,
                                        postid: postid,
                                        userid: userid
                                    });
                                });
                                content.create(contentObj, function (error, contentImg) {
                                    if (error) {
                                        res.status(500).json({ message: error });
                                    }
                                    else {
                                        // res.status(200).json(contentImg);
                                    }
                                })
                            }
                            res.status(200).json(groupPostObj);
                        }
                    });
                }
                else {
                    errorObj.userid = "Userid is required";
                    errorObj.groupid = "Groupid is required";
                    errorObj.text = "Text is required";
                    errorObj.subject = "Suject is required";
                    errorObj.content_url = "Image is required";
                    res.status(400).json({ message: errorObj });
                }
            });
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong" });
    }
}
// getgroupPosts
subscriptionController.getgroupPosts = function (req, res, next) {
    try {
        var userid = req.query.userid;
        PostModel.aggregate([
            {
                $match: {
                    "groupid": { $exists: true },
                }
            },
            {
                "$lookup": {
                    from: 'post_medias',
                    localField: '_id',
                    foreignField: 'postid',
                    as: 'contents'
                }
            },
            {
                "$lookup": {
                    from: 'users',
                    localField: 'userid',
                    foreignField: '_id',
                    as: 'userinfo'
                }
            },
            {
                '$unwind': {
                    path: '$userinfo'
                }
            },
            {
                "$project": {
                    _id: 1,
                    text: 1,
                    subject: 1,
                    groupid: 1,
                    userid: 1,
                    createdAt: 1,
                    "contents": 1,
                    "userinfo._id": 1,
                    "userinfo.name": 1,
                    "userinfo.avatar": 1,
                }
            },

        ]).sort({ createdAt: -1 }).exec(function (error, postlist) {
            if (error) {
                res.status(404).json({ message: error.message });
            }
            else {
                var postids = [];
                postlist.forEach(function (post) {
                    postids.push(post._id);
                });
                if (postids.length > 0) {
                    PostModel.aggregate([
                        {
                            "$match": {
                                parentid: { $in: postids },
                            }
                        },
                        {
                            "$lookup": {
                                from: 'users',
                                localField: 'userid',
                                foreignField: '_id',
                                as: 'userinfo'
                            }
                        },
                        {
                            '$unwind': {
                                path: '$userinfo'
                            }
                        },
                        {
                            "$project": {
                                _id: 1,
                                text: 1,
                                userid: 1,
                                parentid: 1,
                                createdAt: 1,
                                "userinfo.name": 1,
                                "userinfo.avatar": 1
                            }
                        },
                    ]).sort({ createdAt: -1 }).exec(function (error, postcommentlist) {
                        if (error) {
                            res.status(404).json({ message: error.message });
                        } else {
                            if (postcommentlist.length > 0) {
                                var wallpost = {};
                                postcommentlist.forEach(function (item) {
                                    if (Object.keys(wallpost).indexOf(item.parentid.toString()) === -1) {
                                        wallpost[item.parentid] = [item];
                                    } else {
                                        wallpost[item.parentid].push(item)
                                    }
                                });
                                res.status(200).json({ post: postlist, comments: wallpost });
                            } else {
                                res.status(200).json({ post: postlist, comments: {} });
                            }
                        }
                    });
                } else {
                    res.status(200).json({ post: postlist, comments: {} });
                }
                // res.status(200).json(postlist)
            }
        });
    }
    catch (e) {
        res.status(500).json({ message: "Something went wrong" });
    }
}
// Grouplike
subscriptionController.grouplike = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var postid = req.body.postid;
        var profileObj = {};
        var errorObj = {};

        if (typeof userid !== 'undefined' && userid && userid !== '' &&
            typeof postid !== 'undefined' && postid && postid !== ''
        ) {
            profileObj.userid = userid;
            profileObj.postid = postid;

            likes.findOneAndDelete({ postid: postid, userid: userid }).exec(function (error, result) {
                if (error) {
                    res.status(500).json({ message: "Something went wrong" });
                } else {
                    if (result === null) {
                        likes.create(profileObj, function (error, user) {
                            if (error) {
                                res.status(500).json({ message: error });
                            } else {
                                res.status(200).json({ message: 'Like' });
                            }
                        });
                    } else {
                        res.status(200).json({ message: 'Dislike' });
                    }
                }
            })
        }
        else {
            errorObj.postid = 'Postid is required';
            res.status(400).json({ message: errorObj });
        }
    }
    catch (e) {
        res.status(500).json({ message: "Something went wrong" })
    }
}
// Getgrouplike
subscriptionController.getlike = function (req, res, next) {
    try {
        likes.aggregate([
            {
                $group: {
                    _id: { postid: "$postid" },
                    count: { $sum: 1 }
                }
            }
        ]).exec(function (error, likeObj) {
            if (error) {
                res.status(404).json({ message: error.message });
            } else {
                var postlike = {};
                if (likeObj.length > 0) {
                    likeObj.forEach(function (item) {
                        postlike[item._id.postid] = item.count
                    })
                }
                res.status(200).json(postlike);
            }
        })
    }
    catch (e) {
        res.status(500).json({ message: "Something went wrong" })
    }
}
// Group comments
subscriptionController.postreply = function (req, res, next) {
    try {
        var text = req.body.text;
        var userid = req.payload._id;
        var parentid = req.body.parentid;
        var status = 'active'
        var errorObj = {};
        var profileObj = {};

        if (typeof text !== 'undefined' && text && text !== '' &&
            typeof userid !== 'undefined' && userid && userid !== '' &&
            typeof parentid !== 'undefined' && parentid && parentid !== '' &&
            typeof status !== 'undefined' && status && status !== ''
        ) {
            profileObj.text = text;
            profileObj.userid = userid;
            profileObj.parentid = parentid;
            profileObj.status = status

            PostModel.create(profileObj, function (error, user) {
                if (error) {
                    res.status(500).json({ message: error.message });
                } else {
                    res.status(200).json(user);
                }
            });

        } else {
            errorObj.text = 'Message is required';
            res.status(400).json({ message: errorObj });
        }
    }
    catch (e) {
        res.status(500).json({ message: "Something went wrong" })
    }
}
// Delete group
subscriptionController.deletegroup = function (req, res, next) {
    try {
        var id = req.params.id;
        var errorObj = {};
        var profileObj = {};

        if (typeof id !== 'undefined' && id && id !== '') {
            profileObj.id = id;
            PostModel.remove({ _id: mongoose.Types.ObjectId(id) }, function (error, groupdelete) {
                if (error) {
                    res.status(500).json({ message: error.message });
                } else {
                    content.remove({ postid: mongoose.Types.ObjectId(id) }, function (error, user) {
                        if (error) {
                            res.status(500).json({ message: error.message });
                        } else {
                            likes.remove({ postid: mongoose.Types.ObjectId(id) }, function (error, user) {
                                if (error) {
                                    res.status(500).json({ message: error.message });
                                } else {
                                    PostModel.remove({ parentid: id }, function (error, comments) {
                                        if (error) {
                                            res.status(500).json({ message: error.message });
                                        } else {
                                            res.status(200).json({ message: 'Post Deleted Successfully' });
                                        }
                                    })
                                }
                            });
                        }
                    });
                }
            })
        }
        else {
            errorObj.id = 'Id is required';
            res.status(400).json({ message: errorObj });
        }
    }
    catch (e) {
        res.status(500).json({ message: e.message });
    }
}

/*******************Subscribe, duration & trial Duration work******************* */
// UserGroup terial period
subscriptionController.trialperiod = function (req, res, next) {
    try {
        var trial = req.body.trial;
        var d = new Date();
        var endDt = new Date(d.setDate(d.getDate() + trial * 7));
        var userid = req.payload._id;
        var groupId = req.params.id;
        var currency = req.body.currency;
        var groupid = req.params.id;
        var subscriptionid = req.body.userid;
        var endDate = endDt;
        var amount = 0;
        var type = "trial";
        var autoPayement = false;
        var profileObj = {};
        var errorObj = {};

        if (typeof userid !== 'undefined' && userid && userid !== '' &&
            typeof groupId !== 'undefined' && groupId && groupId !== '' &&
            typeof currency !== 'undefined' && currency && currency !== '' &&
            typeof groupid !== 'undefined' && groupid && groupid !== '' &&
            typeof subscriptionid !== 'undefined' && subscriptionid && subscriptionid !== '' &&
            typeof endDate !== 'undefined' && endDate && endDate !== '') {

            profileObj.userid = userid;
            profileObj.groupId = groupId;
            profileObj.type = type;
            profileObj.currency = currency;
            profileObj.amount = amount;
            profileObj.groupid = groupid;
            profileObj.subscriptionid = subscriptionid;
            profileObj.endDate = endDate;
            profileObj.autoPayement = autoPayement;
            profileObj.status = "open";
            GroupSubscriptionModel.findOne({ userid: userid, groupid: groupid, type: "trial" }).then((foundTrialForGroup) => {
                if (foundTrialForGroup === null) {
                    mypurchase.create(profileObj, function (error, trialObj) {
                        if (error) {
                            res.status(500).json({ message: error });
                        } else {
                            GroupSubscriptionModel.create(profileObj, function (error, subscriptionObj) {
                                if (error) {
                                    res.status(500).json({ message: error });
                                } else {
                                    res.status(200).json(subscriptionObj);
                                }
                            });
                        }
                    });
                } else if (foundTrialForGroup.status === "open") {
                    res.status(500).json({ message: "The trial period is going on" });
                } else if (foundTrialForGroup.status === "expired") {
                    res.status(500).json({ message: "You have already used the trial period, for more updates, please take subscription of this group" });
                }
            }).catch((err) => {
                console.log("ERROR IN TRIAL PERIOD 1", err);
                res.status(500).json({ message: "Something went wrong!!!" });
            })
        } else {
            errorObj.userid = "Userid is required";
            errorObj.groupId = "Trial is required";
            errorObj.currency = "Currency is required";
            errorObj.groupid = "Groupid is required";
            errorObj.subscriptionid = "Subscriptionid is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong" });
    }
}
// UserGroup terial period expire
subscriptionController.trialperiodexpire = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var errorObj = {};
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            GroupSubscriptionModel.find({ userid: mongoose.Types.ObjectId(userid) }, function (error, subscriptionObj) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    var currDt = new Date();
                    GroupSubscriptionModel.updateMany({ endDate: { "$lte": currDt }, autoPayement: false }, { status: 'expired' }, function (error, updateSubscriptionObj) {
                        if (error) {
                            res.status(500).json({ message: error });
                        } else {
                            if (subscriptionObj !== null) {
                                var endDate = subscriptionObj.endDate;
                                var currDt = new Date();
                                GroupSubscriptionModel.find({ endDate: { "$gte": currDt }, userid: userid }, function (error, expireTrial) {
                                    if (error) {
                                        res.status(500).json({ message: error });
                                    } else {
                                        var groupData = {};
                                        if (expireTrial.length > 0) {
                                            expireTrial.forEach(function (item) {
                                                groupData[item.groupid] = item.userid;
                                            });
                                        }
                                        res.status(200).json(groupData);
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            errorObj.userid = "Userid is required";
            res.status(500).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong" });
    }
}
// Mypurchase
// Changes this function in freeGroup
subscriptionController.mypurchase = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var errorObj = {};
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            GroupSubscriptionModel.find({ userid: mongoose.Types.ObjectId(userid) }, function (error, purchaseObj) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    var groupData = {};
                    if (purchaseObj.length > 0) {
                        purchaseObj.forEach(function (item) {
                            groupData[item.groupid] = true;
                        });
                    }
                    res.status(200).json(groupData);
                }
            });
        } else {
            errorObj.userid = "Userid is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong" });
    }
}

subscriptionController.suggestedGroups = async (req, res) => {
    try {
        var user = req.payload._id;
        var suggestedGroups = [];
        var groupTypes = [];
        await GroupSubscriptionModel.aggregate([
            {
                "$match": {
                    "userid": mongoose.Types.ObjectId(user),
                    "status": { $ne: "expired" }
                }
            },
            {
                $lookup: {
                    from: "groupsbyusers",
                    localField: "groupid",
                    foreignField: "_id",
                    as: "groupDetails"
                }
            },
            {
                $unwind: "$groupDetails"
            },
            {
                $group: {
                    _id: "$groupDetails.Gtype",
                }
            }
        ]).then(async (subscribedGroups) => {
            if (subscribedGroups.length <= 0) {
                res.status(200).json({ message: [] });
            } else {
                subscribedGroups.forEach(async (subscribedGroupsElement) => {
                    groupTypes.push(subscribedGroupsElement._id);
                })
                await GroupModel.aggregate([
                    {
                        $match: {
                            Gtype: { $in: groupTypes },
                            "userid": { $ne: mongoose.Types.ObjectId(user) },
                            "privacy": { $ne: "secret" }
                        },
                    }, {
                        $project: {
                            "_id": 1,
                            "name": 1,
                            "Gtype": 1,
                            "groupImage": 1,
                            "type": 1
                        }
                    }
                ]).then(async (groups) => {
                    if (groups.length <= 0) {
                        res.status(200).json({ message: [] });
                    } else {
                        var totalGroups = groups.length;
                        var counter = 0;
                        for (let i = 0; i < groups.length; i++) {
                            await GroupSubscriptionModel.findOne({
                                groupid: mongoose.Types.ObjectId(groups[i]._id),
                                userid: user,
                                status: { $ne: "expired" }
                            }).then(async (existingGroup) => {
                                counter++;
                                if (existingGroup) {
                                } else {
                                    await BlockedUserModel.findOne({ groupid: mongoose.Types.ObjectId(groups[i]._id), userid: user }).then((blockedUser) => {
                                        if (blockedUser) {
                                        } else {
                                            suggestedGroups.push(groups[i]);
                                        }
                                    })
                                    // suggestedGroups.push(groups[i]);
                                }
                            }).catch((err) => {
                                console.log("ERROR IN SUGGESTED GROUPS 1", err);
                                res.status(500).json({ message: "Something went wrong!!!" });
                            })
                            if (counter === totalGroups) {
                                res.status(200).json({ message: suggestedGroups });
                            }
                        }
                    }
                }).catch((err) => {
                    console.log("ERROR IN SUGGESTED GROUPS 2", err);
                    res.status(500).json({ message: "Something went wrong!!!" });
                })
            }
        }).catch((err) => {
            console.log("ERROR IN SUGGESTED GROUPS 3", err);
            res.status(500).json({ message: "Something went wrong!!!" });
        })
    } catch (error) {
        console.log("ERROR IN FETCHING SUGGESTED GROUPS", error);
        res.status(500).json({ message: "Something went wrong, please try after sometime" })
    }
}

// Followgroup
// subscriptionController.followgroup = function (req, res, next) {
subscriptionController.followgroup = function (joiningUser, groupToBeJoined, subscriptionId) {
    return new Promise(async (resolve, reject) => {
        try {
            // var userid = req.payload._id;
            // var groupid = req.body.id;
            var userid = joiningUser;
            var groupid = groupToBeJoined;
            var subscriptionid = subscriptionId;
            var type = 'free';
            var followObj = {};
            var errorObj = {};

            if (typeof userid !== 'undefined' && userid && userid !== '' && userid !== null && userid !== undefined &&
                typeof groupid !== 'undefined' && groupid && groupid !== '' && groupid !== null && groupid !== undefined &&
                typeof subscriptionid !== 'undefined' && subscriptionid && subscriptionid !== '') {

                followObj.userid = userid;
                followObj.groupid = groupid;
                followObj.type = type;
                followObj.autoPayement = false;
                followObj.status = "open";
                followObj.subscriptionid = subscriptionid;
                // GroupSubscriptionModel.findOne({ userid: mongoose.Types.ObjectId(userid), groupid: mongoose.Types.ObjectId(groupid) }, function (error, freeDetails) {
                //     if (error) {
                //         res.status(500).json({ message: error })
                //     } else {
                //         console.log('freeDetails ', freeDetails);
                GroupSubscriptionModel.create(followObj, function (error, follow) {
                    if (error) {
                        // res.status(500).json({ message: error });
                        reject({ message: error });
                    } else {
                        // res.status(200).json(follow);
                        resolve(follow);
                    }
                });
                //     }
                // })
            } else {
                errorObj.userid = "Id is required";
                errorObj.groupid = "Id is required";
                errorObj.type = "Type is required";
                // res.status(400).json({ message: errorObj });
                reject({ message: errorObj });
            }
        } catch (e) {
            console.log(e);
            // res.status(500).json({ message: "Something went wrong" });
            reject({ message: "Something went wrong" });
        }
    })
}

// Free group
subscriptionController.freeGroup = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var errorObj = {}
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            GroupSubscriptionModel.find({ userid: mongoose.Types.ObjectId(userid), type: 'free' }, function (error, subscriptionObj) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    var freeGroup = {};
                    if (subscriptionObj.length > 0) {
                        subscriptionObj.forEach(function (element) {
                            freeGroup[element.groupid] = true;
                        });
                    }
                    res.status(200).json(freeGroup)
                }
            });
        } else {
            errorObj.userid = "id is required";
            res.status(400).json({ message: userid });
        }
    } catch (error) {
        res.status(500).json({ message: "Something went wrong" });
    }
}

// Request to join the group
subscriptionController.requestToJoinGroup = async (req, res) => {
    try {
        var userId = req.payload._id;
        var userName = req.payload.name;
        var groupId = req.params.groupId;
        var groupName;
        var autoPayment = false;
        if (req.body.autoPayment !== undefined && req.body.autoPayment !== null && req.body.autoPayment !== "" && req.body.autoPayment) {
            autoPayment = true;
        }
        GroupModel.findOne({ _id: groupId }).then((group) => {
            if (group) {
                groupName = group.name;
                BlockedUserModel.findOne({ groupid: groupId, userid: userId }).then((blockedUser) => {
                    if (blockedUser) {
                        res.status(200).json({ success: false, message: "No group found" });
                    } else {
                        GroupSubscriptionModel.findOne({ groupid: groupId, userid: userId }).then((alreadyFollowing) => {
                            if (alreadyFollowing) {
                                res.status(200).json({ success: false, message: "Already following this group" });
                            } else {
                                GroupJoinRequestsModel.findOne({ groupid: groupId, userid: userId }).then(async (alreadySentRequest) => {
                                    if (alreadySentRequest) {
                                        res.status(200).json({ success: false, message: "Already sent request to this group" });
                                    } else {
                                        UserModel.findOne({ _id: userId }, { avatar: 1 }).then(async (userPic) => {
                                            if (group.type === "free") {
                                                if (group.privacy === "secret") {
                                                    res.status(200).json({ success: false, message: "No such group is there" });
                                                } else if (group.privacy === "private") {
                                                    if (group.adminApprovalForJoiningGroup === true) {
                                                        var reqObject = {
                                                            groupid: groupId,
                                                            groupName: groupName,
                                                            userid: userId,
                                                            userName: userName,
                                                            autoPayment: autoPayment
                                                        }
                                                        if (userPic) {
                                                            reqObject.userPic = userPic.avatar;
                                                        }
                                                        GroupJoinRequestsModel.create(reqObject, async (err, subscriptionobj) => {
                                                            if (err) {
                                                                res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
                                                            } else {
                                                                res.status(200).json({ success: true, message: "Request Sent Successfully!!!" });
                                                            }
                                                        });
                                                    } else if (group.adminApprovalForJoiningGroup === false) {
                                                        await ctrl.followgroup(userId, groupId, group.userid).then(async (result) => {
                                                            return res.status(200).json({ success: true, message: "Joined group!!!" })
                                                        }, (error) => {
                                                            return res.status(200).json({ success: false, message: "Something went wrong" });
                                                        })
                                                    }
                                                } else if (group.privacy === "public") {

                                                    // if (group.adminApprovalForJoiningGroup === true) {
                                                    //     var reqObject = {
                                                    //         groupid: groupId,
                                                    //         groupName: groupName,
                                                    //         userid: userId,
                                                    //         userName: userName,
                                                    //         autoPayment: autoPayment
                                                    //     }
                                                    //     GroupJoinRequestsModel.create(reqObject, async (err, subscriptionobj) => {
                                                    //         if (err) {
                                                    //             console.log("ERROR IN CREATING REQUEST FOR THIS GROUP", err)
                                                    //             res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
                                                    //         } else {
                                                    //             // console.log("CREATED REQUEST", subscriptionobj)
                                                    //             res.status(200).json({ success: true, message: "Request Sent Successfully!!!" });
                                                    //         }
                                                    //     });

                                                    // } else if (group.adminApprovalForJoiningGroup === false) {
                                                    await ctrl.followgroup(userId, groupId, group.userid).then(async (result) => {
                                                        return res.status(200).json({ success: true, message: "Joined group!!!" })
                                                    }, (error) => {
                                                        console.log("ERROR IN ACCEPTANCE OF REQUEST", error);
                                                        return res.status(200).json({ success: false, message: "Something went wrong" });
                                                    })
                                                    // }
                                                }
                                            } else if (group.type === "paid") {
                                                var amount = group.amount;
                                                if (group.privacy === "secret") {
                                                    res.status(200).json({ success: false, message: "No such group is there" });
                                                } else if (group.privacy === "private") {
                                                    if (group.adminApprovalForJoiningGroup === true) {
                                                        // console.log("PAID APPROVAL  REQUIRED");
                                                        var reqObject = {
                                                            groupid: groupId,
                                                            groupName: groupName,
                                                            userid: userId,
                                                            userName: userName,
                                                            autoPayment: autoPayment,
                                                            amount: amount
                                                        }
                                                        if (userPic) {
                                                            reqObject.userPic = userPic.avatar;
                                                        }
                                                        GroupJoinRequestsModel.create(reqObject, async (err, subscriptionobj) => {
                                                            if (err) {
                                                                console.log("ERROR IN CREATING REQUEST FOR THIS GROUP", err)
                                                                res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
                                                            } else {
                                                                // console.log("CREATED REQUEST", subscriptionobj)

                                                                var token = req.headers.authorization;

                                                                var reqObj = {
                                                                    headers: {
                                                                        authorization: token
                                                                    },
                                                                    params: {
                                                                        id: groupId,
                                                                        payements: autoPayment
                                                                    },
                                                                    payload: {
                                                                        _id: userId
                                                                    }
                                                                };
                                                                await PostController.requestForAGroup(reqObj).then((result) => {
                                                                    // var eventName = "sendrRequestToJoinGroup" + group.userid.toString();
                                                                    // ioSocket.emit(eventName, {});
                                                                    console.log("RESUKLT", reqObj);
                                                                    return res.status(200).json({ success: true, message: "Request Send successfully!!!" });
                                                                }, (err) => {
                                                                    console.log("ERROR OF SUBSCRIBERS PAYMENT", err);
                                                                    return res.status(200).json({ success: false, message: err.message });
                                                                })
                                                                // res.status(200).json({ success: true, message: "Request Sent Successfully!!!" });
                                                            }
                                                        });
                                                    } else if (group.adminApprovalForJoiningGroup === false) {
                                                        // console.log("PAID APPROVAL NOT REQUIRED");
                                                        var token = req.headers.authorization;
                                                        var reqObj = {
                                                            headers: {
                                                                authorization: token
                                                            },
                                                            params: {
                                                                id: groupId,
                                                                payements: autoPayment
                                                            },
                                                            payload: {
                                                                _id: userId
                                                            }
                                                        };
                                                        await PostController.subscribeForAGroup(reqObj).then((result) => {
                                                            return res.status(200).json({ success: true, message: "Joined Group!!!" });
                                                        }, (err) => {
                                                            console.log("ERROR OF SUBSCRIBERS PAYMENT", err);
                                                            return res.status(200).json({ success: false, message: err.message });
                                                        })
                                                    }
                                                } else if (group.privacy === "public") {
                                                    var token = req.headers.authorization;
                                                    var reqObj = {
                                                        headers: {
                                                            authorization: token
                                                        },
                                                        params: {
                                                            id: groupId,
                                                            payements: autoPayment
                                                        },
                                                        payload: {
                                                            _id: userId
                                                        }
                                                    };
                                                    // console.log("REQUEST OBJECT", reqObj);
                                                    await PostController.subscribeForAGroup(reqObj).then((result) => {
                                                        return res.status(200).json({ success: true, message: "Joined Group!!!" });
                                                    }, (err) => {
                                                        console.log("ERROR OF SUBSCRIBERS PAYMENT", err);
                                                        return res.status(200).json({ success: false, message: err.message });
                                                    })
                                                }
                                            }
                                        })
                                    }
                                }).catch((err) => {
                                    console.log("ERROR IN REQUEST TO JOIN GROUP 1", err);
                                    res.status(200).json({ success: false, message: "Something went wrong!!!" });
                                })
                            }
                        }).catch((err) => {
                            console.log("ERROR IN REQUEST TO JOIN GROUP 2", err);
                            res.status(200).json({ success: false, message: "Something went wrong!!!" });
                        })
                    }
                }).catch((err) => {
                    console.log("ERROR IN REQUEST TO JOIN GROUP 3", err);
                    res.status(200).json({ success: false, message: "Something went wrong!!!" });
                })
            } else {
                console.log("No such group present");
                res.status(200).json({ success: false, message: "No such group present" });
            }
        }).catch((err) => {
            console.log("ERROR IN REQUEST TO JOIN GROUP 4", err);
            res.status(200).json({ success: false, message: "Something went wrong!!!" });
        })
    } catch (error) {
        console.log("ERROR IN SENDING REQUEST", error);
        res.status(200).json({ success: false, message: "Something went wrong" });
    }
}

//Approval of Request by group admin
subscriptionController.approvalOfRequest = async (req, res) => {
    try {
        var reqObj = {};
        var groupId = req.body.groupid;
        var userId = req.payload._id;
        var approval = req.body.approved;
        var userToBeApproved = req.body.userToBeApproved;
        var purchasetxid;
        GroupModel.findOne({ _id: groupId }).then((foundgroup) => {
            if (foundgroup) {
                if (foundgroup.userid.toString() === userId.toString()) {
                    GroupJoinRequestsModel.findOne({ groupid: groupId, userid: userToBeApproved }).then((foundRequest) => {
                        if (foundRequest) {
                            if (approval === true) {
                                BlockedUserModel.findOne({ groupid: groupId, userid: userId }).then((blockedUser) => {
                                    if (blockedUser) {
                                        return res.status(200).json({ success: false, message: "This user is blocked for the group" });
                                    } else {
                                        GroupSubscriptionModel.findOne({ groupid: groupId, userid: userToBeApproved }).then(async (alreadyFollowing) => {
                                            if (alreadyFollowing) {
                                                res.status(200).json({ success: false, message: "Already following this group" });
                                            } else {
                                                if (foundgroup.type === "free") {
                                                    await ctrl.followgroup(userToBeApproved, groupId, foundgroup.userid).then((result) => {
                                                        GroupJoinRequestsModel.remove({ _id: foundRequest._id }, function (error, removedRequest) {
                                                            if (error) {
                                                                return res.status(200).json({ success: false, message: "Error in removing of user request" });
                                                            } else {
                                                                return res.status(200).json({ success: true, message: "Request approved successfully!!!" })
                                                            }
                                                        });
                                                    }, (error) => {
                                                        console.log("ERROR IN ACCEPTANCE OF REQUEST", error);
                                                        return res.status(200).json({ success: false, message: "Something went wrong" });
                                                    })
                                                } else if (foundgroup.type === "paid") {
                                                    UserModel.findOne({ _id: userToBeApproved }, { _id: 1, email: 1, name: 1, phone: 1, userid: 1, group: 1 }).then(async (userToBeApprovedDetails) => {
                                                        await mypurchase.findOne({ userid: userToBeApproved, groupid: groupId, status: "pending" }).then((foundPendingMyPurchase) => {
                                                            purchasetxid = foundPendingMyPurchase.tr_id;
                                                        }).catch((error) => {
                                                            console.log("ERROR OF MY PURCHASE", error);
                                                            return res.status(200).json({ success: false, message: "Something went wrong" });
                                                        })
                                                        token = UserController.generateJwt(userToBeApprovedDetails);
                                                        reqObj = {
                                                            headers: {
                                                                authorization: token
                                                            },
                                                            params: {
                                                                id: groupId,
                                                                payements: foundRequest.autoPayment,
                                                                approved: true
                                                            },
                                                            payload: {
                                                                _id: userToBeApproved
                                                            }
                                                        };
                                                        await PostController.approveRequest(reqObj, purchasetxid).then((result) => {
                                                            var trialDurations = {
                                                                monthly: 1,
                                                                halfyearly: 6,
                                                                yearly: 12,
                                                            }
                                                            var d = new Date();
                                                            var endDate = new Date(d.setMonth(d.getMonth() + Number(trialDurations[foundgroup.duration])));
                                                            var profileObj = {};
                                                            profileObj.userid = userToBeApproved;
                                                            profileObj.type = "Subscribe";
                                                            profileObj.subscriptionid = foundgroup.userid;
                                                            profileObj.groupid = foundgroup._id;
                                                            profileObj.endDate = endDate;
                                                            profileObj.autoPayement = foundRequest.autoPayment;
                                                            profileObj.status = 'open';
                                                            GroupSubscriptionModel.create(profileObj, function (error, subObj) {
                                                                if (error) {
                                                                    reject({ message: 'Invalid request' });
                                                                } else {
                                                                    GroupJoinRequestsModel.remove({ _id: foundRequest._id }, function (error, removedRequest) {
                                                                        if (error) {
                                                                            return res.status(200).json({ success: false, message: "Error in removing of user request" });
                                                                        } else {
                                                                            console.log("REQUEST ACCEPT SUCCESSFULLYA", subObj);

                                                                            return res.status(200).json({ success: true, message: "Request approved successfully!!!" });
                                                                        }
                                                                    });
                                                                }
                                                            })
                                                        }, (err) => {
                                                            console.log("ERROR OF APPROVE REQUEST", err);
                                                            return res.status(200).json({ success: false, message: "Something went wrong" });
                                                        })
                                                    }).catch((err) => {
                                                        console.log("ERROR IN APPROVAL OF REQUEST 1", err);
                                                        return res.status(200).json({ success: false, message: "Something went wrong!!!" });
                                                    })
                                                }
                                            }
                                        }).catch((err) => {
                                            console.log("ERROR IN APPROVAL OF REQUEST 2", err);
                                            return res.status(200).json({ success: false, message: "Something went wrong!!!" });
                                        })
                                    }
                                }).catch((errorInFindingBlockedUser) => {
                                    console.log("ERROR IN FINDING BLOCKED USER", errorInFindingBlockedUser);
                                    return res.status(200).json({ success: false, message: "Something wemt wrong" });
                                })
                            } else if (approval === false) {
                                if (foundgroup.type === "paid") {
                                    UserModel.findOne({ _id: userToBeApproved }, { _id: 1, email: 1, name: 1, phone: 1, userid: 1, group: 1 }).then(async (userToBeApprovedDetails) => {
                                        await mypurchase.findOne({ userid: userToBeApproved, groupid: groupId, status: "pending" }).then((foundPendingMyPurchase) => {
                                            purchasetxid = foundPendingMyPurchase.tr_id;
                                        }).catch((error) => {
                                            console.log("ERROR OF MY PURCHASE", error);
                                            return res.status(200).json({ success: false, message: "Something went wrong" });
                                        })
                                        token = UserController.generateJwt(userToBeApprovedDetails);
                                        reqObj = {
                                            headers: {
                                                authorization: token
                                            },
                                            params: {
                                                id: groupId,
                                                payements: foundRequest.autoPayment,
                                                approved: false
                                            },
                                            payload: {
                                                _id: userToBeApproved
                                            }
                                        };
                                        await PostController.approveRequest(reqObj, purchasetxid).then((result) => {
                                            if (result) {
                                            } else {
                                                console.log("ERROR IN RESULT OF APPROVE REQUEST", err);
                                                return res.status(200).json({ success: false, message: "Something went wrong" });
                                            }
                                        }, (err) => {
                                            console.log("ERROR OF APPROVE REQUEST", err);
                                            return res.status(200).json({ success: false, message: "Something went wrong" });
                                        })
                                    }).catch((error) => {
                                        console.log("ERROR IN FINDING USER", error);
                                        return res.status(200).json({ success: false, message: "Somrthinng went wrong" });
                                    })
                                }
                                GroupJoinRequestsModel.remove({ _id: foundRequest._id }, function (error, removedRequest) {
                                    if (error) {
                                        return res.status(200).json({ success: false, message: "Error in removing of user request" });
                                    } else {
                                        return res.status(200).json({ success: true, message: "Request rejected successfully!!!" });
                                    }
                                });
                            }
                        } else {
                            return res.status(200).json({ success: false, message: "No such request present" });
                        }
                    }).catch((err) => {
                        console.log("ERROR IN APPROVAL OF REQUEST 3", err);
                        return res.status(200).json({ success: false, message: "Something went wrong!!!" });
                    })
                } else {
                    return res.status(200).json({ success: false, message: "Only admin is allowed to approve the requests to join the community" });
                }
            } else {
                return res.status(200).json({ success: false, message: "No such group" });
            }
        }).catch((err) => {
            console.log("ERROR IN APPROVAL OF REQUEST 4", err);
            return res.status(200).json({ success: false, message: "Something went wrong!!!" });
        })
    } catch (error) {
        console.log("ERROR IN APPROVAL OF REQUEST", error);
        return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
    }

}

//Fetching of Requests
subscriptionController.getRequestListOfUserForParticularGroup = async (req, res) => {
    try {
        var groupId = req.params.groupId;
        var userId = req.payload._id;
        GroupModel.findOne({ _id: groupId }).then(async (foundGroup) => {
            if (foundGroup.userid.toString() === userId.toString()) {
                GroupJoinRequestsModel.find({ groupid: groupId }).then((requestsOfTheGroup) => {
                    if (requestsOfTheGroup.length <= 0) {
                        res.status(200).json({ success: true, message: [] });
                    } else {
                        res.status(200).json({ success: true, message: requestsOfTheGroup });
                    }
                }).catch((error) => {
                    res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
                })
            } else {
                res.status(200).json({ success: false, message: [] });
            }
        }).catch((err) => {
            console.log("ERROR IN TRY BLOCK OF GET REQUEST LIST OF USER FOR PARTICULAR GROUP", err);
            res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
        })
    } catch (error) {
        console.log("ERRORN IN GET REQUEST LIST OF USER FOR PARTICULAR GROUP", error);
        res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
    }
}

//Blocking Of user from a group
subscriptionController.blockingOfUser = async (req, res) => {
    try {
        var groupAdmin = req.payload._id;
        var groupId = req.params.groupid;
        var userToBeBlocked = req.params.userToBeBlocked;

        GroupModel.findOne({ _id: groupId }).then((foundGroup) => {
            // console.log("FOUND GROUP", foundGroup);
            if (foundGroup) {
                var admin = foundGroup.userid;
                if (admin.toString() === groupAdmin.toString()) {
                    GroupSubscriptionModel.findOne({ groupid: foundGroup._id, userid: userToBeBlocked, status: "open" }).then((subscribedUser) => {
                        // console.log("SUBSCRIBED USER", subscribedUser);
                        UserModel.findOne({ _id: userToBeBlocked }, { _id: 1, name: 1 }).then((foundUser) => {
                            if (foundUser) {
                                if (subscribedUser) {
                                    var blockingObject = {
                                        groupid: foundGroup._id,
                                        groupName: foundGroup.name,
                                        userid: foundUser._id,
                                        userName: foundUser.name
                                    }
                                    if (foundGroup.type === "free") {
                                        BlockedUserModel.create(blockingObject).then((blockedUser) => {
                                            // console.log("BLOCKED USER", blockedUser);
                                            if (blockedUser) {
                                                GroupSubscriptionModel.remove({ _id: subscribedUser._id }, function (error, removedUser) {
                                                    if (error) {
                                                        console.log("ERROR IN REMOOVING USER FROM GROUP", error);
                                                        res.status(200).json({ success: false, message: "Error in removing user from group" });
                                                    } else {
                                                        res.status(200).json({ success: true, message: "Blocked!!" })
                                                    }
                                                });
                                            }
                                        }).catch((err) => {
                                            console.log("ERROR IN BLOCKING OF USER 4", err);
                                            res.status(200).json({ success: false, message: "Something went wrong!!!" });
                                        })
                                    } else if (foundGroup.type === "paid") {
                                        BlockedUserModel.create(blockingObject).then((blockedUser) => {
                                            // console.log("BLOCKED USER", blockedUser);
                                            if (blockedUser) {
                                                var updateObject = {
                                                    autoPayement: false
                                                };
                                                GroupSubscriptionModel.update({ _id: subscribedUser._id }, updateObject, function (error, updateUserSubscription) {
                                                    if (error) {
                                                        console.log("ERROR IN UPDATING AUTOPAYMENT", error)
                                                        res.status(200).json({ success: false, message: "Something went wrong" });
                                                    } else {
                                                        res.status(200).json({ success: true, message: "Blocked!!" });
                                                    }
                                                });
                                            } else {
                                                console.log("ERROR IN BLOCKING USER");
                                                res.status(200).json({ success: false, message: "Error in blocking user, please try after sometime" });
                                            }
                                        }).catch((err) => {
                                            console.log("ERROR IN BLOCKING OF USER 5", err);
                                            res.status(200).json({ success: false, message: "Something went wrong!!!" });
                                        })
                                    }
                                } else {
                                    // console.log("USER IS NOT PRESENT IN THIS GROUP");
                                    res.status(200).json({ success: false, message: "User is not present in this group" });
                                }
                            } else {
                                // console.log("NO SUCH USER");
                                res.status(200).json({ success: false, message: "No such user present" });
                            }
                        }).catch((err) => {
                            console.log("ERROR IN BLOCKING OF USER 3", err);
                            res.status(200).json({ success: false, message: "Something went wrong!!!" });
                        })
                    }).catch((err) => {
                        console.log("ERROR IN BLOCKING OF USER 2", err);
                        res.status(200).json({ success: false, message: "Something went wrong!!!" });
                    })
                } else {
                    res.status(200).json({ success: false, message: "Only group admin is allowed to block user" });
                }
            } else {
                res.status(200).json({ success: false, message: "No such group present" });
            }
        }).catch((err) => {
            console.log("ERROR IN BLOCKING OF USER 1", err);
            res.status(200).json({ success: false, message: "Something went wrong!!!" });
        })
    } catch (error) {
        console.log("ERROR IN BLOCKING OF USER", error);
        return res.status(200).status({ success: false, message: "Something went wrong!!!" })
    }
}

// Unblocking of User
subscriptionController.unblockingOfUser = async (req, res) => {
    try {
        var groupAdmin = req.payload._id;
        var groupId = req.body.groupid;
        var userToBeUnblocked = req.body.userToBeUnblocked;
        GroupModel.findOne({ _id: groupId }).then((foundGroup) => {
            if (foundGroup) {
                var admin = foundGroup.userid;
                if (admin.toString() === groupAdmin.toString()) {
                    BlockedUserModel.findOne({ groupid: groupId, userid: userToBeUnblocked }).then((foundUnblockedUser) => {
                        if (foundUnblockedUser) {
                            BlockedUserModel.remove({ _id: foundUnblockedUser._id }, function (error, unblockedUser) {
                                if (error) {
                                    return res.status(200).json({ success: false, message: "Error in unblocking of user" });
                                } else {
                                    return res.status(200).json({ success: true, message: "User unblocked!!!" });
                                }
                            });
                        } else {
                            return res.status(200).json({ success: false, message: "No such blocked user is found" });
                        }
                    }).catch((err) => {
                        console.log("ERROR IN SOME PARAMETERS", err);
                        res.status(200).json({ success: false, message: "Something went wrong!!!" });
                    })
                } else {
                    res.status(200).json({ success: false, message: "Only group admin can unblock a user from a group" });
                }
            } else {
                res.status(200).json({ success: false, message: "No such group present" });
            }
        }).catch((err) => {
            console.log("ERROR IN CATCH OF PROMISE", err);
            res.status(200).json({ success: false, message: "Something went wrong!!!" });
        })
    } catch (error) {
        console.log("ERROR IN UNBLOCKING OF USER", error);
        return res.status(200).json({ success: false, message: "Something went wrong!!!" });
    }
}

//Removing of user from a group
subscriptionController.removingOfUserFromAGroup = async (req, res) => {
    try {
        var groupAdmin = req.payload._id;
        var groupId = req.params.groupid;
        var userToBeRemoved = req.params.userToBeRemoved;
        GroupModel.findOne({ _id: groupId }).then((foundGroup) => {
            if (foundGroup) {
                var admin = foundGroup.userid;
                if (admin.toString() === groupAdmin.toString()) {
                    GroupSubscriptionModel.findOne({ groupid: foundGroup._id, userid: userToBeRemoved, status: "open" }).then((subscribedUser) => {
                        UserModel.findOne({ _id: userToBeRemoved }, { _id: 1 }).then((foundUser) => {
                            if (foundUser) {
                                if (subscribedUser) {
                                    if (foundGroup.type === "free") {
                                        GroupSubscriptionModel.remove({ _id: subscribedUser._id }, function (error, removedUser) {
                                            if (error) {
                                                console.log("ERROR IN REMOVING USER FROM GROUP", error);
                                                res.status(200).json({ success: false, message: "Error in removing user from group" });
                                            } else {
                                                res.status(200).json({ success: true, message: "Removed user from the group" })
                                            }
                                        });
                                    } else if (foundGroup.type === "paid") {
                                        var updateObject = {
                                            autoPayement: false
                                        };
                                        GroupSubscriptionModel.update({ _id: subscribedUser._id }, updateObject, function (error, updateUserSubscription) {
                                            if (error) {
                                                console.log("ERROR IN UPDATING AUTOPAYMENT", error)
                                                res.status(200).json({ success: false, message: "Something went wrong" });
                                            } else {
                                                res.status(200).json({ success: true, message: "You cannot remove the user as this is a paid group, but the auto-payment is removed if the user had selected auto-repayment" });
                                            }
                                        });
                                    }
                                } else {
                                    res.status(200).json({ success: false, message: "User is present in this group" });
                                }
                            } else {
                                res.status(200).json({ success: false, message: "No such user present" });
                            }
                        }).catch((err) => {
                            console.log("ERROR IN REMOVING OF USER 3", err);
                            res.status(200).json({ success: false, message: "Something went wrong!!!" });
                        })
                    }).catch((err) => {
                        console.log("ERROR IN REMOVING OF USER 2", err);
                        res.status(200).json({ success: false, message: "Something went wrong!!!" });
                    })
                } else {
                    res.status(200).json({ success: false, message: "Only group admin is allowed to remove user" });
                }
            } else {
                res.status(200).json({ success: false, message: "No such group present" });
            }
        }).catch((err) => {
            console.log("ERROR IN REMOVING OF USER 1", err);
            res.status(200).json({ success: false, message: "Something went wrong!!!" });
        })
    } catch (error) {
        console.log("ERROR IN REMOVING OF USER", error);
        return res.status(200).status({ success: false, message: "Something went wrong!!!" })
    }
}

// groups followed by user
subscriptionController.groupFollowedByParticularUser = async (req, res) => {
    var userid = mongoose.Types.ObjectId(req.payload._id);
    var finalArray = [];
    GroupSubscriptionModel.aggregate([
        {
            $match: {
                userid: userid,
                status: "open"
            }
        },
        {
            $lookup: {
                from: "groupsbyusers",
                localField: "groupid",
                foreignField: "_id",
                as: "groupInfo"
            }
        },
        {
            $unwind: '$groupInfo'
        },
        {
            $project: {
                "_id": 0,
                "groupInfo._id": 1,
                "groupInfo.name": 1,
                "groupInfo.Gtype": 1,
                "groupInfo.groupImage": 1
            }
        }
    ]).then((groupsFollowedByUser) => {
        // console.log("GROUPS FOLLOWED BY USER", groupsFollowedByUser);
        if (groupsFollowedByUser.length > 0) {
            groupsFollowedByUser.forEach(element => {
                finalArray.push(element.groupInfo);
            });
        }
        return res.status(200).json({ success: true, message: finalArray });
    }).catch((errorInFIndingGroupsOfUser) => {
        console.log("ERROR IN FINDING FOLLOWED GROUPS OF USER", errorInFIndingGroupsOfUser);
        return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime!!! " });
    })
}

//followers Of a group
subscriptionController.followersOfAGroup = async (req, res) => {
    var groupId = mongoose.Types.ObjectId(req.params.groupId);
    var userid = req.payload._id;
    var isAdmin = false;
    GroupSubscriptionModel.aggregate([
        {
            $match: {
                groupid: groupId
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "userid",
                foreignField: "_id",
                as: "userInfo"
            }
        },
        {
            $unwind: "$userInfo"
        },
        {
            $project: {
                "_id": 0,
                subscriptionid: 1,
                "userInfo._id": 1,
                "userInfo.email": 1,
                "userInfo.name": 1,
                "userInfo.avatar": 1
            }
        }
    ]).then((membersOfGroup) => {
        if (membersOfGroup <= 0) {
            return res.status(200).json({ success: true, message: { isAdmin: isAdmin, membersOfGroup: [] } });
        } else {
            var firstElement = membersOfGroup[0];
            var groupCreatorId = firstElement.subscriptionid.toString();
            if (groupCreatorId === userid) {
                isAdmin = true;
            } else {
                isAdmin = false;
            }
            return res.status(200).json({ success: true, message: { isAdmin: isAdmin, membersOfGroup: membersOfGroup } });
        }
    }).catch((errorInFindingMembersOfGroup) => {
        console.log("ERROR IN FINDING MEMBERS OF GROUP", errorInFindingMembersOfGroup);
        return res.status(200).json({ success: false, message: "Error in fetching members of group" });
    })
}

// sent Requests to  group by a user
subscriptionController.groupJoiningRequestsOfAUser = async (req, res) => {
    var userid = mongoose.Types.ObjectId(req.payload._id);
    GroupJoinRequestsModel.aggregate([
        {
            $match: {
                userid: userid
            }
        },
        {
            $lookup: {
                from: "groupsbyusers",
                localField: "groupid",
                foreignField: "_id",
                as: "groupInfo"
            }
        },
        {
            $unwind: "$groupInfo"
        },
        {
            $project: {
                "groupInfo._id": 1,
                "groupInfo.name": 1,
                "groupInfo.groupImage": 1,
                "groupInfo.userid": 1
            }
        }
    ]).then((sentRequests) => {
        return res.status(200).json({ success: true, message: sentRequests });
    }).catch((errorInGettingSentRequests) => {
        console.log("ERROR IN GETTING SENT REQUESTS", errorInGettingSentRequests);
        return res.status(200).json({ success: false, message: "Something went wrong, please try again after sometime!!!" });
    })
}

subscriptionController.groupInvitesOfUser = async (req, res) => {
    try {
        var userid = mongoose.Types.ObjectId(req.payload._id);
        GroupInvitationModel.find({ userid: userid }).then((invitationsOfUser) => {
            console.log("invitationsOfUser", invitationsOfUser);
            if (invitationsOfUser > 0) {
                return res.status(200).json({ success: true, message: invitationsOfUser });
            } else {
                return res.status(200).json({ success: true, message: [] });
            }
        }).catch((errorInGettingGroupInvitations) => {
            console.log("errorInGettingGroupInvitations", errorInGettingGroupInvitations);
            return res.status(200).json({ success: false, message: "Something went wrong, please try again after sometime!!!" });
        })
    } catch (error) {
        console.log("ERROR IN GROUP INVITES OF USER", error);
        return res.status(200).json({ success: false, message: "Something went wrong, please try again after sometime!!!" });
    }
}

var ctrl = module.exports = subscriptionController;