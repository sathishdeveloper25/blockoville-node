// Npm-modules
var multer = require('multer');
var mongoose = require('mongoose')
var fs = require('fs');
var shell = require('shelljs');
var pathmodule = require('path');
var Users = require('../model/user');
const async = require('async');
var AWS = require('aws-sdk');
var s3 = new AWS.S3({ accessKeyId: ' AKIAIH5JPE6VQEURBEMQ', secretAccessKey: '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', region: "ap-south-1" });
var myBucket = "blockoville.s3.amazonaws.com";
var postController = [];
var restAPiCtrl = require('../controller/restApiController');

// Models
const posts = require('../model/userPosts');
const likes = require('../model/post_like');
const comments = require('../model/post_comment');
const content = require('../model/post_medias');
const FollowerModel = require('../model/following');
const postpayement = require('../model/postpayment');
const myPurchase = require('../model/mypurchase');
const BlockedUserModel = require("../model/blockUserFromGroup");
const GroupFollowingModel = require('../model/subscription');
const userprofilemsg = require('../model/userprofilemsg');
const GroupJoinRequestsModel = require("../model/groupJoinRequest");
const postReport = require('../model/report');
const ImageSupported = ['.png', '.jpg', '.jpeg', '.gif', '.PNG', '.JPG', '.JPEG', '.GIF'];
const VideoSupported = ['.3g2', '.3gp', '.3gpp', '.asf', '.avi ', '.dat', '.divx', '.dv', '.f4v', '.flv', '.m2ts', '.m4v', '.mkv',
    '.mod', '.mov', '.mp4', '.mpe', '.mpeg ', '.mpeg4', '.mpg', '.mts', '.nsv', '.ogm', '.ogv ', '.qt', '.tod', '.ts', '.vob', '.wmv'];
const fileattach = ['.pdf', '.txt', '.doc'];
const GroupModel = require('../model/groupByUsers');
const socketFile = require("../socket");

var currencyPercentage = {
    USD: 1,
    EUR: 2,
    BCH: 3,
    BTC: 4
};
// Multer storage define for-imagePost
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        var dir = "./public/uploads/userpost/imageUpload";
        if (!fs.existsSync(dir)) {
            shell.mkdir('-p', dir);
        }
        cb(null, "./public/uploads/userpost/imageUpload"); // where to store it
    },
    filename: function (req, file, cb) {
        if (file.fileSize) {
            var err = new Error();
            return cb(err);
        }
        else if (!file.originalname.toLowerCase().match(/\.(png|jpg|jpeg|gif|mp4|webm|ogg|pdf|doc|txt)$/)) {
            var err = new Error();
            err.code = 'fieltype'; // to check on file type
            return cb(err, null);
        } else {
            var date = new Date();
            var timeStamp = date.getTime();
            var name = file.originalname.toLowerCase();
            var ext = name.substr(file.originalname.lastIndexOf('.') + 1)
            var rename = timeStamp + "." + ext;
            cb(null, rename);
        }
    }
});

var upload = multer({
    storage: storage,
    limits: { fileSize: 20971520 }
}).any();



var ioSocket = {};

postController.loadSocketIO = function (socketIO) {
    if (socketIO) {
        ioSocket = socketIO;
        // console.log(" pOST iosocket------------------------------------>", ioSocket);
    }
};

/************************************* UserProfile post, like and comment work here *****************************************/
// UserProfile post
postController.userpost = function (req, res, next) {
    try {

        console.log('req.body, req.payload', req.body);
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            upload(req, res, async function (err) {
                if (err) {
                    throw err;
                }
                else {
                    var userid = req.payload._id;
                    var text = req.body.text;
                    var subject = req.body.subject;
                    var filesArr = req.files;
                    var privacy = req.body.privacy;
                    var groupid = req.body.groupid;
                    var taggedPeople = JSON.parse(req.body.taggPeople);
                    var payements;
                    var status = 'active';
                    var profileObj = {};
                    var errorObj = {};
                    if (typeof userid !== 'undefined' && userid && userid !== '' &&
                        typeof subject !== 'undefined' && subject && subject !== '' &&
                        typeof text !== 'undefined' && text && text !== '' &&
                        typeof privacy !== 'undefined' && privacy && privacy !== '' &&
                        typeof status !== 'undefined' && status && status !== ''
                    ) {
                        profileObj.userid = userid;
                        profileObj.subject = subject;
                        profileObj.text = text;
                        profileObj.privacy = privacy;
                        profileObj.status = status;

                        if (taggedPeople !== '' && taggedPeople !== undefined && taggedPeople !== null && taggedPeople) {
                            profileObj.taggedPeople = taggedPeople;
                        }

                        if (typeof wallet !== 'undefined' && wallet && wallet !== '') {
                            payements = true;
                        }
                        if (typeof groupid !== 'undefined' && groupid && groupid !== 'null') {
                            profileObj.groupid = groupid;
                            await GroupModel.findOne({ _id: groupid }).then((foundGroup) => {
                                if (foundGroup) {
                                    if (foundGroup.privacy === "public" && foundGroup.type === "free") {
                                        profileObj.payements = false;
                                    } else if (foundGroup.privacy === "private" && foundGroup.type === "free") {
                                        profileObj.payements = false;
                                    } else {
                                        profileObj.payements = true;
                                    }
                                } else {
                                    return res.status(200).json({ success: false, message: "Some error is there while posting in the post" });
                                }
                            }).catch((errorInFindingGroup) => {
                                console.log("ERROR IN FINDING GROUP WHILE POSTING", errorInFindingGroup);
                                return res.status(200).json({ success: false, message: "Some error is there while posting in the post" });
                            })
                        }
                        else {
                            profileObj.payements = false;
                        }

                        posts.create(profileObj, function (error, postrows) {
                            if (error) {
                                res.status(200).json({ success: false, message: error.message });
                            } else {
                                var postid = postrows._id;
                                if (filesArr && filesArr.length > 0) { // Change "VS"
                                    var counter = 0;
                                    var contentObj = [];
                                    var totalFiles = filesArr.length;
                                    for (let i = 0; i < filesArr.length; i++) {
                                        fs.readFile(filesArr[i].path, function (err, data) {
                                            var path = filesArr[i].path.replace("public", '').substr(1);
                                            var ext = pathmodule.extname(path).toString();
                                            var type = '';
                                            if (fileattach.indexOf(ext) !== -1) {
                                                type = 'Files'
                                            }
                                            if (ImageSupported.indexOf(ext) !== -1) {
                                                type = 'Image'
                                            }
                                            if (VideoSupported.indexOf(ext) !== -1) {
                                                type = 'Video'
                                            }
                                            var params = {
                                                Bucket: myBucket,
                                                Key: filesArr[i].filename,
                                                Body: data,
                                                ACL: "public-read"
                                            };
                                            s3.upload(params, function (err, data) {
                                                if (err) {
                                                    console.log("Error", err);
                                                }
                                                if (data) {
                                                    contentObj.push({
                                                        content_url: data.Location,
                                                        contenttype: type,
                                                        postid: postid,
                                                        userid: userid
                                                    });
                                                    counter++;
                                                    if (counter === totalFiles) {
                                                        content.create(contentObj, function (error, conetnResp) {
                                                            if (error) {
                                                                res.status(200).json({ success: false, message: error });
                                                            } else {
                                                                var currency = req.body.currency;
                                                                var wallet = req.body.wallet;
                                                                var amount = req.body.amount;
                                                                var paymentObj = {};
                                                                var errorObj = {};

                                                                if (typeof postid !== 'undefined' && postid && postid !== '' &&
                                                                    typeof currency !== 'undefined' && currency && currency !== '' &&
                                                                    typeof amount !== 'undefined' && amount && amount !== ''
                                                                ) {
                                                                    paymentObj.userid = userid;
                                                                    paymentObj.postid = postid;
                                                                    paymentObj.currency = currency;
                                                                    paymentObj.amount = amount;

                                                                    if (typeof wallet !== 'undefined' && wallet && wallet !== '') {
                                                                        paymentObj.wallet = wallet;
                                                                    }
                                                                    postpayement.create(paymentObj, function (error, paymentObj) {
                                                                        if (error) {
                                                                            res.status(200).json({ success: false, message: error });
                                                                        }
                                                                        else {
                                                                            posts.findOneAndUpdate({ _id: mongoose.Types.ObjectId(postrows._id) }, { payements: true }, function (error, updatepayment) {
                                                                                if (error) {
                                                                                    res.status(200).json({ success: false, message: error });
                                                                                }
                                                                                else {
                                                                                    for (let a = 0; a < filesArr.length; a++) {
                                                                                        var filePath = filesArr[a].path;
                                                                                        fs.unlinkSync(filePath);
                                                                                        if (a === (filesArr.length - 1)) {
                                                                                            res.status(200).json({ success: true, message: conetnResp });
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                } else {
                                                                    for (let a = 0; a < filesArr.length; a++) {
                                                                        var filePath = filesArr[a].path;
                                                                        fs.unlinkSync(filePath);
                                                                        if (a === (filesArr.length - 1)) {
                                                                            let postData = Object.assign({}, postrows._doc);
                                                                            postData.contents = conetnResp;
                                                                            postData.likesCount = 0;
                                                                            postData.likeActive = 0;
                                                                            postData.groupdata = [];

                                                                            Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, { _id: 1, name: 1, avatar: 1 }).exec(function (error, profile) {
                                                                                if (!error && profile !== null) {
                                                                                    postData.userinfo = profile;
                                                                                    res.status(200).json({ success: true, post: postData });
                                                                                } else {
                                                                                    res.status(200).json({ success: true, post: postData });
                                                                                }
                                                                            });
                                                                            // res.status(200).json({ success: true, message: conetnResp });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            });
                                        })
                                    }
                                } else {
                                    var currency = req.body.currency;
                                    var wallet = req.body.wallet;
                                    var amount = req.body.amount;
                                    var paymentObj = {};
                                    var errorObj = {};

                                    if (typeof postid !== 'undefined' && postid && postid !== '' &&
                                        typeof currency !== 'undefined' && currency && currency !== '' &&
                                        typeof amount !== 'undefined' && amount && amount !== ''
                                    ) {
                                        paymentObj.userid = userid;
                                        paymentObj.postid = postid;
                                        paymentObj.currency = currency;
                                        paymentObj.amount = amount;
                                        if (typeof wallet !== 'undefined' && wallet && wallet !== '') {
                                            payements = true;
                                            paymentObj.wallet = wallet;
                                        }
                                        postpayement.create(paymentObj, function (error, paymentObj) {
                                            if (error) {
                                                res.status(200).json({ success: false, message: error });
                                            }
                                            else {
                                                posts.findOneAndUpdate({ _id: postrows._id }, { payements: true },
                                                    function (error, updatepayment) {
                                                        if (error) {
                                                            res.status(200).json({ success: false, message: error });
                                                        }
                                                        else {

                                                            let postData = Object.assign({}, postrows._doc);
                                                            postData.contents = [];
                                                            postData.likesCount = 0;
                                                            postData.likeActive = 0;
                                                            postData.groupdata = [];

                                                            Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, { _id: 1, name: 1, avatar: 1 }).exec(function (error, profile) {
                                                                if (!error && profile !== null) {
                                                                    postData.userinfo = profile;
                                                                    res.status(200).json({ success: true, post: postData });
                                                                } else {
                                                                    res.status(200).json({ success: true, post: postData });
                                                                }
                                                            });
                                                        }
                                                    })
                                            }
                                        })
                                    } else {
                                        let postData = Object.assign({}, postrows._doc);
                                        postData.contents = [];
                                        postData.likesCount = 0;
                                        postData.likeActive = 0;
                                        postData.groupdata = [];

                                        Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, { _id: 1, name: 1, avatar: 1 }).exec(function (error, profile) {
                                            if (!error && profile !== null) {
                                                postData.userinfo = profile;
                                                res.status(200).json({ success: true, post: postData });
                                            } else {
                                                res.status(200).json({ success: true, post: postData });
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                    else {
                        errorObj.text = 'Message is required';
                        errorObj.subject = 'Subject is required';
                        errorObj.privacy = 'privacy is required';
                        errorObj.currency = 'currency is required';
                        errorObj.wallet = 'wallet is required';
                        errorObj.amount = 'amount is required';
                        res.status(400).json({ message: errorObj });
                    }
                }
            });
        }
    }
    catch (e) {
        console.log(e);
        res.status(500).json({ message: e.message })
    }
}

// UserProfile post get
postController.getData = function (req, res) {
    try {

        var userid = req.query.userid;
        Users.aggregate([
            {
                $match: {
                    '_id': mongoose.Types.ObjectId(userid)
                }
            },
            {
                $lookup: {
                    from: 'followings',
                    localField: '_id',
                    foreignField: 'userid',
                    as: 'followers'
                }
            }
        ]).sort({ createdAt: -1 }).exec(function (error, userObj) {
            if (error) {
                res.status(500).json({ message: error })
            }
            else {
                var follow = [];
                console.log("userObj", userObj);
                if (userObj.length > 0) {
                    follow.push(userObj[0]._id);
                    var followers = userObj[0].followers;
                    if (followers.length > 0) {
                        followers.forEach(function (follower) {
                            if (follower.request == 'accepted') {
                                follow.push(follower.followerid);
                            }
                        });
                    }

                }

                var followInString = [];
                follow.forEach((followElement) => {
                    followInString.push(followElement.toString());
                })

                if (follow.length > 0) {
                    posts.aggregate([
                        {
                            $match: {
                                "$or": [
                                    {
                                        "parentid": { $exists: false },
                                        "commentid": { $exists: false },
                                        "status": 'active',
                                        "userid": { $in: follow },
                                        "privacy": { $ne: 'Onlyme' }
                                    },
                                    {
                                        "parentid": { $exists: false },
                                        "commentid": { $exists: false },
                                        "status": 'active',
                                        "userid": mongoose.Types.ObjectId(userid)
                                    },
                                    {
                                        "sharedBy": { $in: follow }
                                    },
                                    {
                                        "taggedPeople": {
                                            $elemMatch: {
                                                _id: { $in: follow }
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            "$lookup": {
                                from: 'post_medias',
                                localField: '_id',
                                foreignField: 'postid',
                                as: 'contents'
                            }
                        },
                        {
                            "$lookup": {
                                from: 'post_likes',
                                localField: '_id',
                                foreignField: 'postid',
                                as: 'likes'
                            }
                        },
                        {
                            "$lookup": {
                                from: 'post_comments',
                                localField: '_id',
                                foreignField: 'postid',
                                as: 'comments'
                            }
                        },
                        {
                            "$lookup": {
                                from: 'userposts',
                                localField: '_id',
                                foreignField: 'sharedPost',
                                as: 'shares'
                            }
                        },
                        {
                            "$lookup": {
                                from: 'groupsbyusers',
                                localField: 'groupid',
                                foreignField: '_id',
                                as: 'groupdata'
                            }
                        },
                        {
                            "$lookup": {
                                from: 'users',
                                localField: 'sharedBy',
                                foreignField: '_id',
                                as: 'sharedUserInfo'
                            }
                        },
                        {
                            "$lookup": {
                                from: 'users',
                                localField: 'userid',
                                foreignField: '_id',
                                as: 'userinfo'
                            }
                        },
                        {
                            '$unwind': {
                                path: '$userinfo'
                            }
                        },
                        {
                            "$sort": {
                                "createdAt": -1
                            }
                        },
                        {
                            "$skip": Number(req.query.limit) * (Number(req.query.page) - 1)
                        },
                        {
                            "$limit": Number(req.query.limit)
                        },
                        {
                            "$project": {
                                _id: 1,
                                text: 1,
                                subject: 1,
                                privacy: 1,
                                groupid: 1,
                                userid: 1,
                                createdAt: 1,
                                payements: 1,
                                taggedPeople: 1,
                                sharedBy: 1,
                                sharedOn: 1,
                                sharedText: 1,
                                sharedUser: { $arrayElemAt: [ "$sharedUserInfo", 0 ] },
                                "likesCount": { $size: "$likes" },
                                "cmtsCount": { $size: "$comments" },
                                "shareCount": { $size: "$shares" },
                                "likeActive": {
                                    $size: {
                                        $filter: {
                                            input: "$likes",
                                            as: "item",
                                            cond: { $eq: ["$$item.userid", mongoose.Types.ObjectId(userid)] }
                                        }
                                    }
                                },
                                "contents": 1,
                                "groupdata": 1,
                                "userinfo._id": 1,
                                "userinfo.name": 1,
                                "userinfo.avatar": 1,
                                // "sharedUser._id": 1,
                                // "sharedUser.name": 1,
                                // "sharedUser.avatar": 1
                            }
                        }
                    ]).sort({ createdAt: -1 }).exec(function (error, postlist) {
                        if (error) {
                            res.status(404).json({ message: error.message });
                        } else {
                            var commonPeople = [];
                            // console.log("POST LISTS", postlist)
                            var postids = [];
                            postlist.forEach((postListElement) => {
                                // console.log(followInString.indexOf(postListElement.userid.toString()));
                                if (followInString.indexOf(postListElement.userid.toString()) < 0) {
                                    postListElement.taggedPeople.forEach((taggedPeopleElement) => {
                                        if (followInString.indexOf(taggedPeopleElement._id.toString()) >= 0) {
                                            commonPeople.push(taggedPeopleElement);
                                        }
                                        postListElement.commonPeople = commonPeople;
                                    })
                                } else {
                                    postListElement.commonPeople = [];
                                }
                            })
                            // console.log("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO", postlist);
                            postlist.forEach((post) => {
                                postids.push(post._id);
                            });
                            if (postids.length > 0) {
                                posts.aggregate([
                                    {
                                        "$match": {
                                            parentid: { $in: postids },
                                            "status": 'active'
                                        }
                                    },
                                    {
                                        "$lookup": {
                                            from: 'users',
                                            localField: 'userid',
                                            foreignField: '_id',
                                            as: 'userinfo'
                                        }
                                    },
                                    {
                                        '$unwind': {
                                            path: '$userinfo'
                                        }
                                    },
                                    {
                                        "$project": {
                                            _id: 1,
                                            text: 1,
                                            userid: 1,
                                            parentid: 1,
                                            createdAt: 1,
                                            "userinfo._id": 1,
                                            "userinfo.name": 1,
                                            "userinfo.avatar": 1
                                        }
                                    },
                                ]).sort({ createdAt: 1 }).exec(function (error, postcommentlist) {
                                    if (error) {
                                        res.status(404).json({ message: error.message });
                                    } else {
                                        if (postcommentlist.length > 0) {
                                            var wallpost = {};
                                            postcommentlist.forEach(function (item) {
                                                if (Object.keys(wallpost).indexOf(item.parentid.toString()) === -1) {
                                                    wallpost[item.parentid] = [item];
                                                } else {
                                                    wallpost[item.parentid].push(item)
                                                }
                                            });
                                            var reply = [];
                                            postcommentlist.forEach(element => {
                                                reply.push(element._id);
                                            });

                                            if (reply.length > 0) {
                                                posts.aggregate([
                                                    {
                                                        "$match": {
                                                            commentid: { $in: reply },
                                                            "status": 'active'
                                                        }
                                                    },
                                                    {
                                                        "$lookup": {
                                                            from: 'users',
                                                            localField: 'userid',
                                                            foreignField: '_id',
                                                            as: 'userinfo'
                                                        }
                                                    },
                                                    {
                                                        '$unwind': {
                                                            path: '$userinfo'
                                                        }
                                                    },
                                                    {
                                                        "$project": {
                                                            _id: 1,
                                                            userid: 1,
                                                            commentid: 1,
                                                            createdAt: 1,
                                                            replyComment: 1,
                                                            "userinfo._id": 1,
                                                            "userinfo.name": 1,
                                                            "userinfo.avatar": 1
                                                        }
                                                    },
                                                ]).sort({ createdAt: 1 }).exec(function (error, CommentreplyObj) {
                                                    if (error) {
                                                        res.status(404).json({ message: error.message });
                                                    } else {
                                                        if (CommentreplyObj.length > 0) {
                                                            var replyCommentPost = {}
                                                            CommentreplyObj.forEach(function (item) {
                                                                if (Object.keys(replyCommentPost).indexOf(item.commentid.toString()) === -1) {
                                                                    replyCommentPost[item.commentid] = [item];
                                                                } else {
                                                                    replyCommentPost[item.commentid].push(item)
                                                                }
                                                            });

                                                            res.status(200).json({ post: postlist, content: wallpost, reply: replyCommentPost });
                                                        } else {
                                                            res.status(200).json({ post: postlist, content: wallpost, reply: {} });
                                                        }
                                                    }
                                                });
                                            }
                                        } else {
                                            res.status(200).json({ post: postlist, content: {}, reply: {} });
                                        }
                                    }
                                });
                            } else {
                                res.status(200).json({ post: postlist, content: {}, reply: {} });
                            }
                        }
                    });
                } else {
                    res.status(200).json({ post: {}, content: {}, reply: {} });
                }
            }
        });
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}

// getPayement
postController.getpayement = function (req, res, next) {
    try {
        postpayement.findOne({ postid: req.params.id }, function (error, payementObj) {
            if (error) {
                res.status(404).json({ message: error.message });
            } else {
                res.status(200).json(payementObj);
            }
        })
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

// UserProfile post delete
postController.deleteData = function (req, res, next) {
    try {
        var id = req.params.id;
        var errorObj = {};
        var profileObj = {};
        var removePost = false;
        var user = req.payload._id;
        var userType = req.payload.group;
        if (typeof id !== 'undefined' && id && id !== '') {
            profileObj.id = id;
            posts.findOne({ _id: mongoose.Types.ObjectId(id) }).exec(async function (error, postdetails) {
                if (error) {
                    res.status(500).json({ message: error.message });
                } else {
                    if (postdetails) {
                        if (postdetails.userid == user) {
                            removePost = true;
                        } else {
                            if (postdetails.groupid !== "" && postdetails.groupid !== null && postdetails.groupid !== undefined && postdetails.groupid) {
                                await GroupModel.findOne({ _id: postdetails.groupid }).then((groupInfo) => {
                                    if (user == groupInfo.userid) {
                                        removePost = true;
                                    }
                                }).catch((err) => {
                                    res.status(500).json({ message: "Something went wrong!!!" });
                                })
                            }
                        }
                        if (removePost === true) {
                            if (postdetails.payements) {
                                posts.findOneAndUpdate({ _id: mongoose.Types.ObjectId(id) }, { status: 'archieve' }, function (error, user) {
                                    if (error) {
                                        res.status(500).json({ message: error.message });
                                    }
                                    else {
                                        res.status(200).json({ message: 'Post Deleted Successfully' });
                                    }
                                });
                            } else {
                                content.remove({ postid: mongoose.Types.ObjectId(id) }, function (error, user) {
                                    if (error) {
                                        res.status(500).json({ message: error.message });
                                    } else {
                                        likes.remove({ postid: mongoose.Types.ObjectId(id) }, function (error, user) {
                                            if (error) {
                                                res.status(500).json({ message: error.message });
                                            } else {
                                                posts.remove({ _id: mongoose.Types.ObjectId(id), payements: false }, function (error, user) {
                                                    if (error) {
                                                        res.status(500).json({ message: error.message });
                                                    }
                                                    else {
                                                        posts.remove({ parentid: mongoose.Types.ObjectId(id) }, function (error, commentdelete) {
                                                            if (error) {
                                                                res.status(500).json({ message: error.message });
                                                            }
                                                            else {
                                                                posts.remove({ _id: mongoose.Types.ObjectId(id) }, function (error, deleteObj) {
                                                                    if (error) {
                                                                        res.status(500).json({ message: error.message });
                                                                    }
                                                                    else {
                                                                        res.status(200).json({ message: 'Post Deleted Successfully' });
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        } else {
                            res.status(500).json({ message: "You are not allowed to delete this post" });
                        }
                    }
                }
            })
        }
        else {
            errorObj.id = 'Id is required';
            res.status(400).json({ message: errorObj });
        }
    }
    catch (e) {
        res.status(500).json({ message: e.message });
    }
}

// UserProfile comments
postController.postreply = function (req, res, next) {
    try {
        var text = req.body.replyText;
        var userid = req.payload._id;
        var postid = req.body.postid;
        var status = 'active'
        var errorObj = {};
        var profileObj = {};

        profileObj.text = text[postid];
        profileObj.userid = userid;
        profileObj.parentid = postid;
        profileObj.status = status;
        profileObj.notification = 'unseen';

        if ((profileObj.text !== 'undefined' && profileObj.text && profileObj.text !== '') &&
            (typeof profileObj.userid !== 'undefined' && profileObj.userid && profileObj.userid !== '') &&
            (typeof profileObj.parentid !== 'undefined' && profileObj.parentid && profileObj.parentid !== '') &&
            (typeof profileObj.status !== 'undefined' && profileObj.status && profileObj.status !== '')
        ) {
            // console.log('profileObj ', profileObj);
            posts.create(profileObj, function (error, commentObj) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" });
                } else {
                    // console.log("COMMENT OBJECT", commentObj);

                    posts.findById(postid).then((foundPost) => {
                        console.log("FOUND POST DURING COMMENTING", foundPost);
                        socketFile.commentNotification(foundPost.userid, commentObj);
                    }).catch((error) => {
                        console.log("ERRROR IN FINDING POST WHILE COMMENTING", error);
                        res.status(200).json({ success: false, message: error });
                    })
                    res.status(200).json(commentObj);
                }
            });

        } else {
            errorObj.text = 'Comment is required';
            res.status(400).json({ message: errorObj.text });
        }
    }
    catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}
// Comment Reply
postController.commentReply = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var replyComment = req.body.replyComment;
        var commentid = req.body.commentid;
        var id = req.body.id;
        var status = 'active'
        var replyObj = {};
        var errorObj = {};
        replyObj.userid = userid;
        replyObj.replyComment = replyComment[id];
        replyObj.commentid = commentid;
        replyObj.status = status;
        replyObj.notification = "unseen";

        if ((replyObj.replyComment !== 'undefined' && replyObj.replyComment && replyObj.replyComment !== '') &&
            (typeof userid !== 'undefined' && userid && userid !== '') &&
            (typeof commentid !== 'undefined' && commentid && commentid !== '')
        ) {
            posts.create(replyObj, function (error, replyObj) {
                if (error) {
                    res.status(500).json({ message: "Bad request" });
                } else {
                    res.status(200).json(replyObj);
                }
            });

        } else {
            errorObj.text = 'Reply is required';
            res.status(400).json({ message: errorObj.text });
        }

    } catch (e) {
        console.log('Catch Error ', e);
        res.status(500).json({ message: "Internal Server Error" })
    }
}

// UserProfile post and comments Like
postController.postlike = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var postid = req.body.postid;
        var profileObj = {};
        var errorObj = {};

        if (typeof userid !== 'undefined' && userid && userid !== '' &&
            typeof postid !== 'undefined' && postid && postid !== ''
        ) {
            profileObj.userid = userid;
            profileObj.postid = postid;
            profileObj.notification = "unseen";

            likes.findOneAndDelete({ postid: postid, userid: userid }).exec(function (error, result) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" });
                } else {
                    if (result === null) {
                        likes.create(profileObj, function (error, user) {
                            if (error) {
                                res.status(500).json({ message: error });
                            } else {
                                res.status(200).json({ message: 'Like' });
                            }
                        });
                    } else {
                        res.status(200).json({ message: 'Dislike' });
                    }
                }
            })
        }
        else {
            errorObj.text = 'Message is required';
            res.status(400).json({ message: errorObj });
        }
    }
    catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}

postController.likeComment = function (req, res, next) {
    try {
        let userid = req.payload._id,
            commentId = req.body.commentId,
            profileObj = {},
            errorObj = {};

        if (userid && commentId) {
            profileObj.userid = userid;
            profileObj.commentId = commentId;
            profileObj.notification = "unseen";

            likes.findOneAndDelete({ commentId: commentId, userid: userid }).exec(function (error, result) {
                if (error) {
                    res.status(500).json({ message: "Internal Server Error" });
                } else {
                    if (result === null) {
                        likes.create(profileObj, function (error, user) {
                            if (error) {
                                res.status(500).json({ message: error });
                            } else {
                                res.status(200).json({ message: 'Like' });
                            }
                        });
                    } else {
                        res.status(200).json({ message: 'Dislike' });
                    }
                }
            })
        }
        else {
            errorObj.text = 'Message is required';
            res.status(400).json({ message: errorObj });
        }
    }
    catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}

postController.postcomment = function (req, res, next) {
    try {

        let userid = req.payload._id,
            postid = req.body.postid,
            comment = req.body.comment,
            profileObj = {},
            errorObj = {};

        if (userid && postid && comment) {
            profileObj.userid = userid;
            profileObj.text = comment;
            profileObj.postid = postid;
            profileObj.notification = "unseen";

            console.log("profileObj", profileObj);
            comments.create(profileObj, async function (error, comment) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    let commentData = {
                        _id: comment._id,
                        text: comment.text,
                        createdAt: comment.createdAt
                    };
                    await Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, { _id: 1, name: 1, avatar: 1 }).exec(function (error, profile) {
                        if (!error && profile !== null) {
                            commentData.userinfo = profile;
                            res.status(200).json({ comment: commentData, message: 'Comment Added' });
                        } else {
                            res.status(200).json({ comment: commentData, message: 'Comment Added' });
                        }
                    });
                }
            });

        } else {
            errorObj.text = 'Message is required';
            res.status(400).json({ message: errorObj });
        }
    }
    catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}

postController.replyComment = function (req, res, next) {
    try {

        let userid = req.payload._id,
            commentId = req.body.commentId,
            comment = req.body.comment,
            profileObj = {},
            errorObj = {};

        if (userid && commentId && comment) {
            profileObj.userid = userid;
            profileObj.text = comment;
            profileObj.commentId = commentId;
            profileObj.notification = "unseen";

            console.log("profileObj", profileObj);
            comments.create(profileObj, async function (error, comment) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    let commentData = {
                        _id: comment._id,
                        text: comment.text,
                        createdAt: comment.createdAt
                    };
                    await Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, { _id: 1, name: 1, avatar: 1 }).exec(function (error, profile) {
                        if (!error && profile !== null) {
                            commentData.userinfo = profile;
                            res.status(200).json({ comment: commentData, message: 'Comment Added' });
                        } else {
                            res.status(200).json({ comment: commentData, message: 'Comment Added' });
                        }
                    });
                }
            });

        } else {
            errorObj.text = 'Message is required';
            res.status(400).json({ message: errorObj });
        }
    }
    catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}

// UserProfile post and comments Get-like
postController.getlike = function (req, res, next) {
    try {
        likes.aggregate([
            {
                $group: {
                    _id: { postid: "$postid", userid: "$userid" },
                    count: { $sum: 1 }
                }
            }
        ]).exec(function (error, likeObj) {
            if (error) {
                res.status(404).json({ message: error.message });
            } else {
                var postlike = {};
                var ownlike = {};
                if (likeObj.length > 0) {
                    likeObj.forEach(function (item) {
                        var postIdstr = (item._id.postid).toString();
                        if (Object.keys(postlike).indexOf(postIdstr) === -1) {
                            postlike[postIdstr] = Number(item.count);
                        } else {
                            postlike[postIdstr] += Number(item.count);
                        }
                        ownlike[item._id.postid + '_' + item._id.userid] = item.count;
                    })
                }
                res.status(200).json({ postlike: postlike, ownlike: ownlike });
            }
        })
    }
    catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}

/***********************************Userprofile follower and following work here****************************/
// Follow request
postController.follow = function (req, res, next) {
    try {
        // var userid = req.query.followerid;
        // var followerid = req.payload._id;
        var userid = req.payload._id;
        var followerid = req.query.followerid;
        var profileObj = {};
        var errorObj = {};

        if (typeof userid !== 'undefined' && userid && userid !== '' &&
            typeof followerid !== 'undefined' && followerid && followerid !== ''
        ) {
            profileObj.userid = userid;
            profileObj.followerid = followerid;
            profileObj.request = "pending";
            profileObj.notification = 'unseen';
            if (followerid !== userid) {
                FollowerModel.findOne({ userid: mongoose.Types.ObjectId(userid), followerid: mongoose.Types.ObjectId(followerid) }, (error, followObj) => {
                    if (error) {
                        res.status(500).json({ message: error })
                    } else {
                        if (followObj === null) {
                            FollowerModel.create(profileObj, function (error, followerdata) {
                                if (error) {
                                    res.status(500).json({ message: error });
                                } else {
                                    res.status(200).json(followerdata);
                                }
                            });
                        } else {
                            res.status(500).json({ message: "Already sent request " });
                        }
                    }
                });
            } else {
                res.status(500).json({ message: "You can't send follow request to your self" });
            }
        }
        else {
            errorObj.user = 'userid is required';
            errorObj.followerid = 'followerid is required';
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}
// Get Follow
postController.fetchUserFollowerRecords = function (flag, userid, callback) {
    var filter = {};
    if (flag === 'follower') {
        filter.followerid = mongoose.Types.ObjectId(userid)
    } else {
        filter.userid = mongoose.Types.ObjectId(userid)
    }
    FollowerModel.count(filter).exec(function (error, records) {
        if (error) {
            return callback(error, null);
        } else {
            if (flag === 'follower') {
                var Obj = {
                    follower: records
                };
                return callback(null, Obj);
            } else {
                var Obj = {
                    following: records
                };
                return callback(null, Obj);
            }
        }
    })
}
postController.getfollowers = async function (req, res, next) {
    try {
        var id = req.params.id;
        var callbackArr = [];
        callbackArr.push(
            function (callback) {
                ctrl.fetchUserFollowerRecords('follower', id, function (error, result) {
                    return callback(null, result)
                })
            }
        )
        callbackArr.push(
            function (callback) {
                ctrl.fetchUserFollowerRecords('following', id, function (error, result) {
                    return callback(null, result)
                })
            }
        )
        async.parallel(callbackArr, function (err, results) {
            if (!err) {
                if (results.length > 1) {
                    var follower = results[0];
                    var following = results[1];
                    var finalObj = {};
                    finalObj[id] = {
                        follower: follower.follower,
                        following: following.following
                    }
                } else {
                    var finalObj = {};
                    finalObj[id] = results[0];
                }
                res.status(200).json(finalObj);
            }
        });
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}
// Get Follower through key
postController.getFollower = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var errorObj = {};

        if (typeof userid !== 'undefined' && userid && userid !== '') {
            FollowerModel.find({ userid: mongoose.Types.ObjectId(userid) }, function (error, followObj) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    var follow = {};
                    if (followObj.length > 0) {
                        followObj.forEach(function (item) {
                            follow[item.followerid] = item.userid;
                            // follow.push([item.userid]= [item]);
                        });
                    }
                    res.status(200).json(follow);
                }
            })
        } else {
            errorObj.userid = "Userid is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}

postController.getPendingRequests = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var errorObj = {};
        console.log("userid", userid);

        if (typeof userid !== 'undefined' && userid && userid !== '') {
            FollowerModel.aggregate([
                {
                    $match: {
                        followerid: mongoose.Types.ObjectId(userid),
                        request: 'pending'
                    }
                },
                {
                    "$lookup": {
                        from: 'users',
                        localField: 'userid',
                        foreignField: '_id',
                        as: 'userinfo'
                    }
                },
                { $unwind: "$userinfo" },
                {
                    $project: {
                        "_id": 1,
                        "userinfo._id": 1,
                        "userinfo.name": 1,
                        "userinfo.avatar": 1
                    }
                }
            ]).then((users) => {
                res.status(200).json(users);
            }).catch(error => {
                res.status(500).json({ message: error });
            })
        } else {
            errorObj.userid = "Userid is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}

postController.getComments = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var postid = req.query.postid;
        var errorObj = {};
        console.log("userid", userid, postid);

        if (typeof postid !== 'undefined' && postid && postid !== '') {
            comments.aggregate([
                {
                    $match: {
                        postid: mongoose.Types.ObjectId(postid)
                    }
                },
                {
                    "$lookup": {
                        from: 'users',
                        localField: 'userid',
                        foreignField: '_id',
                        as: 'userinfo'
                    }
                },
                { $unwind: "$userinfo" },
                {
                    "$lookup": {
                        from: 'post_likes',
                        localField: '_id',
                        foreignField: 'commentId',
                        as: 'commentlikes'
                    }
                },
                {
                    "$lookup": {
                        from: 'post_comments',
                        localField: '_id',
                        foreignField: 'commentId',
                        as: 'commentreplies'
                    }
                },
                {
                    $project: {
                        "_id": 1,
                        "text": 1,
                        "likesCount": { $size: "$commentlikes" },
                        "repliesCount": { $size: "$commentreplies" },
                        "createdAt": 1,
                        "userinfo._id": 1,
                        "userinfo.name": 1,
                        "userinfo.avatar": 1
                    }
                }
            ]).then((comments) => {
                res.status(200).json(comments);
            }).catch(error => {
                res.status(500).json({ message: error });
            })
        } else {
            errorObj.postid = "postid is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}

postController.getReplyComments = function (req, res, next) {
    try {
        let userid = req.payload._id,
            commentId = req.query.commentId,
            errorObj = {};
        console.log("userid", userid, commentId);

        if (typeof commentId !== 'undefined' && commentId && commentId !== '') {
            comments.aggregate([
                {
                    $match: {
                        commentId: mongoose.Types.ObjectId(commentId)
                    }
                },
                {
                    "$lookup": {
                        from: 'users',
                        localField: 'userid',
                        foreignField: '_id',
                        as: 'userinfo'
                    }
                },
                { $unwind: "$userinfo" },
                {
                    "$lookup": {
                        from: 'post_likes',
                        localField: '_id',
                        foreignField: 'commentId',
                        as: 'replylikes'
                    }
                },
                {
                    $project: {
                        "_id": 1,
                        "text": 1,
                        "likesCount": { $size: "$replylikes" },
                        "createdAt": 1,
                        "userinfo._id": 1,
                        "userinfo.name": 1,
                        "userinfo.avatar": 1
                    }
                }
            ]).then((comments) => {
                res.status(200).json(comments);
            }).catch(error => {
                res.status(500).json({ message: error });
            })
        } else {
            errorObj.postid = "postid is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}

// Unfollow request
postController.unfollow = function (req, res, next) {
    try {
        var id = req.params.id;
        var userid = req.payload._id;
        var errorObj = {};
        var profileObj = {};
        if (typeof id !== 'undefined' && id && id !== '') {
            profileObj.id = id;
            FollowerModel.findOne({ userid: mongoose.Types.ObjectId(userid), followerid: mongoose.Types.ObjectId(id) }, (error, followObj) => {
                if (error) {
                    res.status(500).json({ message: error })
                } else {
                    if (followObj !== null) {
                        FollowerModel.deleteOne({ followerid: id, userid: userid }, function (error, unfollowObj) {
                            if (error) {
                                res.status(500).json({ message: error });
                            }
                            else {
                                res.status(200).json(unfollowObj);
                            }
                        });
                    } else {
                        res.status(500).json({ message: "Already unfollow" });
                    }
                }
            });
        }
        else {
            errorObj.id = 'Id is required';
            errorObj.userid = "Userid is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        console.log(e)
        res.status(500).json({ message: "Internal Server Error" })
    }
}
// followRequest accept 
postController.followRequest = function (req, res, next) {
    try {
        var id = req.params.id;
        var userid = req.payload._id;
        var errorObj = {};

        if (typeof id !== 'undefined' && id && id !== '' &&
            typeof userid !== 'undefined' && userid && userid !== '') {

            request = 'accepted';

            FollowerModel.find({ userid: mongoose.Types.ObjectId(userid) }, function (error, followDetails) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    FollowerModel.updateOne({ _id: mongoose.Types.ObjectId(id) }, { $set: { request: request } }, function (error, followObj) {
                        if (error) {
                            res.status(500).json({ message: error })
                        } else {
                            // var eventName = "acceptFollowRequest" + id;
                            // ioSocket.emit(eventName, followObj);
                            // console.log("FOLLOW REQUEST OBJECT", followDetails);
                            // socketFile.acceptRequest(followDetails);
                            res.status(200).json({ message: "request accepted" });
                        }
                    })
                }
            });
        } else {
            errorObj.userid = "Userid is required";
            errorObj.id = "Id is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}
// Follow request delete
postController.deleteRequest = function (req, res, next) {
    try {
        var id = req.params.id;
        var userid = req.payload._id;
        var errorObj = {};
        if (typeof id !== 'undefined' && id && id !== '' &&
            typeof userid !== 'undefined' && userid && userid !== '') {

            FollowerModel.find({ followerid: mongoose.Types.ObjectId(userid) }, function (error, followDetails) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    FollowerModel.deleteOne({ _id: mongoose.Types.ObjectId(id) }, function (error, followObj) {
                        if (error) {
                            res.status(500).json({ message: error })
                        } else {
                            res.status(200).json({ message: "Request deleted" });
                        }
                    })
                }
            });
        } else {
            errorObj.userid = "Userid is required";
            errorObj.id = "Id is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (error) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}
// Following and follower list
postController.getFollowersAndFollowingList = async function (req, res) {
    try {
        let user = req.payload._id// mongoose.Types.ObjectId("5f4156d5baa44803287a5e5a") //req.payload._id),
        overAllList = [];
        console.log('tag user', user);
        await FollowerModel.aggregate([
            {
                // "$facet": {
                //     "totalData": [
                //         {
                //             $match: {
                //                 followerid: user,
                //                 request: 'accepted'
                //             }
                //         },
                //         { "$skip": 0 },
                //         { "$limit": 10 }
                //     ],
                //     "totalCount": [
                //         {
                //             "$group": {
                //                 "_id": null,
                //                 "count": { "$sum": 1 }
                //             }
                //         }
                //     ]
                // }
                $match: {
                    followerid: mongoose.Types.ObjectId(user),
                    request: 'accepted'
                }
            },
            {
                "$lookup": {
                    from: "users",
                    localField: "userid",
                    foreignField: "_id",
                    as: "user"
                }
            },
            { $unwind: "$user" },
            {
                $project: {
                    "user._id": 1,
                    "user.name": 1,
                    "user.avatar": 1,
                    "totalCount": 1
                }
            }
        ]).then((followinglList) => {

            console.log('followinglList', followinglList);
            if (followinglList.length > 0) {
                followinglList.forEach((followingListElement) => {
                    var idInString = String(followingListElement.user._id);
                    if (overAllList.some(overAllListElement => overAllListElement._id == idInString)) {
                    } else {
                        overAllList.push(followingListElement.user);
                    }
                })
            }
        });

        await FollowerModel.aggregate([
            {
                $match: {
                    userid: mongoose.Types.ObjectId(user),
                    request: 'accepted'
                }
            },
            {
                "$lookup": {
                    from: "users",
                    localField: "followerid",
                    foreignField: "_id",
                    as: "user"
                }
            },
            { $unwind: "$user" },
            {
                $project: {
                    "user._id": 1,
                    "user.name": 1,
                    "user.avatar": 1
                }
            }
        ]).then((followerList) => {

            console.log('followerList', followerList);
            if (followerList.length > 0) {
                followerList.forEach((followingListElement) => {
                    var idInString = String(followingListElement.user._id);
                    if (overAllList.some(overAllListElement => overAllListElement._id == idInString)) {
                    } else {
                        overAllList.push(followingListElement.user);
                    }
                })
            }
        }).catch(err => {
            console.log('err', err);
        })
        res.status(200).json(overAllList)
    } catch (error) {
        res.status(400).json({ message: "Error in fetching Friends" });
    }
}
// friend suggestions
postController.friendsSuggestions = async (req, res) => {
    try {
        var suggestedFriends = [];
        var user = req.payload._id;

        await Users
            .findOne({ _id: user }, { city: 1 })
            .then(async (foundUser) => {

                var userLocation = foundUser.city;
                if (userLocation || true) {
                    await Users.aggregate([
                        {
                            $match: {
                                // name: new RegExp(req.query.filter.trim(), "i"),
                                group: "customer",
                                "_id": { $ne: mongoose.Types.ObjectId(user) }
                            }
                        },
                        {
                            "$skip": Number(req.query.limit) * (Number(req.query.page) - 1)
                        },
                        {
                            "$limit": Number(req.query.limit)
                        },
                        {
                            $project: {
                                "_id": 1,
                                "name": 1,
                                "avatar": 1,
                            }
                        }
                    ]).then(async (sameLocationUsers) => {
                        var counter = 0;
                        for (let i = 0; i < sameLocationUsers.length; i++) {
                            await FollowerModel.findOne({
                                followerid: mongoose.Types.ObjectId(sameLocationUsers[i]._id),
                                userid: mongoose.Types.ObjectId(user)
                            }).then(async (alreadyFollowing) => {
                                counter++;
                                if (alreadyFollowing) {
                                } else {
                                    var idInString = String(sameLocationUsers[i]._id);
                                    if (suggestedFriends.findIndex((obj) => obj._id == idInString) >= 0) {
                                        suggestedFriends[suggestedFriends.findIndex((obj) => obj._id == idInString)].mutualFriends++
                                    } else {
                                        sameLocationUsers[i].mutualFriends = 0;
                                        suggestedFriends.push(sameLocationUsers[i]);
                                    }
                                }
                            }).catch((err5) => {
                                console.log("ERR 1 Friends Suggestions", err5)
                                res.status(500).json({ message: "Something went wrong!!!" });
                            })
                            if (counter === sameLocationUsers.length) {
                                // res.status(200).json({ message: suggestedFriends });
                            }
                        }
                    }).catch((err) => {
                        console.log("ERROR OF FRIENDS SUGGESTIONS", err)
                        res.status(500).json({ message: "Something went wrong!!!" });
                    })
                    FollowerModel.aggregate([
                        {
                            $match: {
                                followerid: mongoose.Types.ObjectId(user)
                            }
                        },
                        {
                            $project: {
                                "userid": 1,
                                "_id": 0
                            }
                        }
                    ]).then((followings) => {
                        var followingsArray = [];
                        followings.forEach((followingsElement) => {
                            followingsArray.push(followingsElement.userid);
                        })
                        FollowerModel.aggregate([
                            {
                                $match: {
                                    followerid: { $in: followingsArray },
                                    userid: { $ne: mongoose.Types.ObjectId(user) }
                                }
                            },
                            {
                                $lookup: {
                                    from: "users",
                                    localField: "userid",
                                    foreignField: "_id",
                                    as: "userInfo"
                                }
                            },
                            {
                                $unwind: {
                                    path: "$userInfo"
                                }
                            },
                            {
                                $project: {
                                    "_id": 0,
                                    "userInfo._id": 1,
                                    "userInfo.name": 1,
                                    "userInfo.avatar": 1
                                }
                            }
                        ]).then(async (followersOfFollowings) => {
                            var totalFollowersOfFollowings = followersOfFollowings.length;
                            var counter = 0;
                            if (totalFollowersOfFollowings > 0) {
                                for (let i = 0; i < followersOfFollowings.length; i++) {
                                    await FollowerModel.findOne({
                                        followerid: mongoose.Types.ObjectId(followersOfFollowings[i].userInfo._id),
                                        userid: mongoose.Types.ObjectId(user)
                                    }).then(async (alreadyFollowing) => {
                                        counter++;
                                        if (alreadyFollowing) {
                                        } else {
                                            var idInString = String(followersOfFollowings[i].userInfo._id);
                                            if (suggestedFriends.findIndex((obj) => obj._id == idInString) >= 0) {
                                                suggestedFriends[suggestedFriends.findIndex((obj) => obj._id == idInString)].mutualFriends++
                                            } else {
                                                followersOfFollowings[i].userInfo.mutualFriends = 0;
                                                suggestedFriends.push(followersOfFollowings[i].userInfo);
                                            }
                                        }
                                    }).catch((err4) => {
                                        console.log("ERR 1 Friends Suggestions", err4);
                                        res.status(500).json({ message: "Something went wrong!!!" });
                                    })
                                    if (counter === totalFollowersOfFollowings) {
                                        res.status(200).json({ message: suggestedFriends });
                                    }
                                }
                            } else {
                                res.status(200).json({ message: suggestedFriends });
                            }
                        }).catch((err3) => {
                            console.log("ERR 3 Friends Suggestions", err3);
                            res.status(500).json({ message: "Something went wrong!!!" });
                        })
                    }).catch((err2) => {
                        console.log("ERR 2 Friends Suggestions", err2);
                        res.status(500).json({ message: "Something went wrong!!!" });
                    })
                } else {
                    res.status(200).json({ message: [] });
                }
            }).catch((err1) => {
                console.log("ERR 1 Friends Suggestions", err1);
                res.status(500).json({ message: "Something went wrong!!!" });
            })
    } catch (error) {
        console.log("ERROR OF CATCH Friends Suggestions", error)
        res.status(500).json({ message: "Something went wrong, please try after sometime" });
    }
}

/***********************************UserProfile post payement work here************************************/
// getcurrency for paid post
postController.getcurrency = function (req, res, next) {
    try {
        var tokens = req.headers.authorization;
        restAPiCtrl.performRequest('wallet', 'getcurrencylist', 'GET', tokens, null, function (result) {
            if (result.status === 200) {
                res.status(200).json(result.message);
            } else {
                res.status(result.status).json({ message: result.message });
            }
        });
    } catch (e) {
        res.status(500).json({ message: e.message });
    }
}

postController.getPostProfile = function (req, res, next) {
    try {
        var userid = req.query.userid;
        Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, { _id: 1, name: 1, avatar: 1 }).exec(function (error, profile) {
            if (!error && profile !== null) {
                res.status(200).json(profile);
            }
        })
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}

postController.getPostProfileParticularUser = function (req, res, next) {
    try {
        var userid = req.params.id;
        var selfPosts = false;
        var selfId = req.payload._id;
        var accessPosts = false;
        if (selfId === userid) {
            selfPosts = true;
        } else {
            selfPosts = false;
        }
        // console.log('req.params.userid ', req.params.id);
        Users.findOne({ _id: mongoose.Types.ObjectId(userid) }, { apikey: 0, bankrefcode: 0, password: 0 }).exec(async function (error, profile) {
            // console.log('profileprofileprofileprofileprofileprofileprofile ', profile);
            if (!error && profile !== null) {
                await FollowerModel.findOne({ userid: selfId, followerid: userid }).then((follower) => {
                    // console.log("FOLLOWER", follower);
                    if (follower || selfPosts) {
                        accessPosts = true;
                    } else {
                        accessPosts = false;
                    }
                }).catch((err) => {
                    console.log("ERROR IN GET POST PROFILE PARTICULAR USER 1", err);
                    res.status(500).json({ message: "Something went wrong!!!" });
                })
                // console.log("ACCESS POSTS", accessPosts);
                if (accessPosts) {
                    // console.log('profile._id, selfPosts, selfId profile._id, selfPosts, selfId profile._id, selfPosts, selfId @@@@ ', profile._id, selfPosts, selfId);
                    await ctrl.getPosts(profile._id, selfPosts, selfId).then(function (result) {
                        // console.log('resultresultresultresultresultresultresultresultresult ', result);
                        userPosts = result;
                        // res.status(200).json({ profile, userPosts });
                        return res.status(200).json({ profile: profile, posts: userPosts })
                    }, function (err) {
                        var errorObject = {
                            status: 500,
                            message: "Error in fetching user's posts"
                        }
                        console.log("ERROR IN PROMISE OBJECT OF GET POST PROFILE PARTICULAR USER", err);
                        // res.status(200).json({ profile, errorObject });

                        return res.status(200).json({ profile: profile, posts: {} })
                    })
                } else {
                    // res.status(500).json({ message: "Please follow to see the posts" });
                    return res.status(200).json({ profile: profile, posts: {} })
                }
            } else {
                res.status(500).json({ message: "No such user exists" });
            }
        })
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}
// Paid payement for post 
postController.paidPayement = function (req, res, next) {
    try {
        var tokens = req.headers.authorization;
        var id = req.params.id;
        var errorObj = {};
        if (typeof id !== 'undefined' && id && id !== '') {
            postpayement.findOne({ postid: id }, function (error, AmountObj) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    if (AmountObj !== null) {
                        var PostObj = {
                            postid: AmountObj.postid.toString(),
                            senderid: req.payload._id,
                            receiverid: AmountObj.userid.toString(),
                            currency: AmountObj.currency.toString(),
                            amount: Number(Number(AmountObj.amount) + Number(currencyPercentage[AmountObj.currency]))
                        }
                        restAPiCtrl.performRequest('wallet', 'postpayment', 'GET', tokens, PostObj, function (result) {
                            if (result.status === 200) {
                                var apiResponse = result.message;
                                if (Number(apiResponse.status) === 200) {
                                    var userid = req.payload._id;
                                    var orderid = PostObj.postid;
                                    var amount = apiResponse.message.amount;
                                    var type = 'Post';
                                    var currency = apiResponse.message.currency;
                                    var tr_id = apiResponse.message.tr_id;
                                    var profileObj = {};
                                    var errorObj = {};
                                    if (typeof userid !== 'undefined' && userid && userid !== '' &&
                                        typeof orderid !== 'undefined' && orderid && orderid !== '' &&
                                        typeof amount !== 'undefined' && amount && amount !== '' &&
                                        typeof type !== 'undefined' && type && type !== '' &&
                                        typeof currency !== 'undefined' && currency && currency !== '' &&
                                        typeof tr_id !== 'undefined' && tr_id && tr_id !== ''
                                    ) {
                                        profileObj.userid = userid;
                                        profileObj.orderid = orderid
                                        profileObj.amount = amount;
                                        profileObj.type = type;
                                        profileObj.currency = currency;
                                        profileObj.tr_id = tr_id;

                                        myPurchase.create(profileObj, function (error, purchaseObj) {
                                            if (error) {
                                                res.status(500).json({ message: 'Invalid request' });
                                            } else {
                                                res.status(200).json({ message: 'Successfully' });
                                            }
                                        });
                                    } else {
                                        errorObj.userid = 'userid is required';
                                        errorObj.orderid = 'orderid is required';
                                        errorObj.amount = 'amount is required';
                                        errorObj.type = 'type is required';
                                        errorObj.currency = 'currency is required';
                                        errorObj.tr_id = 'tr_id is required';
                                        res.status(400).json({ message: errorObj });
                                    }
                                } else {
                                    res.status(400).json({ message: apiResponse.message })
                                }
                            }
                            else {
                                res.status(400).json({ message: result.message })
                            }
                        });
                    }
                }
            });
        }
        else {
            errorObj.id = 'Id is required';
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}

// Purchases details for paid content
postController.purchaseDetails = function (req, res, next) {
    var userid = req.payload._id;
    myPurchase.find({ userid: mongoose.Types.ObjectId(userid) }, function (error, purchases) {
        if (error) {
            res.status(500).json("Invalid request")
        } else {
            var purchasesDetails = {};
            if (purchases.length > 0) {
                purchases.forEach(function (item) {
                    purchasesDetails[item.orderid] = true;
                });
            }
            res.status(200).json(purchasesDetails);
        }
    });
}

/************************************UserProfile group work here*********************************************/
// All groups get
postController.Usergroups = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var errorObj = {};
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            GroupModel.find({ userid: mongoose.Types.ObjectId(userid) }, function (error, subscriberObj) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json(subscriberObj);
                }
            });
        }
        else {
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}
// subscribersPayements 
postController.subscribersPayements = async function (req, res, next) {
    try {
        await ctrl.subscribeForAGroup(req).then((result) => {
            res.status(200).json(result);
        }, (err) => {
            console.log("ERROR OF SUBSCRIBERS PAYMENT", err);
            res.status(500).json(err);
        })
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}

postController.subscribeForAGroup = async (req) => {
    return new Promise(async (resolve, reject) => {
        try {
            var tokens = req.headers.authorization;
            var id = req.params.id;
            var userid = req.payload._id;
            var payments = req.params.payements;
            var errorObj = {};
            var trialDurations = {
                monthly: 1,
                halfyearly: 6,
                yearly: 12,
            }
            if (typeof id !== 'undefined' && id && id !== '') {
                GroupModel.findOne({ _id: mongoose.Types.ObjectId(id) }, function (error, AmountObj) {
                    if (error) {
                        // res.status(500).json({ message: error });
                        reject({ message: error });
                    } else {
                        GroupFollowingModel.findOne({ userid: userid, groupid: id, type: "Subscribe" }).then((foundSubscribedGroup) => {
                            if (foundSubscribedGroup === null || foundSubscribedGroup.status === "expired") {
                                if (AmountObj !== null) {
                                    var d = new Date();
                                    var endDt = new Date(d.setMonth(d.getMonth() + Number(trialDurations[AmountObj.duration])));
                                    var PostObj = {
                                        // postid: AmountObj.postid.toString(),
                                        senderid: userid,
                                        receiverid: AmountObj.userid.toString(),
                                        currency: AmountObj.currency.toString(),
                                        amount: Number(Number(AmountObj.amount) + Number(currencyPercentage[AmountObj.currency])),
                                        endDate: endDt
                                    }
                                    restAPiCtrl.performRequest('wallet', 'subscribersPayements', 'GET', tokens, PostObj, function (result) {
                                        if (result.status === 200) {
                                            var apiResponse = result.message;
                                            if (Number(apiResponse.status) === 200) {
                                                var subscriberid = PostObj.receiverid;
                                                var amount = apiResponse.message.amount;
                                                var type = 'Subscribe';
                                                var currency = apiResponse.message.currency;
                                                var tr_id = apiResponse.message.tr_id;
                                                var groupid = id;
                                                var subscriptionid = PostObj.receiverid;
                                                var endDate = PostObj.endDate;
                                                var autoPayement = payments;
                                                var profileObj = {};
                                                var errorObj = {};
                                                if (typeof userid !== 'undefined' && userid && userid !== '' &&
                                                    typeof subscriberid !== 'undefined' && subscriberid && subscriberid !== '' &&
                                                    typeof amount !== 'undefined' && amount && amount !== '' &&
                                                    typeof type !== 'undefined' && type && type !== '' &&
                                                    typeof currency !== 'undefined' && currency && currency !== '' &&
                                                    typeof tr_id !== 'undefined' && tr_id && tr_id !== '' &&
                                                    typeof subscriptionid !== 'undefined' && subscriptionid && subscriptionid !== '' &&
                                                    typeof groupid !== 'undefined' && groupid && groupid !== '' &&
                                                    typeof endDate !== 'undefined' && endDate && endDate !== ''
                                                ) {
                                                    profileObj.userid = userid;
                                                    profileObj.subscriberid = subscriberid;
                                                    profileObj.amount = amount;
                                                    profileObj.type = type;
                                                    profileObj.currency = currency;
                                                    profileObj.tr_id = tr_id;
                                                    profileObj.subscriptionid = subscriptionid;
                                                    profileObj.groupid = groupid;
                                                    profileObj.endDate = endDate;
                                                    profileObj.autoPayement = autoPayement;
                                                    profileObj.status = 'open';

                                                    myPurchase.create(profileObj, function (error, purchaseObj) {
                                                        if (error) {
                                                            reject({ message: 'Invalid request' });
                                                        } else {
                                                            GroupFollowingModel.create(profileObj, function (error, subObj) {
                                                                if (error) {
                                                                    // res.status(500).json({ message: 'Invalid request' });
                                                                    reject({ message: 'Invalid request' });
                                                                } else {
                                                                    // res.status(200).json({ message: 'Successfully' });
                                                                    resolve({ message: 'Successfully' });
                                                                }
                                                            })
                                                        }
                                                    });
                                                } else {
                                                    errorObj.userid = 'userid is required';
                                                    errorObj.subscriberid = 'subscriberid is required';
                                                    errorObj.amount = 'amount is required';
                                                    errorObj.type = 'type is required';
                                                    errorObj.currency = 'currency is required';
                                                    errorObj.tr_id = 'tr_id is required';
                                                    errorObj.subscriptionid = 'Subscriptionid is required';
                                                    errorObj.groupid = "Group id is required";
                                                    errorObj.endDate = "EndDate is required";
                                                    // res.status(400).json({ message: errorObj });
                                                    reject({ message: errorObj })
                                                }
                                            } else {
                                                // res.status(400).json({ message: apiResponse.message })
                                                reject({ message: apiResponse.message })
                                            }
                                        }
                                        else {
                                            // res.status(400).json({ message: result.message });
                                            reject({ message: result.message });
                                        }
                                    });
                                }
                            } else if (foundSubscribedGroup.status === "open") {
                                // res.status(500).json({ message: "You already subscribed this group" });
                                reject({ message: "You already subscribed this group" })
                            }
                        }).catch((err) => {
                            console.log("ERROR IN SUBSCRIBE FOR A GROUP 1", err);
                            res.status(500).json({ message: "Something went wrong!!!" });
                        })
                    }
                });
            }
            else {
                errorObj.id = 'Id is required';
                reject({ message: errorObj });
            }
        } catch (error) {
            console.log("ERROR IN GETTING POSTS", error);
            reject(error.message);
        }
    })
}

postController.requestForAGroup = async (req) => {
    return new Promise(async (resolve, reject) => {
        try {
            var tokens = req.headers.authorization;
            var id = req.params.id;
            var userid = req.payload._id;
            var payments = req.params.payements;
            var errorObj = {};
            var trialDurations = {
                monthly: 1,
                halfyearly: 6,
                yearly: 12,
            }
            if (typeof id !== 'undefined' && id && id !== '') {
                GroupModel.findOne({ _id: mongoose.Types.ObjectId(id) }, function (error, foundGroup) {
                    if (error) {
                        console.log("ERROR OF GROUP NOT FOUND", error);
                        // res.status(500).json({ message: error });
                        reject({ message: "No such group" });
                    } else {
                        GroupFollowingModel.findOne({ userid: userid, groupid: id, type: "Subscribe" }).then((foundSubscribedGroup) => {
                            if (foundSubscribedGroup === null || foundSubscribedGroup.status === "expired") {
                                if (foundGroup !== null) {
                                    var d = new Date();
                                    var endDt = new Date(d.setMonth(d.getMonth() + Number(trialDurations[foundGroup.duration])));
                                    var PostObj = {
                                        // postid: foundGroup.postid.toString(),
                                        senderid: userid,
                                        receiverid: foundGroup.userid.toString(),
                                        currency: foundGroup.currency.toString(),
                                        amount: Number(Number(foundGroup.amount) + Number(currencyPercentage[foundGroup.currency])),
                                        endDate: endDt
                                    }
                                    restAPiCtrl.performRequest('wallet', 'requestToJoinGroup', 'GET', tokens, PostObj, function (result) {
                                        if (result.status === 200) {
                                            var apiResponse = result.message;
                                            if (Number(apiResponse.status) === 200) {
                                                var subscriberid = PostObj.receiverid;
                                                var amount = apiResponse.message.amount;
                                                var type = 'Subscribe';
                                                var currency = apiResponse.message.currency;
                                                var tr_id = apiResponse.message.tr_id;
                                                var groupid = id;
                                                var subscriptionid = PostObj.receiverid;
                                                var endDate = PostObj.endDate;
                                                var autoPayement = payments;
                                                var profileObj = {};
                                                var errorObj = {};
                                                if (typeof userid !== 'undefined' && userid && userid !== '' &&
                                                    typeof subscriberid !== 'undefined' && subscriberid && subscriberid !== '' &&
                                                    typeof amount !== 'undefined' && amount && amount !== '' &&
                                                    typeof type !== 'undefined' && type && type !== '' &&
                                                    typeof currency !== 'undefined' && currency && currency !== '' &&
                                                    typeof tr_id !== 'undefined' && tr_id && tr_id !== '' &&
                                                    typeof subscriptionid !== 'undefined' && subscriptionid && subscriptionid !== '' &&
                                                    typeof groupid !== 'undefined' && groupid && groupid !== '' &&
                                                    typeof endDate !== 'undefined' && endDate && endDate !== ''
                                                ) {
                                                    profileObj.userid = userid;
                                                    profileObj.subscriberid = subscriberid;
                                                    profileObj.amount = amount;
                                                    profileObj.type = type;
                                                    profileObj.currency = currency;
                                                    profileObj.tr_id = tr_id;
                                                    profileObj.subscriptionid = subscriptionid;
                                                    profileObj.groupid = groupid;
                                                    profileObj.endDate = endDate;
                                                    profileObj.autoPayement = autoPayement;
                                                    profileObj.status = 'pending';
                                                    myPurchase.create(profileObj, function (error, purchaseObj) {
                                                        if (error) {
                                                            // res.status(500).json({ message: 'Invalid request' });
                                                            reject({ message: 'Invalid request' });
                                                        } else {
                                                            resolve({ message: "Send successfully" });
                                                        }
                                                    });
                                                } else {
                                                    errorObj.userid = 'userid is required';
                                                    errorObj.subscriberid = 'subscriberid is required';
                                                    errorObj.amount = 'amount is required';
                                                    errorObj.type = 'type is required';
                                                    errorObj.currency = 'currency is required';
                                                    errorObj.tr_id = 'tr_id is required';
                                                    errorObj.subscriptionid = 'Subscriptionid is required';
                                                    errorObj.groupid = "Group id is required";
                                                    errorObj.endDate = "EndDate is required";
                                                    // res.status(400).json({ message: errorObj });
                                                    reject({ message: errorObj })
                                                }
                                            } else {
                                                // res.status(400).json({ message: apiResponse.message })
                                                reject({ message: apiResponse.message })
                                            }
                                        } else {
                                            // res.status(400).json({ message: result.message });
                                            reject({ message: result.message });
                                        }
                                    });
                                }
                            } else if (foundSubscribedGroup.status === "open") {
                                // res.status(500).json({ message: "You already subscribed this group" });
                                reject({ message: "You already subscribed this group" })
                            }
                        }).catch((err) => {
                            console.log("ERROR IN SUBSCRIBE FOR A GROUP 1", err);
                            res.status(500).json({ message: "Something went wrong!!!" });
                        })
                    }
                });
            } else {
                errorObj.id = 'Id is required';
                reject({ message: errorObj });
            }
        } catch (error) {
            console.log("ERROR IN GETTING POSTS", error);
            reject(error.message);
        }
    })
}

postController.approveRequest = async (req, purchasetxnid) => {
    return new Promise(async (resolve, reject) => {
        try {
            var tokens = req.headers.authorization;
            var id = req.params.id;
            var userid = req.payload._id;
            var payments = req.params.payements;
            var errorObj = {};
            var trialDurations = {
                monthly: 1,
                halfyearly: 6,
                yearly: 12,
            }
            if (typeof id !== 'undefined' && id && id !== '') {
                GroupModel.findOne({ _id: mongoose.Types.ObjectId(id) }, function (error, foundGroup) {
                    if (error) {
                        console.log("ERROR OF GROUP NOT FOUND", error);
                        // res.status(500).json({ message: error });
                        reject({ message: "No such group" });
                    } else {
                        if (foundGroup !== undefined && foundGroup !== null && foundGroup !== "" && foundGroup) {
                            GroupFollowingModel.findOne({ userid: userid, groupid: id, type: "Subscribe" }).then((foundSubscribedGroup) => {
                                if (foundSubscribedGroup === null || foundSubscribedGroup.status === "expired") {
                                    if (foundGroup !== null) {
                                        var d = new Date();
                                        var endDt = new Date(d.setMonth(d.getMonth() + Number(trialDurations[foundGroup.duration])));
                                        var PostObj = {
                                            senderid: userid,
                                            receiverid: foundGroup.userid.toString(),
                                            currency: foundGroup.currency.toString(),
                                            amount: Number(Number(foundGroup.amount) + Number(currencyPercentage[foundGroup.currency])),
                                            endDate: endDt,
                                            doneTransactionId: purchasetxnid,
                                            approval: req.params.approved
                                        }
                                        tokens = "Bearer " + tokens;
                                        restAPiCtrl.performRequest('wallet', 'approveRequest', 'GET', tokens, PostObj, function (result) {
                                            if (result.status === 200) {
                                                var apiResponse = result.message;
                                                if (Number(apiResponse.status) === 200) {
                                                    if (req.params.approved === false) {
                                                        myPurchase.findOneAndUpdate({ tr_id: purchasetxnid }, { status: "Rejected" }).then((updatedMyPurchase) => {
                                                            resolve({ message: "Request Rejected" });
                                                        }).catch((errorInUpdatingMyPurchase) => {
                                                            console.log("ERROR IN UPDATING MY PURCHASE", errorInUpdatingMyPurchase);
                                                            reject({ message: 'Something went wrong' });
                                                        })
                                                    } else {
                                                        myPurchase.findOneAndUpdate({ tr_id: purchasetxnid }, { status: "Completed" }).then((updatedMyPurchase) => {
                                                            resolve({ message: "Request Rejected" });
                                                        }).catch((errorInUpdatingMyPurchase) => {
                                                            console.log("ERROR IN UPDATING MY PURCHASE", errorInUpdatingMyPurchase);
                                                            reject({ message: 'Something went wrong' });
                                                        })
                                                    }
                                                } else {
                                                    // res.status(400).json({ message: apiResponse.message })
                                                    reject({ message: apiResponse.message })
                                                }
                                            } else {
                                                // res.status(400).json({ message: result.message });
                                                reject({ message: result.message });
                                            }
                                        });
                                    }
                                } else if (foundSubscribedGroup.status === "open") {
                                    // res.status(500).json({ message: "You already subscribed this group" });
                                    reject({ message: "You already subscribed this group" })
                                }
                            }).catch((err) => {
                                console.log("ERROR IN SUBSCRIBE FOR A GROUP 1", err);
                                res.status(500).json({ message: "Something went wrong!!!" });
                            })
                        } else {
                            console.log("FOUND GROUP", foundGroup);
                            return res.status(200).json({ success: false, message: "No such group present" });
                        }
                    }
                });
            } else {
                errorObj.id = 'Id is required';
                reject({ message: errorObj });
            }
        } catch (error) {
            console.log("ERROR IN GETTING POSTS", error);
            reject(error.message);
        }
    })
}

//Cancel request to join the group
postController.cancelGroupJoiningRequest = async (req, res) => {
    try {
        var tokens = req.headers.authorization;
        var groupId = req.params.groupId;
        var userid = req.payload._id;
        var errorObj = {};
        var purchasetxnid;
        var trialDurations = {
            monthly: 1,
            halfyearly: 6,
            yearly: 12,
        }
        if (typeof groupId !== 'undefined' && groupId && groupId !== '') {
            GroupJoinRequestsModel.findOne({ groupid: mongoose.Types.ObjectId(groupId), userid: userid }, async function (error, foundSentRequest) {
                if (error) {
                    console.log("ERROR OF REQUEST NOT FOUND", error);
                    return res.status(200).json({ success: false, message: "No such group" });
                } else {
                    if (foundSentRequest !== undefined && foundSentRequest !== null && foundSentRequest !== "" && foundSentRequest) {
                        if (foundSentRequest.amount === undefined || foundSentRequest.amount === null || foundSentRequest.amount === "") {
                            GroupJoinRequestsModel.remove({ _id: foundSentRequest._id }).then((removedRequest) => {
                                // var eventName = "cancelGroupJoiningRequest" + groupId;
                                // ioSocket.emit(eventName, followObj);
                                return res.status(200).json({ success: true, message: "Request Cancelled" });
                            }).catch((errorInRemovingRequest) => {
                                console.log("ERROR IN REMOVING REQUEST", errorInRemovingRequest);
                                return res.status(200).json({ success: false, message: "Something went wrong" });
                            })
                        } else if (foundSentRequest.amount) {
                            await myPurchase.findOne({ userid: userid, groupid: groupId, status: "pending" }).then((foundPendingMyPurchase) => {
                                purchasetxnid = foundPendingMyPurchase.tr_id;
                            }).catch((error) => {
                                console.log("ERROR OF MY PURCHASE", error);
                                return res.status(200).json({ success: false, message: "Something went wrong" });
                            })
                            var PostObj = {
                                senderid: userid,
                                doneTransactionId: purchasetxnid,
                            }
                            restAPiCtrl.performRequest('wallet', 'cancelRequest', 'GET', tokens, PostObj, function (result) {
                                if (result.status === 200) {
                                    var apiResponse = result.message;
                                    if (Number(apiResponse.status) === 200) {
                                        myPurchase.findOneAndUpdate({ tr_id: purchasetxnid }, { status: "Cancelled" }).then((updatedMyPurchase) => {
                                            if (updatedMyPurchase !== null && updatedMyPurchase !== undefined && updatedMyPurchase !== "" && updatedMyPurchase) {
                                                GroupJoinRequestsModel.remove({ _id: foundSentRequest._id }).then((removedRequest) => {
                                                    // var eventName = "cancelGroupJoiningRequest" + groupId;
                                                    // ioSocket.emit(eventName, followObj);
                                                    return res.status(200).json({ success: true, message: "Request Cancelled" });
                                                }).catch((errorInRemovingRequest) => {
                                                    console.log("ERROR IN REMOVING REQUEST", errorInRemovingRequest);
                                                    return res.status(200).json({ success: false, message: "Something went wrong" });
                                                })
                                            } else {
                                                return res.status(200).json({ success: false, message: "Something went wrong" });
                                            }
                                        }).catch((errorInUpdatingMyPurchase) => {
                                            console.log("ERROR IN UPDATING MY PURCHASE", errorInUpdatingMyPurchase);
                                            return res.status(200).json({ success: false, message: "Something went wrong" });
                                        })
                                    } else {
                                        return res.status(200).json({ success: false, message: apiResponse.message });
                                    }
                                } else {
                                    return res.status(200).json({ success: false, message: result.message });
                                }
                            })
                        }
                    } else {
                        return res.status(200).json({ success: false, message: "No request is there for this group" });
                    }
                }
            });
        } else {
            errorObj.groupId = 'Id is required';
            res.status(200).json({ success: false, message: errorObj });
        }
    } catch (error) {
        console.log("ERROR IN GETTING POSTS", error);
        res.status(200).json({ success: false, message: error.message });
    }
}

// subscriberget
postController.subscriberget = function (req, res, next) {
    var userid = req.payload._id;
    myPurchase.find({ userid: mongoose.Types.ObjectId(userid) }, function (error, subscriberDetails) {
        if (error) {
            res.status(500).json({ message: error })
        } else {
            var purchasesDetails = {};
            if (subscriberDetails.length > 0) {
                subscriberDetails.forEach(function (item) {
                    purchasesDetails[item.subscriberid] = true;
                });
            }
            res.status(200).json(purchasesDetails);
        }
    });
}

// show subscriber to header
postController.getSubscribe = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var errorObj = {};
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            GroupFollowingModel.countDocuments({ subscriptionid: userid, status: "open" }, function (error, show) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json(show)
                }
            });
        } else {
            errorObj.userid = "Userid is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}

// subscriberget subscription
postController.subscribergetsubscription = function (req, res, next) {
    try {
        GroupFollowingModel.find(function (error, subscriberObj) {
            if (error) {
                res.status(500).json({ message: error })
            } else {
                res.status(200).json(subscriberObj)
            }
        })
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" })
    }
}

// getpayementSubscribe
postController.getpayementSubscribe = function (req, res, next) {
    try {
        GroupModel.findOne({ _id: req.params.id }, function (error, getpayementSubscribe) {
            if (error) {
                res.status(404).json({ message: error.message });
            } else {
                res.status(200).json(getpayementSubscribe);
            }
        })
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

postController.getPosts = async (userid, selfPosts, searcherId) => {
    return new Promise(async (resolve, reject) => {
        try {
            const userId = userid;
            var completePostList;
            var searcherid = searcherId;
            // console.log("GET POSTS OF USER", userId, typeof userId);
            var match;
            if (userId == '') {
                match = {
                    "parentid": { $exists: false },
                    "status": 'active',
                    // "userid": userId
                };
            } else {
                if (selfPosts) {
                    // console.log("SELFPOST FOR TRUE");
                    match = {
                        "parentid": { $exists: false },
                        "status": 'active',
                        "userid": userId,
                        "sharedBy": null
                    };
                } else {
                    // console.log("SELFPOST FOR FALSE");
                    match = {
                        "parentid": { $exists: false },
                        "status": 'active',
                        "userid": userId,
                        "sharedBy": null,
                        "privacy": { $ne: "Onlyme" }
                    };
                }
            }
            posts.aggregate([
                {
                    $match: match
                },
                {
                    "$lookup": {
                        from: 'post_medias',
                        localField: '_id',
                        foreignField: 'postid',
                        as: 'contents'
                    }
                },
                {
                    "$lookup": {
                        from: 'groupsbyusers',
                        localField: 'groupid',
                        foreignField: '_id',
                        as: 'groupdata'
                    }
                },
                {
                    "$lookup": {
                        from: 'users',
                        localField: 'userid',
                        foreignField: '_id',
                        as: 'userinfo'
                    }
                },
                {
                    '$unwind': {
                        path: '$userinfo'
                    }
                },
                {
                    "$project": {
                        _id: 1,
                        text: 1,
                        subject: 1,
                        privacy: 1,
                        groupid: 1,
                        userid: 1,
                        createdAt: 1,
                        payements: 1,
                        taggedPeople: 1,
                        "contents": 1,
                        "groupdata": 1,
                        "userinfo._id": 1,
                        "userinfo.name": 1,
                        "userinfo.avatar": 1
                    }
                }
            ]).sort({ createdAt: -1 }).exec(function (error, postlist) {
                // console.log('postlist ################################# ', postlist);
                if (error) {
                    // res.status(404).json({ message: error.message });
                    reject({ message: error.message })

                } else {
                    var matchCondForTaggedPosts = {};
                    if (selfPosts) {
                        matchCondForTaggedPosts = { taggedPeople: { $elemMatch: { _id: mongoose.Types.ObjectId(userId) } } };
                    } else {
                        matchCondForTaggedPosts = { taggedPeople: { $elemMatch: { _id: mongoose.Types.ObjectId(userId) } }, payements: false };
                    }
                    // posts.find(matchCondForTaggedPosts).then(async (taggedPosts) => {
                    posts.aggregate([
                        {
                            $match: matchCondForTaggedPosts
                        },
                        {
                            $lookup: {
                                from: 'users',
                                localField: 'userid',
                                foreignField: '_id',
                                as: 'userinfo'
                            }
                        },
                        {
                            '$unwind': {
                                path: '$userinfo'
                            }
                        },
                        {
                            "$project": {
                                text: 1,
                                subject: 1,
                                privacy: 1,
                                taggedPeople: 1,
                                "userinfo._id": 1,
                                "userinfo.name": 1,
                                "userinfo.avatar": 1
                            }
                        }
                    ]).sort({ createdAt: -1 }).then(async (taggedPosts) => {
                        // console.log('taggedPoststaggedPoststaggedPosts ', taggedPosts);
                        if (taggedPosts.length > 0) {
                            for (let i = 0; i < taggedPosts.length; i++) {
                                if (taggedPosts[i].privacy === "Public") {
                                    postlist.push(taggedPosts[i]);
                                } else if (taggedPosts[i].privacy === "Friend") {
                                    await FollowerModel.findOne({ userid: taggedPosts[i], followerid: searcherid }).then((friendsMatched) => {
                                        // console.log('friendsMatchedfriendsMatchedfriendsMatchedfriendsMatched ', friendsMatched);
                                        if (friendsMatched || (taggedPosts[i].userid).toString() === searcherid) {
                                            postlist.push(taggedPosts[i]);
                                        }
                                    }).catch((err) => {
                                        // console.log("ERROR IN GET POSTS 1", err);
                                        res.status(500).json({ message: "Something went wrong!!!" });
                                    })
                                }
                            }
                        }
                        var matchCondForSharedPosts = {};
                        if (selfPosts) {
                            matchCondForSharedPosts = { sharedBy: mongoose.Types.ObjectId(userId) };
                        } else {
                            matchCondForSharedPosts = { sharedBy: mongoose.Types.ObjectId(userId), payements: false };
                        }
                        await posts.aggregate([
                            {
                                "$match": matchCondForSharedPosts
                            },
                            {
                                "$lookup": {
                                    from: 'userposts',
                                    localField: 'sharedPost',
                                    foreignField: '_id',
                                    as: 'sharedPost'
                                }
                            },
                            {
                                "$unwind": "$sharedPost"
                            },
                            {
                                "$lookup": {
                                    from: 'users',
                                    localField: 'sharedPost.userid',
                                    foreignField: '_id',
                                    as: 'postOwner'
                                }
                            },
                            {
                                '$unwind': "$postOwner"
                            },
                            {
                                "$lookup": {
                                    from: 'users',
                                    localField: 'sharedBy',
                                    foreignField: '_id',
                                    as: 'shareOwner'
                                }
                            },
                            {
                                '$unwind': "$shareOwner"
                            },
                            {
                                "$project": {
                                    "postOwner._id": 1,
                                    "postOwner.name": 1,
                                    "postOwner.avatar": 1,
                                    "shareOwner._id": 1,
                                    "shareOwner.name": 1,
                                    "shareOwner.avatar": 1,
                                    "sharedPost": 1,
                                }
                            }
                        ]).sort({ createdAt: -1 }).then((responseOfSearchedPosts) => {
                            if (responseOfSearchedPosts.length > 0) {
                                postlist.push(...responseOfSearchedPosts);
                                postlist.sort(function (a, b) {
                                    return b.createdAt - a.createdAt
                                })
                            }
                            // }
                        }).catch((err) => {
                            console.log("ERROR IN GET POSTS", err);
                            res.status(500).json({ message: "Something went wrong!!!" });
                        })
                        var postids = [];
                        postlist.forEach(function (post) {
                            postids.push(post._id);
                        });
                        if (postids.length > 0) {
                            posts.aggregate([
                                {
                                    "$match": {
                                        parentid: { $in: postids },
                                        "status": 'active'
                                    }
                                },
                                {

                                    "$lookup": {
                                        from: 'users',
                                        localField: 'userid',
                                        foreignField: '_id',
                                        as: 'userinfo'
                                    }
                                },
                                {
                                    '$unwind': {
                                        path: '$userinfo'
                                    }
                                },
                                {
                                    "$project": {
                                        _id: 1,
                                        text: 1,
                                        userid: 1,
                                        parentid: 1,
                                        createdAt: 1,
                                        "userinfo._id": 1,
                                        "userinfo.name": 1,
                                        "userinfo.avatar": 1
                                    }
                                },
                            ]).sort({ createdAt: 1 }).exec(function (error, postcommentlist) {
                                if (error) {
                                    reject({ message: error.message });
                                    // res.status(404).json({ message: error.message });
                                } else {
                                    if (postcommentlist.length > 0) {
                                        var wallpost = {};
                                        postcommentlist.forEach(function (item) {
                                            if (Object.keys(wallpost).indexOf(item.parentid.toString()) === -1) {
                                                wallpost[item.parentid] = [item];
                                            } else {
                                                wallpost[item.parentid].push(item)
                                            }
                                        });
                                        completePostList = { post: postlist, content: wallpost };
                                        resolve(completePostList);
                                        // res.status(200).json({ post: postlist, content: wallpost });
                                    } else {
                                        completePostList = { post: postlist, content: {} };
                                        resolve(completePostList);
                                        // res.status(200).json({ post: postlist, content: {} });
                                    }
                                }
                            });
                        } else {
                            completePostList = { post: postlist, content: {} };
                            resolve(completePostList);
                            // res.status(200).json({ post: postlist, content: {} });
                        }
                        var postids = [];
                        postlist.forEach(function (post) {
                            postids.push(post._id);
                        });
                        if (postids.length > 0) {
                            posts.aggregate([
                                {
                                    "$match": {
                                        parentid: { $in: postids },
                                        "status": 'active'
                                    }
                                },
                                {
                                    "$lookup": {
                                        from: 'users',
                                        localField: 'userid',
                                        foreignField: '_id',
                                        as: 'userinfo'
                                    }
                                },
                                {
                                    '$unwind': {
                                        path: '$userinfo'
                                    }
                                },
                                {
                                    "$project": {
                                        _id: 1,
                                        text: 1,
                                        userid: 1,
                                        parentid: 1,
                                        createdAt: 1,
                                        "userinfo._id": 1,
                                        "userinfo.name": 1,
                                        "userinfo.avatar": 1
                                    }
                                },
                            ]).sort({ createdAt: 1 }).exec(function (error, postcommentlist) {
                                if (error) {
                                    reject({ message: error.message });
                                    // res.status(404).json({ message: error.message });
                                } else {
                                    if (postcommentlist.length > 0) {
                                        var wallpost = {};
                                        postcommentlist.forEach(function (item) {
                                            if (Object.keys(wallpost).indexOf(item.parentid.toString()) === -1) {
                                                wallpost[item.parentid] = [item];
                                            } else {
                                                wallpost[item.parentid].push(item)
                                            }
                                        });
                                        // console.log("WALL POSTS", wallpost);
                                        completePostList = { post: postlist, content: wallpost };
                                        resolve(completePostList);
                                        // res.status(200).json({ post: postlist, content: wallpost });
                                    } else {
                                        completePostList = { post: postlist, content: {} };
                                        resolve(completePostList);
                                        // res.status(200).json({ post: postlist, content: {} });
                                    }
                                }
                            });
                        } else {
                            completePostList = { post: postlist, content: {} };
                            resolve(completePostList);
                            // res.status(200).json({ post: postlist, content: {} });
                        }
                    }).catch((err) => {
                        console.log("ERROR IN GET POSTS 2", err);
                        // res.status(500).json({ message: "Something went wrong!!!" });
                        reject({ message: "Something went wrong!!!" });
                    })
                }
            });
        } catch (error) {
            // console.log("ERROR IN GETTING POSTS", error);
            reject(error.message);
        }
    });
}

postController.sharingPost = async (req, res) => {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {

            let sharedBy = req.payload._id,
                userid = req.payload._id,
                sharedPost = req.body.postid,
                sharedText = req.body.text,
                status = 'active',
                postSharable = false,
                profileObj = {};
            console.log('req.body', req.body);
            if (status !== '' && status !== null && status !== undefined && status) {
                profileObj.status = status;
            }
            if (typeof groupid !== 'undefined' && groupid && groupid !== 'null') {
                profileObj.groupid = groupid;
            } else {
                profileObj.payements = false;
            }
            await posts.findOne({ _id: sharedPost }).then(async (postDetails) => {

                if (postDetails.groupid !== undefined && postDetails.groupid !== null && postDetails.groupid !== "" && postDetails.groupid) {
                    await GroupModel.findOne({ _id: postDetails.groupid }).then((foundGroup) => {
                        if (foundGroup.privacy === "public" && foundGroup.type === "free") {
                            postSharable = true;
                        }
                    }).catch((errorInFindingGroup) => {
                        console.log("ERROR IN FINDING GROUP", errorInFindingGroup);
                        return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
                    })
                } else if (!postDetails.payements) {
                    postSharable = true;
                }
                console.log("postDetails", postDetails);

                if (postSharable) {
                    profileObj.sharedPost = sharedPost;
                    profileObj.sharedBy = sharedBy;
                    profileObj.sharedOn = Date.now();
                    profileObj.sharedText = sharedText;
                    profileObj.userid = postDetails.userid;
                    profileObj.text = postDetails.text;
                    profileObj.createdAt = postDetails.createdAt;
                    posts.create(profileObj, function (error, postrows) {
                        if (error) {
                            console.log("ERROR IN SHARING POSTS", error);
                            return res.status(500).json({ message: "Unauthorized Request" });
                        } else {
                            content.find({ postid: mongoose.Types.ObjectId(sharedPost) }, function (error, contentsData) {
                                if (error) {
                                    console.log(error);
                                    res.status(500).json({ message: error });
                                } else {

                                    if (contentsData.length == 0) {
                                        return res.status(200).json({ message: "You shared this post" });
                                    }
                                    for (let i = 0; i < contentsData.length; i++) {
                                        let contentObj = contentsData[i];
                                        delete contentObj._id;
                                        contentObj.postid = postrows._id;
                                        console.log('contentObj', contentObj);
                                        content.create(contentObj, function (error, conetnResp) {
                                            if (error) {
                                                console.log(error);
                                                res.status(500).json({ message: error });
                                            } else {
                                                console.log("contentsData.length", contentsData.length, i);
                                                if (i === contentsData.length-1) {
                                                    return res.status(200).json({ message: "You shared this post" });
                                                }
                                            }
                                        })

                                    }
                                }
                            });
                        }
                    });
                } else {
                    return res.status(200).json({ success: false, message: "You cannot share this post" });
                }
            }).catch((errorInFindingPost) => {
                console.log("ERROR IN FINDING POST", errorInFindingPost);
                return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
            })


        } else {
            return res.status(500).json({ message: "Unauthorized Request" })
        }
    } catch (error) {
        console.log("ERROR IN SHARING POST", error)
        return res.status(500).json({ message: error.message })
    }
}

postController.searchUserForInvitation = async (req, res) => {
    try {
        var search = req.body.searchingString;
        Users.find(
            {
                "$or": [{
                    "name": new RegExp(search.trim(), "i")
                }, {
                    "address": new RegExp(search.trim(), "i")
                }, {
                    "email": new RegExp(search.trim(), "i")
                }]
            },
            {
                _id: 1,
                name: 1,
                address: 1,
                avatar: 1,
                email: 1
            }
        ).then((searchedResults) => {
            // console.log("SEARCHED RESULTS", searchedResults);
            return res.status(200).json(searchedResults)
        }).catch((err) => {
            console.log("ERROR IN SEARCH USERS FOR INVITATION 1", err);
            res.status(500).json({ message: "Something went wrong!!!" });
        })
    } catch (error) {
        console.log("ERROR IN SHARING POST", error)
        return res.status(500).json({ message: error.message })
    }
}

postController.particularGroupPosts = async (req, res) => {
    try {
        // var groupid = mongoose.Types.ObjectId(req.body.groupId);
        var groupid = req.body.groupId;
        var userId = req.payload._id;
        var showGroup = false;
        GroupModel.findOne({ _id: groupid }).then(async (groupInfo) => {
            if (groupInfo.type === "free") {
                showGroup = true;
            } else {
                if (userId == groupInfo.userid) {
                    showGroup = true;
                } else {
                    await GroupFollowingModel.findOne({ groupid: groupid, userid: userId, status: "open" }).then((followedGroups) => {
                        if (followedGroups === null || followedGroups.status === "expired") {
                            showGroup = false;
                        } else if (followedGroups.status === "open") {
                            showGroup = true;
                        }
                    }).catch((err) => {
                        console.log("ERROR IN PARTICULAR GROUPS POSTS 1", err);
                        res.status(500).json({ message: "Something went wrong!!!" });
                    })
                }
            }
            if (showGroup === true) {
                posts.aggregate([
                    {
                        $match: {
                            // "groupid": { $exists: true },
                            groupid: mongoose.Types.ObjectId(groupid)
                        }
                    },
                    {
                        "$lookup": {
                            from: 'post_medias',
                            localField: '_id',
                            foreignField: 'postid',
                            as: 'contents'
                        }
                    },
                    {
                        "$lookup": {
                            from: 'users',
                            localField: 'userid',
                            foreignField: '_id',
                            as: 'userinfo'
                        }
                    },
                    {
                        '$unwind': {
                            path: '$userinfo'
                        }
                    },
                    {
                        "$project": {
                            _id: 1,
                            text: 1,
                            subject: 1,
                            groupid: 1,
                            userid: 1,
                            createdAt: 1,
                            "contents": 1,
                            "userinfo._id": 1,
                            "userinfo.name": 1,
                            "userinfo.avatar": 1,
                        }
                    },

                ]).sort({ createdAt: -1 }).exec(function (error, postlist) {
                    if (error) {
                        res.status(404).json({ message: error.message });
                    }
                    else {

                        var postids = [];
                        postlist.forEach(function (post) {
                            postids.push(post._id);
                        });
                        if (postids.length > 0) {
                            posts.aggregate([
                                {
                                    "$match": {
                                        parentid: { $in: postids },
                                    }
                                },
                                {
                                    "$lookup": {
                                        from: 'users',
                                        localField: 'userid',
                                        foreignField: '_id',
                                        as: 'userinfo'
                                    }
                                },
                                {
                                    '$unwind': {
                                        path: '$userinfo'
                                    }
                                },
                                {
                                    "$project": {
                                        _id: 1,
                                        text: 1,
                                        userid: 1,
                                        parentid: 1,
                                        createdAt: 1,
                                        "userinfo.name": 1,
                                        "userinfo.avatar": 1
                                    }
                                },
                            ]).sort({ createdAt: -1 }).exec(function (error, postcommentlist) {
                                if (error) {
                                    res.status(404).json({ message: error.message });
                                } else {
                                    if (postcommentlist.length > 0) {
                                        var wallpost = {};
                                        postcommentlist.forEach(function (item) {
                                            if (Object.keys(wallpost).indexOf(item.parentid.toString()) === -1) {
                                                wallpost[item.parentid] = [item];
                                            } else {
                                                wallpost[item.parentid].push(item)
                                            }
                                        });
                                        res.status(200).json({ post: postlist, comments: wallpost });
                                    } else {
                                        res.status(200).json({ post: postlist, comments: {} });
                                    }
                                }
                            });
                        } else {
                            res.status(200).json({ post: postlist, comments: {} });
                        }
                        // res.status(200).json(postlist)
                    }
                });
            } else {
                console.log("You have to follow this page");
                res.status(404).json({ message: "You have to follow this page" });
            }
        }).catch((err) => {
            console.log("ERROR IN PARTICULAR GROUP POSTS 2", err);
            res.status(500).json({ message: "Something went wrong!!!" });
        })
    } catch (error) {
        console.log("ERROR IN GETTING GROUP POSTS", error);
        return res.status(500).json({ message: error.message })
    }
}

/************************************************Hash tag work here *********************************************/
// Search in header bar
postController.searchStringFromPosts = async (req, res) => {
    try {
        var userid = req.payload._id;
        var search = '';
        var searchHashArray = [];
        if (req.query.search == 'blank') {
            search = '';
        } else {
            search = req.query.search;
        }
        if (search === "#") {
            return res.status(200).json([]);
        } else if (search === '') {
            return res.status(200).json([]);
        } else {
            var splitString = search.split("");
            if (splitString[0] === "#") {
                // posts.find(
                //     {
                //         "$or": [{
                //             "subject": new RegExp(search.trim(), "i")
                //         }, {
                //             "text": new RegExp(search.trim(), "i")
                //         }],
                //     }
                posts.aggregate([
                    {
                        $match: {
                            $and: [
                                {
                                    "$or": [{
                                        "subject": new RegExp(search.trim(), "i")
                                    }, {
                                        "text": new RegExp(search.trim(), "i")
                                    }]
                                },
                                {
                                    parentid: null
                                }
                            ]
                        }
                    },
                ]).then((searchedPosts) => {
                    searchedPosts.forEach((searchedPostElement) => {
                        var subjectArray = searchedPostElement.subject.split(" ");
                        var textArray = searchedPostElement.text.split(" ");
                        subjectArray.forEach((subjectArrayElement) => {
                            if (subjectArrayElement.startsWith(search)) {
                                if (searchHashArray.indexOf(subjectArrayElement) < 0) {
                                    searchHashArray.push(subjectArrayElement);
                                }
                            }
                        })
                        textArray.forEach((textArrayElement) => {
                            if (textArrayElement.startsWith(search)) {
                                if (searchHashArray.indexOf(textArrayElement) < 0) {
                                    searchHashArray.push(textArrayElement);
                                }
                            }
                        })
                    })
                    return res.status(200).json({ success: true, message: searchHashArray });
                }).catch((err) => {
                    console.log("ERROR IN SEARCH STRING FROM POSTS 2", err);
                    res.status(200).json({ success: false, message: "Something went wrong!!!" });
                })
            } else {
                Users.find(
                    {
                        "$or": [{
                            "name": new RegExp(search.trim(), "i"),
                            name: new RegExp(search.trim(), "i"),
                            _id: { $ne: mongoose.Types.ObjectId(userid) },
                            group: 'customer',
                        },
                            // {
                            //     "email": new RegExp(search.trim(), "i")
                            // }
                        ]
                    },
                    {
                        _id: 1,
                        name: 1,
                        avatar: 1,
                        email: 1
                    }
                ).sort({ name: 1 }).then((searchedResults) => {
                    // console.log("SEARCHED RESULTS", searchedResults);
                    return res.status(200).json({ success: true, message: searchedResults });
                }).catch((err) => {
                    console.log("ERROR IN SEARCH STRINGS FROM POST 1", err);
                    res.status(200).json({ success: false, message: "Something went wrong!!!" });
                })
            }
        }
    } catch (error) {
        return res.status(200).json({ success: false, message: error.message });
    }
}

// Search hashtag in the posts
postController.searchParticuarHashtag = async (req, res) => {
    try {
        var search = '';
        if (search === "#") {
            return res.status(200).json([]);
        } else if (search === '') {
            return res.status(200).json([]);
        } else {
            var splitString = search.split("");
            if (splitString[0] === "#") {

                posts.aggregate([
                    {
                        $match: {
                            "$or": [{
                                "subject": new RegExp(search.trim(), "i")
                            }, {
                                "text": new RegExp(search.trim(), "i")
                            }]
                        }
                    }
                ]).then((searchedPosts) => {
                    return res.status(200).json({ success: true, message: searchedPosts });
                }).catch((err) => {
                    console.log("ERROR IN SEARCH STRING FROM POSTS 2", err);
                    res.status(200).json({ success: false, message: "Something went wrong!!!" });
                })
            }
        }
    } catch (error) {
        return res.status(200).json({ success: false, message: error.message });
    }
}

postController.searchGroups = async (req, res) => {
    try {
        var userid = req.payload._id;
        var finalSearchedGroups = [];
        var search = '';

        if (req.params.search == 'blank') {
            search = '';
        } else {
            search = req.params.search;
        }
        if (search === '') {
            return res.status(200).json([]);
        } else {
            GroupModel.find(
                {
                    $and: [
                        {
                            "$or": [
                                {
                                    "name": new RegExp(search.trim(), "i"),
                                    // userid: {$ne: mongoose.Types.ObjectId(userid)}
                                }
                            ]
                        },
                        {
                            "$or": [
                                {
                                    privacy: "public",
                                },
                                {
                                    privacy: "private",
                                }
                            ]
                        }
                    ]
                },
                {
                    _id: 1,
                    name: 1,
                    Gtype: 1,
                    groupImage: 1
                }
            ).then(async (searchedGroups) => {
                var totalGroups = searchedGroups.length;
                for (let i = 0; i < totalGroups; i++) {
                    await BlockedUserModel.findOne({ groupid: searchedGroups[i]._id, userid: userid }).then((blockedUser) => {
                        // console.log("BLOCKED USER", blockedUser);
                        if (blockedUser) {

                        } else {

                            finalSearchedGroups.push(searchedGroups[i]);
                        }
                    }).catch((err) => {
                        console.log("ERROR IN SEARCH GROUPS 1", err);
                        res.status(500).json({ message: "Something went wrong!!!" });
                    })
                }
                if (finalSearchedGroups.length <= 0) {
                    return res.status(200).json({ message: [] })
                } else {
                    return res.status(200).json({ message: finalSearchedGroups });
                }
            }).catch((err) => {
                console.log("ERROR IN SEARCH GROUPS 2", err);
                res.status(500).json({ message: "Something went wrong!!!" });
            })
        }
    } catch (error) {
        console.log("ERROR IN SEARCHING POST", error)
        return res.status(500).json({ message: error.message })
    }
}

postController.findHastTagForHashTagPage = function (req, res, next) {
    try {
        var hash = req.query.hash.trim();
        var userid = req.payload._id;
        posts.aggregate([
            {
                $match: {
                    $or: [
                        {
                            parentid: { $exists: false },
                            commentid: { $exists: false },
                            groupid: { $exists: false },
                            text: { $regex: hash },
                            status: 'active',
                        },
                        {
                            parentid: { $exists: false },
                            commentid: { $exists: false },
                            groupid: { $exists: false },
                            subject: { $regex: hash },
                            status: 'active',
                        }
                    ]
                }
            },
            {
                "$lookup": {
                    from: 'post_medias',
                    localField: '_id',
                    foreignField: 'postid',
                    as: 'contents'
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'userid',
                    foreignField: '_id',
                    as: 'userinfo'
                }
            },
            {
                $unwind: '$userinfo'
            },
            {
                $project: {
                    id: 1,
                    userid: 1,
                    subject: 1,
                    groupid: 1,
                    text: 1,
                    'contents': 1,
                    'userinfo.avatar': 1,
                    'userinfo.name': 1
                }
            }
        ]).sort({ createdAt: 1 }).exec((error, postObj) => {
            if (error) {
                res.status(500).json({ message: error });
            } else {
                GroupModel.aggregate([
                    {
                        $match: {
                            userid: mongoose.Types.ObjectId(userid)
                        }
                    },
                ]).exec((error, groupObj) => {
                    if (error) {
                        res.status(500).json({ message: error });
                    } else {
                        GroupFollowingModel.aggregate([
                            {
                                $match: {
                                    userid: mongoose.Types.ObjectId(userid),
                                    status: "open"
                                }
                            }
                        ]).exec((error, groupSubscribe) => {
                            if (error) {
                                res.status(500).json({ message: error });
                            } else {
                                var groupid = [];
                                var groupSubscriberDetails = [];
                                if (groupObj.length > 0) {
                                    groupObj.forEach((item) => {
                                        groupid.push(item._id);
                                    });
                                }
                                if (groupSubscribe.length > 0) {
                                    groupSubscribe.forEach((element) => {
                                        groupSubscriberDetails.push(element.groupid)
                                    });
                                }
                                groupid.push(...groupSubscriberDetails);
                                posts.aggregate([
                                    {
                                        $match: {
                                            $or: [
                                                {
                                                    parentid: { $exists: false },
                                                    commentid: { $exists: false },
                                                    text: { $regex: hash },
                                                    status: 'active',
                                                    groupid: { $in: groupid },
                                                },
                                                {
                                                    parentid: { $exists: false },
                                                    commentid: { $exists: false },
                                                    subject: { $regex: hash },
                                                    status: 'active',
                                                    groupid: { $in: groupid },
                                                },
                                            ]
                                        }
                                    },
                                    {
                                        "$lookup": {
                                            from: 'post_medias',
                                            localField: '_id',
                                            foreignField: 'postid',
                                            as: 'contents'
                                        }
                                    },
                                    {
                                        $lookup: {
                                            from: 'users',
                                            localField: 'userid',
                                            foreignField: '_id',
                                            as: 'userinfo'
                                        }
                                    },
                                    {
                                        $unwind: '$userinfo'
                                    },
                                    {
                                        $project: {
                                            userid: 1,
                                            subject: 1,
                                            text: 1,
                                            "contents": 1,
                                            'userinfo.avatar': 1,
                                            'userinfo.name': 1
                                        }
                                    }
                                ]).sort({ createdAt: 1 }).exec((error, groupPost) => {
                                    if (error) {
                                        res.status(500).json({ message: error.message });
                                    } else {
                                        res.status(200).json({ post: postObj, group: groupPost });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    } catch (error) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}
// Search for searchPage
postController.searchFilter = function (req, res, next) {
    try {
        var search = req.params.search.trim();
        var userid = req.payload._id;
        // var userid = '5d7891425e1bb661f45cdd91';
        // var search = 'a';
        Users.aggregate([
            {
                $match: {
                    _id: { $ne: mongoose.Types.ObjectId(userid) },
                    group: 'customer',
                    name: new RegExp(search.trim(), "i"),
                }
            },
            // {
            //     $lookup: {
            //         from: 'followings',
            //         localField: 'userid',
            //         foreignField: 'followerid',
            //         as: 'followDetails'
            //     }
            // },
            {
                $project: {
                    _id: 1,
                    name: 1,
                    avatar: 1,
                    // 'followDetails': 1
                }
            }
        ]).sort({ name: 1 }).exec((error, friendObj) => {
            if (error) {
                res.status(500).json({ message: message.error });
            } else {
                // console.log('friendObj ', friendObj);
                // return;
                GroupModel.aggregate([
                    {
                        $match: {
                            name: new RegExp(search.trim(), "i")
                        }
                    },
                    {
                        $project: {
                            _id: 1,
                            name: 1,
                            groupImage: 1,
                        }
                    }
                ]).sort({ name: 1 }).exec((error, groupObj) => {
                    if (error) {
                        res.status(500).json({ message: message.error });
                    } else {
                        posts.aggregate([
                            {
                                $match: {
                                    parentid: { $exists: false },
                                    commentid: { $exists: false },
                                    groupid: { $exists: false },
                                    subject: new RegExp(search.trim(), "i"),
                                    status: 'active',
                                    payements: false
                                }
                            },
                            {
                                "$lookup": {
                                    from: 'post_medias',
                                    localField: '_id',
                                    foreignField: 'postid',
                                    as: 'contents'
                                }
                            },
                            {
                                $lookup: {
                                    from: 'users',
                                    localField: 'userid',
                                    foreignField: '_id',
                                    as: 'userinfo'
                                }
                            },
                            {
                                $unwind: '$userinfo'
                            },
                            {
                                $project: {
                                    _id: 1,
                                    subject: 1,
                                    text: 1,
                                    'contents': 1,
                                    'userinfo._id': 1,
                                    'userinfo.name': 1,
                                    'userinfo.avatar': 1,
                                }
                            }
                        ]).sort({ 'userinfo.name': 1 }).exec((error, postObj) => {
                            if (error) {
                                res.status(500).json({ message: message.error })
                            } else {
                                posts.aggregate([
                                    {
                                        $match: {
                                            parentid: { $exists: false },
                                            commentid: { $exists: false },
                                            groupid: { $exists: false },
                                            subject: { $regex: '#' + search },
                                            status: 'active',
                                            payements: false
                                        }
                                    },
                                    {
                                        $project: {
                                            _id: 1,
                                            subject: 1,
                                        }
                                    }
                                ]).sort({ subject: 1 }).exec((error, hashObj) => {
                                    if (error) {
                                        res.status(500).json({ message: message.error })
                                    } else {
                                        res.status(200).json({ 'post': postObj, 'friend': friendObj, 'group': groupObj, 'hash': hashObj });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: "Internal Server Error" });
    }
}
// postController.searchFilter();
/**********************************************Peer to peer message work here ************************************/
// Message Sending
postController.sendMessage = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var text = req.body.text;
        var id = req.body.id;
        var msgObj = {};
        var errorObj = {};

        msgObj.userid = userid;
        msgObj.message = text;
        msgObj.receiverid = id;

        if (typeof text !== 'undefined' && text && text !== '' &&
            typeof id !== 'undefined' && id && id !== '' &
            typeof userid !== 'undefined' && userid && userid !== '') {

            if (userid !== id) {


                // userprofilemsg.findOne({
                //     $and: [
                //         {
                //             $or: [
                //                 { "userid": userid },
                //                 { "userid": id }
                //             ]
                //         },
                //         {
                //             $or: [
                //                 { "receiverid": userid },
                //                 { "receiverid": id }
                //             ]
                //         }
                //     ]
                // }).then((foundExistingMessage) => {
                //     console.log("FOUND EXISTING MESSAGE", foundExistingMessage);
                // })



                userprofilemsg.create(msgObj, function (error, chatObj) {
                    if (error) {
                        res.status(500).json({ message: "Internal Server Error" });
                    } else {
                        res.status(200).json(chatObj);
                    }
                });
            } else {
                res.status(500).json({ message: "You can't message to your self" });
            }
        } else {
            errorObj.id = "id is required";
            errorObj.text = "message is required";
            errorObj.userid = "userid is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (e) {
        console.log(error);
        res.status(500).json({ message: "Internal Server Error" });
    }
}
// Message get
postController.receiveMsg = function (req, res, next) {
    try {
        var id = req.params.id;
        var myid = req.payload._id;
        userprofilemsg.aggregate([
            {
                $match: {
                    $or: [
                        {
                            userid: mongoose.Types.ObjectId(myid),
                            receiverid: mongoose.Types.ObjectId(id)
                        },
                        {
                            userid: mongoose.Types.ObjectId(id),
                            receiverid: mongoose.Types.ObjectId(myid)
                        }
                    ]
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'userid',
                    foreignField: '_id',
                    as: 'userinfo'
                }
            },
            {
                $unwind: '$userinfo'
            },
            {
                $project: {
                    message: 1,
                    'userinfo.avatar': 1,
                    'userinfo.name': 1
                }
            }
        ]).sort({ createdAt: 1 }).exec((error, userChatMsgs) => {
            if (error) {
                res.status(500).json({ message: error.message });
            } else {
                res.status(200).json(userChatMsgs);
            }
        })
    } catch (e) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}

/********************************************* Post and group Report ***********************************************/
// Report abuse
postController.reportPost = function (req, res, next) {
    try {
        var id = req.body.id;
        var report = req.body.report;
        var userid = req.payload._id;
        var groupid = req.body.groupid
        var reportObj = {};
        var errorObj = {};
        if (typeof id !== 'undefined' && id && id !== '' &&
            typeof report !== 'undefined' && report && report !== '' &&
            typeof userid !== 'undefined' && userid && userid !== '') {

            reportObj.postid = id;
            reportObj.reportType = report;
            reportObj.userid = userid;
            reportObj.notification = 'unseen';

            if (typeof groupid !== 'undefined' && groupid && groupid !== '') {
                reportObj.groupid = groupid;
            }
            postReport.create(reportObj, function (error, reportData) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json(reportData);
                }
            });
        } else {
            errorObj.id = "Id is required";
            errorObj.report = "Report is required";
            errorObj.userid = "Userid is required";
            res.status(400).json({ message: errorObj });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: "Internal Server Error" });
    }
}
// Get Report abuse for group admin
postController.getGroupAdminReport = function (req, res, next) {
    try {
        var userid = req.payload._id;
        posts.aggregate([
            {
                $match: {
                    userid: mongoose.Types.ObjectId(userid),
                    groupid: { $exists: true }
                }
            }
        ]).exec((error, userObj) => {
            if (error) {
                res.status(500).json({ message: error });
            } else {
                var id = [];
                if (userObj.length > 0) {
                    userObj.forEach((item) => {
                        id.push(item.userid)
                    });
                }
                GroupModel.aggregate([
                    {
                        $match: {
                            userid: { $in: id }
                        }
                    }
                ]).exec((error, groupInfo) => {
                    if (error) {
                        res.status(500).json({ message: error });
                    } else {
                        var postid = [];
                        if (groupInfo.length > 0) {
                            groupInfo.forEach((item) => {
                                postid.push(item._id)
                            });
                        }
                        postReport.aggregate([
                            {
                                $match: {
                                    groupid: { $in: postid }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'groupsbyusers',
                                    localField: 'groupid',
                                    foreignField: '_id',
                                    as: 'groupDetails'
                                }
                            },
                            {
                                $unwind: '$groupDetails'
                            },
                            {
                                $lookup: {
                                    from: 'posts',
                                    localField: 'postid',
                                    foreignField: '_id',
                                    as: 'postObj'
                                }
                            },
                            {
                                $unwind: '$postObj'
                            },
                            {
                                $lookup: {
                                    from: 'users',
                                    localField: 'userid',
                                    foreignField: '_id',
                                    as: 'reportUserinfo'
                                }
                            },
                            {
                                $unwind: '$reportUserinfo'
                            },
                            {
                                $project: {
                                    createdAt: 1,
                                    reportType: 1,
                                    'postObj._id': 1,
                                    'groupDetails.Gtype': 1,
                                    'groupDetails.groupImage': 1,
                                    'reportUserinfo.name': 1,
                                    'reportUserinfo.avatar': 1
                                }
                            }
                        ]).exec((error, reportDetails) => {
                            if (error) {
                                res.status(500).json({ message: error });
                            } else {
                                res.status(200).json(reportDetails);

                            }
                        });
                    }
                });
            }
        });
    } catch (error) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}
// Get Report abuse for admin report
postController.getAdminReport = function (req, res, next) {
    try {
        // var userid = req.payload._id;
        var userid = '5ba73d4fdfd2a11ed8933cef'; /** Admin */
        //  var userid = '5d7891425e1bb661f45cdd91'; /** Gaurav */
        // var userid = '5ce58c8637155653c4206f5a'; /** shubham */
        //  var userid = '5d78d66dab14033b109882a7' /** amit */
        Users.findOne({ _id: mongoose.Types.ObjectId(userid), group: 'super-admin' }, (error, userObj) => {
            if (error) {
                res.status(500).json({ message: error });
            } else {
                if (userObj !== null) {
                    // User report
                    postReport.aggregate([
                        {
                            $lookup: {
                                from: 'users',
                                localField: 'postid',
                                foreignField: '_id',
                                as: "reportuser"
                            }
                        },
                        {
                            $unwind: '$reportuser'
                        },
                        {
                            $lookup: {
                                from: 'users',
                                localField: 'userid',
                                foreignField: '_id',
                                as: "userinfo"
                            }
                        },
                        {
                            $unwind: '$userinfo'
                        },
                        {
                            $project: {
                                'userinfo._id': 1,
                                'userinfo.name': 1,
                                'userinfo.avatar': 1,
                                'reportuser._id': 1,
                                'reportuser.name': 1,
                                'reportuser.avatar': 1
                            }
                        }
                    ]).exec((error, reportDetails) => {
                        if (error) {
                            res.status(500).json({ message: error });
                        } else {
                            // post report
                            postReport.aggregate([
                                {
                                    $match: {
                                        groupid: { $exists: false }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: 'posts',
                                        localField: 'postid',
                                        foreignField: '_id',
                                        as: 'postDetails'
                                    }
                                }
                            ]).exec((error, postObj) => {
                                if (error) {
                                    res.status(500).json({ message: error });
                                } else {
                                    var postid = [];
                                    var id = [];
                                    if (postObj.length > 0) {
                                        postObj.forEach((item) => {
                                            postid.push(item.postid);
                                            id.push(item.userid)
                                        });
                                    }
                                    posts.aggregate([
                                        {
                                            $match: {
                                                _id: { $in: postid }
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: 'postreports',
                                                localField: '_id',
                                                foreignField: 'postid',
                                                as: 'reportDetails'
                                            }
                                        },
                                        {
                                            $unwind: '$reportDetails'
                                        },
                                        {
                                            $lookup: {
                                                from: 'users',
                                                localField: 'reportDetails.userid',
                                                foreignField: '_id',
                                                as: 'reportedDetails'
                                            }
                                        },
                                        {
                                            $unwind: '$reportedDetails'
                                        },
                                        {
                                            $project: {
                                                _id: 1,
                                                'reportDetails.reportType': 1,
                                                'reportedDetails.name': 1,
                                                'reportedDetails.avatar': 1
                                            }
                                        }
                                    ]).exec((error, reportObj) => {
                                        if (error) {
                                            res.status(500).json({ message: error });
                                        } else {
                                            // console.log('postReport #####################', reportObj, 'UserReport', reportDetails);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: "Internal Server Error" });
    }
}
postController.getAdminReport();

/****************************************************Notification bell icon ***************************************/
postController.notification = function (req, res, next) {
    try {
        var userid = req.payload._id;
        // var userid = '5ce58c8637155653c4206f5a';
        posts.aggregate([
            {
                $match: {
                    userid: mongoose.Types.ObjectId(userid),
                    parentid: { $exists: false }
                }
            }
        ]).exec((error, postdetails) => {
            if (error) {
                console.log(error);
                // res.status(500).json({ message: error });
            } else {
                // console.log('postdetails ', postdetails);
                var postid = [];
                if (postdetails.length > 0) {
                    postdetails.forEach((item) => {
                        postid.push(item._id)
                    });
                    // console.log('postid @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ', postid);
                }
                // Post like
                likes.aggregate([
                    {
                        $match: {
                            postid: { $in: postid },
                            userid: { $ne: mongoose.Types.ObjectId(userid) }
                        }
                    },
                    {
                        $lookup: {
                            from: 'users',
                            localField: 'userid',
                            foreignField: '_id',
                            as: 'userinfo'
                        }
                    },
                    {
                        $unwind: '$userinfo'
                    },
                    {
                        $project: {
                            _id: 1,
                            createdAt: 1,
                            "userinfo.name": 1,
                            "userinfo.avatar": 1
                        }
                    }
                ]).exec((error, likeObj) => {
                    if (error) {
                        console.log(error);
                    } else {
                        posts.aggregate([
                            {
                                $match: {
                                    parentid: { $in: postid },
                                    userid: { $ne: mongoose.Types.ObjectId(userid) },
                                }
                            },
                            {
                                $lookup: {
                                    from: 'users',
                                    localField: 'userid',
                                    foreignField: '_id',
                                    as: 'userinfo'
                                }
                            },
                            {
                                $unwind: '$userinfo'
                            },
                            {
                                $project: {
                                    text: 1,
                                    "userinfo.name": 1,
                                    "userinfo.avatar": 1
                                }
                            }
                        ]).sort({ createdAt: -1 }).exec((error, commentObj) => {
                            if (error) {
                                console.log(error);
                                // res.status(500).json({ message: error });
                            } else {
                                // All user comments details
                                posts.aggregate([
                                    {
                                        $match: {
                                            userid: mongoose.Types.ObjectId(userid),
                                            parentid: { $exists: true },
                                        }
                                    }
                                ]).exec((error, postdetails) => {
                                    if (error) {
                                        console.log(error);
                                        // res.status(500).json({ message: error });
                                    } else {
                                        var id = []
                                        if (postdetails.length > 0) {
                                            postdetails.forEach((item) => {
                                                id.push(item._id)
                                            });
                                        }
                                        // console.log('alluseridalluseridalluseridalluserid ', id);
                                        likes.aggregate([
                                            {
                                                $match: {
                                                    postid: { $in: id },
                                                    userid: { $ne: mongoose.Types.ObjectId(userid) }
                                                }
                                            },
                                            {
                                                $lookup: {
                                                    from: 'posts',
                                                    localField: 'postid',
                                                    foreignField: '_id',
                                                    as: 'postDetails'
                                                }
                                            },
                                            {
                                                $unwind: '$postDetails'
                                            },
                                            {
                                                $lookup: {
                                                    from: 'users',
                                                    localField: 'userid',
                                                    foreignField: '_id',
                                                    as: 'userinfo'
                                                }
                                            },
                                            {
                                                $unwind: '$userinfo'
                                            },
                                            {
                                                $project: {
                                                    createdAt: 1,
                                                    'postDetails.text': 1,
                                                    "userinfo.name": 1,
                                                    "userinfo.avatar": 1
                                                }
                                            }
                                        ]).sort({ createdAt: -1 }).exec((error, postObj) => {
                                            if (error) {
                                                console.log(error);
                                                // res.status(500).json({ message: error });
                                            } else {
                                                FollowerModel.aggregate([
                                                    {
                                                        $match: {
                                                            followerid: mongoose.Types.ObjectId(userid),
                                                            request: 'pending'
                                                        }
                                                    },
                                                    {
                                                        $lookup: {
                                                            from: 'users',
                                                            localField: 'userid',
                                                            foreignField: '_id',
                                                            as: 'userinfo'
                                                        }
                                                    },
                                                    {
                                                        $unwind: '$userinfo'
                                                    },
                                                    {
                                                        $project: {
                                                            _id: 1,
                                                            createdAt: 1,
                                                            notification: 1,
                                                            "userinfo.name": 1,
                                                            "userinfo.avatar": 1
                                                        }
                                                    }
                                                ]).sort({ createdAt: -1 }).exec((error, followObj) => {
                                                    if (error) {
                                                        res.status(500).json({ message: error });
                                                    } else {
                                                        var follow = {};
                                                        var unseenCount = 0;
                                                        follow.Objects = followObj;
                                                        if (followObj.length > 0) {
                                                            followObj.forEach((followObjElement) => {
                                                                if (followObjElement.notification === "unseen") {
                                                                    unseenCount++;
                                                                }
                                                            })
                                                        }
                                                        follow.unseenNotificationCount = unseenCount;
                                                        FollowerModel.aggregate([
                                                            {
                                                                $match: {
                                                                    userid: mongoose.Types.ObjectId(userid),
                                                                    request: 'accepted'
                                                                }
                                                            },
                                                            {
                                                                $lookup: {
                                                                    from: 'users',
                                                                    localField: 'followerid',
                                                                    foreignField: '_id',
                                                                    as: 'userinfo'
                                                                }
                                                            },
                                                            {
                                                                $unwind: '$userinfo'
                                                            },
                                                            {
                                                                $project: {
                                                                    _id: 1,
                                                                    createdAt: 1,
                                                                    notification: 1,
                                                                    "userinfo.name": 1,
                                                                    "userinfo.avatar": 1
                                                                }
                                                            }
                                                        ]).exec((error, followNotification) => {
                                                            if (error) {
                                                                res.status(500).json({ message: error });
                                                            } else {
                                                                userprofilemsg.aggregate([
                                                                    {
                                                                        $match: {
                                                                            receiverid: mongoose.Types.ObjectId(userid),
                                                                            notification: 'unseen'
                                                                        }
                                                                    },
                                                                    {
                                                                        $lookup: {
                                                                            from: 'users',
                                                                            localField: 'userid',
                                                                            foreignField: '_id',
                                                                            as: 'userinfo'
                                                                        }
                                                                    },
                                                                    {
                                                                        $unwind: '$userinfo'
                                                                    },
                                                                    {
                                                                        $project: {
                                                                            _id: 1,
                                                                            createdAt: 1,
                                                                            message: 1,
                                                                            "userinfo.name": 1,
                                                                            "userinfo.avatar": 1
                                                                        }
                                                                    }
                                                                ]).sort({ createdAt: -1 }).exec((error, msgObj) => {
                                                                    if (error) {
                                                                        res.status(500).json({ message: error });
                                                                    } else {
                                                                        res.status(200).json({ postLike: likeObj, comment: commentObj, commentLike: postObj, friendnotif: follow, followNotification: followNotification, msgnotif: msgObj });
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: "Internal Server Error" });
    }
}
// postController.notification();

var ctrl = module.exports = postController;