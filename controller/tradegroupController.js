const tradegroup = require('../model/tradeGroup');
const follower = require('../model/following');
const Users = require('../model/user');
const tradeMember = require('../model/tradeGroupMember');
var mongoose = require('mongoose');
var shell = require('shelljs');
var fs = require("fs");
var multer = require('multer');
var tradegroupchats = require('../model/tradegroupmessage');
var mediagroup = require('../model/tradegroupMedia');
var frvtGroup = require('../model/Tradegroupfavourite');
const async = require('async');
var tradeController = [];


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        var dir = "./public/upload/group_icon/" + req.payload._id;
        if (!fs.existsSync(dir)) {
            shell.mkdir('-p', dir);
        }
        cb(null, dir); // where to store it
    },
    filename: function (req, file, cb) {
        if (file.fileSize) {
            var err = new Error();
            return cb(err);
        }
        else if (!file.originalname.toLowerCase().match(/\.(png|jpg|jpeg|svg|pdf|docs|mp4)$/)) {
            var err = new Error();
            err.code = 'fieltype'; // to check on file type
            return cb(err, null);
        } else {
            var name = file.originalname.toLowerCase();
            var ext = name.substr(file.originalname.lastIndexOf('.') + 1)
            var rename = Date.now() + '.' + ext;
            cb(null, rename);
        }
    }
});
//this is new profile profile and update profile media
var upload = multer({
    storage: storage
}).any(); // name in form

//this is for chat message media
var storage1 = multer.diskStorage({
    destination: function (req, file, cb) {
        var dir = "./public/upload/chatmedia" + req.payload._id;
        if (!fs.existsSync(dir)) {
            shell.mkdir('-p', dir);
        }
        cb(null, dir); // where to store it
    },
    filename: function (req, file, cb) {
        if (file.fileSize) {
            var err = new Error();
            return cb(err);
        }
        else if (!file.originalname.toLowerCase().match(/\.(mp4|3gp|3gpp|asf|avi|dat|divx|dv|f4v|flv|m2ts|m4v|mkv|mod|mov|mpe|mpeg|mpeg4|mpg|mts|nsv|ogm|ogv|qt|tod|ts|vob|wmv|pdf|txt|doc|docx|docs|jpeg|jpg|png)$/)) {
            var err = new Error();
            err.code = 'fieltype'; // to check on file type
            return cb(err, null);
        } else {
            var name = file.originalname.toLowerCase();
            var ext = name.substr(file.originalname.lastIndexOf('.') + 1)
            var rename = Date.now() + '.' + ext;
            cb(null, rename);
        }
    }
});
//this is for chat message media
var uploadChatMedia = multer({
    storage: storage1
}).any(); // name in form

var chatRplyStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        var dir = "./public/upload/chatRplymedia/" + req.payload._id;
        if (!fs.existsSync(dir)) {
            shell.mkdir('-p', dir);
        }
        cb(null, dir); // where to store it
    },
    filename: function (req, file, cb) {
        if (file.fileSize) {
            var err = new Error();
            return cb(err);
        }
        else if (!file.originalname.toLowerCase().match(/\.(mp4|3gp|3gpp|asf|avi|dat|divx|dv|f4v|flv|m2ts|m4v|mkv|mod|mov|mpe|mpeg|mpeg4|mpg|mts|nsv|ogm|ogv|qt|tod|ts|vob|wmv|pdf|txt|doc|docx|docs|jpeg|jpg|png)$/)) {
            var err = new Error();
            err.code = 'fieltype'; // to check on file type
            return cb(err, null);
        } else {
            var name = file.originalname.toLowerCase();
            var ext = name.substr(file.originalname.lastIndexOf('.') + 1)
            var rename = Date.now() + '.' + ext;
            cb(null, rename);
        }
    }
});

var uploadChatReplyMedia = multer({
    storage: chatRplyStorage
}).any();


tradeController.getFollower = function (req, res, next) {
    try {
        var userid = req.payload._id;
        if (typeof userid !== 'undefined' && userid && userid != '') {
            follower.aggregate([
                { $match: { followerid: { $ne: mongoose.Types.ObjectId(userid) } } },
                {
                    "$lookup": {
                        from: 'users',
                        localField: 'followerid',
                        foreignField: '_id',
                        as: 'groupInfo'
                    }
                },
                { $match: { userid: mongoose.Types.ObjectId(userid) } },
                { $unwind: "$groupInfo" },
                {
                    $project: {
                        _id: -1,
                        "groupInfo.name": 1,
                        "groupInfo._id": 1,
                        "groupInfo.avatar": 1,
                    }
                },
            ]).exec(function (err, data) {
                if (err) {
                    res.status(500).json(err)
                }
                else {
                    res.status(200).json(data);
                }
            })
        }
        else {
            res.status(400).json({ message: 'unauthorized access' })
        }
    }
    catch (e) {
        res.status(400).json({ message: e.message })
    }
}

tradeController.createGroup = function (req, res, next) {
    try {
        var userid = req.payload._id;
        upload(req, res, function (err) {
            if (err) {
                if (err.code === 'fieltype') {
                    res.status(400).json({ message: 'File type is invalid. Only accepted .png/.jpg/.jpeg/.svg .' });
                }
                else {
                    res.status(400).json({ message: 'File was not able to be uploaded' });
                }
            }
            else {
                var name = req.body.name;
                var purpose = req.body.purpose;
                var groupmember = req.body.id.split(',');
                var memberid = []
                groupmember.forEach(function (item) {
                    memberid.push(item)
                })

                var tdObj = {};
                tdObj.name = name;
                tdObj.purpose = purpose;
                tdObj.memberid = memberid;
                tdObj.createrId = req.payload._id;
                var filesObj = {};
                if (req.files.length > 0) {
                    filesObj = req.files[0];
                    var string = filesObj.path + "";
                    var path = string.replace("public", '');
                    tdObj.image = path;
                }
                if (typeof userid !== 'undefined' && userid && userid != '') {
                    tradegroup.create(tdObj, function (err, result) {
                        if (err) {
                            res.status(500).json({ message: err.message })
                        }
                        else {
                            if (memberid.length > 0) {
                                var insertObjArr1 = [];
                                var creater = {
                                    groupid: result._id,
                                    memberid: req.payload._id
                                }
                                insertObjArr1.push(creater)
                                memberid.forEach(function (item) {
                                    insertObjArr1.push({ groupid: result._id, memberid: item });
                                })
                                tradeMember.create(insertObjArr1, function (err, data) {
                                    if (err) {
                                        res.status(500).json({ message: err.message })
                                    }
                                    else {
                                        res.status(200).json({ message: 'group created successfully' });
                                    }
                                })
                            }
                        }
                    })
                }
                else {
                    res.status(400).json({ message: 'unauthorized access' })
                }
            }
        })
    }
    catch (e) {
        res.status(400).json({ message: e.message })
    }
}
// group detail with members and all
tradeController.groupDetail = function (req, res, next) {
    try {
        var userid = req.payload._id;
        if (typeof userid != 'undefined' && userid && userid != '') {
            tradegroup.aggregate([
                {
                    "$lookup": {
                        from: 'trademembers',
                        localField: '_id',
                        foreignField: 'groupid',
                        as: 'groupdetails'
                    }
                },
                {
                    $unwind: "$groupdetails"
                },
                {
                    $match: {
                        "groupdetails.memberid": mongoose.Types.ObjectId(userid)
                    }
                },
                {
                    $project: {
                        name: 1,
                        purpose: 1,
                        image: 1,
                        createrId: 1,
                        createdAt: 1,
                        memberid: 1,
                        // numOfMembers: { $size: "$groupdetails" },
                    }
                }
            ]).exec(function (err, data) {
                if (err) { res.status(500).json({ message: err.message }) }
                else {
                    tradeController.getAllFavrtGroup(userid, function (resultObj) {
                        res.status(200).json({ data: data, resultObj: resultObj })
                    })

                }
            })

        }
        else {
            res.status(401).json({ message: 'unauthorized accesss' })
        }
    }
    catch (e) {
        res.status(400).json(e)
    }
}
tradeController.getdetailforUpdate = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var groupid = req.query.id;
        if (typeof userid != 'undefined' && userid && userid != '') {
            tradegroup.aggregate([
                { $match: { _id: mongoose.Types.ObjectId(groupid) } },
                {
                    "$lookup": {
                        from: 'trademembers',
                        localField: '_id',
                        foreignField: 'groupid',
                        as: 'groupdetails'
                    }
                }
            ]).exec(function (err, data) {
                if (err) { res.status(500).json({ message: err.message }) }
                else {
                    var obj = {};
                    obj = data[0];
                    res.status(200).json(obj)
                }
            })
        }
        else {
            res.status(401).json({ message: 'unauthorized accesss' })
        }
    }
    catch (e) {
        res.status(400).json(e)
    }
}

tradeController.updateTradeGroup = function (req, res, next) {
    try {
        var userid = req.payload._id;
        upload(req, res, function (err) {
            if (err) {
                if (err.code === 'fieltype') {
                    res.status(400).json({ message: 'File type is invalid. Only accepted .png/.jpg/.jpeg/.svg .' });
                }
                else {
                    res.status(400).json({ message: 'File was not able to be uploaded' });
                }
            }
            else {
                // var memberid = req.body.newid.split(',');
                // var oldFollower = req.body.oldFollower.split(',');
                var editgroup = {
                    name: req.body.name,
                    purpose: req.body.purpose,
                    // image: req.body.groupImage
                }
                // console.log(aiwan)
                var filesObj = {};
                if (req.files.length > 0) {
                    filesObj = req.files[0];
                    var string = filesObj.path + "";
                    var path = string.replace("public/", '');
                    editgroup.image = path;
                }
                if (typeof userid !== 'undefined' && userid && userid != '') {
                    tradegroup.findOneAndUpdate({ _id: req.body.groupID }, editgroup).exec(function (err, result) {
                        if (err) {
                            res.status(500).json({ message: err.message })
                        }
                        else {
                            //     if (memberid != '' && oldFollower) {
                            //         if (oldFollower.length > 0) {
                            //             tradeMember.remove({ groupid: result._id }, function (err1, data) {
                            //                 if (err1) {
                            //                     res.status(500).json({ message: err1.message })
                            //                 }
                            //                 else { }
                            //             })
                            //         }
                            //         var insertObjArr = [];
                            //         memberid.forEach(function (item) {
                            //             insertObjArr.push({ groupid: result._id, memberid: item });
                            //         })
                            //         console.log('-------------------   ',insertObjArr)
                            //         tradeMember.create(insertObjArr, function (err2, data) {
                            //             if (err2) {
                            //                 res.status(500).json({ message: err2.message })
                            //             }
                            //             else { 
                            //                 res.status(200).json({ message: 'Group updated successfully' });
                            //             }
                            //         })
                            res.status(200).json({ message: 'Group updated successfully' });
                            //     }
                            // }
                        }
                    })
                }
                else {
                    res.status(400).json({ message: 'unauthorized access' })
                }
            }
        })
    }
    catch (e) {
        res.status(400).json({ message: e.message })
    }
}

tradeController.deleteTradeGroup = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var groupId = req.query.id;
        var mediapath;
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            tradegroup.findOne({ _id: mongoose.Types.ObjectId(groupId) }, function (error, response) {
                if (error) { res.status(500).json({ message: err.message }) }
                else {
                    mediapath = response.image;
                    tradegroup.deleteOne({ _id: mongoose.Types.ObjectId(groupId) }, function (err, result) {
                        if (err) { res.status(500).json({ message: err.message }) }
                        else {
                            tradeMember.deleteMany({ groupid: mongoose.Types.ObjectId(groupId) }, function (er, data) {
                                if (er) { res.status(500).json({ message: er.message }) }
                                else {
                                    fs.unlink('./public/' + mediapath, function (err) {
                                    })

                                    res.status(200).json({ message: 'Group deleted successfully' })
                                }
                            })
                        }
                    })
                }
            })
        }
        else {
            console.log('unauthorized     + userid    access')
        }
    }
    catch (e) {
        res.status(400).json({ message: e.message })
    }
}

tradeController.getTradeGroupChat = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var groupid = req.query.id;
        if (typeof userid != 'undefined' && userid && userid != '') {
            async.parallel([
                function (callback) {
                    tradegroupchats.aggregate([
                        { $match: { groupid: mongoose.Types.ObjectId(groupid), parentId: { $exists: false } } },
                        {
                            "$lookup": {
                                from: 'tradegroupmedias',
                                localField: '_id',
                                foreignField: 'media_id',
                                as: 'chatInfo'
                            }
                        },
                        {
                            "$lookup": {
                                from: 'users',
                                localField: 'memberid',
                                foreignField: '_id',
                                as: 'userdetail'
                            }
                        },
                    ]).sort({ createdAt: -1 }).exec(function (errs, result) {
                        if (errs) {
                            return callback(errs.message, null);
                        }
                        else {
                            return callback(null, result);
                        }
                    })
                },
                function (callback) {
                    tradegroupchats.aggregate([
                        // { $match: { groupid: mongoose.Types.ObjectId(groupid) , parentId: { $exists: false } }  },
                        {
                            "$lookup": {
                                from: 'tradegroupmedias',
                                localField: '_id',
                                foreignField: 'media_id',
                                as: 'rplyMediaInfo'
                            }
                        },
                        {
                            "$lookup": {
                                from: 'users',
                                localField: "memberid",
                                foreignField: "_id",
                                as: 'chtrplyUser'
                            }
                        },
                        { $match: { groupid: mongoose.Types.ObjectId(groupid), parentId: { $exists: true } } },
                    ]).exec(function (errr, chatrply) {
                        if (errr) {
                            return callback(errr.message, null);
                        }
                        else {
                            return callback(null, chatrply)
                        }
                    })
                }
            ], function (error, cmpltdata) {
                if (error) {
                    console.log(error)
                    res.status(500).json({ message: error.message })
                } else {
                    tradeController.getAllMediaFile(userid, groupid, function (media) {
                        if (media.error) { console.log('error in media file fetching') }
                        else {
                            res.status(200).json({ cmpltdata, media })
                        }
                    })
                    // res.status(200).json(cmpltdata)
                }
            });
        }
        else {
            res.status(400).json({ message: 'unauthorized access' })
        }
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}


tradeController.getAllMediaFile = function (userid, groupid, callback) {
    try {
        if (userid != '' && userid && userid != 'undefined') {
            mediagroup.find({ groupid: mongoose.Types.ObjectId(groupid) }, function (err, media) {
                if (err) {
                    return callback({ status: 500, error: true, message: err.message });
                }
                else {
                    return callback({ error: false, media: media });
                }
            })
        }
        else {
            return callback({ status: 400, error: true, message: 'unauthorized access' });
        }
    }
    catch (e) {
        return callback({ status: 500, error: true, message: e.message });
    }
}


tradeController.saveGroupMessage = function (req, res, next) {
    try {
        var userid = req.payload._id;
        uploadChatMedia(req, res, function (err) {
            if (err) {
                if (err.code === 'fieltype') {
                    res.status(400).json({ message: 'File type is invalid. Only accepted .png/.jpg/.jpeg/.svg .' });
                }
                else {
                    res.status(400).json({ message: 'File was not able to be uploaded' });
                }
            }
            else {
                var messageInfo = {}
                messageInfo.message = req.body.message;
                messageInfo.groupid = req.body.groupid;
                messageInfo.memberid = req.payload._id;
                filesObj = req.files;
                var mediaList = [];
                if (typeof req.files !== 'undefined' && req.files.length > 0) {
                    filesObj.forEach(item => {
                        var path1 = item.path.replace("public", '')
                        mediaList.push(path1)
                    });
                }
                if (typeof userid != 'undefined' && userid && userid != '') {
                    tradegroupchats.create(messageInfo, function (err, data1) {
                        if (err) {
                            res.status(500).json({ message: err.message })
                        }
                        else {
                            var mediaId = data1._id;
                            if (filesObj) {
                                var mediaGroup = []
                                mediaList.forEach(data => {
                                    mediaGroup.push({ groupid: req.body.groupid, memberid: req.payload._id, mediaFile: data, media_id: mediaId })
                                })
                                mediagroup.create(mediaGroup, function (error, result) {
                                    if (error) {
                                        res.status(500).json({ message: error.message })
                                    }
                                    else { }
                                })
                            }
                            res.status(200).json({ message: 'message saved' })
                        }
                    })
                }
                else {
                    res.status(400).json({ message: 'unauthorized access' })
                }
            }
        })
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}

tradeController.saveMesageReply = function (req, res, next) {
    try {
        var userid = req.payload._id;
        uploadChatReplyMedia(req, res, function (err) {
            if (err) {
                if (err.code === 'fieltype') {
                    res.status(400).json({ message: 'File type is invalid. Only accepted .png/.jpg/.jpeg/.svg .' });
                }
                else {
                    res.status(400).json({ message: 'File was not able to be uploaded' });
                }
            }
            else {
                var msgObj = {};
                msgObj.msgrply = req.body.msgrply;
                msgObj.parentId = req.body.id;
                msgObj.groupid = req.body.groupid;
                msgObj.memberid = req.body.memberid;

                var rplymediaArray = [];
                replyMedia = req.files;
                if (typeof req.files !== 'undefined' && req.files.length > 0) {
                    replyMedia.forEach(item => {
                        var path1 = item.path.replace("public", '')
                        rplymediaArray.push(path1)
                    });
                }
                if (typeof userid != '' && userid && userid != 'undefined') {
                    tradegroupchats.create(msgObj, function (error, result) {
                        if (error) {
                            res.status(500).json({ message: error.message })
                        }
                        else {
                            var mediaId = result._id;
                            if (replyMedia) {
                                var mediaGroup = []
                                rplymediaArray.forEach(data => {
                                    mediaGroup.push({ groupid: req.body.groupid, memberid: req.payload._id, mediaFile: data, media_id: mediaId })
                                })
                                mediagroup.create(mediaGroup, function (err, data) {
                                    if (err) {
                                        res.status(500).json({ message: err.message })
                                    }
                                    else { }
                                })
                            }

                            res.status(200).json({ message: 'reply saved successfully' })
                        }
                    })
                }
                else {
                    res.status(400).json({ message: 'unauthorized access' })
                }
            }
        })
    }
    catch (e) {
        status(409).json({ message: e.message })
    }
}

tradeController.getAllGroupMembers = function (req, res, next) {
    try {
        var groupid = req.query.id;
        if (typeof groupid != '' && groupid && groupid != 'undefined') {
            tradeMember.aggregate([
                { $match: { groupid: mongoose.Types.ObjectId(groupid) } },
                {
                    "$lookup": {
                        from: 'users',
                        localField: 'memberid',
                        foreignField: '_id',
                        as: 'userdata'
                    }
                },
                { $unwind: "$userdata" },
                {
                    $project: {
                        "userdata.name": 1,
                        "userdata.avatar": 1,
                        "userdata._id": 1,
                    }
                }
            ]).exec(function (err, result) {
                if (err) { res.status(500).json({ message: err.message }) }
                else {
                    res.status(200).json(result)
                }
            })
        }
    }
    catch (e) {
        res.status(409).json({ message: e.message })
    }
}
tradeController.AddnewMembers = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var groupid = req.body.groupid;
        var memberid = [];
        var selectedOne = req.body.seleteduser;
        selectedOne.forEach(function (item) {
            memberid.push(item.id)
        })
        var insertObjArr = [];
        memberid.forEach(function (item) {
            tradeMember.findOne({ groupid: groupid, memberid: item }).then((memberAlreadyExists) => {
                if (memberAlreadyExists) {

                } else {
                    insertObjArr.push({ groupid: groupid, memberid: item });
                }
            }).catch((errorInFetchingMember) => {
                console.log("ERROR IN FETCHING MEMBER", errorInFetchingMember)
            })
        })
        if (typeof userid != '' && userid && userid != 'undefined') {
            if (memberid <= 0) {
                res.status(200).json({ success: true, message: 'Member(s) are already present in this group' });
            } else {
                tradeMember.create(insertObjArr, function (errs, result) {
                    if (errs) { res.status(500).json({ message: errs.message }) }
                    else {
                        res.status(200).json({ success: true, message: 'Member(s) added successfully' });
                    }
                })
            }
        }
        else {
            res.status(400).json({ message: 'unauthorized access' })
        }
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}

tradeController.setAsFrvtGroup = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var groupid = req.query.id;
        if (typeof userid != '' && userid && userid != 'undefined') {
            frvtGroup.findOne({ groupid: mongoose.Types.ObjectId(groupid), userid: userid }).then((foundAsFavourite) => {
                console.log("FOUND AS FAVOURITE", foundAsFavourite);
                if (foundAsFavourite) {
                    res.status(200).json({ message: 'This group is already your favourite group' });
                } else {
                    frvtGroup.create({ groupid: mongoose.Types.ObjectId(groupid), userid: userid }, function (err, result) {
                        if (err) {
                            res.status(500).json({ message: err.message });
                        }
                        else {
                            res.status(200).json({ message: 'This group is now favourite' });
                        }
                    })
                }
            })
        }
        else {
            res.status(400).json({ message: 'unauthorized access' })
        }
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}
tradeController.getAllFavrtGroup = function (userid, callback) {
    try {
        if (userid != '' && userid && userid != 'undefined') {
            frvtGroup.aggregate([
                {
                    "$lookup": {
                        from: 'tradegroups',
                        localField: 'groupid',
                        foreignField: '_id',
                        as: 'result'
                    }
                },
                { $match: { userid: mongoose.Types.ObjectId(userid) } },
            ]).exec(function (err, allfvrtgrp) {
                if (err) {
                    return callback({ status: 500, message: err.message });
                }
                else {
                    return callback({ allfvrtgrp: allfvrtgrp });
                }
            })
        }
        else {
            return callback({ status: 400, message: 'unauthorized access' });
        }
    }
    catch (e) {
        return callback({ status: 500, message: e.message });
    }
}

tradeController.removefvrtGroup = function (req, res, next) {
    try {
        var userid = req.payload._id;
        var groupid = req.query.id;
        if (typeof userid != '' && userid && userid != 'undefined') {
            frvtGroup.remove({ groupid: mongoose.Types.ObjectId(groupid) }, function (err, result) {
                if (err) { status(500).json({ message: err.message }) }
                else {
                    res.status(200).json({ status: true })
                }
            })
        }
        else {
            res.status(400).json({ message: 'unauthorized access' })
        }
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}
tradeController.allMembers = function (req, res, next) {
    try {
        var userid = req.payload._id;
        if (typeof userid != '' && userid && userid != 'undefined') {
            Users.find({ status: 'Active', _id: { $ne: userid } }).select({ "name": 1, "_id": 1 }).exec(function (err, result) {
                if (err) {
                    res.status(500).json({ message: err.message })
                }
                else {
                    res.status(200).json(result)
                }
            })
        }
        else {
            res.status(400).json({ message: 'unauthorized access' })
        }
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}

var ctrl = module.exports = tradeController
