// Npm-modules
const mongoose = require('mongoose');
const Users = require('../model/user');
const Kyc = require('../model/kyc');
const RestApi = require('./restApiController');
const crypto = require('crypto');
const Function = require('../others/functions');
let csc = require('country-state-city').default;
var basisController = [];
basisController.saveUserBasis = function (req, res, next) {
    try {
        var userid = req.query.userid;
        if (typeof userid !== 'undefined' && userid && userid) {
            Users.findOne({_id: mongoose.Types.ObjectId(userid)}).exec(function (error, udata) {
                if (error) {
                    res.status(500).json({message: 'User Profile not found'})
                } else {
                    if (udata !== null) {
                        if (typeof udata.name !== 'undefined' && udata.name && udata.name !== ''
                            && typeof udata.gender !== 'undefined' && udata.gender && udata.gender !== ''
                            && typeof udata.dob !== 'undefined' && udata.dob && udata.dob !== ''
                            && typeof udata.country !== 'undefined' && udata.country && udata.country !== '') {
                            //console.log('udata',udata);
                            /*
                            {
                                "key": "API key", string, required
                                "first_name": "First name", string, required.
                                "last_name": "Last name", string, required.
                                "middle_name": "Middle name", string.
                                "email": "email", string.
                                "phone": "phone", string.
                                "phone2": "phone2", string.
                                "gender": "gender", string, required
                                "birthday_day": "day", string, required
                                "birthday_month": "month", string, required
                                "birthday_year": "year", string, required
                                "country_nationality": "citizenship", string, required
                                "country_residence": "country", string.
                                "city": "city", string.
                                "address": "address", string.
                                "address2": "address2", string.
                                "zip": "zip", string.
                                "secret": "secret_key_2", string, required.
                             }
                             */
                            var dt = new Date(udata.dob);
                            var day = dt.getDate();
                            var month = Number(dt.getMonth()) + 1;
                            var year = dt.getFullYear();
                            var countryObj=csc.getCountryById(udata.country);
                            var countryCode = '';
                            if(countryObj && countryObj!==null && Object.keys(countryObj).length>0){
                                countryCode = countryObj.sortname;
                            }

                            var Obj = {
                                "key": process.env.BASISID_ACCESS_KEY,
                                "first_name": udata.name,
                                "last_name": udata.name,
                                "email" : udata.email,
                                "gender": udata.gender,
                                "birthday_day": day,
                                "birthday_month": month,
                                "birthday_year": year,
                                "country_nationality": countryCode,
                                "secret": process.env.BASISID_SECRET_KEY
                            }
                            if(udata.phone_verified===1){
                                Obj.phone = udata.phone
                            }
                            if(typeof udata.address!=='undefined' && udata.address && udata.address!==''){
                                Obj.address = udata.address
                            }
                            if(typeof udata.city!=='undefined' && udata.city && udata.city!=='') {
                                var cityObj = csc.getCityById(udata.city);
                                if(cityObj && cityObj!==null && Object.keys(cityObj).length>0){
                                    Obj.city = cityObj.name;
                                }
                            }
                            if(typeof udata.postalcode!=='undefined' && udata.postalcode && udata.postalcode!==''){
                                Obj.zip = udata.postalcode
                            }
                            //console.log('Obj',Obj);
                            //res.status(200).json(Obj);
                            RestApi.performRequest('basisid', 'auth/base-check', 'POST', '', Obj, function (response) {
                                if (response.status === 200) {
                                    res.status(response.status).json(response.message);
                                } else {
                                    res.status(response.status).json({message: response.message});
                                }
                            })
                        } else {
                            res.status(500).json({message: 'Profile not updated, name,gender,dob and country is required'})
                        }
                    } else {
                        res.status(500).json({message: 'User Profile not found'})
                    }
                }
            })
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}
basisController.handleBasisCallback = function (req, res, next) {
    try {
        var response = req.body;
        //console.log(req.body);
        var str = process.env.BASISID_SECRET_KEY2 + response.user_id + response.user_hash + response.status;
        if (typeof response.callback_type !== 'undefined' && response.callback_type && response.callback_type === 'request') {
            if (typeof response.reason !== 'undefined' && response.reason) {
                str = str + response.reason
            }
            if (typeof response.request !== 'undefined' && response.request) {
                str = str + response.request
            }
            if (typeof response.message !== 'undefined' && response.message) {
                str = str + response.message
            }
        } else {
            if (typeof response.autocheck_bad_reasons !== 'undefined' && response.autocheck_bad_reasons) {
                str = str + response.autocheck_bad_reasons
            }
            if (typeof response.screening_status !== 'undefined' && response.screening_status) {
                str = str + response.screening_status
            }
        }
        /*if (typeof response.autocheck_bad_reasons !== 'undefined' && response.autocheck_bad_reasons) {
            str = process.env.BASISID_SECRET_KEY2 + response.user_id + response.user_hash + response.status + response.autocheck_bad_reasons;
        } else {
            str = process.env.BASISID_SECRET_KEY2 + response.user_id + response.user_hash + response.status + response.reason + response.request + response.message;
        }*/
        var status = 'Reject';
        if (response.status === 2 || response.status === 10) {
            status = 'Approve';
        }
        var sign256hex = crypto.createHash('sha256').update(str).digest('hex');
        Kyc.update({hash: response.user_hash}, {
            $set: {
                status: status,
                response: response
            }
        }).exec(function (error, uprows) {
            console.log(error, uprows);
            /*if(status==='Approve'){
                Kyc.findOne({hash:response.user_hash}).exec(function (error,kycObj) {
                    if(!error && kycObj!==null){
                        var signatureStr = kycObj.hash+process.env.BASISID_SECRET_KEY2;
                        var signatureHash = crypto.createHash('sha256').update(signatureStr).digest('hex');
                        var endpoint = 'users/info/'+kycObj.hash+'/'+process.env.BASISID_ACCESS_KEY+'/'+signatureHash
                        RestApi.performRequest('basisid', endpoint, 'POST', '', null, function (response) {
                            /!*if (response.status === 200) {
                                res.status(response.status).json(response.message);
                            } else {
                                res.status(response.status).json({message: response.message});
                            }*!/
                            console.log('response',response)
                        })
                    }
                })
            }*/
            if (status === 'Approve') {
                Kyc.findOne({hash: response.user_hash}).exec(function (error, kycObj) {
                    console.log(error, kycObj);
                    if (!error && kycObj !== null) {
                        var signatureStr = kycObj.hash + process.env.BASISID_SECRET_KEY2;
                        var signatureHash = crypto.createHash('sha256').update(signatureStr).digest('hex');
                        var endpoint = 'users/info/' + kycObj.hash + '/' + process.env.BASISID_ACCESS_KEY + '/' + signatureHash
                        RestApi.performRequest('basisid', endpoint, 'GET', '', null, function (userinfo) {
                            if (userinfo.status === 200) {
                                var docObj = userinfo.message.profile;
                                console.log('response.message', userinfo.message);
                                console.log('docObj', docObj);
                                Kyc.update({hash: kycObj.hash}, {$set: {response: docObj}}).exec(function (error, uprows) {
                                    console.log(error, uprows);
                                    var methods = new Function(req);
                                    var url = process.env.BASISIDAPIURL_DOC + 'mobcontent/' + kycObj.hash + '/' + docObj.access_token;
                                    var dir = process.env.KYCDOCURL + kycObj.userid;
                                    if (typeof docObj.passport !== 'undefined' && docObj.passport && docObj.password !== '') {
                                        methods.downloadKYCFile(url, docObj.passport, dir + '/passport/')
                                    }
                                    if (typeof docObj.photo !== 'undefined' && docObj.photo && docObj.photo !== '') {
                                        methods.downloadKYCFile(url, docObj.photo, dir + '/photo/')
                                    }
                                    if (typeof docObj.utility_bill !== 'undefined' && docObj.utility_bill && docObj.utility_bill !== '') {
                                        methods.downloadKYCFile(url, docObj.utility_bill, dir + '/utility_bill/')
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
        if (sign256hex === response.signature) {
            console.log('Valid response')
            res.status(200).json('OK');
        } else {
            res.status(200).json('OK');
            console.log('Invalid response')
        }
    } catch (e) {
        console.log(e.message)
        res.status(200).json('OK');
    }
}
basisController.updateKYCLevelStatus = function (req, res, next) {
    try {
        var userid = req.payload._id;
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            var level = req.body.level;
            var userid = req.body.userid;
            var hashObj = req.body.hash;
            var status = req.body.status;
            if (typeof level !== 'undefined' && level && level !== ''
                && typeof userid !== 'undefined' && userid && userid !== ''
                && typeof hashObj !== 'undefined' && hashObj && Object.keys(hashObj).length > 0
                && typeof status !== 'undefined' && status && status !== '') {
                Kyc.update({userid: mongoose.Types.ObjectId(userid)}, {
                    level: level,
                    hash: hashObj.user_hash,
                    hashobj: hashObj,
                    status: status
                }).exec(function (error, uprows) {
                    if (error) {
                        res.status(500).json({message: error.message})
                    } else {
                        res.status(200).json("Updated")
                    }
                })
            } else {
                res.status(500).json({message: 'Information Missing'});
            }
        } else {
            res.status(401).json({message: 'Unauthorized Request'});
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}
basisController.getKYCDocuments = function (req, res, next) {
    try {
        var userid = req.payload._id;
        if (typeof userid !== 'undefined' && userid && userid !== '') {
            Kyc.findOne({userid: mongoose.Types.ObjectId(userid)}).exec(function (e, kycObj) {
                if (e) {
                    res.status(500).json({message: e.message})
                } else {
                    res.status(200).json(kycObj);
                }
            })
        } else {
            res.status(401).json({message: 'Unauthorized Request'});
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}
var ctrl = module.exports = basisController;