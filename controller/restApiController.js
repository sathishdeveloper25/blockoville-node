var querystring = require('querystring');
var Request = require("request");

module.exports.performRequest = function (hosttype, endpoint, method, authorization, data, success) {
    try {
        var host = '';
        switch (hosttype) {
            case 'wallet':
                host = process.env.WALLETSERVICE;
                break;
            case 'user':
                host = process.env.USERSERVICE;
                break;
            case 'basisid':
                host = process.env.BASISIDAPIURL;
                break;
            case 'mobcontent':
                host = process.env.BASISIDAPIURL_DOC;
                break;
        }
        if (typeof authorization !== 'undefined' && authorization && authorization !== '') {
            headers = {
                'Content-Type': 'application/json',
                "Authorization": authorization,
            };
        } else {
            headers = {
                'Content-Type': 'application/json'
            };
        }
        var options = {
            headers: headers,
        };
        if (method === 'GET') {
            if (data !== null) {
                endpoint += '?' + querystring.stringify(data);
            }
        }else{
            options.json = true;
            options.body = data;
        }
        if (host !== '') {
            if(method==='POST') {
                var url = host+endpoint;
                Request.post(host + endpoint, options, function (error, response, body) {
                    if (error && error !== null) {
                        success({status: 500, message: error.message});
                    } else {
                        if (typeof response.statusCode !== 'undefined' && response.statusCode && response.statusCode === 200) {
                            success({status: response.statusCode, message: body});
                        } else {
                            success({status: 500, message: response.statusMessage});
                        }
                    }
                });
            }else{
                Request.get(host + endpoint, options, function (error, response, body) {
                    if (error && error !== null) {
                        success({status: 500, message: error});
                    } else {
                        if (typeof response.statusCode !== 'undefined' && response.statusCode && response.statusCode === 200) {
                            var info = JSON.parse(body);
                            success({status: response.statusCode, message: info});
                        } else {
                            success({status: 500, message: response.statusMessage});
                        }
                    }
                });
            }
        }
    } catch (e) {
        console.log(e.message)
    }
}
