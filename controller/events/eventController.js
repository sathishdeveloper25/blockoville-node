
// NPM Modules
var mongoose = require('mongoose');
var multer = require('multer');
var fs = require('fs');
var shell = require('shelljs');
var pathmodule = require('path');


// Controller Name
var eventController = {};

// Required Models
const EventModel = require("../../model/events");
const UserModel = require("../../model/user");
const GroupModel = require("../../model/groupByUsers");

// Required Controllers





// Controller Functions

eventController.createEvent = async (req, res) => {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var eventName = req.body.eventName;
            var startDateAndTime = req.body.startDateAndTime;
            var endDateAndTime = req.body.endDateAndTime;
            var location = req.body.location;
            var description = req.body.description;
            var guestsCanInvite = req.body.guestsCanInvite;
            var groupId = req.body.groupId;
            var groupName = req.body.groupName;
            var userId = req.payload._id;
            var type = req.body.type;
            var paymentRequired = req.body.paymentRequired;
            var amount = req.body.amount;
            var currency = req.body.currency;
            var seatsAvailable = req.body.seatsAvailable;
            var website = req.body.website;
            var contactNumber = req.body.contactNumber;
            var contactEmail = req.body.contactEmail;

            if (startDateAndTime <= Date.now()) {
                return res.status(200).json({ success: false, message: "Cannot add a event in the back date!!!" });
            } else {
                EventModel.findOne({ eventName: eventName, startDateAndTime: startDateAndTime, location: location, creatorId: userId, paymentRequired: paymentRequired }).then((sameEventExists) => {
                    if (sameEventExists) {
                        // console.log("SAME EVENT EXITS");
                        return res.status(200).json({ success: false, message: "Event with the same name, location, date and payment criteria, is already created by you" });
                    } else {
                        var eventObject = {
                            eventName: eventName,
                            startDateAndTime: startDateAndTime,
                            endDateAndTime: endDateAndTime,
                            location: location,
                            description: description,
                            guestsCanInvite: guestsCanInvite,
                            userId: userId,
                            type: type,
                            paymentRequired: paymentRequired,
                            creatorId: userId,
                            seatsAvailable: seatsAvailable
                        };
                        if (groupId !== null && groupId !== undefined && groupId !== '' && groupName !== null && groupName !== undefined && groupName !== '') {
                            eventObject.groupId = groupId;
                            eventObject.groupName = groupName;
                        }
                        if (website !== null && website !== undefined && website !== '') {
                            eventObject.website = website;
                        }
                        if (contactNumber !== null && contactNumber !== undefined && contactNumber !== '') {
                            eventObject.contactNumber = contactNumber;
                        }
                        if (contactEmail !== null && contactEmail !== undefined && contactEmail !== '') {
                            eventObject.contactEmail = contactEmail;
                        }
                        if (paymentRequired) {
                            if (amount === undefined || amount === null || amount === "") {
                                return res.status(200).json({ success: false, message: "Amount Required, please enter amount" });
                            } else if (currency === undefined || currency === null || currency === "") {
                                return res.status(200).json({ success: false, message: "Currency Required, please enter currency" });
                            } else {
                                eventObject.amount = amount;
                                eventObject.currency = currency;
                            }
                        }
                        EventModel.create(eventObject).then((createdEvent) => {
                            if (createdEvent) {
                                return res.status(200).json({ success: true, status: "Event created!!!" });
                            } else {
                                return res.status(200).json({ success: false, message: "Something went wrong while creating event, please try again after sometime!!!" });
                            }
                        }).catch((errorInCreatingEvent) => {
                            console.log("ERROR IN CREATING EVENT", errorInCreatingEvent);
                            return res.status(200).json({ success: false, message: "Something went wrong while creating event, please try again after sometime!!!" });
                        })
                    }
                }).catch((errorInSearchingEvent) => {
                    console.log("ERROR IN FINDING SAME EVENT", errorInSearchingEvent);
                    return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime!!!" });
                })
            }
        } else {
            return res.status(200).json({ success: false, message: "Unauthorized Request!!!" });
        }
    } catch (error) {
        console.log("ERROR IN CREATING EVENT", error);
        return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime!!!" });
    }
}

eventController.updateEvent = async (req, res) => {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var eventId = req.body.eventId;
            var eventName = req.body.eventName;
            var startDateAndTime = req.body.startDateAndTime;
            var endDateAndTime = req.body.endDateAndTime;
            var location = req.body.location;
            var details = req.body.details;
            var guestsCanInvite = req.body.guestsCanInvite;
            // var groupId = req.body.groupId;
            // var groupName = req.body.groupName;
            var userId = req.payload._id;
            var type = req.body.type;
            var paymentRequired = req.body.paymentRequired;
            var amount = req.body.amount;
            var currency = req.body.currency;
            var updateObject = {};

            if (startDateAndTime <= Date.now()) {
                return res.status(200).json({ success: false, message: "Cannot update the starting date of the event in the back date!!!" });
            } else {
                EventModel.findOne({ _id: eventId }).then((foundExistingEvent) => {
                    // console.log("FOUND EXISTING EVENT", foundExistingEvent); 
                    if (foundExistingEvent) {
                        var eventCreator = foundExistingEvent.creatorId.toString();
                        if (eventCreator === userId.toString()) {
                            if (eventName !== "" && eventName !== null && eventName !== undefined && eventName) {
                                updateObject.eventName = eventName;
                            }
                            if (startDateAndTime !== "" && startDateAndTime !== null && startDateAndTime !== undefined && startDateAndTime) {
                                updateObject.startDateAndTime = startDateAndTime;
                            }
                            if (endDateAndTime !== "" && endDateAndTime !== null && endDateAndTime !== undefined && endDateAndTime) {
                                updateObject.endDateAndTime = endDateAndTime;
                            }
                            if (location !== "" && location !== null && location !== undefined && location) {
                                updateObject.location = location;
                            }
                            if (details !== "" && details !== null && details !== undefined && details) {
                                updateObject.details = details;
                            }
                            if (guestsCanInvite !== "" && guestsCanInvite !== null && guestsCanInvite !== undefined) {
                                updateObject.guestsCanInvite = guestsCanInvite;
                            }
                            if (type !== "" && type !== null && type !== undefined && type) {
                                updateObject.type = type;
                            }
                            if (paymentRequired !== "" && paymentRequired !== null && paymentRequired !== undefined) {
                                updateObject.paymentRequired = paymentRequired;
                            }
                            if (paymentRequired) {
                                if (amount === undefined || amount === null || amount === "") {
                                    return res.status(200).json({ success: false, message: "Amount Required, please enter amount" });
                                } else if (currency === undefined || currency === null || currency === "") {
                                    return res.status(200).json({ success: false, message: "Currency Required, please enter currency" });
                                } else {
                                    updateObject.amount = amount;
                                    updateObject.currency = currency;
                                }
                            }
                            // console.log("UPDATE OBJECT", updateObject);
                            EventModel.findByIdAndUpdate(eventId, updateObject).then((updatedObject) => {
                                // console.log("UPDATED OBJECT", updatedObject);
                                if (updatedObject) {
                                    return res.status(200).json({ success: true, message: "Event details has updated" });
                                }
                            }).catch((errorInUpdatingObject) => {
                                console.log("ERROR IN UPDATING OBJECT", errorInUpdatingObject);
                                return res.status(200).json({ success: false, message: "Something went wrong while updating event, please try after sometime!!!" });
                            })
                        } else {
                            return res.status(200).json({ success: false, message: "Only event creator can update the event" });
                        }
                    } else {
                        console.log("EVENT NOT FOUND");
                        return res.status(200).json({ success: false, message: "Something went wrong, in updating event" });
                    }
                }).catch((errorInSearchingEvent) => {
                    console.log("ERROR IN FINDING SAME EVENT", errorInSearchingEvent);
                    return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime!!!" });
                })
            }
        } else {
            return res.status(200).json({ success: false, message: "Unauthorized Request!!!" });
        }
    } catch (error) {
        console.log("ERROR IN UPDATE EVENT", error);
        return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
    }
}

eventController.deleteEvent = async (req, res) => {
    try {
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            var eventId = req.params.eventId;
            var userId = req.payload._id;
            EventModel.findById(eventId).then((foundEvent) => {
                if (foundEvent) {
                    var creator = foundEvent.creatorId.toString();
                    if (creator === userId.toString()) {
                        EventModel.findOneAndDelete({ _id: eventId }).then((deletedEvent) => {
                            // console.log("DELETED EVENT", deletedEvent);
                            return res.status(200).json({ success: true, message: "This event has removed" });
                        }).catch((errorInDeletingEvent) => {
                            console.log("ERROR IN DELETING EVENT", errorInDeletingEvent);
                            return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime!!!" });
                        })
                    } else {
                        return res.status(200).json({ success: false, message: "Only event creator can delete the evemt" });
                    }
                } else {
                    return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime!!!" });
                }
            }).catch((errorInFindingEvent) => {
                console.log("ERROR IN FINDING EVENT IN DELETE EVENT", errorInFindingEvent);
                return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime!!!" });
            })
        } else {
            return res.status(200).json({ success: false, message: "Unauthorized Request!!!" });
        }
    } catch (error) {
        console.log("ERROR IN DELETING EVENT", error);
        return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime!!!" });
    }
}

eventController.allEventsPastPresentFuture = async (req, res) => {
    try {
        var id = req.query.userId;
        var userId = req.payload._id;
        var pastEvents = [];
        var presentEvents = [];
        var futureEvents = [];
        var matchObject = {};
        if (id !== "" && id !== undefined && id !== null && id) {
            matchObject.creatorId = mongoose.Types.ObjectId(id);
        }
        if (typeof req.payload._id !== 'undefined' && req.payload._id && req.payload._id !== '') {
            EventModel.aggregate([
                {
                    $match: matchObject
                }
            ]).sort({ createdAt: -1 }).then((listOfAllEvents) => {
                listOfAllEvents.forEach((elementOfAllEvents) => {
                    if (elementOfAllEvents.endDateAndTime <= Date.now()) {
                        pastEvents.push(elementOfAllEvents);
                    } else if (elementOfAllEvents.startDateAndTime > Date.now()) {
                        futureEvents.push(elementOfAllEvents);
                    } else if (elementOfAllEvents.startDateAndTime <= Date.now() && elementOfAllEvents.endDateAndTime > Date.now()) {
                        presentEvents.push(elementOfAllEvents);
                    }
                })
                return res.status(200).json({ success: true, message: { pastEvents: pastEvents, presentEvents: presentEvents, futureEvents: futureEvents } });
            }).catch((errorInGettingListOfAllEvents) => {
                console.log("ERROR IN GETTING LIST OF ALL EVENTS", errorInGettingListOfAllEvents);
                return res.status(200).json({ success: false, message: "Something went wrong, please try after sometime" });
            })
        } else {
            return res.status(200).json({ success: false, message: "Unauthorized Request!!!" });
        }
    } catch (error) {
        console.log("ERROR IN ALL EVENTS PAST PRESENT AND FUTURE", error);
        return res.status(200).json({ success: false, message: "Error in loading of events" });
    }
}


var control = module.exports = eventController;