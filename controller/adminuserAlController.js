var async = require('async');
var passport = require('passport');
// const Members = require('../model/al/member');
const Users = require('../model/user');
const Security = require('../model/security');
var Function = require('../others/functions');
const Otp = require('../model/otp');
const Referral = require('../model/referral');
const announcement = require('../model/announcements');
const UserCounter = require('../model/usercounter');
const Sendgrid = require('../others/sendgrid');
const crypt = require('../others/cryptojs');
const Kyc = require('../model/kyc')
const Logs = require('../model/logs');
const jwt = require("jsonwebtoken");
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const countrylist = require('countries-list');
const Apiendpoints = require('../model/api_endpoints');
const ApiendpointAccess = require('../model/api_endpoint_access');
const UserApiKeys = require('../model/user_apikeys');
const Userliqproviders = require('../model/user_liqproviders');
const FollowerModel = require("../model/following");
const crypto = require('crypto');
var randomize = require('randomatic');

var mongoose = require('mongoose');

const Groups = require('../model/groups');
const AdminMenu = require('../model/adminmenus');
const AdminRules = require('../model/adminrules');
var passport = require('passport');

var adminuserAlController = {};

// common code start
adminuserAlController.get3faOptions = function (userid, callback) {
    try {
        Security.find({userid: userid}).sort({order: 1}).exec(function (error, options) {
            if (error) {
                callback({status: 500, message: 'Internal Server Error'})
            } else {
                callback({status: 200, message: options})
            }
        })
    } catch (e) {
        callback({status: 500, message: 'Internal Server Error'})
    }
}
adminuserAlController.sendEmailCode = function (userid, email, callback) {
    try {
        if (typeof userid !== 'undefined' && userid && userid !== '' && typeof email !== 'undefined' && email && email !== '') {
            var otpcodeData = {};
            var timestamp = Date.now();
            otpcodeData.userid = userid;
            otpcodeData.code = uniqid();
            otpcodeData.timestamp = timestamp;

            var sendGrid = new Sendgrid();
            var options = {
                email: email,
                code: otpcodeData.code,
                timestamp: timestamp
            };
            sendGrid.sendEmail(
                email,
                'Access Key ',
                "views/emailtemplate/keyemail.ejs",
                options
            );
            Otp.create(otpcodeData, function (error, rows) {
                if (error) {
                    callback({error: true, status: 500, message: 'Internal Server Error'})
                } else {
                    Users.update({email:options.email}, {$set: {otpcodeData:options.code,timestamp:options.timestamp}},function(err, useritem){
                        console.log('useritem : ',useritem);
                    });
                    callback({error: false, status: 200, message: timestamp});
                }
            });
        } else {
            callback({error: true, status: 400, message: 'Invalid Details'})
        }
    } catch (e) {
        callback({error: true, status: 500, message: 'Internal Server Error'})
    }
}
adminuserAlController.sendOTP = function (userid, phone, callback) {
    try {
        if (typeof userid !== 'undefined' && userid && userid !== '' && typeof phone !== 'undefined' && phone && phone !== '') {
            var timestamp = Date.now();
            var otpcodeData = {};
            otpcodeData.userid = userid;
            otpcodeData.code = otp();
            otpcodeData.timestamp = timestamp;
            console.log('phone', phone)
            smsClient.messages.create({
                body: 'Your Blockoville authentication otp is: ' + otpcodeData.code,
                to: phone,
                from: process.env.TWILLO_PHONE_NUMBER,
            }).then((data) => {
                console.log('data', data);
                Otp.create(otpcodeData, function (error, rows) {
                    if (error) {
                        callback({error: true, status: 500, message: 'Internal Server Error'})
                    } else {
                        callback({error: false, status: 200, message: timestamp})

                    }
                })
            }).catch((err) => {
                console.log('err', err)
                callback({error: true, status: 500, message: 'Internal Server Error'})
            });
        } else {
            callback({error: true, status: 400, message: 'Invalid Details'})
        }
    } catch (e) {
        callback({error: true, status: 500, message: 'Internal Server Error'})
    }
}
adminuserAlController.generateJwt = function (valobj) {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);
    //expiry.setMinutes(expiry.getMinutes()+5);
    return jwt.sign({
        _id: valobj._id,
        email: valobj.email,
        name: valobj.name,
        phone: valobj.phone,
        userid: valobj.userid,
        group: valobj.group,
        exp: parseInt(expiry.getTime() / 1000),
    }, process.env.JWT_TOKEN_SECRET); // DO NOT KEEP YOUR SECRET IN THE CODE!
}
adminuserAlController.extend = function (target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    return target;
}
// common code end









adminuserAlController.AdminLogin = function (req, res, next) {
    try {
        console.log('AdminLogin : ');
        var authflag = req.body.authflag;
        passport.authenticate('local', function (err, userinfo, info) {
            console.log(err)
            var token;
            // If Passport throws/catches an error
            if (err) {
                res.status(404).json(err);
                return;
            }
            console.log(userinfo)
            // If a user is found
            if (userinfo) {
                if (userinfo.group !== 'customer') {
                    var methods = new Function(req);
                    if (authflag) {
                        ctrl.get3faOptions(userinfo._id, function (options) {
                            if (options.status === 200 && options.message.length > 0) {
                                var userid = userinfo._id;
                                var email = userinfo.email;
                                var phone = userinfo.phone;
                                options.message.forEach(function (item, index) {
                                    if (item.option === 'email') {
                                        ctrl.sendEmailCode(userid, email, function (result) {
                                        })
                                    }
                                    if (item.option === 'sms') {
                                        ctrl.sendOTP(userid, phone, function (result) {
                                        })
                                    }
                                })
                                res.status(200).json(options.message);
                            } else {
                                methods.addLoginLog(userinfo);
                                token = ctrl.generateJwt(userinfo);
                                res.status(200);
                                res.json({
                                    "token": token
                                });
                            }
                        })
                    } else {
                        methods.addLoginLog(userinfo);
                        token = ctrl.generateJwt(userinfo);
                        res.status(200);
                        res.json({
                            "token": token
                        });
                    }
                } else {
                    res.status(403).json({message: 'Only admin user allowed'});
                }
            } else {
                // If user is not found
                res.status(401).json(info);
            }
        })(req, res);
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}





adminuserAlController.getAdminPanelMenu = function (req,res,next) {
    try{
        console.log('getAdminPanelMenu : ');
        var userid = req.payload._id;
        console.log('userid : ',userid);
        if(typeof userid!=='undefined' && userid && userid!==''){
            Groups.aggregate([
                {
                    "$lookup": {
                        from: "users",
                        localField: "group",
                        foreignField: "group",
                        as: "user"
                    }
                },
                {
                    "$match":{
                        "user._id":mongoose.Types.ObjectId(userid)
                    }
                },
                {
                    "$unwind":"$user"
                },
                {
                    "$project":{
                        group:1,
                        type:1,
                        status:1,
                        createdAt:1,
                        "user._id":1
                    }
                }
            ]).exec(function (error,group) {
                if(error){
                    console.log(error)
                }else{
                    console.log('group : ',group);
                    if(group.length>0) {
                        AdminRules.aggregate([
                            {
                                "$lookup": {
                                    from: "adminmenus",
                                    localField: "menu_id",
                                    foreignField: "_id",
                                    as: "menus"
                                },
                            },
                            {
                                "$unwind": "$menus"
                            },
                            {
                                "$match": {
                                    "group_id": group[0]._id
                                }
                            }
                        ]).exec(function (error, rules) {
                            if (error) {
                                console.log(error, rules)
                            } else {
                                if (rules.length > 0) {
                                    var menulist = [];
                                    var submenulist = {};
                                    rules.forEach(function (item) {
                                        if (typeof item.menus.parentmenu !== 'undefined' && item.menus.parentmenu && item.menus.parentmenu !== '') {
                                            if (Object.keys(submenulist).indexOf(item.menus.parentmenu.toString()) === -1) {
                                                submenulist[item.menus.parentmenu] = [{
                                                    _id: item.menus._id.toString(),
                                                    permission: item.permission,
                                                    menu: item.menus.menu,
                                                    menu_url: item.menus.menu_url,
                                                    order: item.menus.order,
                                                    styleclass: item.menus.styleclass,
                                                    status: item.menus.status
                                                }]
                                            } else {
                                                submenulist[item.menus.parentmenu].push({
                                                    _id: item.menus._id.toString(),
                                                    permission: item.permission,
                                                    menu: item.menus.menu,
                                                    menu_url: item.menus.menu_url,
                                                    order: item.menus.order,
                                                    styleclass: item.menus.styleclass,
                                                    status: item.menus.status
                                                });
                                            }
                                        } else {
                                            menulist.push({
                                                _id: item.menus._id.toString(),
                                                permission: item.permission,
                                                menu: item.menus.menu,
                                                menu_url: item.menus.menu_url,
                                                order: item.menus.order,
                                                styleclass: item.menus.styleclass,
                                                status: item.menus.status
                                            })
                                        }

                                    })
                                    res.status(200).json({menu: menulist, submenu: submenulist});
                                }else{
                                    res.status(500).json({message: 'Menu not assigned'})
                                }
                            }
                        })
                    }else{
                        res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                    }
                }
            })

            /*AdminMenu.aggregate([
                {
                    "$lookup": {
                        from: "adminmenus",
                        localField: "_id",
                        foreignField: "parentmenu",
                        as: "submenu"
                    },
                    "$lookup": {
                        from: "adminrules",
                        localField: "_id",
                        foreignField: "menu_id",
                        as: "rules"
                    }
                },
                {
                    $match: {
                        "parentmenu": {$exists: false},
                        ""
                    }
                },
                {"$sort": {"order": 1}}
            ]).exec(function (error, menulist) {
                if (error) {
                    res.status(500).json({message: 'An anonymous error was occurred, please try again.'})
                } else {
                    console.log(menulist);
                    res.status(200).json(menulist);
                }
            })*/
        }else{
            res.status(401).json({message: 'Unauthorized Request'})
        }
    }catch (error){
        res.status(500).json({message: error.message})
    }
}






var ctrl = module.exports = adminuserAlController

