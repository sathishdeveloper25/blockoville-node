var async = require('async');
var passport = require('passport');
// const Members = require('../model/al/member');
const Users = require('../model/user');
const Security = require('../model/security');
var Function = require('../others/functions');
const Otp = require('../model/otp');
const Referral = require('../model/referral');
const announcement = require('../model/announcements');
const UserCounter = require('../model/usercounter');
const Sendgrid = require('../others/sendgrid');
const crypt = require('../others/cryptojs');
const Kyc = require('../model/kyc')
const Logs = require('../model/logs');
const jwt = require("jsonwebtoken");
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const countrylist = require('countries-list');
const Apiendpoints = require('../model/api_endpoints');
const ApiendpointAccess = require('../model/api_endpoint_access');
const UserApiKeys = require('../model/user_apikeys');
const Userliqproviders = require('../model/user_liqproviders');
const FollowerModel = require("../model/following");
const crypto = require('crypto');
var randomize = require('randomatic');
const { base64encode, base64decode } = require('nodejs-base64');
var request = require('request');

var randomstring = require("randomstring");
var uniqid = require('uniqid');
var rn = require('random-number');
var otp = rn.generator({
    min: 1000000,
    max: 10000000,
    integer: true
});
var mongoose = require('mongoose');

var accountSid = process.env.TWILLO_ACCOUNT_SID;
var authToken = process.env.TWILLO_ACCOUNT_AUTH_TOKEN;
var twilio = require('twilio');
// var smsClient = new twilio(accountSid, authToken);


var Actions = require('../model/actions');
var useragent = require('useragent');
var fs = require("fs");
var shell = require('shelljs')
var multer = require('multer');
const RestApi = require('./restApiController');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        var dir = process.env.KYCDOCURL + req.payload._id
        //var dir = "./public/uploads/kyc/" + req.payload._id;
        if (!fs.existsSync(dir)) {
            shell.mkdir('-p', dir);
        }
        cb(null, process.env.KYCDOCURL + req.payload._id); // where to store it
    },
    filename: function (req, file, cb) {
        if (file.fileSize) {
            var err = new Error();
            return cb(err);
        }
        else if (!file.originalname.toLowerCase().match(/\.(png|jpg|jpeg|pdf)$/)) {
            var err = new Error();
            err.code = 'fieltype'; // to check on file type
            return cb(err, null);
        } else {
            var date = new Date();
            var timeStamp = date.getTime();
            var name = file.originalname.toLowerCase();
            var ext = name.substr(file.originalname.lastIndexOf('.') + 1)
            var rename = timeStamp + "." + ext;
            cb(null, rename);
        }
    }
});
var upload = multer({
    storage: storage,
    limits: {fileSize: 3145728} // Max file size: 3MB (3145728 Bytes)
}).any(); // name in form
var bcryptJS = require('bcrypt');
const Kycplans = require('../model/kycplans');
const ct = require('countries-and-timezones');
const DeviceDetector = require('node-device-detector');

var userAlController = {};


// common code start
userAlController.get3faOptions = function (userid, callback) {
    try {
        Security.find({userid: userid}).sort({order: 1}).exec(function (error, options) {
            if (error) {
                callback({status: 500, message: 'Internal Server Error'})
            } else {
                callback({status: 200, message: options})
            }
        })
    } catch (e) {
        callback({status: 500, message: 'Internal Server Error'})
    }
}
userAlController.sendEmailCode = function (userid, email, callback) {
    try {
        if (typeof userid !== 'undefined' && userid && userid !== '' && typeof email !== 'undefined' && email && email !== '') {
            var otpcodeData = {};
            var timestamp = Date.now();
            otpcodeData.userid = userid;
            otpcodeData.code = uniqid();
            otpcodeData.timestamp = timestamp;

            var sendGrid = new Sendgrid();
            var options = {
                email: email,
                code: otpcodeData.code,
                timestamp: timestamp
            };
            sendGrid.sendEmail(
                email,
                'Access Key ',
                "views/emailtemplate/keyemail.ejs",
                options
            );
            Otp.create(otpcodeData, function (error, rows) {
                if (error) {
                    callback({error: true, status: 500, message: 'Internal Server Error'})
                } else {
                    Users.update({email:options.email}, {$set: {otpcodeData:options.code,timestamp:options.timestamp}},function(err, useritem){
                        console.log('useritem : ',useritem);
                    });
                    callback({error: false, status: 200, message: timestamp});
                }
            });
        } else {
            callback({error: true, status: 400, message: 'Invalid Details'})
        }
    } catch (e) {
        callback({error: true, status: 500, message: 'Internal Server Error'})
    }
}
userAlController.sendOTP = function (userid, phone, callback) {
    try {
        if (typeof userid !== 'undefined' && userid && userid !== '' && typeof phone !== 'undefined' && phone && phone !== '') {
            var timestamp = Date.now();
            var otpcodeData = {};
            otpcodeData.userid = userid;
            otpcodeData.code = otp();
            otpcodeData.timestamp = timestamp;
            console.log('phone', phone)
            smsClient.messages.create({
                body: 'Your Blockoville authentication otp is: ' + otpcodeData.code,
                to: phone,
                from: process.env.TWILLO_PHONE_NUMBER,
            }).then((data) => {
                console.log('data', data);
                Otp.create(otpcodeData, function (error, rows) {
                    if (error) {
                        callback({error: true, status: 500, message: 'Internal Server Error'})
                    } else {
                        callback({error: false, status: 200, message: timestamp})

                    }
                })
            }).catch((err) => {
                console.log('err', err)
                callback({error: true, status: 500, message: 'Internal Server Error'})
            });
        } else {
            callback({error: true, status: 400, message: 'Invalid Details'})
        }
    } catch (e) {
        callback({error: true, status: 500, message: 'Internal Server Error'})
    }
}
userAlController.generateJwt = function (valobj) {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);
    //expiry.setMinutes(expiry.getMinutes()+5);
    console.log(valobj.name,'valobj.name')
    return jwt.sign({
        _id: valobj._id,
        email: valobj.email,
        name: valobj.name,
        phone: valobj.phone,
        userid: valobj.userid,
        group: valobj.group,
        exp: parseInt(expiry.getTime() / 1000),
    }, process.env.JWT_TOKEN_SECRET); // DO NOT KEEP YOUR SECRET IN THE CODE!
}
userAlController.extend = function (target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    return target;
}
// common code end


userAlController.UserLogin = function (req, res, next) {
    try {
        console.log('req.body.email in UserLogin: ',req.body.email);
        var authflag = req.body.authflag;
        Users.findOne({email: req.body.email}).exec(function (error, isuser) {
            console.log('isuser : ',isuser);
            if(isuser == null){
                res.status(401).json({message:'Email Id not found'});
            } else {
                if(!isuser.email_verified){
                    res.status(401).json({message:'Email not activated'});
                } else {
                    passport.authenticate('local', function (err, userinfo, info) {
                        var token;
                        // If Passport throws/catches an error
                        if (err) {
                            res.status(404).json(err);
                            return;
                        }
                        // console.log('err : ',err);
                        // console.log('userinfo : ',userinfo);
                        // console.log('info : ',info);
                        // If a user is found
                        if (userinfo) {
                            if (userinfo.group == 'customer') {
                                var methods = new Function(req);
                                if (authflag) {
                                    ctrl.get3faOptions(userinfo._id, function (options) {
                                        if (options.status === 200 && options.message.length > 0) {
                                            var userid = userinfo._id;
                                            var email = userinfo.email;
                                            var phone = userinfo.phone;
                                            options.message.forEach(function (item, index) {
                                                if (item.option === 'email') {
                                                    ctrl.sendEmailCode(userid, email, function (result) {
                                                    })
                                                }
                                                if (item.option === 'sms') {
                                                    ctrl.sendOTP(userid, phone, function (result) {
                                                    })
                                                }
                                            })
                                            res.status(200).json(options.message);
                                        } else {
                                            methods.addLoginLog(userinfo);
                                            token = ctrl.generateJwt(userinfo);
                                            var ret_json ={
                                                "token": token,
                                                "userinfo": userinfo.legalname,
                                                "message":"Login Successful"
                                            };
                                            console.log('ret_json : ',ret_json);
                                            res.status(200);
                                            res.json(ret_json);
                                        }
                                    })
                                } else {
                                    methods.addLoginLog(userinfo);
                                    token = ctrl.generateJwt(userinfo);
                                    var ret_json ={
                                        "token": token,
                                        "userinfo": userinfo.legalname,
                                        "message":"Login Successful"
                                    };
                                    console.log('ret_json : ',ret_json);
                                    res.status(200);
                                    res.json(ret_json);
                                }
                            } else {
                                res.status(403).json({message: 'Only user allowed'});
                            }
                        } else {
                            res.status(401).json({message:'Incorrect Password'});
                        }
                    })(req, res);
                }
            }
        });
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}

userAlController.saveUser = function (req, res, next) {
    console.log('userAlController in saveUser : ');
    try {
        var email = req.body.email;
        var phone = '';
        if(typeof req.body.full_phone!='undefined'){
            var phone = req.body.full_phone;
        }

        var password = req.body.password;
        var passwordrepeat = req.body.passwordrepeat;
        // var group = req.body.group;
        var group = 'customer';
        var name = req.body.name;
        var liquidity = req.body.liquidity;

        var errorObj = {};

        var passtest = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
        var emailtest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var profileObj = {};

        profileObj.email = email;
        profileObj.name = name;
        profileObj.country = req.body.country;
        profileObj.password = password;
        profileObj.passwordrepeat = passwordrepeat;
        profileObj.group = group;

        console.log('validation : ');
        if (typeof group !== 'undefined' && group !== '') {
            console.log('group')
            profileObj.group = group;
        } else {
            console.log("usergroup")
            errorObj.group = 'User Group is required';
        }

        if (Object.keys(errorObj).length === 0) {
            console.log('errorObj : ',errorObj);
            bcryptJS.hash(req.body.password, 10, function (err, hash) {
                if (err) {
                    res.status(500).json({message: err.message});
                } else {
                    //var passobj = jwtmodel.setPassword(req.body.password);
                    profileObj = ctrl.extend({}, profileObj, {password: hash});

                    var counterseqence = {};

                    async.waterfall([
                        function (done) {
                            UserCounter.findOne({findfield:'userid'}).exec(function (error, counterIndex) {
                                if(error){
                                    console.log('error : ',error);
                                    res.status(500).json({message: 'Error occured, Please try again.'})
                                } else {
                                    console.log('counterIndex : ',counterIndex);
                                    if(counterIndex!=null){
                                        counterseqence = counterIndex;
                                        done()
                                    } else {
                                        var insArr = {
                                            'findfield': 'userid',
                                            'sequence_value': 0
                                        };
                                        var newcounter = new UserCounter(insArr);
                                        console.log('newcounter : ',newcounter);
                                        newcounter.save(function(error) {
                                            if(error){
                                                console.log('newcounter save error : ',error);
                                                res.status(500).json({message: 'Error occured, Please try again.'})
                                            } else {
                                                console.log('newcounter : ',newcounter);
                                                counterseqence = newcounter;
                                                done();
                                            }
                                        });
                                    }
                                }
                            });
                        },
                        function (done) {
                            console.log('counterseqence : ',counterseqence);

                            var oldSeqVal = Number(counterseqence.sequence_value);
                            profileObj.userid = oldSeqVal+1;
                            var profileId = Math.random().toString(36).substr(2, 8);
                            var depid = randomize('000000000');
                            var refcode = 'DEP' +profileId +depid;
                            profileObj.profileid = profileId;
                            profileObj.bankrefcode = refcode;
                            // profileObj.phone = randomize('0  00000000');;
                            console.log('profileObj : ',profileObj);

                            Users.create(profileObj, function (error, user) {
                                if (error) {
                                    console.log('Users.create error : ',error);
                                    if (error.code && error.code === 11000) {
                                        if (error.errmsg.indexOf('email_1') > -1) {
                                            res.status(400).json({
                                                message: "Email already exists, please try another."
                                            })
                                        } else if (error.errmsg.indexOf('phone_1') > -1) {
                                            res.status(400).json({
                                                message: "Phone already exists, please try another."
                                            })
                                        } else {
                                            res.status(400).json({
                                                message: "Enter all required fields, please try again."
                                            })
                                        }
                                    } else {
                                        console.log('user : ',user);
                                        res.status(500).json({message: "Internal Server Error"});
                                    }
                                } else {
                                    console.log('user create : ');
                                    if (group !== 'customer') {
                                        console.log("customer");
                                        if (typeof liquidity !== 'undefined' && liquidity && Object.keys(liquidity).length > 0) {
                                            var liqkeys = Object.keys(liquidity);
                                            var liqArr = []
                                            liqkeys.forEach((liq) => {
                                                liqArr.push({
                                                    userid: mongoose.Types.ObjectId(user._id),
                                                    liquidity: liq
                                                })
                                            })
                                            if (liqArr.length > 0) {
                                                Userliqproviders.create(liqArr, function (error, liqrows) {
                                                })
                                            }
                                        }
                                    }

                                    var otpcodeData = {};
                                    var timestamp = Date.now();
                                    otpcodeData.userid = user._id;
                                    otpcodeData.code = uniqid();
                                    otpcodeData.timestamp = timestamp;
                                    var sendGrid = new Sendgrid();
                                    var verificationObj = {
                                        'id': user._id,
                                        'email': profileObj.email,
                                        'phone': profileObj.phone,
                                        'code': otpcodeData.code,
                                        'timestamp': otpcodeData.timestamp
                                    }
                                    var encrptStr = crypt.encrypt(JSON.stringify(verificationObj));
                                    var host = '';

                                    if (group === 'customer') {
                                        host = req.headers.origin + '/signup/';
                                    } else {
                                        host = req.headers.origin + '/signup/';
                                    }
                                    const options = {};
                                    options.LINK = host + encrptStr;

                                    sendGrid.sendEmail(
                                        email,
                                        'Welcome to tradias',
                                        // "views/emailtemplate/confirmemail.ejs",
                                        "views/tradiasEmailTemplate/welcome-email.ejs",
                                        options
                                    );
                                    UserCounter.findOneAndUpdate({findfield:'userid'}, {$inc: {sequence_value: 1}}, {new: true}, function (err, counterseqenceupd) {
                                        if (err) {
                                            console.log('UserCounter err : ',err);
                                        } else {
                                            console.log('counterseqenceupd : ',counterseqenceupd);
                                        }
                                        Otp.create(otpcodeData, function (error, rows) {
                                            console.log('rows : ',rows);
                                            if (error) {
                                                console.log('error : ',error);
                                                res.status(500).json({message: "Internal Server Error"});
                                            } else {
                                               done();
                                                //res.status(200).json({message: "A verification email has been send. Please verify your email."});
                                            }
                                        });
                                    });
                                }
                            });
                        },
                        function(done){
                          var api_key = "c5891c9a-e0b1-4862-8a86-90db7d7dfe13";
                          var secretkey = "c2VjcmV0bmFtZQ0K";
                          const timestamp = new Date().toUTCString();
                          let secretencoded = base64decode(secretkey); // "aGV5ICB0aGVyZQ=="
                           var args = {
                              "Currency": "ETH",
                              "Info": email,
                            };
                          var message = timestamp+'POST/v1/addresses'+JSON.stringify(args);
                          console.log(message,'message')
                          const hash = crypto.createHmac('sha256', secretencoded)
                                         .update(message)
                                         .digest('hex');
                          let encoded = base64encode('mario.rossi:Hello1234'); // "aGV5ICB0aGVyZQ=="
                              var header = {
                                  "Authorization": "Basic "+encoded,
                                  "Date": timestamp,
                                  "Finoa-Api-Key": api_key,
                                  "Finoa-Api-Digest": hash,
                                  'User-Agent': 'request'
                              };
                              const options = {
                              url: "https://demoapi.finoa.io/v1/addresses",
                              method: "POST",
                              headers: header,
                              body: JSON.stringify(args),
                              };
                              request(options, function (error, response, body) {
                                  console.log(body,'body')
                                  console.log(response.statusCode,'response.statusCode')
                                //  res.json({status:true})
                                  res.status(200).json({message: "User added successfully and send address request finoa"});
                              });
                        }
                    ], function (err) {
                    });

                }
            })
        } else {
            res.status(400).json({
                message: errorObj
            })
        }
    } catch (e) {
        if (e.message == 'Invalid country calling code') {
            res.status(400).json({message: "Invalid Phone"})
        } else {
            res.status(500).json({message: e.message})
        }
    }
}

userAlController.saveAdminUser = function (req, res, next) {
    console.log('userAlController in saveUser : ');
    try {
        var email = req.body.email;
        var phone = '';
        if(typeof req.body.full_phone!='undefined'){
            var phone = req.body.full_phone;
        }

        var password = req.body.password;
        var passwordrepeat = req.body.passwordrepeat;
        // var group = req.body.group;
        var group = 'customer';
        var name = req.body.name;
        var liquidity = req.body.liquidity;

        var errorObj = {};

        var passtest = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
        var emailtest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var profileObj = {};

        profileObj.email = email;
        profileObj.name = name;
        profileObj.country = req.body.country;
        profileObj.password = password;
        profileObj.passwordrepeat = passwordrepeat;
        profileObj.group = group;

        console.log('validation : ');
        if (typeof group !== 'undefined' && group !== '') {
            console.log('group')
            profileObj.group = group;
        } else {
            console.log("usergroup")
            errorObj.group = 'User Group is required';
        }

        if (Object.keys(errorObj).length === 0) {
            console.log('errorObj : ',errorObj);
            bcryptJS.hash(req.body.password, 10, function (err, hash) {
                if (err) {
                    res.status(500).json({message: err.message});
                } else {
                    //var passobj = jwtmodel.setPassword(req.body.password);
                    profileObj = ctrl.extend({}, profileObj, {password: hash});

                    var counterseqence = {};

                    async.waterfall([
                        function (done) {
                            UserCounter.findOne({findfield:'userid'}).exec(function (error, counterIndex) {
                                if(error){
                                    console.log('error : ',error);
                                    res.status(500).json({message: 'Error occured, Please try again.'})
                                } else {
                                    console.log('counterIndex : ',counterIndex);
                                    if(counterIndex!=null){
                                        counterseqence = counterIndex;
                                        done()
                                    } else {
                                        var insArr = {
                                            'findfield': 'userid',
                                            'sequence_value': 0
                                        };
                                        var newcounter = new UserCounter(insArr);
                                        console.log('newcounter : ',newcounter);
                                        newcounter.save(function(error) {
                                            if(error){
                                                console.log('newcounter save error : ',error);
                                                res.status(500).json({message: 'Error occured, Please try again.'})
                                            } else {
                                                console.log('newcounter : ',newcounter);
                                                counterseqence = newcounter;
                                                done();
                                            }
                                        });
                                    }
                                }
                            });
                        },
                        function (done) {
                            console.log('counterseqence : ',counterseqence);

                            var oldSeqVal = Number(counterseqence.sequence_value);
                            profileObj.userid = oldSeqVal+1;
                            var profileId = Math.random().toString(36).substr(2, 8);
                            var depid = randomize('000000000');
                            var refcode = 'DEP' +profileId +depid;
                            profileObj.profileid = profileId;
                            profileObj.bankrefcode = refcode;
                            profileObj.status = "Active";
                            profileObj.email_verified = 1;
                            // profileObj.phone = randomize('0  00000000');;
                            console.log('profileObj : ',profileObj);

                            Users.create(profileObj, function (error, user) {
                                if (error) {
                                    console.log('Users.create error : ',error);
                                    if (error.code && error.code === 11000) {
                                        if (error.errmsg.indexOf('email_1') > -1) {
                                            res.status(400).json({
                                                message: "Email already exists, please try another."
                                            })
                                        } else if (error.errmsg.indexOf('phone_1') > -1) {
                                            res.status(400).json({
                                                message: "Phone already exists, please try another."
                                            })
                                        } else {
                                            res.status(400).json({
                                                message: "Enter all required fields, please try again."
                                            })
                                        }
                                    } else {
                                        console.log('user : ',user);
                                        res.status(500).json({message: "Internal Server Error"});
                                    }
                                } else {
                                    console.log('user create : ');
                                    if (group !== 'customer') {
                                        console.log("customer");
                                        if (typeof liquidity !== 'undefined' && liquidity && Object.keys(liquidity).length > 0) {
                                            var liqkeys = Object.keys(liquidity);
                                            var liqArr = []
                                            liqkeys.forEach((liq) => {
                                                liqArr.push({
                                                    userid: mongoose.Types.ObjectId(user._id),
                                                    liquidity: liq
                                                })
                                            })
                                            if (liqArr.length > 0) {
                                                Userliqproviders.create(liqArr, function (error, liqrows) {
                                                })
                                            }
                                        }
                                    }

                                    var otpcodeData = {};
                                    var timestamp = Date.now();
                                    otpcodeData.userid = user._id;
                                    otpcodeData.code = uniqid();
                                    otpcodeData.timestamp = timestamp;
                                    var sendGrid = new Sendgrid();
                                    var verificationObj = {
                                        'id': user._id,
                                        'email': profileObj.email,
                                        'phone': profileObj.phone,
                                        'code': otpcodeData.code,
                                        'timestamp': otpcodeData.timestamp
                                    }
                                    var encrptStr = crypt.encrypt(JSON.stringify(verificationObj));
                                    var host = '';

                                    if (group === 'customer') {
                                        host = req.headers.origin + '/signup/';
                                    } else {
                                        host = req.headers.origin + '/signup/';
                                    }
                                    const options = {};
                                    options.LINK = host + encrptStr;

                                    // sendGrid.sendEmail(
                                    //     email,
                                    //     'Welcome to tradias',
                                    //     // "views/emailtemplate/confirmemail.ejs",
                                    //     "views/tradiasEmailTemplate/welcome-email.ejs",
                                    //     options
                                    // );
                                    UserCounter.findOneAndUpdate({findfield:'userid'}, {$inc: {sequence_value: 1}}, {new: true}, function (err, counterseqenceupd) {
                                        if (err) {
                                            console.log('UserCounter err : ',err);
                                        } else {
                                            console.log('counterseqenceupd : ',counterseqenceupd);
                                        }
                                        Otp.create(otpcodeData, function (error, rows) {
                                            console.log('rows : ',rows);
                                            if (error) {
                                                console.log('error : ',error);
                                                res.status(500).json({message: "Internal Server Error"});
                                            } else {
                                                done();
                                                //res.status(200).json({message: "User added successfully."});
                                            }
                                        });
                                    });
                                }
                            });
                        },
                        function(done){
                          var api_key = "c5891c9a-e0b1-4862-8a86-90db7d7dfe13";
                          var secretkey = "c2VjcmV0bmFtZQ0K";
                          const timestamp = new Date().toUTCString();
                          let secretencoded = base64decode(secretkey); // "aGV5ICB0aGVyZQ=="
                           var args = {
                              "Currency": "ETH",
                              "Info": email,
                            };
                          var message = timestamp+'POST/v1/addresses'+JSON.stringify(args);
                          console.log(message,'message')
                          const hash = crypto.createHmac('sha256', secretencoded)
                                         .update(message)
                                         .digest('hex');
                          let encoded = base64encode('mario.rossi:Hello1234'); // "aGV5ICB0aGVyZQ=="
                              var header = {
                                  "Authorization": "Basic "+encoded,
                                  "Date": timestamp,
                                  "Finoa-Api-Key": api_key,
                                  "Finoa-Api-Digest": hash,
                                  'User-Agent': 'request'
                              };
                              const options = {
                              url: "https://demoapi.finoa.io/v1/addresses",
                              method: "POST",
                              headers: header,
                              body: JSON.stringify(args),
                              };
                              request(options, function (error, response, body) {
                                  console.log(body,'body')
                                  console.log(response.statusCode,'response.statusCode')
                                  done();
                                  //res.status(200).json({message: "User added successfully and send address request finoa"});
                              });
                        },
                        function(done){
                          var api_key = "c5891c9a-e0b1-4862-8a86-90db7d7dfe13";
                          var secretkey = "c2VjcmV0bmFtZQ0K";
                          const timestamp = new Date().toUTCString();
                          let secretencoded = base64decode(secretkey); // "aGV5ICB0aGVyZQ=="
                           var args = {
                              "Currency": "BTC",
                              "Info": email,
                            };
                          var message = timestamp+'POST/v1/addresses'+JSON.stringify(args);
                          console.log(message,'message')
                          const hash = crypto.createHmac('sha256', secretencoded)
                                         .update(message)
                                         .digest('hex');
                          let encoded = base64encode('mario.rossi:Hello1234'); // "aGV5ICB0aGVyZQ=="
                              var header = {
                                  "Authorization": "Basic "+encoded,
                                  "Date": timestamp,
                                  "Finoa-Api-Key": api_key,
                                  "Finoa-Api-Digest": hash,
                                  'User-Agent': 'request'
                              };
                              const options = {
                              url: "https://demoapi.finoa.io/v1/addresses",
                              method: "POST",
                              headers: header,
                              body: JSON.stringify(args),
                              };
                              request(options, function (error, response, body) {
                                  console.log(body,'body')
                                  console.log(response.statusCode,'response.statusCode')
                                //  res.json({status:true})
                                  res.status(200).json({message: "User added successfully and send address request finoa"});
                              });
                        }
                    ], function (err) {
                    });

                }
            })
        } else {
            res.status(400).json({
                message: errorObj
            })
        }
    } catch (e) {
        if (e.message == 'Invalid country calling code') {
            res.status(400).json({message: "Invalid Phone"})
        } else {
            res.status(500).json({message: e.message})
        }
    }
}

userAlController.generateWallet = function (req, res, next) {
  var pdata = mongoose.Types.ObjectId(req.body.customerid);
  console.log(pdata);
  var email = "";
  async.waterfall([
      function (done) {
        Users.findById(pdata).exec(function (error, user) {
          if(!error){
            email = user.email;
            done();
          }
        })
      },
      function(done){
        console.log(email);
        var api_key = "c5891c9a-e0b1-4862-8a86-90db7d7dfe13";
        var secretkey = "c2VjcmV0bmFtZQ0K";
        const timestamp = new Date().toUTCString();
        let secretencoded = base64decode(secretkey); // "aGV5ICB0aGVyZQ=="
         var args = {
            "Currency": "ETH",
            "Info": email,
          };
        var message = timestamp+'POST/v1/addresses'+JSON.stringify(args);
        console.log(message,'message')
        const hash = crypto.createHmac('sha256', secretencoded)
                       .update(message)
                       .digest('hex');
        let encoded = base64encode('mario.rossi:Hello1234'); // "aGV5ICB0aGVyZQ=="
            var header = {
                "Authorization": "Basic "+encoded,
                "Date": timestamp,
                "Finoa-Api-Key": api_key,
                "Finoa-Api-Digest": hash,
                'User-Agent': 'request'
            };
            const options = {
            url: "https://demoapi.finoa.io/v1/addresses",
            method: "POST",
            headers: header,
            body: JSON.stringify(args),
            };
            request(options, function (error, response, body) {
                console.log(body,'body')
                console.log(response.statusCode,'response.statusCode')
              //  res.json({status:true})
                done();
                //res.status(200).json({message: "User added successfully and send address request finoa"});
            });
      },
      function(done){
        var api_key = "c5891c9a-e0b1-4862-8a86-90db7d7dfe13";
        var secretkey = "c2VjcmV0bmFtZQ0K";
        const timestamp = new Date().toUTCString();
        let secretencoded = base64decode(secretkey); // "aGV5ICB0aGVyZQ=="
         var args = {
            "Currency": "BTC",
            "Info": email,
          };
        var message = timestamp+'POST/v1/addresses'+JSON.stringify(args);
        console.log(message,'message')
        const hash = crypto.createHmac('sha256', secretencoded)
                       .update(message)
                       .digest('hex');
        let encoded = base64encode('mario.rossi:Hello1234'); // "aGV5ICB0aGVyZQ=="
            var header = {
                "Authorization": "Basic "+encoded,
                "Date": timestamp,
                "Finoa-Api-Key": api_key,
                "Finoa-Api-Digest": hash,
                'User-Agent': 'request'
            };
            const options = {
            url: "https://demoapi.finoa.io/v1/addresses",
            method: "POST",
            headers: header,
            body: JSON.stringify(args),
            };
            request(options, function (error, response, body) {
                console.log(body,'body')
                console.log(response.statusCode,'response.statusCode')
                res.json({status:true,message: "Request Sent to Admin"});
            });
      }
  ], function (err) {
  });
}

userAlController.verifyAccount = function (req, res, next) {
    try {
        var data = JSON.parse(crypt.decrypt(req.query.str));
        console.log('data : ',data);
        var userid = data.id;
        var code = data.code;
        var authkey = data.timestamp;
        var email = data.email;
        if (typeof userid !== 'undefined' && userid !== '' &&
            typeof code !== 'undefined' && code !== '' &&
            typeof authkey !== 'undefined' && authkey !== '' &&
            typeof email !== 'undefined' && email !== ''
        ) {
            Users.findOne({_id: userid}).exec(function (error, rows) {
                if (error) {
                    res.status(500).json({message: "Internal Server Error"})
                } else {
                    if (rows == null) {
                        res.status(400).json({message: "No Account Found"});
                    } else {
                        if (rows.email_verified == '1') {
                            res.status(400).json({message: "Already Verified"});
                        } else {
                            Otp.findOne({
                                userid: userid,
                                timestamp: authkey,
                                code: code
                            }).sort({createdAt: -1}).exec(function (error, rows) {
                                if (error) {
                                    res.status(500).json({message: "Internal Server Error"})
                                } else {
                                    if (rows === null) {
                                        res.status(400).json({message: "Invalid Request"})
                                    } else {
                                        var currentTmeStmp = Date.now();
                                        var OTPAge = (currentTmeStmp - authkey) / 100;
                                        if (OTPAge > 900000) {
                                            res.status(400).json({message: "Link Expired"});
                                        } else {
                                            Users.update({_id: mongoose.Types.ObjectId(userid)}, {email_verified: 1}).exec(function (error, uprows) {
                                                if (error) {
                                                    res.status(500).json({message: "Internal Server Error"})
                                                } else {
                                                    var kycObj = {
                                                        userid: userid,
                                                        status: 'Verify Account'
                                                    }
                                                    Actions.findOne({action: 'new_device_added'}).exec(function (error, action) {
                                                        if (!error && action !== null) {
                                                            var ipAdd = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                                                            var agentinfo = req.headers['user-agent'];
                                                            const detector = new DeviceDetector;
                                                            const result = detector.detect(agentinfo);
                                                            const DEVICE_TYPE = require('node-device-detector/parser/const/device-type');
                                                            const isTabled = result.device && [DEVICE_TYPE.TABLET].indexOf(result.device.type) !== -1;
                                                            const isMobile = result.device && [DEVICE_TYPE.SMARTPHONE, DEVICE_TYPE.FEATURE_PHONE].indexOf(result.device.type) !== -1;
                                                            const isPhablet = result.device && [DEVICE_TYPE.PHABLET].indexOf(result.device.type) !== -1;
                                                            const isIOS = result.os && result.os.family === 'iOS';
                                                            const isAndroid = result.os && result.os.family === 'Android';
                                                            const isDesktop = !isTabled && !isMobile && !isPhablet;
                                                            var device = {
                                                                id: '',
                                                                type: '',
                                                                brand: '',
                                                                model: ''
                                                            }
                                                            if (isDesktop) {
                                                                const result = detector.parseOs(agentinfo);
                                                                device.type = 'Desktop';
                                                                device.model = result.name + ', ' + result.version + ', ' + result.platform
                                                            } else {
                                                                var type = result.device.type
                                                                device.type = type.charAt(0).toUpperCase() + type.slice(1);
                                                                device.model = result.device.brand + ', ' + result.device.model;
                                                            }
                                                            var agentparse = useragent.parse(agentinfo);
                                                            var agentStr = agentparse.toAgent();
                                                            var os = agentparse.os.toString();
                                                            var agent = agentStr + ' ' + os;

                                                            var logData = {
                                                                userid: userid,
                                                                ipaddress: ipAdd,
                                                                agent: agent,
                                                                description: agentinfo,
                                                                action: action._id,
                                                                type: device.type,
                                                                model: device.model,
                                                                timestamp: Date.now()
                                                            }
                                                            Logs.create(logData, function (error, added) {
                                                                Kyc.create(kycObj, function (error, kycrows) {
                                                                    res.status(200).json({message: "Account Verified"})
                                                                })
                                                            })
                                                        } else {
                                                            Kyc.create(kycObj, function (error, kycrows) {
                                                                res.status(200).json({message: "Account Verified"})
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
        else {
            res.status(400).json({message: 'Link is Invalid'})
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}

userAlController.sendForgetPassRecoveryCode = function (req, res, next) {
    try {
        console.log('sendForgetPassRecoveryCode : ')
        console.log('req.query : ',req.query);
        if(typeof req.query.email!='undefined'){
            var target = req.query.email;
        } else if(typeof req.query.phone!='undefined'){
            var target = req.query.phone;
        } else if(typeof req.query.str!='undefined'){
            var target = req.query.str;
        }
        if (typeof target !== 'undefined' && target && target !== '') {
            var flag = '';
            var emailtest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (emailtest.test(String(target).toLowerCase())) {
                flag = 'email';
            } else {
                target = '+' + target.trim();
                const number = phoneUtil.parseAndKeepRawInput(target);
                if (phoneUtil.isPossibleNumber(number) && phoneUtil.isValidNumber(number)) {
                    flag = 'phone';
                }
            }
            if (flag !== '') {
                var filter = {};
                if (flag === 'email') {
                    filter.email = target;
                }
                if (flag === 'phone') {
                    filter.phone = target;
                }

                Users.findOne(filter).exec(function (error, user) {
                    if (error) {
                        res.status(500).json({message: 'Internal Server Error'})
                    } else {
                        if (user !== null) {
                            if (flag === 'email') {
                                ctrl.sendEmailCode(user._id, target, function (response) {
                                    res.status(response.status).json({message: response.message, name: user.name});
                                })
                            }
                            if (flag === 'phone') {
                                ctrl.sendOTP(user._id, target, function (response) {
                                    res.status(response.status).json({message: response.message, name: user.name});
                                })
                            }
                        } else {
                            res.status(400).json({message: 'Invalid Email'})
                        }
                    }
                })
            } else {
                res.status(400).json({message: 'Invalid Email '})
            }

        } else {
            res.status(400).json({message: 'Invalid Email'})
        }

    } catch (e) {
        if (e.message == 'The string supplied did not seem to be a email') {
            res.status(400).json({message: 'Invalid Email'})
        } else {
            res.status(500).json({message: 'Internal Server Error'})
        }
        //The string supplied did not seem to be a phone number

    }
}



userAlController.setForgetPassword = function (req, res, next) {
    try {
        var target = req.body.target;
        console.log('req.body : ',req.body);
        var password = req.body.password;
        var confirmpassword = req.body.confirmpassword;
        var step = req.body.step;
        var otpcodeData = req.body.otpcodeData;

        var validationRes = false;
        if(typeof otpcodeData !== 'undefined' && otpcodeData && otpcodeData !== ''){
            validationRes = true;
        }
        if(validationRes==true){
            if (password === confirmpassword) {
                var flag = '';
                var emailtest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                console.log(emailtest.test(String(target).toLowerCase()))
                flag = 'email';
                if (emailtest.test(String(target).toLowerCase())) {
                    flag = 'email';
                } else {
                    const number = phoneUtil.parseAndKeepRawInput(target);
                    if (phoneUtil.isPossibleNumber(number) && phoneUtil.isValidNumber(number)) {
                        flag = 'phone';
                    }
                }
                if (flag !== '') {
                    console.log('flagnotempty')
                    var filter = {};
                    if (flag === 'email') {
                        filter.email = target;
                    }
                    if (flag === 'phone') {
                        filter.phone = target;
                    }
                    console.log('filter : ',filter);
                    Users.findOne(filter).exec(function (error, user) {
                        console.log('error : ',error);
                        console.log('user : ',user)
                        if(user ==null){
                            res.status(400).json({message: 'Email not found'})
                        }else{
                            if(user.otpcodeData != otpcodeData){
                                res.status(400).json({message: 'Wrong Authentication Key'});
                            }else{
                                if (error) {
                                    res.status(500).json({message: 'Internal Server Error'})
                                } else {
                                    if (user !== null) {
                                        bcryptJS.hash(password, 10, function (err, hash) {
                                            if (err) {
                                                res.status(500).json({message: 'Internal Server Error'})
                                            } else {
                                                Users.update({_id: user._id}, {otpcodeData: '',password: hash,passwordrepeat: confirmpassword}).exec(function (error, updatedrow) {
                                                    if (error) {
                                                        res.status(500).json({message: 'Internal Server Error'})
                                                    } else {
                                                        res.status(200).json({message: 'Password has been set successfully'})
                                                    }
                                                })
                                            }
                                        });
                                    } else {
                                        res.status(400).json({message: 'Invalid Details'})
                                    }
                                }
                            }
                        }
                    })
                } else {
                    res.status(400).json({message: 'Invalid Details'})
                }
            } else {
                res.status(400).json({message: 'Password and Confirm Password does not match'})
            }
        } else {
            res.status(400).json({message: 'Invalid Details'})
        }
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}

var ctrl = module.exports = userAlController
