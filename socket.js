var socketObject = {};
var ioObj = {};
var socketObj = {};

socketObject.start = (io) => {
    if (io) {
        ioObj = io;
        // console.log("ioObj-->", ioObj);
    }
}

socketObject.sendFollowRequest = (doc) => {
    try {
        if (Object.keys(doc).length > 0) {
            var userId = doc.followerid.toString();
            var eventName = "sendFollowRequest" + userId;
            ioObj.emit(eventName, doc);
        }
    } catch (e) {
        console.log("ERROR OF SOCKET OF SEND FOLLOW REQUEST", e)
    }
}

socketObject.postLike = (postDoc) => {
    try {
        if (Object.keys(postDoc).length > 0) {
            var userId = postDoc.userid.toString();
            var eventName = "postLike" + userId;
            ioObj.emit(eventName, postDoc);
        }
    } catch (error) {
        console.log("ERROR OF SOCKET OF POST LIKE", error);
    }
}

socketObject.messageNotification = (messageDoc) => {
    // console.log("MESSAGE DOC", messageDoc);
    try {
        if (Object.keys(messageDoc).length > 0) {
            var userId = messageDoc.receiverid.toString();
            var eventName = "receiveMessage" + userId;
            ioObj.emit(eventName, messageDoc);
        }
    } catch (error) {
        console.log("ERROR OF SOCKET OF POST LIKE", error);
    }
}

socketObject.commentNotification = (postOwnerId, commentDoc) => {
    // console.log("MESSAGE DOC", messageDoc);
    try {
        if (Object.keys(commentDoc).length > 0) {
            var userId = postOwnerId.toString();
            var eventName = "getCommentNotification" + userId;
            ioObj.emit(eventName, commentDoc);
        }
    } catch (error) {
        console.log("ERROR OF SOCKET OF POST LIKE", error);
    }
}

socketObject.acceptRequest = (acceptRequestDoc) => {
    // console.log("MESSAGE DOC", acceptRequestDoc);
    try {
        if (Object.keys(acceptRequestDoc).length > 0) {
            var userId = acceptRequestDoc[0].userid.toString();
            var eventName = "getAcceptRequestNotification" + userId;
            ioObj.emit(eventName, acceptRequestDoc);
        }
    } catch (error) {
        console.log("ERROR OF SOCKET OF POST LIKE", error);
    }
}

module.exports = socketObject;