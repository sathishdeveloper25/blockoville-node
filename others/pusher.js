var Pusher = require('pusher');
var Users = require('../model/user');
var mongoose = require('mongoose');
var Request = require("request");
//var Kyc = require('../model/kyc');
var pusher = new Pusher({
    appId: process.env.PUSHER_APPID,
    key: process.env.PUSHER_KEY,
    secret: process.env.PUSHER_SECRET,
    cluster: process.env.PUSHER_CLUSTER,
    useTLS: true
});
var channel = process.env.PUSHERCHANNEL;

var PusherModel = {};

PusherModel.pushUser = function (user) {
    if (user) {
        pusher.trigger(channel, 'blockville-user', {
                "message": "connected", "data": user
            }
        );
    }
}

PusherModel.kycAddNotification = function (doc) {
    if (doc) {
        var pusstr = 'kyc-notification-add-' + doc.userid;
        pusher.trigger(channel, pusstr, {
                "message": "connected", "data": doc
            }
        );
        var addstr = 'kyc-notification-add';
        pusher.trigger(channel, addstr, {
                "message": "connected", "data": doc
            }
        );
    }
}
PusherModel.kycNotification = function (filter, Kyc) {
    if (filter) {
        this.retrieveKyc(filter, Kyc, function (obj) {
            if (obj && typeof obj.userid !== 'undefined') {
                var pusstr = 'kyc-notification-' + obj.userid;
                pusher.trigger(channel, pusstr, {
                        "message": "connected", "data": obj
                    }
                );
            }
        })
    }
}
PusherModel.retrieveKyc = function (filter, Kyc, callback) {
    Kyc.findOne(filter, function (err, wt) {
        var obj = "";
        if (!err && wt !== null && typeof wt != 'undefined') {
            callback(wt);
        } else {
            callback(obj);
        }
    });
}
PusherModel.userCurrencyNotification = function (filter, updateField) {
    if (filter) {
        if (typeof updateField.usercurrency && updateField.usercurrency && updateField.usercurrency !== '') {
            var pusstr = 'user-currency-' + filter._id;
            pusher.trigger(channel, pusstr, {
                    "message": "connected", "data": updateField
                }
            );
        }
        if (typeof updateField.avatar && updateField.avatar && updateField.avatar !== '') {
            var pusstr = 'user-avatar-' + filter._id;
            pusher.trigger(channel, pusstr, {
                    "message": "connected", "data": updateField
                }
            );
        }
        var profilestr = 'updateprofile';
        pusher.trigger(channel, profilestr, {
                "message": "connected", "data": updateField
            }
        );
        //console.log('updateField',updateField)
        if (typeof updateField.status && updateField.status && updateField.status === 'Deactive') {
            var pusstr = 'inactiveuser-' + filter._id;
            //console.log('pusstr',pusstr)
            pusher.trigger(channel, pusstr, {
                    "message": "connected", "data": updateField
                }
            );
        }
    }
}
PusherModel.pushSecurity = function (user) {
    if (user) {
        pusher.trigger(channel, 'blockville-user', {
                "message": "connected", "data": user
            }
        );
    }
}
PusherModel.securityNotification = function (filter, updateField) {
    if (filter) {
        if (typeof updateField.usercurrency && updateField.usercurrency && updateField.usercurrency !== '') {
            var pusstr = 'user-currency-' + filter._id;
            pusher.trigger(channel, pusstr, {
                    "message": "connected", "data": updateField
                }
            );
        }
    }
}

//forum pusher
PusherModel.addForumNotification = function (user) {
    if (user) {
        pusher.trigger(channel, 'add-forum', {
                "message": "connected", "data": user
            }
        );
    }
}
//forum notification
PusherModel.forumUpdateNotification = function (postid, forumModel) {
    if (postid) {
        var userid = postid;
        var psrtgr = 'update-form-' + userid;
        pusher.trigger(channel, psrtgr, {
            "message": "connected", "data": 'Feedback Received',
        });

    }
}


// message pusher
PusherModel.addMessageNotification = function (groupid) {
    if (groupid) {
        pusher.trigger(channel, 'add-message', {
            "message": "connected", "data": groupid
        });
    }
}

// this is for adding new members in the group
PusherModel.groupNewMember = function (data) {
    if (data) {
        pusher.trigger(channel, 'newMembers', {
            "message": "connected", "data": data,
        });
    }
}

// this is for creating new group
PusherModel.addNewGroup = function (data) {
    pusher.trigger(channel, 'add-addNewGroup', {
        "message": "connected", "data": data,
    });
}

// this is forupade group
PusherModel.updateOldGroup = function (data) {
    if (data) {
        pusher.trigger(channel, 'updateOldGroup', {
            "message": "connected", "data": data,
        });
    }
}

PusherModel.sendMediaFiles = function (data) {
    if (data) {
        pusher.trigger(channel, 'tradegroupMediaFile', {
            "message": "connected", "data": data,
        });
    }
}

// Group
PusherModel.UpdatepeAdminGroups = function (data) {
    if (data) {
        var str = 'admingroupsupdate' + data.group;
        pusher.trigger(channel, str, {
            "message": "connected", "data": data,
        });
    }
}

//announcements
PusherModel.AddAnnouncementNotification = function (data) {
    if (data) {
        var str = 'addannouncement';
        pusher.trigger(channel, str, {
            "message": "connected", "data": data,
        });
    }
}
PusherModel.updateAnnouncementNotification = function (data) {
    if (data) {
        var str = 'updateannouncement';
        pusher.trigger(channel, str, {
            "message": "connected", "data": data,
        });
    }
}
PusherModel.deleteAnnouncementNotification = function (data) {
    if (data) {
        var str = 'deleteannouncement';
        pusher.trigger(channel, str, {
            "message": "connected", "data": data,
        });
    }
}


module.exports = PusherModel;

