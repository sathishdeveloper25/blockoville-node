Users = require('../model/user');
Security = require('../model/security');
Loginlog = require('../model/logs');
const Sendgrid = require('./sendgrid');
const Fs = require('fs')
const Path = require('path')
const Axios = require('axios')
const shell = require('shelljs');
var googleAuth = require('google_authenticator').authenticator;
var Actions = require('../model/actions');
var ga = new googleAuth();
var useragent = require("useragent")
var mongoose = require('mongoose');
const DeviceDetector = require('node-device-detector');

function Function(req) {
    this.req = req;
}

Function.prototype.googleAuthenticator = function (res, callback) {
    callback(ga);
}
Function.prototype.isGoogleAuthActive = function (userid, callback) {
    Security.findOne({ userid: mongoose.Types.ObjectId(userid), option: 'twofa' }, function (error, res) {
        var check = false;
        if (error) {
            check = false;
        } else {
            if (res !== null && typeof res != 'undefined' && res.secret) {
                check = true;
            } else {
                check = false;
            }
        }
        callback(check);
    });
}
Function.prototype.addLoginLog = function (userinfo) {
    var userid = userinfo._id;
    var email = userinfo.email;

    var agentinfo = this.req.headers['user-agent'];
    const detector = new DeviceDetector;
    const result = detector.detect(agentinfo);
    const DEVICE_TYPE = require('node-device-detector/parser/const/device-type');
    const isTabled = result.device && [DEVICE_TYPE.TABLET].indexOf(result.device.type) !== -1;
    const isMobile = result.device && [DEVICE_TYPE.SMARTPHONE, DEVICE_TYPE.FEATURE_PHONE].indexOf(result.device.type) !== -1;
    const isPhablet = result.device && [DEVICE_TYPE.PHABLET].indexOf(result.device.type) !== -1;
    const isIOS = result.os && result.os.family === 'iOS';
    const isAndroid = result.os && result.os.family === 'Android';
    const isDesktop = !isTabled && !isMobile && !isPhablet;
    var device = {
        id: '',
        type: '',
        brand: '',
        model: ''
    }
    if (isDesktop) {
        const result = detector.parseOs(agentinfo);
        device.type = 'Desktop';
        device.model = result.name + ', ' + result.version + ', ' + result.platform
    } else {
        var type = result.device.type
        device.type = type.charAt(0).toUpperCase() + type.slice(1);
        device.model = result.device.brand + ', ' + result.device.model;
    }

    var ip = this.req.headers['x-forwarded-for'] || this.req.connection.remoteAddress;
    Actions.findOne({ action: 'login' }).exec(function (error, action) {
        if (error) {
            res.status(500).json({ message: "Internal Server Error" })
        } else {
            var agentparse = useragent.parse(agentinfo);
            var agentStr = agentparse.toAgent();
            var os = agentparse.os.toString();
            var agent = agentStr + ' ' + os;

            var logData = {
                userid: userid,
                ipaddress: ip,
                agent: agent,
                description: agentinfo,
                action: action && action._id ? action._id : '', //Change "VS"
                type: device.type,
                model: device.model
            }
            if (ip !== null && typeof ip !== 'undefined' && ip !== "") {
                Loginlog.create(logData);
                var sendGrid = new Sendgrid();
                var options = { email: email, ip: ip, timestamp: logData.timestamp };
                sendGrid.sendEmail(
                    email,
                    'Blockoville Notification',
                    "views/emailtemplate/loginlog.ejs",
                    options
                );
            }
        }
    })
};
Function.prototype.verifyDevice = function (user, ipaddress, callback) {
    if (user.group !== 'admin') {
        if (user.email === 'booze007@gmail.com' || true) {
            callback({ error: false, result: true })
        } else {
            Loginlog.find({ userid: mongoose.Types.ObjectId(user._id), ipaddress: ipaddress }).exec(function (error, result) {
                if (error) {
                    callback({ error: true, result: error.message })
                } else {
                    if (result.length === 0) {
                        callback({ error: false, result: false })
                    } else {
                        callback({ error: false, result: true })
                    }
                }
            })
        }
    } else {
        callback({ error: false, result: true })
    }
};
Function.prototype.downloadKYCFile = async function (apiUrl, filesource, location) {
    try {
        if (!Fs.existsSync(location)) {
            shell.mkdir('-p', location);
        }
        var filename = Path.basename(filesource);
        var url = apiUrl + filesource;
        const pathName = location + filename;
        //Path.resolve(__dirname, location, filename)
        console.log('pathName', pathName)
        const writer = Fs.createWriteStream(pathName)
        const response = await Axios({
            url,
            method: 'GET',
            responseType: 'stream'
        })
        response.data.pipe(writer)
        return new Promise((resolve, reject) => {
            writer.on('finish', resolve)
            writer.on('error', reject)
        })
    } catch (e) {
        console.log(e.message);
    }
}

module.exports = Function;