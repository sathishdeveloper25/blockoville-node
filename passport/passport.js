var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
// var Members = require("../model/al/member");
var Users = require("../model/user");
var bcryptJS = require('bcrypt');

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function (username, password, done) {
        Users.findOne({email:username}).exec(function(error,rows){
            if (error) {
                return done(error);
            }
            // Return if user not found in database
            if (rows===null) {
                return done(null, false, {
                    message: 'User does not exist.'
                });
            }else{
                if(rows.email_verified=='0'){
                    return done(null, false, {
                        message: 'not_verified'
                    });
                }
                if(rows.status!=='Active'){
                    return done(null, false, {
                        message: 'not_active'
                    });
                }
                // Return if password is wrong
                bcryptJS.compare(password,rows.password,function (err, resp) {
                    if(err){
                        return done(null, false, {
                            message: 'Internal Server Error'
                        });
                    }else{
                        if(!resp){
                            return done(null, false, {
                                message: 'Password is wrong'
                            });
                        }else{
                            return done(null, rows);
                        }
                    }
                })
                /*if (!jwtmodel.validPassword(password,rows)) {
                    return done(null, false, {
                        message: 'Password is wrong'
                    });
                }*/
                // If credentials are correct, return the user object

            }
        })
    }
));