var mongoose = require('mongoose');
var subscripitionschema = new mongoose.Schema({
    subscriptionid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    autoPayement: {
        type: Boolean
    },
    type: {
        type: String,
        trim: true
    },
    groupid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    purchasedate: {
        type: Date,
        default: Date.now
    },
    startdate: {
        type: Date,
        default: Date.now
    },
    endDate: {
        type: Date
    },
    status: {
        type: String,
        trim: true
    }
});

var subscription = module.exports = mongoose.model('subscription', subscripitionschema);