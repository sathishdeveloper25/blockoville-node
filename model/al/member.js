var mongoose = require('mongoose');
var Pusher = require('../../others/pusher');
var MemberSchema = new mongoose.Schema({
    userid: {
        type: Number,
        required: true,
    },
    name: {
        type: String,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    phone: {
        type: String,
        unique: true
    },
    otpcodeData: {
        type: String,
    },
    password: {
        type: String,
        required: true,
    },
    salt: {
        type: String,
    },
    passwordrepeat: {
        type: String,
    },
    dob: {
        type: String,
    },
    gender: {
        type: String,
        trim: true,
        default: 'male'
    },
    address: {
        type: String,
        trim: true
    },
    city: {
        type: String,
        trim: true
    },
    state: {
        type: String,
        trim: true
    },
    postalcode: {
        type: String,
        trim: true
    },
    country: {
        type: String,
        trim: true
    },
    email_verified: {
        type: Number,
        required: true,
        default: 0
    },
    phone_verified: {
        type: Number,
        required: true,
        default: 0
    },
    status: {
        type: String,
        trim: true,
        required: true,
        default: 'Active'
    },
    group: {
        type: String,
        trim: true,
        required: true
    },
    avatar: {
        type: String,
        trim: true,
    },
    usercurrency: {
        type: String,
        trim: true,
        default: 'USD'
    },
    apikey: {
        type: String,
        trim: true
    },
    timezone: {
        type: String,
        trim: true
    },
    profileid: {
        type: String,
        trim: true,
        required: true
    },
    bankrefcode: {
        type: String,
        trim: true,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

MemberSchema.post('save', function (doc) {
    if (doc && typeof doc._id != 'undefined' && doc._id !== null && doc._id != "") {
        Pusher.pushUser(doc);
    }
});
MemberSchema.post("update", function (doc) {
    var filter = this._conditions;
    if (Object.keys(this._update).indexOf('$set') !== -1) {
        var updateObj = this._update.$set;
    } else {
        var updateObj = this._update;
    }
    Pusher.userCurrencyNotification(filter, updateObj);
});
MemberSchema.index({userid: -1, email: -1, phone: -1}, {
    name: 'userid_email_phone',
    background: true,
    unique: true
}); // schema level
MemberSchema.index({group:-1}, {
    name: 'group',
    background: true
}); // schema level

var client = module.exports = mongoose.model('Member', MemberSchema);

