var mongoose = require("mongoose");

var eventsSchema = new mongoose.Schema({
    eventName: {
        type: String,
        required: true,
        trim: true
    },
    startDateAndTime: {
        type: Date,
        required: true
    },
    endDateAndTime: {
        type: Date,
        required: true
    },
    location: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String,
        required: true,
        trim: true
    },
    guestsCanInvite: {
        type: Boolean,
        required: true,
        trim: true
    },
    groupId: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    groupName: {
        type: String,
        trim: true
    },
    creatorId: {
        type: mongoose.Schema.ObjectId,
        trim: true,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now
    },
    type: {
        type: String, // offline or online
        required: true,
        trim: true
    },
    paymentRequired: {
        type: Boolean,
        required: true,
        trim: true
    },
    amount: {
        type: Number,
        trim: true,
    },
    currency: {
        type: String,
        trim: true
    },
    seatsAvailable: {
        type: Number,
        trim: true
    },
    website: {
        type: String,
        trim: true
    },
    contactNumber: {
        type: String,
        trim: true
    },
    contactEmail: {
        type: String,
        trim: true
    }
})

var events = mongoose.model("Event", eventsSchema);
module.exports = events;