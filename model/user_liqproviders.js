var mongoose = require('mongoose');
var UserliqproviderSchema  = new mongoose.Schema({
    userid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    liquidity: {
        type: String,
        required: true,
        trim: true,
    },
    status:{
        type:String,
        trim:true,
        default:'active'
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})
var Userliqproviders = mongoose.model('Userliqproviders',UserliqproviderSchema);
module.exports = Userliqproviders;