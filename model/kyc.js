var mongoose = require('mongoose');
var Pusher = require('../others/pusher');
var KycSchema = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    photoproof:{
        type: String,
        trim: true
    },
    idprooffront:{
        type: String,
        trim: true
    },
    idproofback:{
        type: String,
        trim: true
    },
    idnumber: {
        type: String,
        trim:true,
    },
    idproofdoc: {
        type: String,
        trim:true,
    },
    addressproof:{
        type: String,
        trim: true
    },
    addressproofdoc: {
        type: String,
        trim:true,
    },
    hash: {
        type: String,
        trim: true
    },
    hashobj: {
        type: Object,
        trim: true
    },
    level: {
        type: Number,
        trim: true,
        require: true,
        default: 1
    },
    status: {
        type: String,
        trim: true,
        require: true,
        default: 'New'
    },
    response: {
        type: Object,
        trim: true,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})
KycSchema.post("save", function (doc) {
    Pusher.kycAddNotification(doc);
});
KycSchema.post("update", function (doc) {
    var filter = this._conditions;
    Pusher.kycNotification(filter, this);
});
KycSchema.post("updateOne", function (doc) {
    var filter = this._conditions;
    Pusher.kycNotification(filter, this);
});

KycSchema.index({userid: -1}, {
    name: 'userid',
    background: true
}); // schema level
var client = module.exports = mongoose.model('Kyc', KycSchema);
/*
client.listIndexes().then((data) => {
    if (data.length === 1) {
        client.ensureIndexes(function (err) {
            console.log('ENSURE INDEX----Kyc')
            if (err) console.log(err)
        })
    }
})*/
