var mongoose = require('mongoose');
var followerSchema  = new mongoose.Schema({
    userid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    followerid:{
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})
var addFollow = mongoose.model('addFollow',followerSchema);
module.exports = addFollow;