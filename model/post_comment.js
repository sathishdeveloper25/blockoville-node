var mongoose = require('mongoose');
var liked = new mongoose.Schema({
    postid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    commentId: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    notification: {
        type: String,
        trim: true
    },
    text: {
        type: String,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

var postSchema = require("./userPosts");
var socketFile = require("../socket");

var post_comments = mongoose.model('post_comments', liked);
module.exports = post_comments; 