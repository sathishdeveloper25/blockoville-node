var mongoose = require('mongoose');
var ApiendpointaccessSchema  = new mongoose.Schema({
    userid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    keyid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    endpointid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    access: {
        type: String,
        required: true,
        trim: true,
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})
ApiendpointaccessSchema.index({userid: -1,keyid:-1}, {
    name: 'userid_keyid',
    background: true
}); // schema level
var client = module.exports = mongoose.model('Apiendpointaccess', ApiendpointaccessSchema);
/*
client.listIndexes().then((data) => {
    if (data.length === 1) {
        client.ensureIndexes(function (err) {
            console.log('ENSURE INDEX----Apiendpointaccess')
            if (err) console.log(err)
        })
    }
})*/
