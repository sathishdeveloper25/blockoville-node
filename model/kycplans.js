var mongoose = require('mongoose');
var Pusher = require('../others/pusher');
var KycPlansSchema  = new mongoose.Schema({
    title:{
        type: String,
        trim: true,
        require: true
    },
    order: {
        type: Number,
        trim:true,
        require: true
    },
    features: {
        type: Array,
        trim:true,
    },
    requirements:{
        type: Array,
        trim: true
    },
    currency:{
        type: String,
        trim: true,
        default:'EUR'
    },
    dl:{
        type:Boolean,
        trim: true,
        require:true
    },
    dl_unlimited:{
        type:Boolean,
        trim: true,
        require:true
    },
    dl_overall:{
        type:Number,
        trim: true,
    },
    ddl: {
        type: Number,
        trim:true,
    },
    mdl: {
        type: Number,
        trim:true,
    },
    wl:{
        type:Boolean,
        trim: true,
        require:true
    },
    wl_unlimited:{
        type:Boolean,
        trim: true,
        require:true
    },
    wl_overall:{
        type:Number,
        trim: true,
    },
    dwl: {
        type: Number,
        trim:true,
    },
    mwl: {
        type: Number,
        trim:true,
    },
    status: {
        type: String,
        trim: true,
        require:true,
        default:'Active'
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
});
/*
    dfc: {
        type: Number,
        trim:true,
    },
    dff: {
        type: String,
        trim:true,
        default:'percent'
    },
    wfc: {
        type: Number,
        trim:true,
    },
    wff: {
        type: String,
        trim:true,
        default:'percent'
    },
*/

var Kycplans = mongoose.model('Kycplans',KycPlansSchema);
module.exports = Kycplans;