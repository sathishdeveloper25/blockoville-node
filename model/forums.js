var mongoose = require('mongoose');
var Pusher = require('../others/pusher');
var forumSchema  = new mongoose.Schema({
    userid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    subject: {
        type: String,
        trim:true,
    },
    body: {
        type: String,
        trim:true
    },
    parentid:{
        type:mongoose.Schema.ObjectId,
        trim: true,
    },
    reviews: {
        type: Number,
    },
    status: {
        type: String,
        trim: true,
        default:'Active'
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})

forumSchema.post('save', function(doc) {
    if(doc && typeof doc._id!='undefined' && doc._id!==null && doc._id!=""){
        Pusher.addForumNotification(doc);
    }
});

forumSchema.post("findOneAndUpdate", function(doc) {
    var postid=doc.userid;
    Pusher.forumUpdateNotification(postid,this);
});

var forums = mongoose.model('forums',forumSchema);
module.exports = forums;