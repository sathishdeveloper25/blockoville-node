var mongoose = require('mongoose');
var Pusher = require('../others/pusher')
var groupSchema  = new mongoose.Schema({
    name:{
        type: String,
        required: true,
        trim: true
    },
    purpose:{
        type: String,
        trim: true
    },
    image:{
        type: String,
        trim: true
    },
    createrId:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})


groupSchema.post('save', function(doc) {
    if(doc && typeof doc._id!='undefined' && doc._id!==null && doc._id!=""){
        // var groupid = doc.groupid;
        Pusher.addNewGroup(doc);
    }
});


groupSchema.post('findOneAndUpdate', function(doc) {
    if(doc && typeof doc._id!='undefined' && doc._id!==null && doc._id!=""){
        Pusher.updateOldGroup(doc);
    }
});

var tradegroup = mongoose.model('tradegroup',groupSchema);
module.exports = tradegroup;