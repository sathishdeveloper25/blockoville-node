var mongoose = require('mongoose');
// var Pusher = require('../others/pusher');
var groupInvitationSchema = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
    },
    userName: {
        type: String,
        required: true
    },
    groupName: {
        type: String,
        required: true
    },
    groupId: {
        type: mongoose.Schema.ObjectId,
        required: true
    },
    groupAdmin: {
        type: mongoose.Schema.ObjectId,
        required: true
    },
    groupImage: {
        type: String,
        required: true
    },
})

// groupInvitationSchema.post('save', function(doc) {
//     if(doc && typeof doc._id!='undefined' && doc._id!==null && doc._id!=""){
//         Pusher.pushUser(doc);
//     }
// });
// groupInvitationSchema.post("update", function(doc) {
//     var filter=this._conditions;
//     var field =this._update;
//     Pusher.userCurrencyNotification(filter,field);
// });
var Users = mongoose.model('groupInvitation', groupInvitationSchema);
module.exports = Users;