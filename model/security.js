var mongoose = require('mongoose');
var Pusher = require('../others/pusher');
var SecuritySchema = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    option: {
        type: String,
        required: true,
        trim: true,
    },
    order: {
        type: Number,
        trim: true,
    },
    secret: {
        type: String,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})
SecuritySchema.post('save', function (doc) {
    if (doc && typeof doc._id != 'undefined' && doc._id !== null && doc._id != "") {
        Pusher.pushSecurity(doc);
    }
});
SecuritySchema.post("update", function (doc) {
    var filter = this._conditions;
    var field = this._update;
    Pusher.securityNotification(filter, field);

});
SecuritySchema.index({userid: -1, option: -1}, {
    name: 'userid_option',
    background: true
}); // schema level
var client = module.exports = mongoose.model('Security', SecuritySchema);
/*
client.listIndexes().then((data) => {
    if (data.length === 1) {
        client.ensureIndexes(function (err) {
            console.log('ENSURE INDEX----Security')
            if (err) console.log(err)
        })
    }
})*/
