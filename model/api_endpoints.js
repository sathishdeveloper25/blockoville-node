var mongoose = require('mongoose');
var ApiendpointSchema  = new mongoose.Schema({
    endpoint: {
        type: String,
        required: true,
        trim: true,
    },
    title: {
        type: String,
        required: true,
        trim: true,
    },
    status:{
        type:String,
        trim:true,
        default:'active'
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})
ApiendpointSchema.index({status: -1}, {
    name: 'status',
    background: true
}); // schema level
var client = module.exports = mongoose.model('Apiendpoints', ApiendpointSchema);
/*
client.listIndexes().then((data) => {
    if (data.length === 1) {
        client.ensureIndexes(function (err) {
            console.log('ENSURE INDEX----Apiendpoints')
            if (err) console.log(err)
        })
    }
})*/
