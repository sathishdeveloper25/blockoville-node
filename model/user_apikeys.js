var mongoose = require('mongoose');
var UserapikeysSchema  = new mongoose.Schema({
    userid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    label: {
        type: String,
        required: true,
        trim: true,
    },
    key: {
        type: String,
        required: true,
        trim: true,
    },
    status:{
        type:String,
        trim:true,
        default:'active'
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})
UserapikeysSchema.index({userid:-1}, {
    name: 'userid',
    background: true
}); // schema level
UserapikeysSchema.index({key:-1,status:-1}, {
    name: 'key_status',
    background: true
}); // schema level
var Userapikeys = mongoose.model('Userapikeys',UserapikeysSchema);
module.exports = Userapikeys;