/**
 * Created by Pawan
 */
var mongoose = require('mongoose');
var AdminRulesSchema = new mongoose.Schema({
    menu_id:{
        type: mongoose.Schema.ObjectId,
        required:true,
        trim: true
    },
    group_id:{
        type: mongoose.Schema.ObjectId,
        required:true,
        trim:true
    },
    permission:{
        type: String,
        trim: true,
        required: true
    },
    createdAt:{
        type : Date,
        default : Date.now
    }
});
AdminRulesSchema.index({menu_id: -1}, {
    name: 'menu_id',
    background: true
}); // schema level

AdminRulesSchema.index({group_id: -1}, {
    name: 'group_id',
    background: true
}); // schema level
var Adminrules = mongoose.model('adminrules', AdminRulesSchema);
module.exports = Adminrules;
