var mongoose = require('mongoose');
var LogSchema = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    ipaddress: {
        type: String,
        required: true,
        trim: true
    },
    agent: {
        type: String,
        trim: true,
        required: true,
    },
    description: {
        type: String,
        trim: true,
        required: true,
    },
    action: {
        type: mongoose.Schema.ObjectId,
        trim: true,
        required: true,
    },
    type: {
        type: String,
        trim: true,
    },
    model: {
        type: String,
        trim: true,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})
LogSchema.index({userid: -1, ipaddress: -1}, {
    name: 'userid_ipaddress',
    background: true
}); // schema level
var client = module.exports = mongoose.model('Logs', LogSchema);
/*
client.listIndexes().then((data) => {
    if (data.length === 1) {
        client.ensureIndexes(function (err) {
            console.log('ENSURE INDEX----Logs')
            if (err) console.log(err)
        })
    }
})*/
