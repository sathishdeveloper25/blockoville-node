var mongoose = require('mongoose');
var paidScheema  = new mongoose.Schema({
    postid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    userid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    amount: {
        type: String,
        trim: true
    },
    currency: {
        type: String,
        trim: true
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})
var paidservice= mongoose.model('paidservice',paidScheema);
module.exports = paidservice; 