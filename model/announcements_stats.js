var mongoose = require('mongoose');
var Pusher = require('../others/pusher');
var AnnouncementStatsSchema  = new mongoose.Schema({
    ann_id: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true,
    },
    userid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    checked:{
        type: Number,
        trim: true,
        default:0
    },
    read: {
        type: Number,
        trim: true,
        default:0
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})
/*AnnouncementStatsSchema.post("save", function(doc) {
    Pusher.AddAnnouncementNotification(doc);
});
AnnouncementStatsSchema.post("update", function(doc) {
    Pusher.updateAnnouncementNotification(doc);
});
AnnouncementStatsSchema.post("findOneAndDelete", function (doc) {
    Pusher.deleteAnnouncementNotification(doc);
});*/
var schema = module.exports =mongoose.model('AnnouncementStats',AnnouncementStatsSchema);