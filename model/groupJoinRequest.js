var mongoose = require('mongoose');

var groupJoinRequest = new mongoose.Schema({

    groupid: {
        type: mongoose.Schema.ObjectId,
        trim: true,
        required: true
    },
    groupName: {
        type: String,
        required: true,
        trim: true
    },
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    userName: {
        type: String,
        required: true,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    autoPayment: {
        type: Boolean,
        default: false
    },
    userPic: {
        type: String,
        trim: true
    },
    amount: {
        type: Number,
        trim: true
    }
})

var joiningGroupRequests = mongoose.model('groupJoiningRequest', groupJoinRequest);
module.exports = joiningGroupRequests;