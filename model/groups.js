var mongoose = require('mongoose');
var shell = require('shelljs');
var PusherModel = require('../others/pusher');

var GroupSchema  = new mongoose.Schema({
    group: {
        type: String,
        required: true,
        trim: true,
        unique: true,
    },
    type:{
        type: String,
        required: true,
        trim: true
    },
    title:{
        type: String,
        trim: true
    },
    permission:{
        type: String,
        trim: true,
        required: true
    },
    status: {
        type: String,
        trim:true,
        required: true,
        default: 'active'
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
});

GroupSchema.post("update", function(doc) {
    this.findOne(this._conditions).exec(function (error, groupdata) {
        if (!error && groupdata !== null) {
            PusherModel.UpdatepeAdminGroups(groupdata);
        }
    })
});

var Groups = mongoose.model('Groups',GroupSchema);
module.exports = Groups;