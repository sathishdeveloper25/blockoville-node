var mongoose = require('mongoose');
var followerSchema = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    followerid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    request: {
        type: String
    },
    notification: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

var socketFile = require("../socket");

followerSchema.post("save", (doc) => {
    // console.log("DDDOOOCCC", doc);
    if (doc != null) {
        // console.log("AAAAAAAAAAA");
        // socketFile.sendFollowRequest(doc);
    }
});


var following = mongoose.model('following', followerSchema);
module.exports = following;