var mongoose = require('mongoose');
var contentSchema = new mongoose.Schema({
    postid: {
        type: mongoose.Schema.ObjectId,
        trim: true,
    },
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    contenttype: {
        type: String,
        trim: true
    },
    content_url: {
        type: String,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})
var post_medias = mongoose.model('post_medias', contentSchema);
module.exports = post_medias;