var mongoose =require('mongoose');
var report = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    postid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    groupid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    reportType: {
        type: String,
        trim: true
    },
    notification: {
        type: String,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

var postReport = mongoose.model('Report', report);
module.exports = postReport;