var mongoose = require('mongoose');
var UsersCounterSchema = new mongoose.Schema({
    findfield:{
        type : String
    },
    sequence_value:{
        type: Number,
        trim: true,
    },
    createdAt:{
        type : Date,
        default : Date.now
    }
});
var UsersCounter = mongoose.model('UsersCounter', UsersCounterSchema);
module.exports = UsersCounter;