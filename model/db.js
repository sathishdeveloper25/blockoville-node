var bluebird = require('bluebird');
var mongoose = require('mongoose')
mongoose.Promise = bluebird
const options = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 // Use IPv4, skip trying IPv6
};
var db_str = process.env.DB_STR;
const uri = "mongodb+srv://blockovilleDB:blockovilleDB123@cluster0.y3g12.mongodb.net/blockovilleDB?retryWrites=true&w=majority";
mongoose.set('useFindAndModify', false);

mongoose.connect(uri, options)
    .then((asd) => {
        console.log(`Successfully Connecting to the Mongodb Database at URL : ` + uri);
    })
    .catch((das) => {
        console.log(`Error Connecting to the Mongodb Database at URL : ` + uri)
    })
