var mongoose = require('mongoose');
var liked = new mongoose.Schema({
    postid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    commentId: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    notification: {
        type: String,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

var postSchema = require("./userPosts");
var socketFile = require("../socket");

liked.post("save", (doc) => {
    if (doc != null) {
        postSchema.findOne({_id: doc.postid}).then((foundPost) => {
            console.log("POST", foundPost);
            socketFile.postLike(foundPost);
        }).catch((error) => {
            console.log("ERROR IN FINDING POST", error);
        })
    }    
});
var post_likes = mongoose.model('post_likes', liked);
module.exports = post_likes; 