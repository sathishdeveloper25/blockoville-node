var mongoose = require('mongoose');
var msgScheema = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    receiverid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    message: {
        type: String
    },
    notification: {
        type: String,
        default: "unseen"
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    // channelName: {
    //     type: String,
    //     trim: true,
    //     required: true
    // }
});

var socketFile = require("../socket");

msgScheema.post("save", (doc) => {
    if (doc != null) {
            socketFile.messageNotification(doc);
    }
});
msgScheema.index({userid: -1}, {
    name: 'userid',
    background: true
}); // schema level

var userprofilemsg = mongoose.model('userprofilemsg', msgScheema);
module.exports = userprofilemsg;