var mongoose = require('mongoose');
var Pusher = require('../others/pusher')
var tradememberSchema  = new mongoose.Schema({
    groupid:{
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    memberid:{
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})

// tradememberSchema.post('save', function(doc) {
//     if(doc && typeof doc._id!='undefined' && doc._id!==null && doc._id!=""){
//         Pusher.groupNewMember(doc);
//     }
// });


var trademember = mongoose.model('trademember',tradememberSchema);
module.exports = trademember;