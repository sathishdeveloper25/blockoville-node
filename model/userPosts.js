var mongoose = require('mongoose');
var user = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    groupid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    payements: {
        type: Boolean,
        // default:false
    },
    status: {
        type: String,
        trim: true
    },
    subject: {
        type: String,
        trim: true
    },
    text: {
        type: String,
        trim: true
    },
    privacy: {
        type: String,
        trim: true
    },
    parentid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    replyComment: {
        type: String,
        trim: true
    },
    commentid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    notification: {
        type: String,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    taggedPeople: [{
        _id: {
            type: mongoose.Schema.ObjectId,
            // required: true
        },
        name: {
            type: String,
            // required: true
        }
    }],
    sharedBy: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    sharedPost: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    sharedText: {
        type: String
    },
    sharedOn: {
        type: Date,
        default: Date.now
    }
}, {
        usePushEach: true
    })
var posts = mongoose.model('userposts', user);
module.exports = posts;