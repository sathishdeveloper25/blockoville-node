var mongoose = require('mongoose');
var PurchaseSchema  = new mongoose.Schema({
    userid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    amount:{
        type: Number,
        trim: true
    },
    currency:{
        type: String,
        trim: true
    },
    type:{
        type: String,
        trim: true
    },
    orderid:{
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    subscriberid:{
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    groupid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    tr_id:{
        type: String,
        trim: true
    },
    createdAt: {
        type: Date,
        default : Date.now
    }, 
    status: {
        type: String,
        trim: true
    }
})
var myPurchase = mongoose.model('myPurchase',PurchaseSchema);
module.exports = myPurchase;