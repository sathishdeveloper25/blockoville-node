var mongoose = require('mongoose');
// var Pusher = require('../others/pusher');
var CodeSchema  = new mongoose.Schema({
    userid:{
        type :mongoose.Schema.ObjectId,
        required: true,
    },
    code: {
        type: String,
        trim: true,
        unique: true
    }
})

var Referral = mongoose.model('Referral',CodeSchema);
module.exports = Referral;