var mongoose = require('mongoose');
var ActionSchema  = new mongoose.Schema({
    action:{
        type: String,
        required: true,
        trim: true,
        unique:true
    },
    message:{
        type: String,
        trim:true,
        required: true,
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})

ActionSchema.index({action:-1 },{name:'action',unique:true,background:true}); // schema level
var client = module.exports = mongoose.model('Actions',ActionSchema);
/*
client.listIndexes().then((data)=>{
    if(data.length===1){
        client.ensureIndexes(function (err) {
            console.log('ENSURE INDEX----Actions')
            if (err) console.log(err)
        })
    }
})*/
