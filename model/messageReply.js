var mongoose = require('mongoose');
var msgSchema =   new  mongoose.Schema({
    chatId: {
        type: mongoose.Schema.ObjectId,
        trim: true,
        required: true
    },
    msgrply:{
        type: String,
        trim: true,
        required: true
    },
    memberid:{
        type: mongoose.Schema.ObjectId,
        trim: true,
        required: true
    },
    groupid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    createdAt:{
        type: Date,
        default: Date.now
    }
})
var replymsg = mongoose.model('messageReply',msgSchema);
module.exports = replymsg;