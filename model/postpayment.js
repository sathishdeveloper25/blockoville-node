var mongoose = require('mongoose');
var payementSchema  = new mongoose.Schema({
    userid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    postid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    currency: {
        type:String,
        trim: true
    },
    wallet: {
        type:String,
        trim: true
    },
    amount: {
        type:Number,
        trim: true
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})
var postpayement = mongoose.model('postpayment',payementSchema);
module.exports = postpayement;