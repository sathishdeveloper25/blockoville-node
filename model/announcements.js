var mongoose = require('mongoose');
var Pusher = require('../others/pusher');
var AnnouncementSchema  = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim: true,
    },
    body:{
        type: String,
        required: true,
        trim: true
    },
    type:{
        type: String,
        required: true,
        trim: true,
    },
    status: {
        type: String,
        trim:true,
        required: true,
        default: 'Active'
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})
AnnouncementSchema.post("save", function(doc) {
    Pusher.AddAnnouncementNotification(doc);
});
AnnouncementSchema.post("update", function(doc) {
    Pusher.updateAnnouncementNotification(doc);
});
AnnouncementSchema.post("findOneAndDelete", function (doc) {
    Pusher.deleteAnnouncementNotification(doc);
});
var Announcements = mongoose.model('Announcements',AnnouncementSchema);
module.exports = Announcements;