var mongoose = require('mongoose');
var Pusher = require('../others/pusher')
var MediaSchema = new mongoose.Schema({
    groupid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    memberid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    media_id:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    type:{
        type: String,
        // required: true,
        trim: true
    },
    mediaFile:{
        type: String,
        trim: true
    },
    createdAt:{
        type: Date,
        default: Date.now
    }
})
MediaSchema.post('save', function(doc) {
    if(doc && typeof doc._id!='undefined' && doc._id!==null && doc._id!=""){
        Pusher.sendMediaFiles(doc);
    }
});


var groupMedia = mongoose.model('tradegroupMedia',MediaSchema);
module.exports = groupMedia;