var mongoose = require('mongoose');
var Pusher = require('../others/pusher');
var UserSchema = new mongoose.Schema({
    userid: {
        type: Number,
        required: true,
    },
    name: {
        type: String,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    phone: {
        type: String,
        unique: true
    },
    otpcodeData: {
        type: String,
    },
    password: {
        type: String,
        required: true,
    },
    salt: {
        type: String,
    },
    passwordrepeat: {
        type: String,
    },
    dob: {
        type: String,
    },
    gender: {
        type: String,
        trim: true,
        default: 'male'
    },
    address: {
        type: String,
        trim: true
    },
    city: {
        type: String,
        trim: true
    },
    state: {
        type: String,
        trim: true
    },
    postalcode: {
        type: String,
        trim: true
    },
    country: {
        type: String,
        trim: true
    },
    email_verified: {
        type: Number,
        required: true,
        default: 0
    },
    phone_verified: {
        type: Number,
        required: true,
        default: 0
    },
    status: {
        type: String,
        trim: true,
        required: true,
        default: 'Active'
    },
    group: {
        type: String,
        trim: true,
        required: true
    },
    avatar: {
        type: String,
        trim: true,
    },
    usercurrency: {
        type: String,
        trim: true,
        default: 'USD'
    },
    apikey: {
        type: String,
        trim: true
    },
    timezone: {
        type: String,
        trim: true
    },
    profileid: {
        type: String,
        trim: true,
        required: true
    },
    bankrefcode: {
        type: String,
        trim: true,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    countrycode:{
        type:String,
    },
    createdAt: {
        type : Date,
        default : Date.now
    },
    otpcodeData: {
        type: String,
    },
    nickname:{
        type: String,
    },
    legalname:{
        type: String,
    },
    pin:{
        type: String,
    },
    language:{
        type: String,
    },
    timezone:{
        type: String,
    },
    phoneno:{
        type: String,
    },
    phonecode:{
        type: String,
    },
    stateCode:{
        type: String,
    }
});

UserSchema.post('save', function (doc) {
    if (doc && typeof doc._id != 'undefined' && doc._id !== null && doc._id != "") {
        // Pusher.pushUser(doc);
    }
});
UserSchema.post("update", function (doc) {
    var filter = this._conditions;
    if (Object.keys(this._update).indexOf('$set') !== -1) {
        var updateObj = this._update.$set;
    } else {
        var updateObj = this._update;
    }
    // Pusher.userCurrencyNotification(filter, updateObj);
});
UserSchema.index({userid: -1, email: -1}, {
    name: 'userid_email',
    background: true,
    unique: true
}); // schema level
UserSchema.index({group:-1}, {
    name: 'group',
    background: true
}); // schema level

// UserSchema.dropIndex({phone:-1}); // schema level

var client = module.exports = mongoose.model('User', UserSchema);

// var User = mongoose.model('User', UserSchema);
// Dropping an Index in MongoDB
// User.collection.dropIndex('phone_1', function(err, result) {
//     if (err) {
//         console.log('Error in dropping index!', err);
//     } else {
//         console.log('User schema result : ');
//     }
// });
// var client = module.exports = User;
