var mongoose = require('mongoose');
var Pusher = require('../others/pusher');
var msgSchema  = new mongoose.Schema({
    groupid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    memberid:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true,
        default: null
    },
    message: {
        type: String,
        trim:true
        },
    parentId: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    msgrply:{
        type: String,
        trim: true
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})

msgSchema.post('save', function(doc) {
    if(doc && typeof doc._id!='undefined' && doc._id!==null && doc._id!=""){
        var groupid = doc.groupid;
        Pusher.addMessageNotification(groupid);
    }
});

var gruopmessage = mongoose.model('tradegroupchat',msgSchema);
module.exports = gruopmessage;

