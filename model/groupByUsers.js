var mongoose = require('mongoose');
var subscriptionScheema = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    name: {
        type: String,
        trim: true
    },
    currency: {
        type: String,
        trim: true
    },
    groupImage: {
        type: String,
        trim: true
    },
    type: {
        type: String,
        trim: true
    },
    amount: {
        type: Number,
        trim: true
    },
    wallet: {
        type: String,
        trim: true
    },
    status: {
        type: String,
        trim: true
    },
    trialDuration: {
        type: String,
        trim: true
    },
    Gtype: {
        type: String,
        trim: true
    },
    duration: {
        type: String,
        trim: true
    },
    trial: {
        type: Boolean,
        trim: true
    },
    subscription: {
        type: Boolean
    },
    type: {
        type: String,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    privacy: {
        type: String,
        trim: true,
        required: true,
        default: "public"
    },
    adminApprovalForJoiningGroup: {
        type: Boolean,
        trim: true,
        default: true
    },
    membersCanInvite: {
        type: Boolean,
        trim: true,
        default: true,
    }
});
var groupSubscribe = mongoose.model('groupsbyusers', subscriptionScheema);
module.exports = groupSubscribe;