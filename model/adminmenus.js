/**
 * Created by Pawan
 */
var mongoose = require('mongoose');
var AdminMenusSchema = new mongoose.Schema({
    menu:{
        type: String,
        required:true,
        trim: true
    },
    parentmenu:{
        type: mongoose.Schema.ObjectId,
        trim:true
    },
    menu_url:{
        type: String,
        trim: true,
    },
    order:{
        type: Number,
        trim: true
    },
    styleclass:{
        type: String,
        trim: true
    },
    status:{
        type:Number,
        trim: true,
        required: true,
        default:1
    },
    createdAt:{
        type : Date,
        default : Date.now
    }
});
AdminMenusSchema.index({parentmenu: -1}, {
    name: 'parentmenu',
    background: true
}); // schema level
var AdminMenus = mongoose.model('adminmenus', AdminMenusSchema);
module.exports = AdminMenus;