var mongoose = require('mongoose');

var blockUserFromGroup = new mongoose.Schema({

    groupid: {
        type: mongoose.Schema.ObjectId,
        trim: true,
        required: true
    },
    groupName: {
        type: String,
        required: true,
        trim: true
    },
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    userName: {
        type: String,
        required: true,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

var blockingUserFromGroup = mongoose.model('blockUserFromGroup', blockUserFromGroup);
module.exports = blockingUserFromGroup;