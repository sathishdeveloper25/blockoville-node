var mongoose = require('mongoose');

var frvSchema = new mongoose.Schema({
    groupid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    },
    userid: {
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true
    }
})

var favrtGroup = mongoose.model('tradegroupfavourite',frvSchema);
module.exports = favrtGroup;