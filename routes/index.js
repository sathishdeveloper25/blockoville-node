var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var jwt_secret = "MY_SECRET";//process.env.JWT_TOKEN_SECRET;
var auth = jwt({
    secret: jwt_secret,
    userProperty: 'payload'
});

var userCtrl = require('../controller/userController');
var postCtrl = require('../controller/postController');
var adminCtrl = require('../controller/adminController');
var announceCtrl = require('../controller/announcementController');
var forumCtrl = require('../controller/forumController');
var tradeCtrl = require('../controller/tradegroupController');
var subscriptionCtrl = require('../controller/subscription');
var eventController = require("../controller/events/eventController");
var basisCtrl = require('../controller/basisController');



// New controller
var userAlCtrl = require('../controller/userAlController');
var adminuserAlCtrl = require('../controller/adminuserAlController');

/* GET home page. */
// common calls
router.get('/csrf', userCtrl.getCSRF); // get csrf token to authenticate post request
router.get('/authoptions', auth, userCtrl.getAuthenticationOptions);
router.post('/verifyauthentication', auth, userCtrl.verifyAuthentication)
router.get('/countrylist', userCtrl.getCountryList);
router.post('/getState', userCtrl.getState);





// New controller routes
// router.post('/userlogin', userAlCtrl.UserLogin);
// router.post('/registration', userAlCtrl.saveUser);
router.post('/registrationAdm', userAlCtrl.saveAdminUser);
router.post('/generateWallet', userAlCtrl.generateWallet);

router.get('/accountverify', userAlCtrl.verifyAccount);
router.get('/sendfpwcode', userAlCtrl.sendForgetPassRecoveryCode);
router.post('/setforgetpasswordal', userAlCtrl.setForgetPassword);

router.post('/adminlogin', adminuserAlCtrl.AdminLogin);
router.get('/menurules', auth, adminuserAlCtrl.getAdminPanelMenu)


// login,registration call
router.post('/registration', userCtrl.saveUser); // save new user
// router.post('/registrationAdm', userCtrl.saveUserAdmin); // save new user
router.post('/login', userCtrl.Login); // get login
// router.post('/adminlogin', userCtrl.AdminLogin); // get login
router.post('/resendconfirm', userCtrl.resentConfirmEmail)
router.get('/bankrefcode', auth, userCtrl.getUserBankRefCode)


// profile callsgetkycdetails
router.get('/getprofile', auth, userCtrl.getProfile);
router.get('/getkyc', auth, userCtrl.getkyc);
router.get('/getkycdetails', auth, userCtrl.getkycdetails);
router.post('/updateprofile', auth, userCtrl.updateProfile);
router.post('/securityauth', userCtrl.verifyAuthenticationLogin)
router.get('/getReferralCode', auth, userCtrl.generateReferralCode)
router.get('/gettimezone', auth, userCtrl.getTimeZoneByCountryCode)
router.get('/verificationotp', auth, userCtrl.sendPhoneVerificationOTP);
router.post('/verifyphone', auth, userCtrl.getVerifiedPhone)

// authentication option 3fa configuration calls
router.get('/security', auth, userCtrl.getSecurity);
router.get('/emailauthcode', auth, userCtrl.sendEmailAuthenticationCode);
router.post('/enemailauth', auth, userCtrl.enableEmailSecurity);
router.get('/sscode', auth, userCtrl.sscode);
router.post('/ensmsauth', auth, userCtrl.enableSMSSecurity);
router.post('/twofa/verify', auth, userCtrl.VerifyTwofa);
router.post('/twofa/disable', auth, userCtrl.DisableTwofa);
router.post('/rmemailauth', auth, userCtrl.disableEmailSecurity);
router.post('/rmsmsauth', auth, userCtrl.disableSMSSecurity);
router.post('/setpriority', auth, userCtrl.setAuthPriority);
router.get('/userauthoption', auth, userCtrl.getUserAuthOptions)

/*router.post('/security',userCtrl.saveSecurity);*/

// forget password api calls
router.get('/sendfpcode', userCtrl.sendForgetPassRecoveryCode); // send otp to email or phone for forget password recovery
router.get('/vryfrpasscode', userCtrl.verifyForgetRecoveryCode); // verify the forget password otp
router.post('/setforgetpassword', userCtrl.setForgetPassword); // set new password

// change password api calls
router.post('/changepassword', auth, userCtrl.changePassword);

// admin customer api calls
router.get('/customer', auth, userCtrl.getCustomer);
router.get('/customerbyid', auth, userCtrl.getCustomerById);
router.get('/custstatusupdate', auth, userCtrl.updateCustomerStatusById);
router.get('/deletecustomer', auth, userCtrl.deleteCustomerById);

// kyc
router.post('/kycupdate', auth, userCtrl.updateKyc)
router.get('/getcustomerkyc', auth, userCtrl.getCustomerKyc)
router.get('/getcustomerkycplan', auth, userCtrl.getCustomerKycWithPlan)
router.get('/customerskyc', auth, userCtrl.getCustomerKycAdmin)//admin api call
router.get('/updatekyc', auth, userCtrl.updateKYCStatusAdmin)
router.get('/removekyc', auth, userCtrl.deleteKYCAdmin)
router.get('/getProgressStatus', auth, userCtrl.KycDetailStatus);
router.post('/addkycplan', auth, userCtrl.addKYCPlan);
router.get('/getkycplan', auth, userCtrl.getAllKYCPlans);
router.get('/kycplanbyid', auth, userCtrl.getKYCPlanById);

//admin kyc
router.get('/getadminkycplan', auth, userCtrl.getAllKYCPlans);



//user related
router.get('/usercurrency', auth, userCtrl.getUserCurrency);
router.get('/updateusercurrency', auth, userCtrl.updateUserCurrency);

//login logs api
router.get('/getloginlogs', auth, userCtrl.getloginlogs);

//new device call

router.get('/newdevicealert', userCtrl.sendNewDeviceAlert)
router.get('/addnewdevice', userCtrl.addNewDevice);

// generatSecretId
router.post('/generateKey', auth, userCtrl.generatekey);
router.post('/updategenerateapikey', auth, userCtrl.updateGenerateKey);
// getGenerateKey
router.get('/getGenerateKey', auth, userCtrl.getUserApiKeys);
//delete api key
router.get('/deleteapikey', auth, userCtrl.deleteUserAPIkey)
// Authentication
router.get('/authentication', userCtrl.authentication);
// admin user api call

router.get('/getgroups', auth, adminCtrl.getGroups);
router.get('/adminuser', auth, adminCtrl.getAdminUserList);
router.get('/adminuserbyid', auth, userCtrl.getAdminUserById);
router.post('/updateadminuser', auth, userCtrl.updateAdminUser)
router.get('/adminmenu', auth, adminCtrl.getMenus);
router.get('/grouplist', auth, adminCtrl.getGroupsList);
router.get('/changegroupstatus', auth, adminCtrl.updateGroupStatus);
router.post('/savegroup', auth, adminCtrl.saveGroup);
router.get('/deletegroup', auth, adminCtrl.deleteGroup);
router.get('/groupbyid', auth, adminCtrl.getGroupById);
router.post('/updategroup', auth, adminCtrl.updateGroup)
//router.get('/menurules', auth, adminCtrl.getAdminPanelMenu)
router.get('/getalladmin', auth, adminCtrl.getAllAdminUsers)


//extra call

router.post('/sendotp', userCtrl.sendOTP);
router.post('/verifyotp', userCtrl.verifyOTP);
router.get('/sendconfirmemail', userCtrl.sendConfirmationEmail);
router.post('/verifyemail', userCtrl.verifyEmail);
router.get('/twofa/verify', userCtrl.getVerifyTwofa);

/******************************User-Profile routes ***************************/
// User-post api &  User-get api & delete api
router.post('/userpost', auth, postCtrl.userpost);
router.get('/getData', auth, postCtrl.getData);
// delete posts
router.get('/deleteData/:id', auth, postCtrl.deleteData)
// Get currencies
router.get('/getcurrency', auth, postCtrl.getcurrency);
// User-post reply api
router.post('/postreply', auth, postCtrl.postreply);
// Comment Reply
router.post('/commentReply', auth, postCtrl.commentReply);
// All groups get
router.get('/Usergroups', auth, postCtrl.Usergroups);
// Follow api
router.get('/follow', auth, postCtrl.follow);
// get follow api
router.get('/getfollowers/:id', auth, postCtrl.getfollowers);
// Get Follower through key
router.get('/getFollower', auth, postCtrl.getFollower);
// Unfollow api
router.get('/unfollow/:id', auth, postCtrl.unfollow);
router.get('/getPendingRequests', auth, postCtrl.getPendingRequests);
// Follower and follwing list
router.get('/taggingPeopleList', auth, postCtrl.getFollowersAndFollowingList);
// Follow request accept
router.get('/followRequest/:id', auth, postCtrl.followRequest);
// Delete follow request
router.get('/deleteRequest/:id', auth, postCtrl.deleteRequest);
// Suggestions for friends
router.get('/friendsSuggestions', auth, postCtrl.friendsSuggestions);
// User-post payement-get
router.get('/getpayement/:id', auth, postCtrl.getpayement); 
// User-Like api & User-get like
router.post('/like', auth, postCtrl.postlike);
router.post('/likeComment', auth, postCtrl.likeComment);
router.post('/comment', auth, postCtrl.postcomment);
router.post('/replyComment', auth, postCtrl.replyComment);
router.get('/getComments', auth, postCtrl.getComments);
router.get('/getReplyComments', auth, postCtrl.getReplyComments);
router.get('/getlike', auth, postCtrl.getlike);
router.get('/postprofile', auth, postCtrl.getPostProfile);
router.get('/postprofileParticularUser/:id', auth, postCtrl.getPostProfileParticularUser);
router.post('/sharingPost', auth, postCtrl.sharingPost);

// Paid payement for post
router.get('/paidPayement/:id', auth, postCtrl.paidPayement);
// subscribersPayements
// router.get('/subscribersPayements/:id', auth, postCtrl.subscribersPayements);
router.get('/subscribersPayements/:id/:payements', auth, postCtrl.subscribersPayements);
// subscriber get
router.get('/subscriberget', auth, postCtrl.subscriberget);
// Purchase details
router.get('/purchaseDetails', auth, postCtrl.purchaseDetails);
// Get Subscribe to show how many subscriber
router.get('/getSubscribe', auth, postCtrl.getSubscribe);
// subscribergetsubscription
router.get('/subscribergetsubscription', auth, postCtrl.subscribergetsubscription);
// User-post payement-get
router.get('/getpayementSubscribe/:id', auth, postCtrl.getpayementSubscribe);
// Find user for Inviting
router.get('/searchForUser', auth, postCtrl.searchUserForInvitation);
// Find groups through string
router.get('/searchGroup/:search', auth, postCtrl.searchGroups);
// Get particular Group's Post
router.get('/getParticularGroupPost', auth, postCtrl.particularGroupPosts);

//getRequestListOfUserForParticularGroup
router.get('/getRequestListOfUserForParticularGroup/:groupId', auth, subscriptionCtrl.getRequestListOfUserForParticularGroup);

//Invite users to group
router.post('/inviteUserToGroup', auth, subscriptionCtrl.invitePeopleToJoinGroup);

// Accept Group Invitations
// router.get('/acceptInvitationOfGroup/:id', auth, subscriptionCtrl.acceptInvitationOfGroup);

// Suggestions for groups
router.get('/suggestedGroups', auth, subscriptionCtrl.suggestedGroups);

// Search bar header
router.get('/searchStringInPosts', auth, postCtrl.searchStringFromPosts);

// Search particular Hashtag
router.get('/searchParticuarHashtag', auth, postCtrl.searchParticuarHashtag);

// All hash tag details
router.get('/hashselect', auth, postCtrl.findHastTagForHashTagPage);
// Perr to peer message work here.
router.post('/sendMessage', auth, postCtrl.sendMessage);
// Peero to peer get message
router.get('/receiveMsg/:id', auth, postCtrl.receiveMsg);
// Post Report Abuse
router.post('/reportPost', auth, postCtrl.reportPost);
// Get Report abuse for group admin
router.get('/getreport', auth, postCtrl.getGroupAdminReport);
// notification work
router.get('/notification', auth, postCtrl.notification);
// Search for searchPage
router.get('/searchFilter/:search', auth, postCtrl.searchFilter);
/*****************************USer-Profile routes end here*******************/

/*****************************Group Work routes******************************/
// Subscription work
router.post('/subscription', auth, subscriptionCtrl.Postsubscription);
// Get Subscription
router.get('/getSubscription', auth, subscriptionCtrl.getSubscription);
// Get Details
router.get('/getDetails/:id', auth, subscriptionCtrl.getDetails)
// Update Subscription
router.post('/updateSubscription/:id', auth, subscriptionCtrl.updateSubscription);
// deleteData
router.get('/deletesubscribe/:id', auth, subscriptionCtrl.deletesubscribe);
// User-Group work
router.get('/getDetailsByid/:id', auth, subscriptionCtrl.getDetailsByid);
// groupPosts
router.post('/groupPosts', auth, subscriptionCtrl.groupPosts);
// getgroupPosts
router.get('/getgroupPosts', auth, subscriptionCtrl.getgroupPosts);
// Group Like
router.post('/grouplike', auth, subscriptionCtrl.grouplike);
// get group like
router.get('/getgroupLike', auth, subscriptionCtrl.getlike);
// Group comment
router.post('/postreplys', auth, subscriptionCtrl.postreply);
// groupDelete
router.get('/deletegroup/:id', auth, subscriptionCtrl.deletegroup)
// get currency in subscription
router.get('/getCurrency', auth, subscriptionCtrl.getcurrency);
// Subscribe, duration & trial Duration work
router.post('/trialPeriod/:id', auth, subscriptionCtrl.trialperiod);
// trial period get
router.get('/trialperiodexpire', auth, subscriptionCtrl.trialperiodexpire);
// my purchase get
router.get('/mypurchase', auth, subscriptionCtrl.mypurchase);
// followgroup
// router.post('/followgroup', auth, subscriptionCtrl.requestToJoinGroup);
// free group get
router.get('/freeGroup', auth, subscriptionCtrl.freeGroup);

// Request to Join Group
router.get('/requestToJoinGroup/:groupId', auth, subscriptionCtrl.requestToJoinGroup);

//Cancel group joining request
router.get('/cancelGroupJoiningRequest/:groupId', auth, postCtrl.cancelGroupJoiningRequest);

// Approval of joining request
router.post('/approveRequest', auth, subscriptionCtrl.approvalOfRequest);

//Blocking Of User
router.get('/blockingOfUser/:userToBeBlocked/:groupid', auth, subscriptionCtrl.blockingOfUser);

// Unblocking of user
router.get('/unblockingUser', auth, subscriptionCtrl.unblockingOfUser);

// Removing of user
// router.get('/removingOfUser/:groupid/:userToBeRemoved', auth, subscriptionCtrl.removingOfUserFromAGroup);

// groups foillowed by user
router.get('/groupsFollowedByUser', auth, subscriptionCtrl.groupFollowedByParticularUser);

// members of groups
router.get("/membersOfGroup/:groupId", auth, subscriptionCtrl.followersOfAGroup);

// Get sent requests of the user
router.get('/getSentRequest', auth, subscriptionCtrl.groupJoiningRequestsOfAUser);

// Removing of user in group
router.get('/removingOfUser/:userToBeRemoved/:groupid', auth, subscriptionCtrl.removingOfUserFromAGroup);


/*****************************Group routes wnd here**************************/

//announcement api
router.post('/saveannouncement', auth, announceCtrl.saveAnnouncement);
router.get('/getannouncements', auth, announceCtrl.getAnnouncement);
router.get('/upannoucestatus', auth, announceCtrl.updateStatus);
router.get('/deleteannounce', auth, announceCtrl.deleteAnnouncement);
router.get('/getNotifications', auth, announceCtrl.getNotificationDetail);
router.get('/getNotification', auth, announceCtrl.getAllNotificationDetail);
router.get('/getNotificationsbyid', auth, announceCtrl.getNotificationsbyid);
router.get('/checkannouncements', auth, announceCtrl.markAsCheckedAnnouncements)

router.post('/saveforum', auth, forumCtrl.saveForum);
router.get('/getforumdata', auth, forumCtrl.getForum);
router.get('/getforumDataById/:id', auth, forumCtrl.getforumDataById);
router.post('/forumpostreply', auth, forumCtrl.forumreply);
router.get('/replyreview', auth, forumCtrl.updatereview);

// Trade group member
router.get('/followers', auth, tradeCtrl.getFollower);
router.post('/newGroup', auth, tradeCtrl.createGroup);
router.get('/groupdetails', auth, tradeCtrl.groupDetail);
router.get('/getEditingdetail', auth, tradeCtrl.getdetailforUpdate);
router.post('/updateTradeGroup', auth, tradeCtrl.updateTradeGroup);
router.get('/deletetradegroup', auth, tradeCtrl.deleteTradeGroup);
router.get('/gettingtradegroupMessage', auth, tradeCtrl.getTradeGroupChat);
router.post('/savingMessage', auth, tradeCtrl.saveGroupMessage);
router.post('/msgreply', auth, tradeCtrl.saveMesageReply);
router.get('/groupMember', auth, tradeCtrl.getAllGroupMembers);
// router.get('/gettingtradegroup', auth, tradeCtrl.getTradeGroupInformation);
router.post('/updatemember', auth, tradeCtrl.AddnewMembers);
router.get('/addfavirategroup', auth, tradeCtrl.setAsFrvtGroup);
router.get('/getAllusersofTable', auth, tradeCtrl.allMembers);
router.get('/deletefvrtgrp', auth, tradeCtrl.removefvrtGroup);
// router.get('/favirategroup', auth,tradeCtrl.getAllFavrtGroup);



//====================== BASIS ID INTEGRATION KYC============================

router.get('/register_user_basis', auth, basisCtrl.saveUserBasis);
router.post('/updatekyclevel', auth, basisCtrl.updateKYCLevelStatus);
router.get('/kycdocs', auth, basisCtrl.getKYCDocuments)



// EVENTS ROUTES

// creation of event
router.post("/createEvent", auth, eventController.createEvent);
// updation of event details
router.post("/updateEvent", auth, eventController.updateEvent);
// deletion of event
router.get("/deleteEvent/:eventId", auth, eventController.deleteEvent);
// list of all events
router.get("/listOfAllEvents", auth, eventController.allEventsPastPresentFuture);



module.exports = router;
